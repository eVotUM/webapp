FROM centos:7

RUN yum -y install epel-release
RUN yum -y update
RUN yum -y install python-devel
RUN yum -y install python-pip
RUN yum -y install python-six
# meriadb provides mysqladmin for docker entrypoint mysql checks
RUN yum -y install mariadb
RUN yum -y install mariadb-devel
RUN yum -y install gcc-c++
RUN yum -y install npm
RUN yum -y install libffi-devel
RUN yum -y install openssl-devel
RUN yum -y install freetype-devel
RUN yum -y install libjpeg-devel
RUN yum -y install libpng-devel
RUN yum -y install gettext
RUN yum -y install git
RUN yum -y install xmlsec1-devel
RUN yum -y install xmlsec1-openssl-devel
RUN yum -y install libtool-ltdl-devel

RUN yum -y install zip
RUN yum -y install wget

RUN npm install --global bower

# Disable Python Output Buffer
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONPATH $PYTHONPATH:/app/evote
ENV TERM=linux
ENV TOX_TESTENV_PASSENV=PYTHONPATH

# Create app directory
RUN mkdir /app
WORKDIR /app

# Add all files [Except the ones in dockerignore file]
ADD . /app/

# update pip
RUN pip install pip --upgrade
RUN pip install setuptools --upgrade
# install project dependencies
RUN pip install -r evote/evote/requirements/docker.txt --upgrade
