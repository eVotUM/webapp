#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from __future__ import unicode_literals

import os
import sys
import codecs


LICENSE = """eVotUM - Electronic Voting System
Copyright (c) 2020 Universidade do Minho
Developed by Eurotux (dev@eurotux.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>."""

PROJECT_NAME = 'evote'
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.abspath(os.path.join(BASE_DIR, PROJECT_NAME))

# relative to PROJECT_DIR
EXCLUDE = [
    'static/vendor/',
]


def main(argv):
    dirpath = os.path.realpath(PROJECT_DIR)
    for (root, dirs, filenames) in os.walk(dirpath):
        for filename in filenames:
            filepath = os.path.join(root, filename)
            if not to_exclude(filepath):
                extension = os.path.splitext(filename)[-1].lower()
                if extension == '.py':
                    print "Prepending license: %s" % filepath
                    prepend_license(filepath, get_license(),
                                    prepend="# -*- coding: utf-8 -*-\n#\n")
                elif extension == '.js':
                    print "Prepending license: %s" % filepath
                    prepend_license(filepath, get_license("// "))


def to_exclude(filepath):
    for exclude in EXCLUDE:
        to_exclude = os.path.abspath(os.path.join(PROJECT_DIR, exclude))
        if to_exclude in filepath:
            return True
    return False


def prepend_license(filepath, license, prepend=""):
    with codecs.open(filepath, "r+", 'utf-8') as f:
        content = f.read()
        if "# -*- coding: utf-8 -*-" in content:
            content = "".join(content.splitlines(True)[1:])
        f.seek(0, 0)
        to_prepend = "{}\n\n{}"
        if prepend:
            to_prepend = prepend + to_prepend
        f.write(to_prepend.format(license, content))


def get_license(prefix="# "):
    lines = [(prefix + line).strip() for line in LICENSE.splitlines()]
    return "\n".join(lines)


if __name__ == "__main__":
    main(sys.argv[1:])
