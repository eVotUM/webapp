# Releases

## Regras de versionamento

A versão do spec tem de ser igual à versão do ficheiro `evote/evote/__init__.py` apenas com os 3 digitos referentes a MAJOR, MINOR, PATCH de acordo com [http://semver.org/]().

Para a nomenclatura da versão seguir os exemplos de [https://fedoraproject.org/wiki/Package_Versioning_Examples]().

## Actualização da versão

De forma a proceder à release de uma nova versão do projeto devem ser seguidos os seguintes passos:

1. Atualizar a versão no ficheiro SPEC localizado em `builds/evote.spec` por exemplo:

        Version: 0.1.0
        Release: 1%{?dist}

    para:

        Version: 0.2.0
        Release: 1%{?dist}


2. No mesmo ficheiro adicionar ao changelog o detalhe das respetivas modificações seguindo o formato:

        - Mon Mar 27 2017 Eurotux <dev@eurotux.com> 0.12.3-1
        + Updated source code

3. Atualizar a versão no ficheiro init do projeto localizado em `evote/evote/__init__.py`;

4. Efetuar os merges para o branch `dev`.


## Geração de RPM (desenvolvimento)

Este procedimento deve ser **sempre** efetuado primeiro para ambiente de staging e só depois de tudo validado deve ser gerado o rpm para produção.

1.  Aceder ao sistema de Continuous Integration usado para gerar RPM's e proceder à sua geração:

        Para Staging (DEV) -> evotum-webapp-devel
        Para Produção (Master) -> evotum-webapp
    

2.  Após o rpm ser gerado com sucesso, aceder às máquinas e efetuar a respetiva atualização seguindo os passos):

        $ sudo su
        $ yum update evote --enablerepo=evote-devel

    Enquanto este processo decorre deve ser verificado o seu output de forma a garantir que tudo corre conforme esperado. No final do processo aceder ao url do website de forma a garantir que este está a responder corretamente.

## Geração de RPM (produção)

1. Efetuar o merge da branch `dev` para `master`.
2. Criar a respetiva `tag` no Gitlab [https://gitlab.com/eVotUM/webapp/tags/new]() sobre a branch `master` com o nome da versão (por exemplo: v0.2.0)
3. Proceder à geração do RPM de produção;
4. Com o RPM de produção gerado efetuar a sua instalação nas respetivas máquinas executando:

        $ sudo su
        $ yum update evote

