# Checks aplicacionais 
Foram configurados vários checks nas várias máquinas da infraestrutura para monitorização dos vários serviços que sustentam a aplicação. 

Neste documento são descritas as várias **acções** que podem ser tomadas em cada um deles para o caso de **falha**.

Estes checks estão integrados com o sistema de monitorização da Universidade do Minho.

## Serviços

Os serviços são geridos por um sistema de gestão de processos. O sistema usado é o `supervisord` e para cada um dos processos foram configurados os seguintes checks:

| Check | Máquinas | Programa supervisor | Ficheiro de log | HTTP code |
| ----- | -------- | ------------------- | --------------- | --------------- |
| `admin` | `web01` e `web02` | `gunicorn_admin` | `/srv/et/evote/logs/gunicorn_admin.log` | `502` |
| `management` | `web01` e `web02` | `gunicorn_management` | `/srv/et/evote/logs/gunicorn_management.log` | `502` |
| `webapp` | `web01` e `web02` | `gunicorn_webapp` | `/srv/et/evote/logs/gunicorn_webapp.log` | `502` |
| `gunicorn` | `as01`, `as02`, `cs01` e `cs02` | `gunicorn` | `/srv/et/evote-counter/logs/gunicorn.log` ou<br>`/srv/et/evote-anonymizer/logs/gunicorn.log` | `502` |
| `celery` | `web01` e `web02` | `celery` | `/srv/et/evote/logs/celery.log` | `504` |
| `celerybeat` | `web01` | `celerybeat` | `/srv/et/evote/logs/celerybeat.log` | -- |
 
### Diagnóstico

#### passo 1:

Verificar o estado dos vários serviços. Para isso basta correr o comando:

```bash
$ sudo supervisorctl status evote:
```

> `evote` é o nome do agrupamento de todos os serviços

e o output esperado é com o estado `RUNNING`:
```bash
evote:gunicorn_webapp            RUNNING   pid 2449, uptime 23:02:25
evote:gunicorn_management        RUNNING   pid 2448, uptime 23:02:25
evote:gunicorn_admin             RUNNING   pid 2447, uptime 23:02:25
evote:celery                     RUNNING   pid 2446, uptime 23:02:25
evote:celerybeat                 RUNNING   pid 2445, uptime 23:02:25
```


Caso algum dos serviços tenha um estado diferente de `RUNNING`, algo como `FATAL` ou `BACKOFF`, ir para o passo seguinte.

#### passo 2:

Verificar logs dos serviços que apresentaram problemas e consultar tabela com possíveis soluções: [Problemas/Acções](#problemas-accoes).

#### passo 3:

Após ter identificado o problema e ter tomado as acções corretivas, deve efectuar restart ao serviço correndo o seguinte comando:

```shell
$ sudo supervisorctl restart evote:gunicorn
```

Pode também ser feito o restart a todos os serviços caso seja necessário:

```shell
$ sudo supervisorctl restart evote:
```

#### passo 4:

Verificar se output do comando corrido no **passo 4** está como esperado:

```shell
evote:gunicorn: stopped
evote:gunicorn: started
```

Caso surja a mensagem `ERROR (abnormal termination)` em vez de `stopped` ou `started`, voltar ao [passo 1](#passo-1). Caso contrário, verificar se no sistema de monitorização o check fica normalizado. 

### Problemas / Acções

| Checks | Problema | Acção |
| ----------- | -------------- | --------- |
| `admin`, `management`, `webapp`, `gunicorn` e `celery` | Introdução de erro ao nível de código python | Analisar traceback do erro, corrigir o problema e ir para o [passo 3](#passo-3) |
| `admin`, `management`, `webapp` e `gunicorn` | O lock file do `gunicorn` está corrompido | Apagar todos os ficheiros `.lock` presentes na pasta `/srv/et/evote/run/` e ir para o [passo 3](#passo-3) |
| `celery` e `celerybeat` | Os logs apresentam algo como: `Cannot connect to amqp://...` | O `rabbitmq-server` está em baixo, será necessário efectuar um restart. Consultar [Serviços](/diagnosis/services.md#rabbitmq-server). |


## Logs críticos

| Check | Máquinas | Ficheiro de log |
| ----- | -------- | --------------- | 
| `logs criticos` | `web01`, `web02`, `as01`, `as02`, `cs01` e `cs02` | `/srv/et/evote/logs/critical.log` ou<br> `/srv/et/evote-counter/logs/critical.log` ou <br>`/srv/et/evote-anonymizer/logs/critical.log` |

### Diagnóstico

#### passo 1:

Consultar o ficheiro de log crítico e procurar na tabela as possíveis soluções [Problemas/Acções](#problemas-accoes_1).

#### passo 2:

Após tomadas as devidas acções para resolver o problema, o ficheiro de log **deve ser limpo** (não apagado) de forma a **normalizar** o check no sistema de monitorização. 

> **Importante**: não limpar o ficheiro sem tomar as devidas acções de resolução.


### Problemas / Acções
A coluna *Problema* representa a **mensagem** registada no **ficheiro de log**.
 
| Problema | Causa | Acção |
| -------------- | ---------- | --------- |
| No Validator Service Certificate found | Certificado do Validator Service não introduzido | N. A. |
| No Voting System Private Key found | Certificado de votação privado não introduzido | N. A. |
| No Voting System Private Key password found | Password do certificado  de votação privado não introduzida | N. A. |
| Certificate integrity failed | Integridade dos certificados encontra-se comprometida | N. A. |
| Unchained Voting System certificate | Certificado do sistema de votação não está encadeado ao certificado de raíz | N. A. |
| Expired Voting System certificate | Certificado do sistema de votação expirou | N. A. |
| Could not sign election+user ID pkiutils.signObject=* | Certificado de votação privado ou password incorretos | N. A. |
| Could not sign election ID pkiutils.signObject=* | Certificado de votação privado ou password incorretos | N. A. |
| - Invalid election+user signature pkiutils.verifyObjectSignature=* <br>- Could not validate object signature verifyObjectSignature=* | Assinatura do ID de eleição está incorrecta | N. A. |
| Invalid vote blind signature eccblind.verifySignature=* | Blind signature do objecto do voto está errada |  |
| Vote not found for the given blind_hash: * and election_id: * | O estado de voto do votante é diferente de pendente ou referência de voto não associada | Simular votação |
| Anonymize vote: url=* status_code=* | Resposta inesperada na comunicação com o Anonymizer Service ao tentar inserir o voto | Verificar logs do Anonymizer Service |
| Scrutiny results: url=* status_code=* | Resposta inesperada na comunicação com o Counter Service ao tentar iniciar ou obter os resultados do escrutínio | Verificar logs do Counter Service |
| Scrutiny results: error message=* | Counter Service retornou mensagem de erro ao tentar iniciar ou obter os resultados do escrutínio | Analisar mensagem de erro retornada |
| Unsigned object contains no election_id | Não foi possível obter o id da eleição ao tentar validar o voto no Validator Service | Verificar recorrência deste problema, caso seja muito frequente, poderá tratar-se de uma tentativa de ataque. Desta forma, devem ser tomadas providencias de forma a bloquear o user. |
| Unsigned object contains no user_id | Não foi possível obter o id do user ao tentar validar o voto no Validator Service | Verificar recorrência deste problema, caso seja muito frequente, poderá tratar-se de uma tentativa de ataque. Desta forma, devem ser tomadas providencias de forma a bloquear o user. |
| Election * not in voting period | A eleição não se encontra no estado de votação | Verificar recorrência deste problema, caso seja muito frequente, poderá tratar-se de uma tentativa de ataque. Desta forma, devem ser tomadas providencias de forma a bloquear o user. |
| User can't vote in this election | O votante está a tentar votar noutra eleição que não a sua | Verificar recorrência deste problema, caso seja muito frequente, poderá tratar-se de uma tentativa de ataque. Desta forma, devem ser tomadas providencias de forma a bloquear o user. |
| Vote not available for this user code=* | O votante já votou na eleição (code=1) ou ultrapassou o tempo de votação (code=2) | Verificar código de erro retornado. Caso seja 1: poderá tratar-se de uma tentativa de ataque de múltiplos votos; Caso seja 2: pode ter ocorrido algum erro na inserção do voto, sendo necessário aguardar algum tempo para garantir que a tentativa de voto é novamente realizada. Em qualquer um dos casos, se a frequência for elevada, e não ser identificado qualquer problema na comunicação com os serviços, deverá ser considerado um ataque. |
| Could sign the blind data eccblind.generateBlindSignature=* | Tentativa de alteração de variáveis a meio do processo de inserção de voto | Verificar recorrência deste problema, caso seja muito frequente, poderá tratar-se de uma tentativa de ataque. Desta forma, devem ser tomadas providencias de forma a bloquear o user. |
| generate_blind_signature failed return=None | Tentativa de alteração de variáveis a meio do processo de inserção de voto | Verificar recorrência deste problema, caso seja muito frequente, poderá tratar-se de uma tentativa de ataque. Desta forma, devem ser tomadas providencias de forma a bloquear o user. |
| Send SMS Failed: message=\* to=\* | Webservice da UM não está a responder ou está a retornar algo inesperado | Verificar traceback do log e perceber se problema é da plataforma ou do serviço da UM. Caso seja do serviço da UM, contactar Eng. Mario Necho. |

## Serviços externos

| Check | Máquinas |
| ----- | -------- | 
| `members` | `web01` e `web02` |
| `sms` | `web01` e `web02` |

### Diagnóstico

Activar o ambiente virtual:
```shell
$ sudo su evote
$ cd /srv/et/evote/
$ source env/bin/activate
$ cd evote
```

Caso o problema esteja associado com o serviço de `sms`:
```shell
$ python manage.py check_sms_service 
```

Caso o problema esteja associado com o serviço de `member`:
```shell
$ python manage.py check_member_service
```

### Problemas / Acções
 
| Checks | Problema | Acção |
| ----------- | -------------- | --------- |
| `members` e `sms` | Comunicação falha com o serviço | Entrar em contacto com DTSI |
