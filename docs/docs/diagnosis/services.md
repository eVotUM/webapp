# Serviços

Nesta página estão listados os serviços de mais alto nível que não estão diretamente ligados à parte aplicacional. É uma página de apoio 

## `rabbitmq-server`
Este serviço encontra-se instalado e configurado nas máquinas: `web01` e `web02`.

Verificar o estado do serviço:
```shell
$ sudo systemctl status rabbitmq-server
```
Fazer restart ao serviço:
```shell
$ sudo systemctl restart rabbitmq-server
```

## `nginx`
Este serviço encontra-se instalado e configurado nas máquinas: `web01`, `web02`, `as01`, `as02`, `cs01` e `cs02`.

Verificar o estado do serviço:
```shell
$ sudo systemctl status nginx
```
Fazer restart ao serviço:
```shell
$ sudo systemctl restart nginx
```

## `supervisord`
Este serviço encontra-se instalado e configurado nas máquinas: `web01`, `web02`, `as01`, `as02`, `cs01` e `cs02`.

Verificar o estado do serviço:
```shell
$ sudo systemctl status supervisord
```
Fazer restart ao serviço:
```shell
$ sudo systemctl restart supervisord
```


## `lsyncd`
Este serviço encontra-se instalado e configurado nas máquinas: `web01`, `web02`. 

> Caso este serviço falhe pode provocar incongruência nos ficheiros que são carregados para a plataforma. O comportamento típico: acabou de carregar um ficheiro e ao tentar acedê-lo deu erro `404 Not Found`, ao fazer refresh já serviu correctamente o ficheiro, fez novamente refresh e voltou a dar `404 Not Found`.

Verificar o estado do serviço:
```shell
$ sudo systemctl status lsyncd
```
Fazer restart ao serviço:
```shell
$ sudo systemctl restart lsyncd
```

## `redis`
Este serviço encontra-se instalado e configurado nas máquinas: `db01`.

Verificar o estado do serviço:
```shell
$ sudo systemctl status redis
```
Fazer restart ao serviço:
```shell
$ sudo systemctl restart redis
```