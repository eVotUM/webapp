# Checks aplicacionais do SCOM

Os seguintes checks podem estar a verde - não sendo necessário proceder com qualquer ação - ou então a vermelho e nesse deve ser executada a ação correspondente.

| Check | Ação |
| ----- | -------- |
| `evote.monitoring.check.check.http.admin` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.as` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.as01` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.as02` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.autenticacao` | Contactar a AMA a reportar que o seguinte endereço não está a responder https://autenticacao.gov.pt/fa/Default.aspx (quando nos tentamos ligar por POST) |
| `evote.monitoring.check.check.http.cert.as` | Significa que o certificado HTTP está expirado (tem duração de 10 anos). É necessário escalar para o suporte. |
| `evote.monitoring.check.check.http.cert.cs` | Significa que o certificado HTTP está expirado (tem duração de 10 anos). É necessário escalar para o suporte.  |
| `evote.monitoring.check.check.http.cert.evotum` | Significa que o certificado HTTP está expirado (tem duração de 10 anos). É necessário escalar para o suporte.  |
| `evote.monitoring.check.check.http.cs` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.cs01` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.cs02` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.evotum` | - Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.http.login.admin` | Significa que o serviço de autenticação da U.M. está a falhar. Reportar à DTSI. |
| `evote.monitoring.check.check.http.login.evotum` | Significa que o serviço de autenticação da U.M. está a falhar. Reportar à DTSI. |
| `evote.monitoring.check.check.http.login.manage` | Significa que o serviço de autenticação da U.M. está a falhar. Reportar à DTSI. |
| `evote.monitoring.check.check.http.manage` |  Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Caso não estejam a vermelho, reiniciar o [NGINX](/diagnosis/services.md#nginx). <br>- Caso se mantenha a vermelho, escalar para o suporte. |
| `evote.monitoring.check.check.logs.critical` | Consultar o log critico da máquina correspondente. [Como fazer diagnóstico aqui](/diagnosis/checks.md#logs-criticos). |
| `evote.monitoring.check.check.logs.worm` | - Significa que não estão a ser escritos os logs auditáveis. Diagnóstico:<br>- Verificar se algum dos checks `evote.monitoring.check.check.supervisor.*` está a vermelho e proceder com as respetivas [ações](/diagnosis/checks.md#servicos) <br>- Verificar se não há problemas de rede (DTSI) <br>- Caso nenhuma das situações anteriores se verifiquem, é provável que a área worm esteja com problemas e nesse caso é necessário conactar o fornecedor da mesma |
| `evote.monitoring.check.check.manage.members.service` | Proceder com as ações relativas aos serviços externos. [Ver aqui](/diagnosis/checks.md#servicos-externos). |
| `evote.monitoring.check.check.manage.sms.service` | Proceder com as ações relativas aos serviços externos. [Ver aqui](/diagnosis/checks.md#servicos-externos). |
| `evote.monitoring.check.check.procs.lsyncd` | Reiniciar o serviço. [Ver aqui](/diagnosis/services.md#lsyncd). |
| `evote.monitoring.check.check.procs.redis` | Reiniciar o serviço. [Ver aqui](/diagnosis/services.md#redis). |
| `evote.monitoring.check.check.redis` | Caso o `evote.monitoring.check.check.procs.redis` esteja a vermelho é necessário proceder com a sua resolução, caso contrário significa que existem problemas de rede e é necessário falar com a DTSI.|
| `evote.monitoring.check.check.ssl.cert.autenticacao` | Significa que o certificado não é válido. É necessário escalar para o suporte.  |
| `evote.monitoring.check.check.ssl.cert.evotum` | Significa que o certificado não é válido. É necessário escalar para o suporte. |
| `evote.monitoring.check.check.supervisor.admin` | [Ver aqui como diagnosticar](/diagnosis/checks.md#servicos) |
| `evote.monitoring.check.check.supervisor.celery` | [Ver aqui como diagnosticar](/diagnosis/checks.md#servicos) |
| `evote.monitoring.check.check.supervisor.celerybeat` | [Ver aqui como diagnosticar](/diagnosis/checks.md#servicos) |
| `evote.monitoring.check.check.supervisor.gunicorn` | [Ver aqui como diagnosticar](/diagnosis/checks.md#servicos) |
| `evote.monitoring.check.check.supervisor.mgmt` | [Ver aqui como diagnosticar](/diagnosis/checks.md#servicos) |
| `evote.monitoring.check.check.supervisor.webapp` | [Ver aqui como diagnosticar](/diagnosis/checks.md#servicos) |
| `evote.monitoring.check.check.tcp.rabbitmq` | [Reiniciar o serviço](/diagnosis/services.md#rabbitmq-server) |
| `evote.monitoring.check.check.tcp.redis` | Reiniciar o serviço. [Ver aqui](/diagnosis/services.md#redis). |


Outros checks que surjam no SCOM e não estejam presentes na listagem acima, são alertas genéricos de sistema.