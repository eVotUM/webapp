# Logs aplicacionais
Os logs aplicacionais podem ser consultados no ficheiro: `/srv/et/evote/logs/evote.log`. Todos os logs acima do nível `INFO` são enviados para este ficheiro.

Os logs de nível `CRTITICAL` são enviados também para um ficheiro à parte: `/srv/et/evote/logs/critical.log`. Este ficheiro é usado para monitorização, isto é, assim que um log é adicionado ao ficheiro, é disparado um alerta. Para saber mais, consultar [Checks aplicacionais](/diagnosis/checks/#logs-criticos).

Estes estão organizados da seguinte forma:

| Logger | Descrição | Eventos |
| ----------- | -------------- | ------------ |
| `evote` | Logger de root. | --- | 
| `evote.core` | Eventos core da aplicação | - Tempos de execução na geração de ficheiros pdf;<br>- Mudanças de estado da eleição;<br>- Mudanças de estado do processo eleitoral;<br>- Envio de notificação à comissão eleitoral; |
| `evote.importers` | Eventos associados aos importadores da aplicação | - Charset do ficheiro a ser importado não é suportado. |
| `evote.mailings` | Eventos associados ao envio de emails | - Erros na tentativa de envio de email. |
| `evote.messages` | Eventos associados à mensagens | - Resumo de envio de mensagens;<br>- Resumo de actualização de permissões;<br>- Resumo de utilizadores notificados. |
| `evote.auditlogs` | Eventos associados aos logs auditáveis | - Operações CRUD sobre a base de dados. |
| `evote.crypto` | Eventos associados à parte criptográfica | - Erros no uso de funções criptográficas. |
| `evote.services` | Eventos associados à comunicação com o Counter e Anonymizer Service | - Problemas de comunicação com os serviços. |
| `evote.security` | Eventos associados com segurança | - Introdução de pin de votação;<br>- User tentou saltar passos nos steps de votação;<br>- Tentativa de autenticação com a AMA com o CC diferente. |
| `evote.contrib` | Logger de root | --- |
| `evote.contrib.umws` | Eventos associados aos serviços da UM | - Comunicação com os serviços;<br> - Envio de SMS's;<br>- Erros na comunicação com os serviços.  |
| `evote.contrib.ama` | Eventos associados à Autenticação.GOV | - Erros na verificação da assinatura do response.  |
| `evote.contrib.adfsauth` | Eventos associados à autenticação com a UM | - Autenticação na plataforma;<br>- Erro na validação da assinatura;<br>- Problemas nos dados enviados pela UM. |

