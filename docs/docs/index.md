# Documentação eVotUM

 * [Instalação](instalation.md)
 * [Realese](releases.md)
 * Diagnóstico:
    * [Logs aplicacionais](diagnosis/logging.md)
    * [Checks aplicacionais](diagnosis/checks.md)
    * [Serviços](diagnosis/services.md)
    * [Checks SCOM](diagnosis/scom-checks.md)
