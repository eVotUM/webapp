# Instalação

A instalação do sistema tem de ser efectuada em várias máquinas que compõem a sua arquitectura. De seguida são apresentados os pré-requisitos gerais do sistema e na próxima secção a descrição da instalação de cada componente.

## Pré-requisitos
Em todas as máquinas onde seja necessário instalar pacotes RPM provenientes do repositório eVotUM terá que ser instalado o seguinte pacote RPM (a ser fornecido):

```shell
$ rpm -Uvh evote-release.x86_64 evote-release-0.1-1.el7.x86_64.rpm
```

## Instalação dos componentes
Nesta secção vamos indicar como instalar todos os componentes necessários para o funcionamento da aplicação.

### Base de dados
Garantir que na máquina (ou máquinas) reservada para a Base de Dados seja instalado os pacotes disponibilizados pelo gestor de pacotes `yum`:

```shell
$ yum install mariadb-server.x86_64
$ yum install redis.x86_64
```

Garantir que estes serviços sejam devidamente inicializados e adicionados ao `chkconfig`. Garantir também a configuração de uma base de dados e respectivo utilizador com permissões totais.

### Service counter

Aceder à maquina (ou máquinas) via SSH destinada ao serviço **Service counter** e com o utilizador `root` instalar o pacote disponibilizado pelo gestor de pacotes `yum`.

```shell
$ yum install evote-counter.x86_64
```

Após instalação do pacote indicado, será necessário correr a script de instalação. Seguem os parâmetros necessários para a configuração da aplicação.

```shell
$ ./srv/et/evote-counter/scripts/install.sh --help
install.sh -- eVote instalation script

Usage: install.sh [options]

Options:
    --help      show this help text
    --db-host   insert database hostname (default: localhost)
    --db-name   insert database name
    --db-user   insert database user name 
    --db-pass   insert database user password
    --sn        insert server name for the counter service
```

### Service Anonymizer
Aceder à maquina (ou máquinas) via SSH destinada ao serviço **Service anonymizer** e com o utilizador `root` instalar o pacote disponibilizado pelo gestor de pacotes `yum`.

```shell
$ yum install evote-anonymizer.x86_64 
```

Após instalação do pacote indicado, será necessário correr a script de instalação. Seguem os parâmetros necessários para a configuração da aplicação.

```shell
$ ./srv/et/evote-anonymizer/scripts/install.sh --help
install.sh -- eVote instalation script

Usage: install.sh [options]

Options:
    --help      show this help text
    --db-host   insert database hostname (default: localhost)
    --db-name   insert database name
    --db-user   insert database user name 
    --db-pass   insert database user password
    --sn        insert server name for the anonymizer service
    --redis     insert the redis url (ex.: 'redis://localhost:6379/0')
```

### eVotUM Webapp
Aceder à maquina (ou máquinas) via SSH destinada à aplicação Web **eVotUM** e com o utilizador `root` instalar o pacote disponibilizado pelo gestor de pacotes `yum`.

```shell
$ yum install rabbitmq-server.noarch
$ yum install evote.x86_64
```

Após instalação dos pacotes indicados, será necessário, primeiro, garantir que o serviço do `rabbitmq-server` é inicializado e adicionado ao `chkconfig`. Idealmente e mais seguro, configurar um utilizador próprio também.

Segundom, será necessário correr a script de instalação do `evote`. Seguem os parâmetros necessários para a configuração da aplicação.

```shell
$ ./srv/et/evote/scripts/install.sh --help
install.sh -- eVote instalation script

Usage: install.sh [options]

Options:
    --help      show this help text
    --db-host   insert database hostname (default: localhost)
    --db-name   insert database name
    --db-user   insert database user name 
    --db-pass   insert database user password
    --sn-admin  insert server name for admin site
    --sn-manage insert server name for manage site
    --sn-evote  insert server name for evote site
    --broker    insert the broker url (ex.: 'amqp://guest:guest@localhost//')
    --redis     insert the redis url (ex.: 'redis://localhost:6379/0')
```

De seguida, será necessário adicionar os certificados da **Autenticação.GOV** e do sistema de **autenticação da Univerdade do Minho** (ADFS), na pasta: `/srv/et/evote/data/certs`. Nome dos ficheiros a atribuir:

 * `ama_autenticacao_gov_pt.crt` - Certificado fornecido pela **AMA**;
 * `evotum.uminho.pt.crt` - Certificado fornecido pela UM;
 * `evotum.uminho.pt.private.key` - Chave privada fornecida pela UM.
