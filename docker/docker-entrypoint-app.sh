#!/bin/bash

set -eu pipefail

find . -name \*.pyc -delete

mkdir -p logs data

# Wait for MySQL / MariaDB
# ------------------------
printf "Waiting for MySQL/MariaDB to initialize... \n"
while ! mysqladmin ping --host=db --password=$MYSQL_PASSWORD --silent; do
  sleep 1
done

# Generate the vendor frontend dependencies
bower install --allow-root

# Change to the project working dir. [Defined in config.env]
cd $PROJ_DIR;

# Run django migrations
python manage.py migrate --noinput

# Compile translations files
python manage.py compilemessages

# run main development server
python manage.py runserver_plus 0.0.0.0:8000 --cert-file /app/data/cert.crt --nopin
