#!/bin/bash

# Wait for MySQL / MariaDB
# ------------------------
printf "Waiting for MySQL/MariaDB to initialize... \n"
while ! mysqladmin ping --host=db --password=$MYSQL_PASSWORD --silent; do
  sleep 1
done

# Change to the project working dir. [Defined in config.env]
cd $PROJ_DIR;

# Run celery worker
celery worker -A $PROJ_NAME -l info -B -s /app/data/celerybeat-schedule
