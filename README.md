[![build status](https://gitlab.com/eVotUM/webapp/badges/master/build.svg)](https://gitlab.com/eVotUM/webapp/commits/master)
[![coverage report](https://gitlab.com/eVotUM/webapp/badges/master/coverage.svg)](https://gitlab.com/eVotUM/webapp/commits/master)

# eVote Webapp #

## Table of contents
 1. [Dependencies](#dependencies);
 1. [Starting developement](#starting-developement);
 1. [Running tests](#running-tests).

## Dependencies
 * [docker 1.10.0+](https://docs.docker.com/engine/installation/);
 * [docker-compose 1.7.1+](https://docs.docker.com/compose/install/);

## Starting developement 
```bash
// clone project
>>> git clone --recursive git@gitlab.com:eVotUM/webapp.git
>>> cd webapp

// build containers
>>> docker-compose up --build

// open another terminal and create a superuser
>>> docker-compose exec app bash
>>> cd evote
>>> python manage.py createsuperuser
```

After all this steps, the container are available on the following address:
 * Voting System - [http://127.0.0.1:8000](http://127.0.0.1:8000);
 * Election Management System - [http://127.0.0.1:8001](http://127.0.0.1:8001);
 * Admin System - [http://127.0.0.1:8002](http://127.0.0.1:8002).

### Using the full Docker stack ###
To launch additional Docker containers, for webservice integrations
simply run the following docker-compose command:

``` bash
// Launch all available containers
>>> docker-compose -f docker-compose.yml -f submodules/docker-compose-stack.yml up
```

## Running tests
To run tests you need to enter on **`app` container** and run one of the following examples code:
```bash
// running all tests suite (unit, coverage, flake8, docs)
>>> tox

// running only unit tests (for faster tests)
>>> tox -e unit

// running unit tests of specific app (even faster tests)
>>> tox -e unit evotevoting.processes

// running coverage
>>> tox -e coverage
```
