#!/bin/sh

rm -rf .rpmbuild

mkdir -p .rpmbuild/{SOURCES,SPECS,RPMS}

# copy all specs
cp builds/evote*.spec .rpmbuild/SPECS

# create src.rpm
tar -cvzf .rpmbuild/SOURCES/sources-evote.tgz --exclude=.rpmbuild --exclude=.git* --transform "s/^./sources-evote/" .
rpmbuild --nodeps -bs --define "_topdir .rpmbuild/" .rpmbuild/SPECS/evote.spec

git archive --format=tgz --prefix=sources-anonymizer/ --remote git@gitlab.com:eVotUM/service-anonymizer.git dev -o .rpmbuild/SOURCES/sources-anonymizer.tgz
rpmbuild --nodeps -bs --define "_topdir .rpmbuild/" .rpmbuild/SPECS/evote-anonymizer.spec

git archive --format=tgz --prefix=sources-counter/ --remote git@gitlab.com:eVotUM/service-counter.git dev -o .rpmbuild/SOURCES/sources-counter.tgz
rpmbuild --nodeps -bs --define "_topdir .rpmbuild/" .rpmbuild/SPECS/evote-counter.spec

# build rpms
echo "rm -f /tmp/evote*.src.rpm" | mock -r etux-7-x86_64 --shell
mock -r etux-7-x86_64 --copyin .rpmbuild/SRPMS/evote*.src.rpm /tmp/
mock -r etux-7-x86_64 --installdeps .rpmbuild/SRPMS/evote*.src.rpm
echo "export http_proxy=http://10.10.4.254:3128/ https_proxy=http://10.10.4.254:3128/; rpmbuild --rebuild /tmp/*rpm" | mock -r etux-7-x86_64 --shell

# copy out rpms
mock -r etux-7-x86_64 --copyout /builddir/build/RPMS/evote*.rpm .rpmbuild/RPMS/

# build release rpm
cp builds/evote.repo .rpmbuild/SOURCES/
rpmbuild --nodeps -bs --define "_topdir .rpmbuild/" .rpmbuild/SPECS/evote-release.spec
mock -r etux-7-x86_64 .rpmbuild/SRPMS/evote-release*.src.rpm

mock -r etux-7-x86_64 --copyout /builddir/build/RPMS/evote*.rpm .rpmbuild/RPMS/

# upload and sign rpms to repository
for f in .rpmbuild/RPMS/*rpm; do
    curl -X POST -u "$PWD_LDAPUSER:$PWD_LDAPPASS" -F "rpmfile=@$f" -F 'Repo=evotum-devel' http://rpmsign.gestao.eurotux.local/cgi-bin/upload.cgi
done

