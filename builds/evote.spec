%{!?enviroment:%define enviroment production}
%global _python_bytecompile_errors_terminate_build 0
%define  debug_package %{nil}

Name:           evote
Version:        1.0.3
Release:        1%{?dist}
Summary:        eVote Project

Group:          Applications/Internet
License:        GPLv2+
URL:            http://eurotux.com
Source0:        sources-evote.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python
BuildRequires: python-pip
BuildRequires: python-devel
BuildRequires: mariadb-devel
BuildRequires: openssl-devel
BuildRequires: libffi-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: python-virtualenv
BuildRequires: gcc
BuildRequires: gcc-c++
Requires: mariadb
Requires: python-virtualenv
Requires: python-pip
Requires: supervisor
Requires: nginx
Requires: haveged

AutoReqProv:	no

%description
eVote Project


%prep
%setup -q -n sources-evote


%build


%install
rm -rf $RPM_BUILD_ROOT
# Create all directories
mkdir -p $RPM_BUILD_ROOT/srv/et/evote/docs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote/data/certs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote/logs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote/run
# Copy code to final path
cp -r evote $RPM_BUILD_ROOT/srv/et/evote/
# Copy sripts to final path
cp -r builds/scripts $RPM_BUILD_ROOT/srv/et/evote/
# Copy config to final path
cp -r builds/config.env $RPM_BUILD_ROOT/srv/et/evote/
# Install all python dependencies with pip
cd $RPM_BUILD_ROOT/srv/et/evote/
virtualenv env
env/bin/pip install pip --upgrade
env/bin/pip install setuptools --upgrade
env/bin/pip install -r evote/evote/requirements/%{enviroment}.txt;
echo "source /srv/et/evote/config.env" >> env/bin/activate
# Remove RPM_BUILD_ROOT string from files
sed -i "s|${RPM_BUILD_ROOT}||g" env/bin/*
# Add script service to nginx (symbolic links)
mkdir -p $RPM_BUILD_ROOT/etc/nginx/conf.d
ln -s /srv/et/evote/scripts/evote-manage.conf $RPM_BUILD_ROOT/etc/nginx/conf.d/evote-manage.conf
ln -s /srv/et/evote/scripts/evote-admin.conf $RPM_BUILD_ROOT/etc/nginx/conf.d/evote-admin.conf
ln -s /srv/et/evote/scripts/evote.conf $RPM_BUILD_ROOT/etc/nginx/conf.d/evote.conf
ln -s /srv/et/evote/scripts/evote.server $RPM_BUILD_ROOT/etc/nginx/conf.d/evote.server
ln -s /srv/et/evote/scripts/evote-common.conf $RPM_BUILD_ROOT/etc/nginx/conf.d/00-evote-common.conf
# Add script service to supervisor (symbolic links)
mkdir -p $RPM_BUILD_ROOT/etc/supervisord.d
ln -s /srv/et/evote/scripts/supervisor.ini $RPM_BUILD_ROOT/etc/supervisord.d/evote.ini


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
/srv/et/evote/docs
/srv/et/evote/data
/srv/et/evote/logs
/srv/et/evote/run
/srv/et/evote/evote
/srv/et/evote/env
/srv/et/evote/scripts/install.sh
%config(noreplace) /srv/et/evote/scripts/*_start
%config(noreplace) /srv/et/evote/config.env
%config(noreplace) /srv/et/evote/scripts/evote-manage.conf
%config(noreplace) /srv/et/evote/scripts/evote-admin.conf
%config(noreplace) /srv/et/evote/scripts/evote.conf
%config(noreplace) /srv/et/evote/scripts/evote-common.conf
%config(noreplace) /srv/et/evote/scripts/evote.server
%config(noreplace) /srv/et/evote/scripts/supervisor.ini
%config(noreplace) /etc/nginx/conf.d/evote-manage.conf
%config(noreplace) /etc/nginx/conf.d/evote-admin.conf
%config(noreplace) /etc/nginx/conf.d/evote.conf
%config(noreplace) /etc/nginx/conf.d/00-evote-common.conf
%config(noreplace) /etc/nginx/conf.d/evote.server
%config(noreplace) /etc/supervisord.d/evote.ini

%post
# Code for install rpm
if [ "$1" == "1" ]; then
    useradd evote
    chown -R evote:evote /srv/et/evote
    chmod u+x /srv/et/evote/scripts/*_start

    systemctl enable haveged
    systemctl start haveged

    systemctl enable nginx.service
    systemctl start nginx.service

    systemctl enable supervisord.service
    systemctl start supervisord.service
    supervisorctl update
    supervisorctl start evote:

    echo "To finish the instalation use the command '/srv/et/evote/scripts/install.sh'"

# Code for update rpm
else
    chown -R evote:evote /srv/et/evote
    chmod u+x /srv/et/evote/scripts/*_start

    source /srv/et/evote/env/bin/activate
    cd /srv/et/evote/evote/
    python manage.py compilemessages
    python manage.py collectstatic --noinput
    python manage.py compress
    python manage.py migrate --noinput

    systemctl reload nginx.service
    supervisorctl restart evote:
fi


%changelog

* Tue Oct 09 2018 Sandro Rodrigues <sfr@eurotux.com> 1.0.3-1
- Updated source code

* Tue Jul 04 2017 Sandro Rodrigues <sfr@eurotux.com> 1.0.2-1
- Updated source code

* Thu Jun 01 2017 Sandro Rodrigues <sfr@eurotux.com> 1.0.1-1
- Updated source code

* Thu Jun 01 2017 Sandro Rodrigues <sfr@eurotux.com> 1.0.0-1
- Updated source code

* Tue May 30 2017 Sandro Rodrigues <sfr@eurotux.com> 1.0.0-0.1.rc1
- Updated source code

* Mon Mar 27 2017 Sandro Rodrigues <sfr@eurotux.com> 0.12.3-1
- Updated source code

* Mon Mar 27 2017 Sandro Rodrigues <sfr@eurotux.com> 0.12.2-1
- Updated source code

* Sun Mar 19 2017 Sandro Rodrigues <sfr@eurotux.com> 0.12.1-1
- Updated source code

* Sun Mar 19 2017 Sandro Rodrigues <sfr@eurotux.com> 0.12.0-1
- Updated source code

* Thu Mar 16 2017 Sandro Rodrigues <sfr@eurotux.com> 0.12.0-0.2.rc1
- Updated source code

* Thu Mar 16 2017 Sandro Rodrigues <sfr@eurotux.com> 0.12.0-0.1.alpha1
- Updated source code

* Fri Mar 10 2017 Sandro Rodrigues <sfr@eurotux.com> 0.11-1
- Final / Stable release

* Fri Mar 10 2017 Sandro Rodrigues <sfr@eurotux.com> 0.11rc1-1
- Updated source code

* Fri Mar 10 2017 Sandro Rodrigues <sfr@eurotux.com> 0.11b1-1
- Updated source code
- Remove localpkgs dependency

* Fri Feb 24 2017 Sandro Rodrigues <sfr@eurotux.com> 0.10.3-1
- Updated source code

* Tue Feb 21 2017 Sandro Rodrigues <sfr@eurotux.com> 0.10.2-1
- Updated source code

* Fri Feb 17 2017 Sandro Rodrigues <sfr@eurotux.com> 0.10.1-1
- Updated source code

* Tue Feb 14 2017 Sandro Rodrigues <sfr@eurotux.com> 0.10.0-1
- Updated source code

* Thu Feb 9 2017 Sandro Rodrigues <sfr@eurotux.com> 0.9.0-1
- Updated source code

* Wed Feb 8 2017 Sandro Rodrigues <sfr@eurotux.com> 0.8.1-1
- Updated source code

* Wed Feb 8 2017 Sandro Rodrigues <sfr@eurotux.com> 0.8.0-1
- Updated source code

* Tue Feb 1 2017 Sandro Rodrigues <sfr@eurotux.com> 0.7.1-1
- Updated source code

* Tue Jan 31 2017 Sandro Rodrigues <sfr@eurotux.com> 0.7.0-1
- Removed backup and cache folder
- Updated source code

* Fri Jan 27 2017 Sandro Rodrigues <sfr@eurotux.com> 0.6.0-1
- Updated source code

* Wed Jan 25 2017 André Rocha <asr@eurotux.com> 0.5.10-1
- Replaced gracefull reload of gunicorn to restart
- Added additional nginx .conf files
- Updated source code

* Tue Jan 24 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.9-1
- Added python manage.py compilemessages to updates
- Added %config(noreplace) to /srv/et/evote/scripts/*_start
- Updated source code

* Mon Jan 20 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.8-1
- Updated source code

* Mon Jan 16 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.7-1
- Updated source code

* Wed Jan 11 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.6-1
- Updated source code

* Tue Jan 10 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.5-1
- Added chmod to give execute permissions to *_start scripts
- Updated source code

* Mon Jan 9 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.4-1
- Updated source code

* Mon Jan 9 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.3-1
- Updated source code

* Fri Jan 6 2017 Luis Silva <lms@eurotux.com> 0.5.2-1
- Updated source code

* Thu Jan 5 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.1-1
- Updated source code

* Tue Jan 4 2017 Luis Silva <lms@eurotux.com> 0.5.0-2
- Created new release

* Tue Jan 3 2017 Luis Silva <lms@eurotux.com> 0.5.0-1
- Updated source code
- Removed rabbitmq from this package

* Wed Dec 28 2016 Sandro Rodrigues <sfr@eurotux.com> 0.4.3-1
- Updated source code

* Fri Dec 23 2016 Sandro Rodrigues <sfr@eurotux.com> 0.4.2-1
- Updated source code
- added haveged requirement

* Thu Dec 22 2016 Sandro Rodrigues <sfr@eurotux.com> 0.4.1-1
- Updated source code

* Tue Dec 20 2016 Sandro Rodrigues <sfr@eurotux.com> 0.4-3
- Updated source code

* Mon Dec 19 2016 Sandro Rodrigues <sfr@eurotux.com> 0.4-2
- Updated source code

* Mon Dec 19 2016 Sandro Rodrigues <sfr@eurotux.com> 0.4-1
- Removed redis dependency
- eVote release 0.4

* Sun Nov 27 2016 Sandro Rodrigues <sfr@eurotux.com> 0.3-5
- Added django management commands to run on rpm update

* Sun Nov 27 2016 Sandro Rodrigues <sfr@eurotux.com> 0.3-4
- Updated source code

* Fri Nov 25 2016 Luis Silva <lms@eurotux.com> 0.3-3
- Added BuildRequires python-devel

* Fri Nov 25 2016 Luis Silva <lms@eurotux.com> 0.3-2
- bug fixed in update rpm

* Fri Nov 25 2016 Luis Silva <lms@eurotux.com> 0.3-1
- eVote release 0.3

* Mon Nov 7 2016 Luis Silva <lms@eurotux.com> 0.2-1
- eVote release 0.2

* Thu Oct 20 2016 Luis Silva <lms@eurotux.com> 0.1-1
- eVote first version
