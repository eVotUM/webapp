%define debug_package %{nil}

Name:           evote-release
Version:        0.1
Release:        1%{?dist}
Summary:        Evote release file
Group:          System Environment/Base
License:        GPLv2+
Source0:        evote.repo

%description
Evote release files

%prep
#%setup -q

%build
echo OK

%install
rm -rf $RPM_BUILD_ROOT

# create /etc
mkdir -p $RPM_BUILD_ROOT/etc

mkdir -p $RPM_BUILD_ROOT/etc/yum.repos.d
install -m 644 %{SOURCE0} $RPM_BUILD_ROOT/etc/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%config %attr(0644,root,root) /etc/yum.repos.d/*

%changelog
* Tue Nov 08 2016 Carlos Rodrigues <cmar@eurotux.com> 0.1-1
- Evote first release

