#!/bin/bash

ENV='dev'
SECRET_KEY="`tr -dc "A-Za-z0-9A-Za-z0-9!@#$%&(_=)><" < /dev/urandom | head -c 128`"
BROKER_URL='amqp://guest:guest@localhost//'
REDIS_URL='redis://localhost:6379/0'
SAML_PK_PASSPHRASE='ola12345'

MYSQL_HOST='127.0.0.1'
MYSQL_DATABASE='evote'
MYSQL_USER='root'
MYSQL_PASSWORD='123456'

SN_EVOTE='evote.dev.eurotux.com'
SN_MANAGE='evote-manage.dev.eurotux.com'
SN_ADMIN='evote-admin.dev.eurotux.com'


usage="$(basename "$0") -- eVote instalation script

Usage: $(basename "$0") [options]

Options:
    --help      show this help text
    --env       insert environment (dev|staging|production)
    --db-host   insert database hostname (default: localhost)
    --db-name   insert database name
    --db-user   insert database user name
    --db-pass   insert database user password
    --sn-admin  insert server name for admin site
    --sn-manage insert server name for manage site
    --sn-evote  insert server name for evote site
    --sn-as     insert server name for evote anonymizer service
    --sn-cs     insert server name for evote counter service
    --broker    insert the broker url (ex.: 'amqp://guest:guest@localhost//')
    --redis     insert the redis url (ex.: 'redis://localhost:6379/0')
    --saml-pass insert the passphrase for SAML signature
    "


while [ "$1" != "" ]; do
    case $1 in
        --help      ) shift
                    echo "$usage"
                    exit 1;
                    ;;
        --env       ) shift
                    ENV=$1
                    ;;
        --db-host   ) shift
                    MYSQL_HOST=$1
                    ;;
        --db-name   ) shift
                    MYSQL_DATABASE=$1
                    ;;
        --db-user   ) shift
                    MYSQL_USER=$1
                    ;;
        --db-pass   ) shift
                    MYSQL_PASSWORD=$1
                    ;;
        --sn-admin  ) shift
                    SN_ADMIN=$1
                    ;;
        --sn-manage ) shift
                    SN_MANAGE=$1
                    ;;
        --sn-evote  ) shift
                    SN_EVOTE=$1
                    ;;
        --sn-as     ) shift
                    SN_EVOTE_AS=$1
                    ;;
        --sn-cs     ) shift
                    SN_EVOTE_CS=$1
                    ;;
        --broker    ) shift
                    BROKER_URL=$1
                    ;;
        --redis     ) shift
                    REDIS_URL=$1
                    ;;
        --saml-pass ) shift
                    SAML_PK_PASSPHRASE=$1
                    ;;
        * )         echo "Option not found. Try './install.sh --help' for more information."
                    exit 1
    esac
    shift
done

# Database check
if ! mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD -h $MYSQL_HOST -e "use $MYSQL_DATABASE"; then
    if ! mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD -h $MYSQL_HOST -e "create database $MYSQL_DATABASE"; then
        echo "No database found and no permissions to create new database"
        exit 1
    fi
fi

function escape {
    echo $1 | sed -e 's/\\/\\\\/g; s/\//\\\//g; s/&/\\\&/g'
}

sed -i "s/DJANGO_SETTINGS_MODULE=.*/DJANGO_SETTINGS_MODULE='evote\.settings\.$ENV'/;" /srv/et/evote/config.env
sed -i "s/SECRET_KEY=.*/SECRET_KEY='$(escape $SECRET_KEY)'/;" /srv/et/evote/config.env
sed -i "s/BROKER_URL=.*/BROKER_URL='$(escape $BROKER_URL)'/;" /srv/et/evote/config.env
sed -i "s/REDIS_URL=.*/REDIS_URL='$(escape $REDIS_URL)'/;" /srv/et/evote/config.env
sed -i "s/SAML_PK_PASSPHRASE=.*/SAML_PK_PASSPHRASE='$(escape $SAML_PK_PASSPHRASE)'/;" /srv/et/evote/config.env

sed -i "s/MYSQL_HOST=.*/MYSQL_HOST='$(escape $MYSQL_HOST)'/;" /srv/et/evote/config.env
sed -i "s/MYSQL_DATABASE=.*/MYSQL_DATABASE='$MYSQL_DATABASE'/;" /srv/et/evote/config.env
sed -i "s/MYSQL_USER=.*/MYSQL_USER='$MYSQL_USER'/;" /srv/et/evote/config.env
sed -i "s/MYSQL_PASSWORD=.*/MYSQL_PASSWORD='$MYSQL_PASSWORD'/;" /srv/et/evote/config.env

sed -i "s/SN_EVOTE=.*/SN_EVOTE='$(escape $SN_EVOTE)'/;" /srv/et/evote/config.env
sed -i "s/SN_MANAGE=.*/SN_MANAGE='$(escape $SN_MANAGE)'/;" /srv/et/evote/config.env
sed -i "s/SN_ADMIN=.*/SN_ADMIN='$(escape $SN_ADMIN)'/;" /srv/et/evote/config.env
sed -i "s/SN_EVOTE_AS=.*/SN_EVOTE_AS='$(escape $SN_EVOTE_AS)'/;" /srv/et/evote/config.env
sed -i "s/SN_EVOTE_CS=.*/SN_EVOTE_CS='$(escape $SN_EVOTE_CS)'/;" /srv/et/evote/config.env

sed -i "s/server_name .*/server_name $(escape $SN_ADMIN);/;" /srv/et/evote/scripts/evote-admin.conf
sed -i "s/server_name .*/server_name $(escape $SN_MANAGE);/;" /srv/et/evote/scripts/evote-manage.conf
sed -i "s/server_name .*/server_name $(escape $SN_EVOTE);/;" /srv/et/evote/scripts/evote.conf

systemctl restart nginx.service
supervisorctl update
supervisorctl restart evote:

cd /srv/et/evote/evote
source /srv/et/evote/config.env
/srv/et/evote/env/bin/python manage.py collectstatic --no-input --settings=$DJANGO_SETTINGS_MODULE
/srv/et/evote/env/bin/python manage.py compilemessages --settings=$DJANGO_SETTINGS_MODULE
/srv/et/evote/env/bin/python manage.py compress --settings=$DJANGO_SETTINGS_MODULE
/srv/et/evote/env/bin/python manage.py migrate --no-input --settings=$DJANGO_SETTINGS_MODULE
