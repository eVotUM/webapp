# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.utils import timezone

from evotedocs.documents import PdfDocument

from reportlab.platypus import Indenter


class ElectoralProcessPdf(PdfDocument):

    def __init__(self, electoral_process):
        super(ElectoralProcessPdf, self).__init__()
        self.electoral_process = electoral_process

    def set_story(self):
        self.story.append(self.title(self.get_title()))

        self.story.append(self.pure_sec_title(_("Summary")))
        self.story.append(self.space())
        self.story.append(self.pure_sec_title("1. " + _("General Data")))
        self.story.append(self.get_process_table())

        # Adds all elections
        for i, election in enumerate(self.electoral_process.elections.all()):
            self.story.append(self.pure_sec_title(
                "1.{} {} {}".format(i + 1, _('General Data | Election'),
                                    election)))
            self.story.append(self.get_election_table(election))
            self.story.append(self.space(height=10))
            self.story.append(self.thcell(_('Electoral timetable')))
            self.story.append(Indenter(15, 15))
            self.get_electoral_calendar(election)
            self.story.append(Indenter(-15, -15))

        self.story.append(self.space(height=10))
        self.story.append(Indenter(0, 0))
        self.story.append(
            self.pure_sec_title("2. " + _("Electoral Comission")))
        self.story.append(Indenter(-15, 0))
        self.story.append(self.space())
        self.story.append(self.get_members_table())

    def get_process_table(self):
        """ Returns the table with electoral process identification """
        image = ''
        if self.electoral_process.logo:
            image = self.image(self.electoral_process.logo.path, 58)

        table_data = []
        table_data.append([
            _("Logo"),
            _("Electoral process identifier"),
            _("Description")
        ])
        table_data.append([
            image,
            self.tdcell(self.electoral_process.identifier),
            self.tdcell(self.electoral_process.description)
        ])

        return self.table(table_data, col_percs=[0.2, 0.4, 0.4])

    def get_election_table(self, election):
        """ Returns the table with election identification """
        table_data = []
        table_data.append([
            self.thcell(_("Election identifier")),
            self.thcell(_("Description"))
        ])
        table_data.append([
            self.tdcell(election.identifier),
            self.tdcell(election.description)
        ])
        return self.table(table_data, col_percs=[0.5, 0.5])

    def get_electoral_calendar(self, election):
        """ Appends to story the electoral calendar of election """
        self.story.append(self.pure_sec_title(_("Proposition dates")))
        self.story.append(self.get_date_table(
            _("Begin date"),
            self.get_formated_date(election.proposition_begin_date, icon=True),
            _("End date"),
            self.get_formated_date(election.proposition_end_date, icon=True)))
        self.story.append(self.pure_sec_title(_("Electoral roll")))
        self.story.append(self.get_date_table(
            _("Publication date before verification"),
            self.get_formated_date(election.roll_begin_date, icon=True),
            _("Publication date after verification and correction"),
            self.get_formated_date(election.roll_end_date, icon=True)))
        self.story.append(self.get_date_table(
            _("Complaint period - start"),
            self.get_formated_date(election.complaint_begin_date, icon=True),
            _("Complaint period - end"),
            self.get_formated_date(election.complaint_end_date, icon=True)))
        self.story.append(self.pure_sec_title(_("Voting time")))
        self.story.append(self.get_date_table(
            _("Begin date"),
            self.get_formated_date(election.voting_begin_date, icon=True),
            _("End date"),
            self.get_formated_date(election.voting_end_date, icon=True)))
        self.story.append(self.pure_sec_title(_("Voting results")))
        self.story.append(self.get_date_table(
            _("Date of publication"),
            self.get_formated_date(election.minute_publication_date,
                                   icon=True)))
        self.story.append(self.get_date_table(
            _("Approval period - start"),
            self.get_formated_date(election.minute_begin_date, icon=True),
            _("Approval period - end"),
            self.get_formated_date(election.minute_end_date, icon=True))
        )

    def get_date_table(self, id_one, field_one, id_two=None, field_two=None):
        """ Returns the table with electoral process identification """
        col_percs = [1]
        table_data = []
        table_data.append([
            self.thcell(id_one),
        ])
        table_data.append([
            self.tdcell("{} {}".format(field_one, _('GMT Timezone'))),
        ])
        if id_two and field_two:
            col_percs = [0.5, 0.5]
            table_data[0].append(self.thcell(id_two))
            table_data[1].append(
                self.tdcell("{} {}".format(field_two, _('GMT Timezone'))))
        return self.table(table_data, col_percs=col_percs)

    def get_members_table(self):
        """ Returns the table of members """
        electoral_members = self.electoral_process.electoral_members.all()
        table_members = []
        table_members.append(['#', _('Name'), _('Role'), _('Designation'),
                              _('Email address'), _('Contact')])
        for i, member in enumerate(electoral_members):
            table_members.append([
                str(i + 1),
                self.tdcell(member.user.name),
                self.tdcell(member.get_role_display()),
                self.tdcell(member.designation),
                self.tdcell(member.user.get_email()),
                self.tdcell(str(member.phone))
            ])
        return self.long_table(table_members,
                               col_percs=[0.03, 0.26, 0.13, 0.20, 0.21, 0.17])

    def get_title(self):
        return "{} - {}".format(self.electoral_process, _("Summary"))

    def set_footer(self):
        self.footer.append(self.hr(space_after=0))
        now = timezone.now().strftime('%d-%M-%Y %H:%M:%S')
        text = "{}: {}".format(_('Generated in'), now)
        self.footer.append(self.body(text))
