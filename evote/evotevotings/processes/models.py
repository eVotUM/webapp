# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.db.models.signals import post_save, post_init, pre_delete
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.conf import settings
from django.urls import reverse
from django.db import models

from django.contrib.contenttypes.models import ContentType
from phonenumber_field.modelfields import PhoneNumberField
from django_extensions.db.models import TimeStampedModel
from evotesearch.decorators import searchable
from evotemailings.mixins import MailingListMixin
from guardian.shortcuts import assign_perm, remove_perm
from evotecore.mixins import ModelFieldChangedMixin
from evotedocs.models import Document
from evotecore.utils import images_upload_to
from guardian.utils import get_anonymous_user
from sorl.thumbnail import ImageField
from django_fsm import FSMIntegerField, transition
from autoslug import AutoSlugField
from evotelogs.decorators import auditable

from .managers import (ElectoralProcessQuerySet, ElectoralMemberManager,
                       ElectoralActionManager)
from .tasks import publish_electoral_process


@python_2_unicode_compatible
class Action(TimeStampedModel):
    """ """
    designation = models.CharField(_("action"), max_length=128, db_index=True)
    order = models.PositiveSmallIntegerField(_("order"), default=0)

    class Meta:
        verbose_name = _("action")
        verbose_name_plural = _("actions")
        ordering = ['order']

    def __str__(self):
        return self.designation


class ElectoralProcessStatesMixin(models.Model):
    """ """

    class State:
        NEW = 0
        OPEN = 1
        CLOSED = 2
        ARCHIVED = 3
        DRAFT = 4

        choices = (
            (NEW, _("Created")),
            (OPEN, _("Open")),
            (CLOSED, _("Closed")),
            (ARCHIVED, _("Archived")),
            (DRAFT, _("Draft")),
        )

    state = FSMIntegerField(
        _("status"), choices=State.choices, default=State.DRAFT)

    class Meta:
        abstract = True

    def is_draft(self):
        """Check if electoral process is in state draft"""
        return self.state == self.State.DRAFT

    def is_open(self):
        """Check if electoral process is in state open"""
        return self.state == self.State.OPEN

    def is_new(self):
        """Check if electoral process is in state open"""
        return self.state == self.State.NEW

    def can_submit(self):
        """Check if electoral process has members"""
        return self.electoral_members.exists()

    def can_publish(self):
        """Check if publication date has been reached, otherwise cant publish
        """
        return self.publication_date <= timezone.now()

    def can_close(self):
        """Check if all elections are closed, otherwise cant close
        """
        return self.elections.closed().count() == self.elections.count()

    def can_archive(self):
        """Check if all elections have been archived"""
        return self.elections.archived().count() == self.elections.count()

    @transition("state", source=State.DRAFT, target=State.NEW,
                conditions=[can_submit])
    def submit_new(self):
        """Submit new electoral process"""
        chairmans = self.electoral_members.filter(
            role=ElectoralMember.Role.CHAIRMAN)
        if chairmans.exists():
            ElectoralMember.objects.mark_as_responsible(
                self, chairmans.first())

    @transition("state", source=State.NEW, target=State.OPEN,
                conditions=[can_publish])
    def publish(self):
        """Publish the electoral process when publication_date is reached and
        all associated elections
        """
        for election in self.elections.all():
            election.publish()
            election.save()

    @transition("state", source=State.OPEN, target=State.CLOSED,
                conditions=[can_close])
    def close(self):
        """Transition only called after all his elections are closed"""
        self.closed_on = timezone.now()

    @transition("state", source=State.CLOSED, target=State.ARCHIVED,
                conditions=[can_archive])
    def archive(self):
        """ """


@auditable
@searchable(title='identifier', content='description',
            extra={'is_test': False})
@python_2_unicode_compatible
class ElectoralProcess(
        ElectoralProcessStatesMixin, MailingListMixin, ModelFieldChangedMixin,
        TimeStampedModel):
    """ """

    logo = ImageField(_('Logo (minimum size 135px x 100px)'),
                      upload_to=images_upload_to, blank=True)
    slug = AutoSlugField(
        populate_from='identifier', unique=True, editable=False)
    identifier = models.CharField(
        _("electoral process identifier"), max_length=128, db_index=True,
        help_text=_("Insert the electoral process designation"))
    description = models.CharField(
        _("electoral process description"), max_length=512,
        help_text=_("brief description text about electoral process"))

    closed_on = models.DateTimeField(_("closed on"), blank=True, null=True)
    actions = models.ManyToManyField(Action, through="ElectoralAction",
                                     verbose_name=_("actions"), blank=True)
    actions_configured = models.BooleanField(default=False, editable=False)
    publication_date = models.DateTimeField(_("publication date"), blank=True,
                                            null=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("created by"),
        on_delete=models.CASCADE, related_name="electoral_processes")
    is_test = models.BooleanField(_("Is test electoral process?"),
                                  default=False)
    objects = ElectoralProcessQuerySet.as_manager()

    class Meta:
        ordering = ['-publication_date']
        verbose_name = _("electoral process")
        verbose_name_plural = _("electoral processes")
        permissions = (
            ('view_electoralprocess', 'View electoral process'),
            ('manage_electoralprocess', 'Manage electoral process')
        )

    def __str__(self):
        return self.identifier

    def get_absolute_url(self):
        return reverse('voters:electoralprocess-detail', kwargs={
            'slug': self.slug
        })

    @property
    def audit_group(self):
        group_type = ContentType.objects.get_for_model(self)
        return (group_type.id, self.pk)

    @property
    def can_edit_members(self):
        """ If draft, can edit members of process, otherwise check if
        proposition_end_date has been achived by any election """
        if self.is_draft():
            return True
        return not self.elections.filter(
            proposition_end_date__lte=timezone.now()).exists()

    def get_mailing_list(self):
        """ """
        emails = []
        for member in self.electoral_members.all():
            emails.append([
                member.user.name,
                member.user.get_email()
            ])
        return emails

    @staticmethod
    def post_init(sender, instance, **kwargs):
        """Register attributes to track changes"""
        instance.track_changes(['publication_date'])

    @staticmethod
    def post_save_changes(sender, instance, **kwargs):
        """Track attributes changes on post save"""
        if instance.has_changed('publication_date') and not instance.closed_on:
            publish_electoral_process.si(
                instance.pk).apply_async(eta=instance.publication_date)

    @staticmethod
    def post_save_anonymous_permissions(sender, instance, **kwargs):
        """Assign permission of anonymous user on new created electoral
        processes"""
        if kwargs.get('created'):
            assign_perm("change_electoralprocess", instance.created_by,
                        instance)
            if not instance.is_test:
                user = get_anonymous_user()
                assign_perm("view_electoralprocess", user, instance)

    @staticmethod
    def pre_delete_anonymous_permissions(sender, instance, **kwargs):
        """Remove permissions of anonymous user on this electoral process"""
        remove_perm("change_electoralprocess", instance.created_by, instance)
        if not instance.is_test:
            user = get_anonymous_user()
            remove_perm("view_electoralprocess", user, instance)


post_init.connect(ElectoralProcess.post_init, sender=ElectoralProcess)
post_save.connect(ElectoralProcess.post_save_changes, sender=ElectoralProcess)
post_save.connect(ElectoralProcess.post_save_anonymous_permissions,
                  sender=ElectoralProcess)
pre_delete.connect(ElectoralProcess.pre_delete_anonymous_permissions,
                   sender=ElectoralProcess)


@auditable
@python_2_unicode_compatible
class ElectoralAction(TimeStampedModel):
    """ """
    class Strength:
        SIMPLE = 0
        STRONG = 1
        CRITICAL = 2

        choices = (
            (SIMPLE, _("Simple")),
            (STRONG, _("Strong")),
            (CRITICAL, _("Critical")),
        )

    action = models.ForeignKey(Action, verbose_name=_("action"),
                               on_delete=models.CASCADE,
                               related_name="electoral_actions")
    electoral_process = models.ForeignKey(
        ElectoralProcess, verbose_name=_("electoral process"),
        on_delete=models.CASCADE, related_name="electoral_actions")
    strength = models.PositiveSmallIntegerField(
        _("strength"), choices=Strength.choices, blank=True,
        default=Strength.STRONG)
    objects = ElectoralActionManager()

    class Meta:
        verbose_name = _("electoral action")
        verbose_name_plural = _("electoral actions")
        ordering = ['action__order']

    def __str__(self):
        return "{} ({})".format(self.action, self.get_strength_display())

    @property
    def audit_group(self):
        elec_proc = self.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)


@auditable
@python_2_unicode_compatible
class ElectoralMember(TimeStampedModel):
    """ """

    class Role:
        CHAIRMAN = 0
        OTHER = 1

        choices = (
            (CHAIRMAN, _("Chairman")),
            (OTHER, _("Member")),
        )

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("user"),
        on_delete=models.CASCADE, related_name="electoral_members")
    electoral_process = models.ForeignKey(
        ElectoralProcess, verbose_name=_("electoral process"),
        related_name="electoral_members", on_delete=models.CASCADE)
    designation = models.CharField(_("designation"), max_length=128,
                                   db_index=True)
    role = models.PositiveSmallIntegerField(_("role"), choices=Role.choices)
    phone = PhoneNumberField(_("contact phone number"), blank=True)
    private_key = models.TextField(_("key"), blank=True)
    is_comunication_responsible = models.BooleanField(default=False)
    objects = ElectoralMemberManager()

    class Meta:
        verbose_name = _("electoral member")
        verbose_name_plural = _("electoral members")

    def __str__(self):
        return self.user.name + " (" + self.get_role_display() + ")"

    @property
    def audit_group(self):
        elec_proc = self.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)

    @staticmethod
    def post_save(sender, instance, created, **kwargs):
        if created:
            assign_perm("manage_electoralprocess", instance.user,
                        instance.electoral_process)

    @staticmethod
    def pre_delete(sender, instance, **kwargs):
        remove_perm("manage_electoralprocess", instance.user,
                    instance.electoral_process)


post_save.connect(ElectoralMember.post_save, sender=ElectoralMember)
pre_delete.connect(ElectoralMember.pre_delete, sender=ElectoralMember)


@auditable
@python_2_unicode_compatible
class ElectoralDocument(TimeStampedModel):
    """ """
    electoral_process = models.ForeignKey(
        ElectoralProcess, verbose_name=_("electoral process"),
        related_name="electoral_documents", on_delete=models.CASCADE)
    designation = models.CharField(_("designation"), max_length=128,
                                   db_index=True)
    documents = models.ManyToManyField(Document, verbose_name=_("documents"),
                                       blank=True)

    class Meta:
        verbose_name = _("Electoral document")
        verbose_name_plural = _("Electoral documents")

    def __str__(self):
        return self.designation

    @property
    def audit_group(self):
        elec_proc = self.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)
