# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.utils.html import format_html

from .models import ElectoralProcess

import django_tables2 as tables

from django.utils.translation import ugettext as _


class StateColumn(tables.Column):
    """
    Custom Column of states
    """

    def render(self, record):
        icon = ""
        if record.state == ElectoralProcess.State.OPEN:
            icon = "{} <i class='ev-icon evi-open-state'></i>"\
                .format(record.get_state_display())
        elif record.state in [ElectoralProcess.State.CLOSED,
                              ElectoralProcess.State.ARCHIVED]:
            icon = _("Closed") + "<i class='ev-icon evi-lock-state'></i>"
        elif record.state == ElectoralProcess.State.DRAFT:
            icon = "{} <i class='ev-icon evi-edit'></i>"\
                .format(record.get_state_display())
        elif record.state == ElectoralProcess.State.NEW:
            icon = record.get_state_display()
        return format_html(icon)
