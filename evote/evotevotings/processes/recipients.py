# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from evotemessages.decorators import recipients
from evotemessages.recipients import BaseRecipientsGroup
from guardian.shortcuts import get_objects_for_user

from .models import ElectoralProcess, ElectoralMember


@recipients
class ElectoralCommissionRecipientsGroup(BaseRecipientsGroup):
    """ """
    model = ElectoralProcess
    prefix = _("Electoral commission:")
    namespace = "management"

    def search(self, query, group_object_id, **kwargs):
        extra = {}
        if group_object_id:  # is an electoral member
            extra.update({'pk': group_object_id})
            qs = get_objects_for_user(
                kwargs.get('user'), 'manage_electoralprocess',
                self.get_queryset(), use_groups=False,
                with_superuser=False).public()
        else:  # is an election voter
            qs = get_objects_for_user(
                kwargs.get('user'), 'view_electoralprocess',
                self.get_queryset(), use_groups=False,
                with_superuser=False).published()
        return qs.filter(identifier__icontains=query, **extra).order_by()

    def get_user_ids(self, obj):
        if obj:
            return obj.electoral_members.values_list("user_id", flat=True)\
                .order_by()
        return []

    def get_group_object(self, obj):
        return obj

    def check_permission(self, user):
        is_member = getattr(user, "is_electoral_member", False)
        is_voter = getattr(user, "is_election_voter", False)
        return bool(is_member or is_voter)


@recipients
class ElectoralMemberRecipientsGroup(BaseRecipientsGroup):
    """ """
    model = ElectoralMember
    prefix = _("Electoral member:")
    namespace = "management"

    def search(self, query, group_object_id, **kwargs):
        if not group_object_id:  # if no group provided, return empty
            return self.model.objects.none()
        return self.get_queryset()\
            .select_related('user')\
            .filter(Q(designation__icontains=query) |
                    Q(user__email__icontains=query) |
                    Q(user__notice_email__icontains=query),
                    electoral_process_id=group_object_id)\
            .order_by()

    def get_user_ids(self, obj):
        if obj:
            return [obj.user_id]
        return []

    def get_group_object(self, obj):
        return obj.electoral_process

    def check_permission(self, user):
        return getattr(user, "is_electoral_member", False)
