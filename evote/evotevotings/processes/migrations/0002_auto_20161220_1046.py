# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


# Generated by Django 1.10.4 on 2016-12-20 10:46
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('evoteprocesses', '0001_initial'),
        ('evotedocs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='electoralprocess',
            name='created_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='electoral_processes', to=settings.AUTH_USER_MODEL, verbose_name='created by'),
        ),
        migrations.AddField(
            model_name='electoralmember',
            name='electoral_process',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='electoral_members', to='evoteprocesses.ElectoralProcess', verbose_name='electoral process'),
        ),
        migrations.AddField(
            model_name='electoralmember',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='electoral_members', to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
        migrations.AddField(
            model_name='electoraldocument',
            name='documents',
            field=models.ManyToManyField(blank=True, to='evotedocs.Document', verbose_name='documents'),
        ),
        migrations.AddField(
            model_name='electoraldocument',
            name='electoral_process',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='electoral_documents', to='evoteprocesses.ElectoralProcess', verbose_name='electoral process'),
        ),
        migrations.AddField(
            model_name='electoralaction',
            name='action',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='electoral_actions', to='evoteprocesses.Action', verbose_name='action'),
        ),
        migrations.AddField(
            model_name='electoralaction',
            name='electoral_process',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='electoral_actions', to='evoteprocesses.ElectoralProcess', verbose_name='electoral process'),
        ),
    ]
