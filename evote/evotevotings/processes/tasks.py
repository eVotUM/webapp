# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.apps import apps

from django_fsm import can_proceed
from celery import shared_task


logger = logging.getLogger('evote.core')


def get_electoral_process(pk):
    ElectoralProcess = apps.get_model('evoteprocesses.ElectoralProcess')  # noqa
    return ElectoralProcess.objects.get(pk=pk)


@shared_task(ignore_result=True)
def publish_electoral_process(electoral_process_id):
    """ """
    electoral_process = get_electoral_process(electoral_process_id)
    if can_proceed(electoral_process.publish):
        old_state = electoral_process.get_state_display()
        electoral_process.publish()
        electoral_process.save()
        logger.info("Electoral_process=%d: changed status='%s' => status='%s'",
                    electoral_process.pk, old_state,
                    electoral_process.get_state_display())
    else:
        logger.warning("Electoral_process=%d: can't change to status='Open'",
                       electoral_process.pk)
    return electoral_process


@shared_task(ignore_result=True)
def close_electoral_process(electoral_process_id):
    """ """
    electoral_process = get_electoral_process(electoral_process_id)
    if can_proceed(electoral_process.close):
        old_state = electoral_process.get_state_display()
        electoral_process.close()
        electoral_process.save()
        logger.info("Electoral_process=%d: changed status='%s' => status='%s'",
                    electoral_process.pk, old_state,
                    electoral_process.get_state_display())
    else:
        logger.warning("Electoral_process=%d: can't change to status='Closed'",
                       electoral_process.pk)
    return electoral_process


@shared_task(ignore_result=True)
def archive_electoral_process(electoral_process_id):
    """ """
    electoral_process = get_electoral_process(electoral_process_id)
    if can_proceed(electoral_process.archive):
        old_state = electoral_process.get_state_display()
        electoral_process.archive()
        electoral_process.save()
        logger.info("Electoral_process=%d: changed status='%s' => status='%s'",
                    electoral_process.pk, old_state,
                    electoral_process.get_state_display())
    else:
        logger.warning(
            "Electoral_process=%d: can't change to status='Archived'",
            electoral_process.pk)
    return electoral_process
