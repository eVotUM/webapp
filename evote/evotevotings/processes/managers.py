# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.db.models.functions import Now
from django.apps import apps
from django.db import models

from evoteusers.models import User
from guardian.shortcuts import get_objects_for_user


class ElectoralProcessQuerySet(models.QuerySet):

    def can_view(self, user):
        return get_objects_for_user(
            user, 'view_electoralprocess', self.all(), use_groups=False,
            with_superuser=False)

    def open(self, user=None):
        """Return opened Electoral Process
        """
        return self.filter(state=self.model.State.OPEN)

    def published(self):
        """Return published Electoral Process (Publication date is greater
        than actual)
        """
        return self.open().filter(publication_date__lte=Now())

    def closed(self, user=None):
        """Return closed Electoral Process
        """
        states = [self.model.State.CLOSED, self.model.State.ARCHIVED]
        return self.filter(state__in=states)

    def public(self, user=None):
        """
        Return Electoral Process that are public (In state New or Open or
        Closed)
        """
        states = [self.model.State.NEW, self.model.State.OPEN,
                  self.model.State.CLOSED]
        return self.filter(state__in=states)


class ElectoralMemberManager(models.Manager):

    def update_or_create_member(
            self, username, email, name, designation, role, phone,
            electoral_process):
        """
        Create new Electoral Member if user exists, otherwise create one
        """
        user, created = User.objects.update_or_create(
            username=username, defaults={
                'email': email,
                'name': name,
                'primary_phone': phone
            })
        member, created = self.update_or_create(
            user=user, electoral_process=electoral_process, defaults={
                'designation': designation,
                'role': role,
                'phone': phone
            })
        return member, created

    def mark_as_responsible(self, electoral_process, user):
        self.filter(electoral_process=electoral_process).update(
            is_comunication_responsible=False)
        user.is_comunication_responsible = True
        user.save()


class ElectoralActionManager(models.Manager):

    def prepopulate(self, electoral_process_id=None):
        if self.instance:
            electoral_process_id = self.instance.pk
        elif electoral_process_id is None:
            raise ValueError("You need to provide an electoral process id")
        Action = apps.get_model('evoteprocesses.Action')  # noqa
        created_action_ids = self.values_list("action_id", flat=True)
        missing_action_ids = Action.objects\
            .exclude(pk__in=created_action_ids)\
            .values_list("pk", flat=True)
        electoral_actions = []
        for missing_action_id in missing_action_ids:
            electoral_action = self.model(
                action_id=missing_action_id,
                electoral_process_id=electoral_process_id
            )
            # for others actions, default security level is simples
            # check fixtures/actions.json
            if missing_action_id == 7:
                electoral_action.strength = self.model.Strength.SIMPLE
            electoral_actions.append(electoral_action)
        self.bulk_create(electoral_actions)
