# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.contrib import admin

from modeltranslation.admin import TabbedTranslationAdmin
from sorl.thumbnail.admin import AdminImageMixin
from guardian.admin import GuardedModelAdmin

from .models import (Action, ElectoralProcess, ElectoralMember,
                     ElectoralDocument, ElectoralAction)


class ActionAdmin(TabbedTranslationAdmin):
    pass


admin.site.register(Action, ActionAdmin)


class ElectoralMemberInline(admin.TabularInline):
    model = ElectoralMember


class ElectoralDocumentInline(admin.TabularInline):
    model = ElectoralDocument


class ElectoralActionInline(admin.TabularInline):
    model = ElectoralAction


class ElectoralProcessAdmin(GuardedModelAdmin, AdminImageMixin,
                            TabbedTranslationAdmin):
    inlines = [
        ElectoralMemberInline,
        ElectoralDocumentInline,
        ElectoralActionInline
    ]


admin.site.register(ElectoralProcess, ElectoralProcessAdmin)
