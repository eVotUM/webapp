# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import patch, call, Mock

from django.utils import timezone
from django.test import SimpleTestCase

from evotevotings.processes.models import (Action, ElectoralProcess,
                                           ElectoralAction, ElectoralMember,
                                           ElectoralDocument)
from evoteusers.models import User


class ActionTest(SimpleTestCase):
    """ """

    def test_str_equal_to_designation(self):
        instance = Action(designation="designation")
        self.assertEqual(str(instance), instance.designation)


class ElectoralProcessStatesTest(SimpleTestCase):

    def test_is_draft(self):
        instance = ElectoralProcess(state=ElectoralProcess.State.DRAFT)
        self.assertTrue(instance.is_draft())
        instance = ElectoralProcess(state=ElectoralProcess.State.OPEN)
        self.assertFalse(instance.is_draft())

    def test_is_open(self):
        instance = ElectoralProcess(state=ElectoralProcess.State.OPEN)
        self.assertTrue(instance.is_open())
        instance = ElectoralProcess(state=ElectoralProcess.State.NEW)
        self.assertFalse(instance.is_open())

    def test_is_new(self):
        instance = ElectoralProcess(state=ElectoralProcess.State.NEW)
        self.assertTrue(instance.is_new())
        instance = ElectoralProcess(state=ElectoralProcess.State.OPEN)
        self.assertFalse(instance.is_new())

    @patch.object(ElectoralProcess, 'electoral_members')
    def test_can_submit_condition(self, electoral_members):
        """ """
        electoral_members.exists.return_value = True
        process = ElectoralProcess()
        self.assertTrue(process.can_submit())
        electoral_members.exists.return_value = False
        self.assertFalse(process.can_submit())
        self.assertEqual(electoral_members.exists.call_count, 2)

    def test_can_publish_condition(self):
        """ """
        published_process = ElectoralProcess(
            publication_date=timezone.now() - timezone.timedelta(minutes=60))
        unpublished_process = ElectoralProcess(
            publication_date=timezone.now() + timezone.timedelta(minutes=60))
        self.assertTrue(published_process.can_publish())
        self.assertFalse(unpublished_process.can_publish())

    @patch.object(ElectoralProcess, 'elections')
    def test_can_close_condition(self, elections):
        """ """
        elections.closed().count.return_value = 2
        elections.count.return_value = 2
        process = ElectoralProcess()
        self.assertTrue(process.can_close())
        elections.count.return_value = 4
        self.assertFalse(process.can_close())
        self.assertEqual(elections.closed().count.call_count, 2)
        self.assertEqual(elections.count.call_count, 2)

    @patch.object(ElectoralProcess, 'elections')
    def test_can_archive_condition(self, elections):
        """ """
        elections.archived().count.return_value = 2
        elections.count.return_value = 2
        process = ElectoralProcess()
        self.assertTrue(process.can_archive())
        elections.count.return_value = 4
        self.assertFalse(process.can_archive())
        self.assertEqual(elections.archived().count.call_count, 2)
        self.assertEqual(elections.count.call_count, 2)

    @patch.object(ElectoralProcess, 'electoral_members')
    @patch('evotevotings.processes.models.ElectoralMember')
    def test_submit_new_transition(self, member_model, electoral_members):
        """ """
        electoral_members.filter().exists.return_value = True
        electoral_members.filter().first.return_value = "test"
        process = ElectoralProcess(state=ElectoralProcess.State.DRAFT)
        process.submit_new()
        self.assertEqual(process.state, ElectoralProcess.State.NEW)
        electoral_members.filter.assert_called_with(
            role=member_model.Role.CHAIRMAN)  # chairman
        electoral_members.filter().exists.assert_called()
        electoral_members.filter().first.assert_called()
        member_model.objects.mark_as_responsible.assert_called_with(
            process, 'test')

    @patch.object(ElectoralProcess, 'elections')
    def test_publish_transition(self, elections):
        """ """
        election = Mock()
        elections.all.return_value = [election]
        process = ElectoralProcess(state=ElectoralProcess.State.NEW,
                                   publication_date=timezone.now())
        process.publish()
        self.assertEqual(process.state, ElectoralProcess.State.OPEN)
        elections.all.assert_called()
        election.publish.assert_called()
        election.save.assert_called()

    def test_close_transition(self):
        """ """
        process = ElectoralProcess(state=ElectoralProcess.State.OPEN)
        process.close()
        self.assertIsNotNone(process.closed_on)
        self.assertEqual(process.state, ElectoralProcess.State.CLOSED)

    def test_archive_transition(self):
        """ """
        process = ElectoralProcess(state=ElectoralProcess.State.CLOSED)
        process.archive()
        self.assertEqual(process.state, ElectoralProcess.State.ARCHIVED)


class ElectoralProcessTest(SimpleTestCase):
    """ """

    def test_str_equal_to_identifier(self):
        """ """
        instance = ElectoralProcess(identifier="identifier")
        self.assertEqual(str(instance), instance.identifier)

    def test_can_edit_members_with_state_draft(self):
        process = ElectoralProcess(state=ElectoralProcess.State.DRAFT)
        self.assertTrue(process.can_edit_members)

    @patch.object(ElectoralProcess, 'elections')
    def test_can_edit_members_with_state_not_draft(self, elections):
        elections.filter().exists.return_value = False
        process = ElectoralProcess(state=ElectoralProcess.State.OPEN)
        self.assertTrue(process.can_edit_members)
        elections.filter().exists.return_value = True
        self.assertFalse(process.can_edit_members)
        elections.filter.assert_called()
        elections.filter().exists.assert_called()

    @patch.object(ElectoralProcess, 'electoral_members')
    def test_get_mailing_list(self, electoral_members):
        member = Mock()
        electoral_members.all.return_value = [member]
        process = ElectoralProcess()
        mails = process.get_mailing_list()
        self.assertEqual(mails, [[member.user.name, member.user.get_email()]])
        electoral_members.all.assert_called()


class ElectoralProcessSignalsTest(SimpleTestCase):
    """ """

    def test_post_init(self):
        process = ElectoralProcess()
        self.assertTrue(process.is_tracking('publication_date'))

    @patch("evotevotings.processes.models.publish_electoral_process")
    def test_post_save_changes(self, publish_electoral_process):
        process = ElectoralProcess(publication_date=timezone.now())
        process.publication_date = timezone.now() - timezone.timedelta(hours=1)
        ElectoralProcess.post_save_changes(ElectoralProcess, process)
        publish_electoral_process.si.assert_called_with(None)
        publish_electoral_process.si().apply_async.assert_called_with(
            eta=process.publication_date)

    @patch("evotevotings.processes.models.get_anonymous_user")
    @patch("evotevotings.processes.models.assign_perm")
    def test_post_save_anonymous_permissions_new_instance(self, assign_perm,
                                                          get_anonymous_user):
        get_anonymous_user.return_value = Mock()
        process = ElectoralProcess(created_by=User())
        ElectoralProcess.post_save_anonymous_permissions(
            ElectoralProcess, process, created=True)
        assign_perm.assert_has_calls([
            call("change_electoralprocess", process.created_by, process),
            call("view_electoralprocess", get_anonymous_user(), process)
        ])

    @patch("evotevotings.processes.models.assign_perm")
    def test_post_save_anonymous_permissions_new_instance_in_test_mode(
            self, assign_perm):
        process = ElectoralProcess(created_by=User(), is_test=True)
        ElectoralProcess.post_save_anonymous_permissions(
            ElectoralProcess, process, created=True)
        assign_perm.assert_called_with(
            "change_electoralprocess", process.created_by, process)

    @patch("evotevotings.processes.models.assign_perm")
    def test_post_save_anonymous_permissions_existing_instance(
            self, assign_perm):
        process = ElectoralProcess(created_by=User())
        ElectoralProcess.post_save_anonymous_permissions(
            ElectoralProcess, process, created=False)
        assign_perm.assert_not_called()

    @patch("evotevotings.processes.models.get_anonymous_user")
    @patch("evotevotings.processes.models.remove_perm")
    def test_pre_delete_anonymous_permissions(self, remove_perm,
                                              get_anonymous_user):
        get_anonymous_user.return_value = Mock()
        process = ElectoralProcess(created_by=User())
        ElectoralProcess.pre_delete_anonymous_permissions(
            ElectoralProcess, process)
        remove_perm.assert_has_calls([
            call("change_electoralprocess", process.created_by, process),
            call("view_electoralprocess", get_anonymous_user(), process)
        ])

    @patch("evotevotings.processes.models.remove_perm")
    def test_pre_delete_anonymous_permissions_with_instance_in_test_mode(
            self, remove_perm):
        process = ElectoralProcess(created_by=User(), is_test=True)
        ElectoralProcess.pre_delete_anonymous_permissions(
            ElectoralProcess, process)
        remove_perm.assert_called_with(
            "change_electoralprocess", process.created_by, process)


class ElectoralActionTest(SimpleTestCase):
    """ """

    def test_str_equal_to_action_and_strength(self):
        electoral_action = ElectoralAction(
            action=Action(designation="designation"),
            strength=ElectoralAction.Strength.SIMPLE)
        text = "{} ({})".format(
            electoral_action.action, electoral_action.get_strength_display())
        self.assertEqual(str(electoral_action), text)


class ElectoralMemberTest(SimpleTestCase):
    """ """

    def test_str_equal_to_designation(self):
        user = User(name="Dev")
        instance = ElectoralMember(user=user, role=0)
        expected = instance.user.name + \
            " (" + instance.get_role_display() + ")"
        self.assertEqual(str(instance), expected)

    @patch("evotevotings.processes.models.assign_perm")
    def test_post_save_new_instance(self, assign_perm):
        member = ElectoralMember(
            user=User(), electoral_process=ElectoralProcess())
        ElectoralMember.post_save(ElectoralMember, member, created=True)
        assign_perm.assert_called_with(
            "manage_electoralprocess", member.user, member.electoral_process)

    @patch("evotevotings.processes.models.assign_perm")
    def test_post_save_existing_instance(self, assign_perm):
        member = ElectoralMember()
        ElectoralMember.post_save(ElectoralMember, member, created=False)
        assign_perm.assert_not_called()

    @patch("evotevotings.processes.models.remove_perm")
    def test_pre_delete(self, remove_perm):
        member = ElectoralMember(
            user=User(), electoral_process=ElectoralProcess())
        ElectoralMember.pre_delete(ElectoralMember, member)
        remove_perm.assert_called_with(
            "manage_electoralprocess", member.user, member.electoral_process)


class ElectoralDocumentTest(SimpleTestCase):
    """ """

    def test_str_equal_to_designation(self):
        instance = ElectoralDocument(designation="designation")
        self.assertEqual(str(instance), instance.designation)
