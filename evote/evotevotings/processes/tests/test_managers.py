# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import TestCase
from django.utils import timezone
from django.db.models.functions import Now

from evotevotings.processes.models import ElectoralProcess

from evotevotings.processes.tests.utils import ProcessesTestUtils
from evoteusers.tests.utils import UserTestUtils


class ElectoralProcessManagerTest(ProcessesTestUtils, UserTestUtils, TestCase):
    """
    Test Managers of Electoral Process
    """

    def test_open(self):
        self.create_electoral_process(state=ElectoralProcess.State.OPEN)
        self.create_electoral_process(state=ElectoralProcess.State.NEW)
        valid_qs = ElectoralProcess.objects.filter(
            state=ElectoralProcess.State.OPEN)
        expected_qs = ElectoralProcess.objects.open()
        self.assertQuerysetEqual(valid_qs, map(repr, expected_qs))

    def test_published(self):
        self.create_electoral_process(publication_date=timezone.now())
        self.create_electoral_process(publication_date=timezone.now())
        self.create_electoral_process(
            publication_date=timezone.now(), state=ElectoralProcess.State.OPEN)
        valid_qs = ElectoralProcess.objects.filter(
            publication_date__lte=Now(),
            state=ElectoralProcess.State.OPEN)
        expected_qs = ElectoralProcess.objects.published()
        self.assertQuerysetEqual(valid_qs, map(repr, expected_qs))

    def test_closed(self):
        self.create_electoral_process(state=ElectoralProcess.State.OPEN)
        self.create_electoral_process(state=ElectoralProcess.State.CLOSED)
        self.create_electoral_process(state=ElectoralProcess.State.ARCHIVED)
        expected_states = [
            ElectoralProcess.State.CLOSED, ElectoralProcess.State.ARCHIVED]
        valid_qs = ElectoralProcess.objects.filter(state__in=expected_states)
        expected_qs = ElectoralProcess.objects.closed()
        self.assertEqual(expected_qs.count(), 2)
        self.assertQuerysetEqual(valid_qs, map(repr, expected_qs))

    def test_public(self):
        self.create_electoral_process(state=ElectoralProcess.State.OPEN)
        self.create_electoral_process(state=ElectoralProcess.State.NEW)
        self.create_electoral_process(state=ElectoralProcess.State.CLOSED)
        self.create_electoral_process(state=ElectoralProcess.State.ARCHIVED)
        expected_states = [
            ElectoralProcess.State.NEW, ElectoralProcess.State.OPEN,
            ElectoralProcess.State.CLOSED]
        valid_qs = ElectoralProcess.objects.filter(state__in=expected_states)
        expected_qs = ElectoralProcess.objects.public()
        self.assertEqual(expected_qs.count(), 3)
        self.assertQuerysetEqual(valid_qs, map(repr, expected_qs))
