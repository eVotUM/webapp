# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase

from evotevotings.processes.tables import StateColumn
from evotevotings.processes.models import ElectoralProcess


class StateColumnTest(SimpleTestCase):
    """ """

    def setUp(self):
        self.electoral_process = ElectoralProcess()
        self.state_column = StateColumn()

    def test_render_state_valid(self):
        self.electoral_process.state = ElectoralProcess.State.OPEN
        expected = "Open <i class='ev-icon evi-open-state'></i>"
        rendered = self.state_column.render(self.electoral_process)
        self.assertInHTML(expected, rendered)

    def test_render_state_closed(self):
        self.electoral_process.state = ElectoralProcess.State.CLOSED
        expected = "Closed <i class='ev-icon evi-lock-state'></i>"
        rendered = self.state_column.render(self.electoral_process)
        self.assertInHTML(expected, rendered)

    def test_render_state_archived(self):
        self.electoral_process.state = ElectoralProcess.State.ARCHIVED
        expected = "Closed <i class='ev-icon evi-lock-state'></i>"
        rendered = self.state_column.render(self.electoral_process)
        self.assertInHTML(expected, rendered)

    def test_render_state_draft(self):
        self.electoral_process.state = ElectoralProcess.State.DRAFT
        expected = "Draft <i class='ev-icon evi-edit'></i>"
        rendered = self.state_column.render(self.electoral_process)
        self.assertInHTML(expected, rendered)

    def test_render_state_new(self):
        self.electoral_process.state = ElectoralProcess.State.NEW
        expected = "Created"
        rendered = self.state_column.render(self.electoral_process)
        self.assertInHTML(expected, rendered)
