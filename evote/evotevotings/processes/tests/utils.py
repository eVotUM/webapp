# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotevotings.processes.models import (ElectoralMember, ElectoralProcess,
                                           Action, ElectoralAction,
                                           ElectoralDocument)
from autofixture import create_one, create

from evoteusers.tests.utils import UserTestUtils


class ProcessesTestUtils(UserTestUtils):

    def create_action(self, **field_values):
        return create_one(Action, field_values=field_values)

    def create_actions(self, num, **field_values):
        return create(Action, num, field_values=field_values)

    def create_electoral_process(self, **field_values):
        return create_one(ElectoralProcess, generate_fk=True,
                          field_values=field_values)

    def create_electoral_processes(self, num, **field_values):
        return create(ElectoralProcess, num, generate_fk=True,
                      field_values=field_values)

    def create_electoral_action(self, **field_values):
        return create_one(ElectoralAction, generate_fk=True,
                          field_values=field_values)

    def create_electoral_member(self, **field_values):
        username = field_values.pop('username', None)
        password = field_values.pop('password', None)
        user = field_values.pop('user', None)
        if username and not user:
            field_values.update({
                'user': self.create_user(username=username, password=password)
            })
        return create_one(ElectoralMember, generate_fk=True,
                          field_values=field_values)

    def create_electoral_document(self, **field_values):
        return create_one(ElectoralDocument, generate_fk=True,
                          field_values=field_values)

    def create_institutional_member(self, **field_values):
        username = field_values.pop('username', None)
        password = field_values.pop('password', None)
        user = field_values.pop('user', None)
        if username and not user:
            field_values.update({
                'user': self.create_user(
                    username=username, password=password,
                    is_institutional_responsible=True)
            })
        return create_one(ElectoralMember, generate_fk=True,
                          field_values=field_values)
