# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import TestCase

from evotevotings.elections.models import Candidate
from .utils import ElectionsTestUtils


class CandidateManagerTestCase(ElectionsTestUtils, TestCase):
    """ """

    def setUp(self):
        self.election = self.create_election()

    def test_bulk_import(self):
        """ """
        data = [{
            'personnel_number': '123',
            'designation': 'candidate 1'
        }, {
            'personnel_number': '124',
            'designation': 'candidate 2'
        }]
        imported = Candidate.objects.bulk_import(self.election, data)
        found = Candidate.objects.filter(write_in=True, election=self.election)
        self.assertQuerysetEqual(found, map(repr, imported))

    def test_bulk_import_with_existing_candidate_non_write_in(self):
        """ """
        self.create_candidate(election=self.election)
        data = [{
            'personnel_number': '123',
            'designation': 'candidate 1'
        }, {
            'personnel_number': '124',
            'designation': 'candidate 2'
        }]
        imported = Candidate.objects.bulk_import(self.election, data)
        found = Candidate.objects.filter(write_in=True, election=self.election)
        self.assertQuerysetEqual(found, map(repr, imported))

    def test_bulk_import_update_existing_write_in_candidates(self):
        self.create_candidate(
            election=self.election, write_in=True, personnel_number='123',
            designation="Existing candidate")
        data = [{
            'personnel_number': '123',
            'designation': 'Updated candidate'
        }, {
            'personnel_number': '124',
            'designation': 'candidate'
        }]
        candidate = Candidate.objects.get(personnel_number="123")
        self.assertEqual(candidate.designation, "Existing candidate")
        Candidate.objects.bulk_import(self.election, data)
        candidate.refresh_from_db()
        self.assertEqual(candidate.designation, "Updated candidate")

    def test_representation_values_as_writein(self):
        obj = self.create_candidate(designation="Test", write_in=True)
        self.create_candidate(designation="Old candidate", write_in=False)
        expected = {str(obj.pk): obj.__str__()}
        self.assertEqual(expected,
                         Candidate.objects.representation_values(True))

    def test_representation_values_not_writein(self):
        obj = self.create_candidate(designation="Test", write_in=False)
        self.create_candidate(designation="Old candidate", write_in=True)
        expected = {str(obj.pk): obj.__str__()}
        self.assertEqual(expected,
                         Candidate.objects.representation_values(False))
