# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from mock import patch

from django.db.models.signals import post_save
from django.utils import timezone
from django.test import TestCase
from django.conf import settings

from evotevotings.elections.models import Election
from evotevotings.elections.tasks import (send_elections_notifications,
                                          publish_provisional_roll,
                                          publish_definitive_roll,
                                          open_election_voting,
                                          close_election_voting,
                                          close_election_counting,
                                          begin_minute_approval,
                                          close_election, archive_election)
from celery.exceptions import Retry

from .utils import ElectionsTestUtils


class TasksTest(ElectionsTestUtils, TestCase):

    def setUp(self):
        # to avoid the tasks publish_provisional_roll and
        # publish_definitive_roll being called
        post_save.disconnect(Election.post_save_changes, Election)

    def test_publish_provisional_roll_task(self):
        """ """
        election = self.create_election(
            state=Election.State.OPEN, roll_begin_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.OPEN)
        result = publish_provisional_roll.si(election.pk)\
            .apply_async(eta=election.roll_begin_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state,
                         Election.State.PROVISIONAL_ELECTORAL_ROLL)

    def test_publish_provisional_roll_task_cant_proceed(self):
        """ """
        election = self.create_election(
            state=Election.State.NEW, roll_begin_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = publish_provisional_roll.si(election.pk)\
            .apply_async(eta=election.roll_begin_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    @patch('evotevotings.elections.models.ASrouter')
    def test_publish_definitive_roll_task(self, mock_router):
        """ """
        mock_router.return_value.initialize_election.return_value = True
        election = self.create_election(
            state=Election.State.PROVISIONAL_ELECTORAL_ROLL,
            roll_end_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state,
                         Election.State.PROVISIONAL_ELECTORAL_ROLL)
        result = publish_definitive_roll.si(election.pk)\
            .apply_async(eta=election.roll_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state,
                         Election.State.DEFINITIVE_ELECTORAL_ROLL)

    def test_publish_definitive_roll_task_cant_proceed(self):
        """ """
        election = self.create_election(
            state=Election.State.NEW, roll_end_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = publish_definitive_roll.si(election.pk)\
            .apply_async(eta=election.roll_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    def test_open_election_voting_task(self):
        """ """
        election = self.create_election(
            state=Election.State.DEFINITIVE_ELECTORAL_ROLL,
            voting_begin_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state,
                         Election.State.DEFINITIVE_ELECTORAL_ROLL)
        result = open_election_voting.si(election.pk)\
            .apply_async(eta=election.voting_begin_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.IN_TO_VOTE)

    def test_open_election_voting_task_cant_proceed(self):
        """ """
        election = self.create_election(
            state=Election.State.NEW, voting_begin_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = open_election_voting.si(election.pk)\
            .apply_async(eta=election.voting_begin_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    def test_close_election_voting_task(self):
        """ """
        election = self.create_election(
            state=Election.State.IN_TO_VOTE, voting_end_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.IN_TO_VOTE)
        result = close_election_voting.si(election.pk)\
            .apply_async(eta=election.voting_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.IN_COUNTING)

    def test_close_election_voting_task_cant_proceed(self):
        """ """
        election = self.create_election(
            state=Election.State.NEW, voting_end_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = close_election_voting.si(election.pk)\
            .apply_async(eta=election.voting_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    @patch("evotevotings.elections.tasks.CSrouter")
    def test_close_election_counting_task_with_finished_status(self, mock_fn):
        """ """
        mock_fn().check_results_status.return_value = 5
        election = self.create_election(state=Election.State.IN_COUNTING)
        result = close_election_counting.si(election.pk).delay()
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.VOTES_SCRUTINIZED)

    @patch("evotevotings.elections.tasks.CSrouter")
    def test_close_election_counting_task_with_finished_status_cant_proceed(
            self, mock_fn):
        """ """
        mock_fn().check_results_status.return_value = 5
        election = self.create_election(state=Election.State.NEW)
        result = close_election_counting.si(election.pk).delay()
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    @patch.object(close_election_counting, "retry")
    @patch("evotevotings.elections.tasks.CSrouter")
    def test_close_election_counting_task_with_unfinished_status(
            self, mock_counter, mock_retry):
        """ """
        mock_counter().check_results_status.return_value = 1
        mock_retry.side_effect = Retry
        election = self.create_election(state=Election.State.IN_COUNTING)
        with self.assertRaises(Retry):
            close_election_counting.si(election.pk).delay()
        mock_retry.assert_called_with(countdown=10, max_retries=None)

    @patch("evotevotings.elections.tasks.CSrouter")
    def test_close_election_counting_task_with_error_status(
            self, mock_counter):
        """ """
        mock_counter().check_results_status.return_value = 0
        election = self.create_election(state=Election.State.IN_COUNTING)
        result = close_election_counting.si(election.pk).delay()
        self.assertIsNone(result.result)

    @patch("evotevotings.elections.tasks.CSrouter")
    def test_close_election_counting_task_with_none_status(
            self, mock_counter):
        """ """
        mock_counter().check_results_status.return_value = None
        election = self.create_election(state=Election.State.IN_COUNTING)
        result = close_election_counting.si(election.pk).delay()
        self.assertIsNone(result.result)

    def test_begin_minute_approval_task(self):
        """ """
        election = self.create_election(
            state=Election.State.VOTES_SCRUTINIZED,
            minute_begin_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.VOTES_SCRUTINIZED)
        result = begin_minute_approval.si(election.pk)\
            .apply_async(eta=election.minute_begin_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state,
                         Election.State.MINUTE_IN_APPROVAL)

    def test_begin_minute_approval_task_cant_proceed(self):
        """ """
        election = self.create_election(
            state=Election.State.NEW, minute_begin_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = begin_minute_approval.si(election.pk)\
            .apply_async(eta=election.minute_begin_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    def test_close_election_task(self):
        """ """
        election = self.create_election(
            state=Election.State.MINUTE_IN_APPROVAL,
            minute_end_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.MINUTE_IN_APPROVAL)
        result = close_election.si(election.pk)\
            .apply_async(eta=election.minute_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.CLOSED)

    def test_close_election_task_cant_proceed(self):
        """ """
        election = self.create_election(
            state=Election.State.NEW, minute_end_date=timezone.now())
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = close_election.si(election.pk)\
            .apply_async(eta=election.minute_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    @patch("evotevotings.elections.models.ASrouter")
    @patch("evotevotings.elections.models.CSrouter")
    def test_archive_election_task(self, csrouter, asrouter):
        """ """
        days = settings.EVOTE_ELECTIONS.get('archive_after')
        election = self.create_election(
            state=Election.State.CLOSED,
            minute_end_date=timezone.now() - timezone.timedelta(days=days))
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.CLOSED)
        result = archive_election.si(election.pk)\
            .apply_async(eta=election.minute_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.ARCHIVED)

    def test_archive_election_task_cant_proceed(self):
        """ """
        days = settings.EVOTE_ELECTIONS.get('archive_after')
        election = self.create_election(
            state=Election.State.NEW,
            minute_end_date=timezone.now() - timezone.timedelta(days=days))
        # to ensure that there was no changes on creation
        self.assertEqual(election.state, Election.State.NEW)
        result = archive_election.si(election.pk)\
            .apply_async(eta=election.minute_end_date)
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result.state, Election.State.NEW)

    def test_send_elections_notifications_task(self):
        """
        Test the number of emails send
        It assumes that is sends email 1 day  and 3 before
        If Settings are changed, this test needs to change too
        """
        date = timezone.now()
        self.election = self.create_election(
            proposition_begin_date=date + timezone.timedelta(days=1),
            proposition_end_date=date + timezone.timedelta(days=2),
            minute_end_date=date + timezone.timedelta(days=3),
            voting_begin_date=date + timezone.timedelta(days=4),
            roll_begin_date=date + timezone.timedelta(days=5),
            roll_end_date=date + timezone.timedelta(days=6),
            state=Election.State.OPEN
        )
        result = send_elections_notifications.delay()
        self.assertEqual(result.successful(), True)
        self.assertEqual(result.result, 2)
