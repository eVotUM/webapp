# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from StringIO import StringIO
from csvvalidator import UNIQUE_CHECK_FAILED

from django.test import TestCase

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.elections.importers import CandidateImporter


class CandidateImporterTestCase(ElectionsTestUtils, TestCase):
    """ """

    def setUp(self):
        """ """
        self.election = self.create_election()
        self.candidate_importer = CandidateImporter(self.election)
        self.header = "personnel_number;designation\n"

    def _get_csv(self, lines=None):
        """ """
        csv = self.header
        if lines:
            csv += "\n".join(lines)
        return StringIO(csv)

    def test_validate_with_valid_header_fields(self):
        """ """
        csv = self._get_csv()
        result = self.candidate_importer.validate(csv)
        self.assertEqual(result, [])

    def test_validate_with_invalid_header_fields(self):
        """ """
        invalid_csv_header = StringIO("personnel_number;designations")
        result = self.candidate_importer.validate(invalid_csv_header)
        self.assertEqual(result[0]['unexpected'], set(["designations"]))
        self.assertEqual(result[0]['missing'], set(["designation"]))
        self.assertEqual(result[0]['message'], "Invalid header")

    def test_validate_with_invalid_record_length(self):
        """ """
        invalid_csv = self._get_csv([
            "123;test candidate;Test",
        ])
        result = self.candidate_importer.validate(invalid_csv)
        self.assertEqual(result[0]['message'], "Invalid record length")

    def test_validate_unique_personnel_number(self):
        """ """
        csv = self._get_csv([
            "123;test candidate",
            "123;test candidate"
        ])
        result = self.candidate_importer.validate(csv)
        self.assertEqual(result[0]['code'], UNIQUE_CHECK_FAILED)
        self.assertEqual(result[0]['key'], "personnel_number")
        self.assertEqual(
            str(result[0]['message']),
            CandidateImporter.error_messages['unique_personnel_number'])

    def test_run(self):
        csv = self._get_csv([
            "1234;test candidate",
            "1235;test candidate 2",
        ])
        imported = self.candidate_importer.run(csv)
        self.assertIsNotNone(imported)
        self.assertEqual(len(imported), 2)
