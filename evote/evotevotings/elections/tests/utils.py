# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotevotings.elections.models import (Election, Category, Candidate)
from evotevotings.admin.models import SecretEntry
from evotevotings.voters.models import ElectionVoter
from autofixture import create_one, create


class ElectionsTestUtils(object):

    def create_election(self, **field_values):
        return create_one(Election, generate_fk=True,
                          field_values=field_values)

    def create_elections(self, num, **field_values):
        return create(Election, num, generate_fk=True,
                      field_values=field_values)

    def create_category(self, **field_values):
        return create_one(Category, generate_fk=True,
                          field_values=field_values)

    def create_voter(self, **field_values):
        return create_one(ElectionVoter, generate_fk=True,
                          field_values=field_values)

    def create_candidate(self, **field_values):
        return create_one(Candidate, generate_fk=True,
                          field_values=field_values)

    def create_candidates(self, num, **field_values):
        return create(Candidate, num, generate_fk=True,
                      field_values=field_values)

    def create_certificate(self, **field_values):
        return create_one(SecretEntry, generate_fk=True,
                          field_values=field_values)
