# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from unittest import skip
from mock import patch

from django.contrib.auth import get_user_model
from django.utils import timezone
from django.test import SimpleTestCase
from django.conf import settings

from evotevotings.elections.models import (Election, Category, Candidate,
                                           ElectionCountQuery)
from evotevotings.elections.utils import calc_archive_date


class ElectionCalendarTest(SimpleTestCase):

    def test_is_within_complaint_period_with_valid_period(self):
        begin = timezone.now() - timezone.timedelta(minutes=10)
        end = timezone.now() + timezone.timedelta(minutes=10)
        election = Election(complaint_begin_date=begin, complaint_end_date=end)
        self.assertTrue(election.is_within_complaint_period)

    def test_is_within_complaint_period_with_future_period(self):
        begin = timezone.now() + timezone.timedelta(minutes=10)
        end = timezone.now() + timezone.timedelta(minutes=15)
        election = Election(complaint_begin_date=begin, complaint_end_date=end)
        self.assertFalse(election.is_within_complaint_period)

    def test_is_within_complaint_period_with_past_period(self):
        begin = timezone.now() - timezone.timedelta(minutes=10)
        end = timezone.now() - timezone.timedelta(minutes=15)
        election = Election(complaint_begin_date=begin, complaint_end_date=end)
        self.assertFalse(election.is_within_complaint_period)

    def test_is_complaint_period_ended_with_before_period(self):
        end = timezone.now() + timezone.timedelta(minutes=15)
        election = Election(complaint_end_date=end)
        self.assertFalse(election.is_complaint_period_ended)

    def test_is_complaint_period_ended_with_after_period(self):
        end = timezone.now() - timezone.timedelta(minutes=15)
        election = Election(complaint_end_date=end)
        self.assertTrue(election.is_complaint_period_ended)

    def test_is_voting_ended_with_before_period(self):
        end = timezone.now() + timezone.timedelta(minutes=15)
        election = Election(voting_end_date=end)
        self.assertFalse(election.is_voting_ended)

    def test_is_voting_ended_with_after_period(self):
        end = timezone.now() - timezone.timedelta(minutes=15)
        election = Election(voting_end_date=end)
        self.assertTrue(election.is_voting_ended)


class ElectionBulletinTest(SimpleTestCase):
    """ """

    def test_is_list_mode(self):
        election = Election(presentation_mode=Election.PresentationMode.LIST)
        self.assertTrue(election.is_list_mode())
        election.presentation_mode = Election.PresentationMode.DROPDOWN
        self.assertFalse(election.is_list_mode())

    def test_is_dropdown_mode(self):
        election = Election(
            presentation_mode=Election.PresentationMode.DROPDOWN)
        self.assertTrue(election.is_dropdown_mode())
        election.presentation_mode = Election.PresentationMode.LIST
        self.assertFalse(election.is_dropdown_mode())


class ElectionManageTest(SimpleTestCase):
    """ """

    def test_can_manage_open(self):
        election = Election(state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertTrue(election.can_manage_open())
        election.state = Election.State.IN_TO_VOTE
        self.assertFalse(election.can_manage_open())

    def test_can_manage_close(self):
        election = Election(state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertTrue(election.can_manage_close())
        election.state = Election.State.IN_TO_VOTE
        self.assertTrue(election.can_manage_close())
        election.state = Election.State.CLOSED
        self.assertFalse(election.can_manage_close())

    def test_can_manage_counting(self):
        election = Election(state=Election.State.IN_COUNTING)
        self.assertTrue(election.can_manage_counting())
        election.state = Election.State.CLOSED
        self.assertTrue(election.can_manage_counting())
        election.state = Election.State.OPEN
        self.assertFalse(election.can_manage_counting())

    @patch.object(Election, 'electoral_process')
    def test_can_change_with_action_configured(self, mock_fn):
        mock_fn.actions_configured = True
        election = Election(state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertTrue(election.can_change())
        election.state = Election.State.CLOSED
        self.assertFalse(election.can_change())

    @patch.object(Election, 'electoral_process')
    def test_can_change_with_action_unconfigured(self, mock_fn):
        mock_fn.actions_configured = False
        election = Election(state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertFalse(election.can_change())

    @patch.object(Election, 'can_change', return_value=True)
    def test_can_change_configs(self, mock_fn):
        election = Election()
        self.assertTrue(election.can_change_configs())

    @patch.object(Election, 'electoral_process')
    def test_can_change_calendar(self, mock_fn):
        mock_fn.actions_configured = True
        election = Election()
        self.assertTrue(election.can_change_calendar())

    @patch.object(Election, 'electoral_process')
    def test_can_change_calendar_with_actions_not_configured(self, mock_fn):
        mock_fn.actions_configured = False
        election = Election()
        self.assertFalse(election.can_change_calendar())

    @patch.object(Election, 'can_change', return_value=True)
    def test_can_change_categories(self, mock_fn):
        election = Election(weight_votes=True)
        self.assertTrue(election.can_change_categories())
        election.weight_votes = False
        self.assertFalse(election.can_change_categories())

    @patch.object(Election, 'can_change', return_value=True)
    def test_cant_change_categories_in_definitive_eletoral_roll(self, mock_fn):
        election = Election(weight_votes=True,
                            state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertFalse(election.can_change_categories())

    @patch.object(Election, 'can_change', return_value=True)
    def test_can_change_rolls(self, mock_fn):
        election = Election()
        self.assertTrue(election.can_change_rolls())

    @patch.object(Election, 'can_change', return_value=True)
    def test_cant_change_rolls_in_definitive_eletoral_roll(self, mock_fn):
        election = Election(weight_votes=True,
                            state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertFalse(election.can_change_rolls())

    @patch.object(Election, 'can_change', return_value=True)
    def test_can_change_candidates(self, mock_fn):
        election = Election(proposition_begin_date=timezone.now())
        self.assertTrue(election.can_change_candidates())
        election.proposition_begin_date = timezone.now() + \
            timezone.timedelta(days=2)
        self.assertFalse(election.can_change_candidates())

    @patch.object(Election, 'candidates')
    @patch.object(Election, 'can_change', return_value=True)
    def test_can_change_bulletin_with_configs_filled(self, mock_fn,
                                                     mock_candidates):
        mock_candidates.exists.return_value = True
        election = Election(configs_filled=True)
        self.assertTrue(election.can_change_bulletin())
        mock_candidates.exists.return_value = False
        self.assertFalse(election.can_change_bulletin())

    @patch.object(Election, 'candidates')
    @patch.object(Election, 'can_change', return_value=True)
    def test_can_change_bulletin_with_configs_not_filled(self, mock_fn,
                                                         mock_candidates):
        mock_candidates.exists.return_value = True
        election = Election(configs_filled=True)
        self.assertTrue(election.can_change_bulletin())
        election.configs_filled = False
        self.assertFalse(election.can_change_bulletin())


class ElectionStatesTest(SimpleTestCase):
    """ """
    # transitions conditions test

    def test_is_closed(self):
        election = Election(state=Election.State.CLOSED)
        self.assertTrue(election.is_closed)
        election.state = Election.State.OPEN
        self.assertFalse(election.is_closed)

    def test_is_in_definitive_eletoral_roll(self):
        election = Election(state=Election.State.DEFINITIVE_ELECTORAL_ROLL)
        self.assertTrue(election.is_in_definitive_eletoral_roll)
        election.state = Election.State.OPEN
        self.assertFalse(election.is_in_definitive_eletoral_roll)

    def test_is_definitive_eletoral_roll(self):
        election = Election(state=Election.State.IN_TO_VOTE)
        self.assertTrue(election.is_definitive_eletoral_roll)
        election.state = Election.State.PROVISIONAL_ELECTORAL_ROLL
        self.assertFalse(election.is_definitive_eletoral_roll)

    def test_is_in_to_vote(self):
        election = Election(state=Election.State.IN_TO_VOTE)
        self.assertTrue(election.is_in_to_vote)
        election.state = Election.State.OPEN
        self.assertFalse(election.is_in_to_vote)

    @patch.object(Election, 'electoral_process')
    def test_can_publish(self, mock):
        mock.can_publish.return_value = True
        election = Election()
        self.assertTrue(election.can_publish())

    def test_can_publish_provisional_roll_with_date_surpassed(self):
        election = Election(roll_begin_date=timezone.now())
        self.assertTrue(election.can_publish_provisional_roll())

    def test_can_publish_provisional_roll_with_date_on_future(self):
        date = timezone.now() + timezone.timedelta(hours=1)
        election = Election(roll_begin_date=date)
        self.assertFalse(election.can_publish_provisional_roll())

    def test_can_publish_definitive_roll_with_date_surpassed(self):
        election = Election(roll_end_date=timezone.now())
        self.assertTrue(election.can_publish_definitive_roll())

    def test_can_publish_definitive_roll_with_date_on_future(self):
        date = timezone.now() + timezone.timedelta(hours=1)
        election = Election(roll_end_date=date)
        self.assertFalse(election.can_publish_definitive_roll())

    def test_can_begin_voting_with_date_surpassed(self):
        election = Election(voting_begin_date=timezone.now())
        self.assertTrue(election.can_begin_voting())

    def test_can_begin_voting_with_date_on_future(self):
        date = timezone.now() + timezone.timedelta(hours=1)
        election = Election(voting_begin_date=date)
        self.assertFalse(election.can_begin_voting())

    def test_can_end_voting_with_date_surpassed(self):
        election = Election(voting_end_date=timezone.now())
        self.assertTrue(election.can_end_voting())

    def test_can_end_voting_with_date_on_future(self):
        date = timezone.now() + timezone.timedelta(hours=1)
        election = Election(voting_end_date=date)
        self.assertFalse(election.can_end_voting())

    def test_can_close_with_date_surpassed(self):
        election = Election(minute_end_date=timezone.now())
        self.assertTrue(election.can_close())

    def test_can_close_with_date_on_future(self):
        date = timezone.now() + timezone.timedelta(hours=1)
        election = Election(minute_end_date=date)
        self.assertFalse(election.can_close())

    def test_can_archive_with_date_surpassed(self):
        days = settings.EVOTE_ELECTIONS.get('archive_after')
        date = timezone.now() - timezone.timedelta(days=days)
        election = Election(minute_end_date=date)
        self.assertTrue(election.can_archive())

    def test_can_archive_with_date_on_future(self):
        election = Election(minute_end_date=timezone.now())
        self.assertFalse(election.can_archive())

    # transitions test

    @patch.object(Election, 'electoral_process')
    def test_publish_transition(self, mock_fn):
        mock_fn.can_publish.return_value = True
        election = Election(state=Election.State.NEW)
        election.publish()
        self.assertEqual(election.state, Election.State.OPEN)

    def test_publish_provisional_roll_transition(self):
        election = Election(state=Election.State.OPEN,
                            roll_begin_date=timezone.now())
        election.publish_provisional_roll()
        self.assertEqual(election.state,
                         Election.State.PROVISIONAL_ELECTORAL_ROLL)

    @patch('evotevotings.elections.models.ASrouter')
    def test_publish_definitive_roll_transition(self, asrouter):
        asrouter().initialize_election.return_value = True
        election = Election(state=Election.State.PROVISIONAL_ELECTORAL_ROLL,
                            roll_end_date=timezone.now())
        election.publish_definitive_roll()
        self.assertEqual(election.state,
                         Election.State.DEFINITIVE_ELECTORAL_ROLL)

    def test_voting_transition(self):
        election = Election(state=Election.State.DEFINITIVE_ELECTORAL_ROLL,
                            voting_begin_date=timezone.now())
        election.voting()
        self.assertEqual(election.state, Election.State.IN_TO_VOTE)

    def test_counting_transition(self):
        election = Election(state=Election.State.IN_TO_VOTE,
                            voting_end_date=timezone.now())
        election.counting()
        self.assertEqual(election.state, Election.State.IN_COUNTING)

    def test_scrutinized_transition(self):
        election = Election(state=Election.State.IN_COUNTING)
        election.scrutinized()
        self.assertEqual(election.state, Election.State.VOTES_SCRUTINIZED)

    def test_begin_approval_transition(self):
        election = Election(state=Election.State.VOTES_SCRUTINIZED,
                            minute_begin_date=timezone.now())
        election.begin_approval()
        self.assertEqual(election.state, Election.State.MINUTE_IN_APPROVAL)

    @patch('evotevotings.elections.models.archive_election')
    def test_close_transition(self, archive_election):
        election = Election(state=Election.State.MINUTE_IN_APPROVAL,
                            minute_end_date=timezone.now())
        election.close()
        self.assertEqual(election.state, Election.State.CLOSED)
        archive_election.si.assert_called_with(None)  # None: id election
        archive_election.si().apply_async.assert_called_with(
            eta=calc_archive_date(election.minute_end_date))

    @patch('evotevotings.elections.models.ASrouter')
    @patch('evotevotings.elections.models.CSrouter')
    def test_archive_transition(self, csrouter, asrouter):
        days = settings.EVOTE_ELECTIONS.get('archive_after')
        date = timezone.now() - timezone.timedelta(days=days)
        election = Election(state=Election.State.CLOSED, minute_end_date=date)
        election.archive()
        self.assertEqual(election.state, Election.State.ARCHIVED)
        asrouter().close_election.assert_called_with(None)  # None: id election
        csrouter().clean_results.assert_called_with(None)  # None: id election


class ElectionTest(SimpleTestCase):
    """ """
    def test_str_equal_to_identifier(self):
        instance = Election(identifier="identifier")
        self.assertEqual(str(instance), instance.identifier)

    @skip("TO IMPLEMENT")
    def test_get_absolute_url(self):
        """ """

    @skip("TO IMPLEMENT")
    def test_audit_group(self):
        """ """

    @skip("TO IMPLEMENT")
    def test_generate_pdf(self):
        """ """

    @skip("TO IMPLEMENT")
    def test_generate_voters_pdf(self):
        """ """

    @patch.object(Election, 'electoral_rolls')
    def test_get_last_published_roll(self, electoral_rolls):
        election = Election(pk=1)
        election.get_last_published_roll()
        electoral_rolls.published.assert_called_with(1)


class ElectionSignalsTest(SimpleTestCase):
    """ """

    def test_post_init(self):
        election = Election()
        self.assertTrue(election.is_tracking('roll_begin_date'))
        self.assertTrue(election.is_tracking('roll_end_date'))
        self.assertTrue(election.is_tracking('minute_begin_date'))
        self.assertTrue(election.is_tracking('minute_end_date'))

    @patch('evotevotings.elections.models.publish_provisional_roll')
    def test_post_save_changes_for_roll_begin_date(self, mock_fn):
        election = Election(roll_begin_date=timezone.now())
        election.roll_begin_date = timezone.now() - timezone.timedelta(hours=1)
        Election.post_save_changes(Election, election)
        mock_fn.si.assert_called_with(None)  # None: id election
        mock_fn.si.return_value.apply_async.assert_called_with(
            eta=election.roll_begin_date)

    @patch('evotevotings.elections.models.publish_definitive_roll')
    def test_post_save_changes_for_roll_end_date(self, mock_fn):
        election = Election(roll_end_date=timezone.now())
        election.roll_end_date = timezone.now() - timezone.timedelta(hours=1)
        Election.post_save_changes(Election, election)
        mock_fn.si.assert_called_with(None)  # None: id election
        mock_fn.si.return_value.apply_async.assert_called_with(
            eta=election.roll_end_date)

    @patch('evotevotings.elections.models.begin_minute_approval')
    def test_post_save_changes_for_minute_begin_date(self, mock_fn):
        election = Election(minute_begin_date=timezone.now())
        election.minute_begin_date = \
            timezone.now() - timezone.timedelta(hours=1)
        Election.post_save_changes(Election, election)
        mock_fn.si.assert_called_with(None)  # None: id election
        mock_fn.si.return_value.apply_async.assert_called_with(
            eta=election.minute_begin_date)

    @patch('evotevotings.elections.models.close_election')
    def test_post_save_changes_for_minute_end_date(self, mock_fn):
        election = Election(minute_end_date=timezone.now())
        election.minute_end_date = timezone.now() - timezone.timedelta(hours=1)
        Election.post_save_changes(Election, election)
        mock_fn.si.assert_called_with(None)  # None: id election
        mock_fn.si.return_value.apply_async.assert_called_with(
            eta=election.minute_end_date)

    @patch.object(Election, 'categories')
    def test_post_save_create_category(self, mock_fn):
        election = Election()
        Election.post_save_create_category(Election, election, created=True)
        mock_fn.create.assert_called()
        mock_fn.create.assert_called_with(
            default=True, designation_en="One", designation_pt="Um")

    @patch("evotevotings.elections.models.initialize_blind_signatures",
           return_value=("test 1", "test 2"))
    def test_pre_save(self, mock_fn):
        election = Election()
        election._state.adding = True
        Election.pre_save(Election, election)
        mock_fn.assert_called()
        self.assertEqual(election.blindsig_init_components, "test 1")
        self.assertEqual(election.blindsig_pr_dash_components, "test 2")


class CategoryTest(SimpleTestCase):
    """ """

    def test_str_equal_to_designation(self):
        instance = Category(weight=2)
        self.assertEqual(str(instance), str(instance.weight))


class CandidateTest(SimpleTestCase):
    """ """

    def test_str_equal_to_designation(self):
        instance = Candidate(designation="designation")
        self.assertEqual(str(instance), instance.designation)


class ElectionCountQueryTest(SimpleTestCase):
    """ """

    def test_str_equal_to_expected_value(self):
        user = get_user_model()(name="test")
        election = Election(identifier="identifier")
        instance = ElectionCountQuery(
            election=election, user=user, total_count=10)
        expected = "{} ({}) - {}".format(
            election.identifier, user, instance.total_count)
        self.assertEqual(str(instance), expected)

    @skip("TO IMPLEMENT")
    def test_audit_group(self):
        """ """
