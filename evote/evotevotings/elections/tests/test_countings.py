# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import SimpleTestCase

from evotevotings.elections.countings import (Vote, ListVote, VoteCounting,
                                              CountingMethods)


class VoteTest(SimpleTestCase):

    def test_unit_weight(self):
        """ """
        result = Vote(1, 5)
        self.assertEqual(result.total, 5)
        self.assertEqual(result.total_weight, 5)

    def test_weight_not_unit(self):
        result = Vote(2, 5)
        self.assertEqual(result.total, 5)
        self.assertEqual(result.total_weight, 10)


class ListVoteTest(SimpleTestCase):

    def setUp(self):
        """ """
        self.weights = {'cat1': 1, 'cat2': 2}
        self.data = {'cat1': 20, 'cat2': 30}

    def test_init(self):
        """ """
        result = ListVote(self.weights, self.data)
        self.assertEqual(result.total, 50)
        self.assertEqual(result.total_weight, 80)

    def test_init_with_unexistent_weight(self):
        """ """
        self.data['cat'] = 20  # cat is not defined on weight object
        with self.assertRaisesMessage(ValueError, "Weight 'cat' don't exists"):
            ListVote(self.weights, self.data)


class VoteCountingTest(SimpleTestCase):

    def setUp(self):
        """ """
        self.weights = {'cat1': 1, 'cat2': 2}
        self.data = {
            'candidates': {
                'cand1': {'cat1': 20, 'cat2': 30},
                'cand2': {'cat1': 30, 'cat2': 20},
            },
            'blank_votes': {'cat1': 20, 'cat2': 30},
            'total_votes': 150,
            'invalid_votes': 10
        }
        self.candidates_name = {
            'cand1': 'Candidate 1',
            'cand2': 'Candidate 2'
        }
        self.counting = VoteCounting(self.weights, self.candidates_name,
                                     self.data)

    def test_init(self):
        """ """
        self.assertEqual(self.counting.total, 150)
        self.assertEqual(self.counting.total_weight, 230)
        self.assertEqual(self.counting.blank_votes.total, 50)
        self.assertEqual(self.counting.blank_votes.total_weight, 80)
        self.assertEqual(self.counting.total_votes, self.data['total_votes'])
        self.assertEqual(self.counting.invalid_votes,
                         self.data['invalid_votes'])

    def test_perc(self):
        perc = self.counting.perc(100, 1000)
        self.assertEqual(perc, 10)

    def test_perc_invalid_total(self):
        perc = self.counting.perc(100, 0)
        self.assertEqual(perc, 0)

    def test_filter_candidate(self):
        self.counting.count()
        candidate = self.counting.filter('cand1')
        self.assertIsNotNone(candidate)

    def test_filter_unexistent_candidate(self):
        self.counting.count()
        candidate = self.counting.filter('cand10')
        self.assertIsNone(candidate)

    def test_data(self):
        """ """
        self.counting.count(seats=100)
        self.assertEqual(self.counting.data,
                         [r.__dict__ for r in self.counting])

    def test_proportional_count(self):
        self.counting.count()
        self.assertEqual(len(self.counting), 3)
        self.assertEqual(self.counting[0].cid, 'cand1')
        self.assertEqual(self.counting[0].name, 'Candidate 1')
        self.assertEqual(self.counting[0].total, 80)
        self.assertEqual(self.counting[0].perc, 34.78260869565217)
        self.assertEqual(self.counting[0].seats, 0)

    def test_proportional_count_with_candidates_without_votes(self):
        """ """
        self.candidates_name.update({'cand3': 'Candidate 3'})
        counting = VoteCounting(self.weights, self.candidates_name, self.data)
        counting.count()
        self.assertEqual(len(counting), 4)
        self.assertEqual(counting[2].cid, 'cand3')
        self.assertEqual(counting[2].name, 'Candidate 3')
        self.assertEqual(counting[2].total, 0)
        self.assertEqual(counting[2].perc, 0)
        self.assertEqual(counting[2].seats, 0)

    def test_count_reset_the_list(self):
        self.counting.count()
        self.assertEqual(len(self.counting), 3)
        self.counting.count()
        self.assertEqual(len(self.counting), 3)

    def test_dhondt_count(self):
        """ """
        self.counting.count(seats=100, method=CountingMethods.DHONDT)
        self.assertEqual(self.counting.filter('cand1').seats, 53)
        self.assertEqual(self.counting.filter('cand2').seats, 47)

    def test_dhondt_count_with_empty_candidates(self):
        """ """
        self.data['candidates'] = {}
        counting = VoteCounting(self.weights, self.candidates_name, self.data)
        counting.count(seats=100, method=CountingMethods.DHONDT)
        self.assertEqual(len(counting), 3)
