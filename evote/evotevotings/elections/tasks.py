# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.utils import timezone, translation
from django.conf import settings
from django.apps import apps

from evotevotings.services.routers import CSrouter
from evotevotings.processes.tasks import (
    archive_electoral_process, close_electoral_process)
from celery.schedules import crontab
from evote.celery import app
from django_fsm import can_proceed

from .mailings import ElectionNotificationDateMail


logger = logging.getLogger('evote.core')


def get_election(pk):
    Election = apps.get_model("evoteelections.Election")  # noqa
    return Election.objects.get(pk=pk)


@app.task(ignore_result=True)
def publish_provisional_roll(election_id):
    """Task to change the election state from open to provisional electoral
    roll"""
    election = get_election(election_id)
    if can_proceed(election.publish_provisional_roll):
        old_state = election.get_state_display()
        election.publish_provisional_roll()
        election.save()
        logger.info("Election=%d: changed status='%s' => status='%s'",
                    election_id, old_state, election.get_state_display())
    else:
        logger.warning(
            "Election=%d: can't change to status='Provisional electoral roll'",
            election_id)
    return election


@app.task(ignore_result=True)
def publish_definitive_roll(election_id):
    """Task to change the election state from provisional electoral roll to
    definitive electoral roll"""
    election = get_election(election_id)
    if can_proceed(election.publish_definitive_roll):
        old_state = election.get_state_display()
        election.publish_definitive_roll()
        election.save()
        logger.info("Election=%d: changed status='%s' => status='%s'",
                    election_id, old_state, election.get_state_display())
    else:
        logger.warning(
            "Election=%d: can't change to status='Definitive electoral roll'",
            election_id)
    return election


@app.task(ignore_result=True)
def open_election_voting(election_id):
    """Task to change the election state from definitive electoral roll to
    in to vote"""
    election = get_election(election_id)
    if can_proceed(election.voting):
        old_state = election.get_state_display()
        election.voting()
        election.save()
        logger.info("Election=%d: changed status='%s' => status='%s'",
                    election_id, old_state, election.get_state_display())
    else:
        logger.warning("Election=%d: can't change to status='In to vote'",
                       election_id)
    return election


@app.task(ignore_result=True)
def close_election_voting(election_id):
    """Task to change the election state from in to vote to in counting"""
    election = get_election(election_id)
    if can_proceed(election.counting):
        old_state = election.get_state_display()
        election.counting()
        election.save()
        logger.info("Election=%d: changed status='%s' => status='%s'",
                    election_id, old_state, election.get_state_display())
    else:
        logger.warning("Election=%d: can't change to status='In counting'",
                       election_id)
    return election


@app.task(ignore_result=True, bind=True, max_retries=None)
def close_election_counting(self, election_id):
    """Task to change the election state from in counting to votes scrutinized.
    http://docs.celeryproject.org/en/latest/userguide/tasks.html#retrying
    """
    conf = getattr(settings, "EVOTE_ELECTIONS")
    stop_status = [conf.get('counting_finish_state'),
                   conf.get('counting_error_state')]
    status = CSrouter().check_results_status(election_id)
    # counting in progress
    if status is not None and status not in stop_status:
        logger.info("Election=%d: counting in progress status=%s", election_id,
                    status)
        raise self.retry(countdown=10, max_retries=None)
    # counting finished
    elif status == conf.get('counting_finish_state'):
        logger.info("Election=%d: counting finished status=%s", election_id,
                    status)
        election = get_election(election_id)
        if can_proceed(election.scrutinized):
            old_state = election.get_state_display()
            election.scrutinized()
            election.save()
            logger.info("Election=%d: changed status='%s' => status='%s'",
                        election_id, old_state, election.get_state_display())
        else:
            logger.warning(
                "Election=%d: can't change to status='Votes scrutinized'",
                election_id)
        return election
    # error or counting not initialize yet
    elif status is None:
        logger.info("Election=%d: counting not initialized on Counter Service",
                    election_id)
    return None


@app.task(ignore_result=True)
def begin_minute_approval(election_id):
    """Task to change the election state from votes scrutinized to
    minute in approval"""
    election = get_election(election_id)
    if can_proceed(election.begin_approval):
        old_state = election.get_state_display()
        election.begin_approval()
        election.save()
        logger.info("Election=%d: changed status='%s' => status='%s'",
                    election_id, old_state, election.get_state_display())
    else:
        logger.warning(
            "Election=%d: can't change to status='Minute in approval'",
            election_id)
    return election


@app.task(ignore_result=True)
def close_election(election_id):
    """Task to close an election"""
    election = get_election(election_id)
    if can_proceed(election.close):
        old_state_el = election.get_state_display()
        election.close()
        election.save()
        close_electoral_process.si(election.electoral_process_id).delay()
        logger.info("Election=%d: changed status='%s' => status='%s'",
                    election_id, old_state_el, election.get_state_display())
    else:
        logger.warning("Election=%d: can't change to status='Closed'",
                       election_id)
    return election


@app.task(ignore_result=True)
def archive_election(election_id):
    """Task to archive an election"""
    election = get_election(election_id)
    if can_proceed(election.archive):
        old_state_el = election.get_state_display()
        election.archive()
        election.save()
        archive_electoral_process.si(election.electoral_process_id).delay()
        logger.info("Election=%d: changed status status='%s' => status='%s'",
                    election_id, old_state_el, election.get_state_display())
    else:
        logger.warning("Election=%d: can't change to status='Archived'",
                       election_id)
    return election


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    """ """
    conf = getattr(settings, "EVOTE_ELECTIONS")
    # declare send_elections_notifications task
    sender.add_periodic_task(
        crontab(
            hour=conf.get('notification_time')[0],
            minute=conf.get('notification_time')[1]
        ),
        send_elections_notifications.s(),
    )


@app.task
def send_elections_notifications():
    """ """
    conf = getattr(settings, "EVOTE_ELECTIONS")
    sent = 0
    Election = apps.get_model("evoteelections.Election") # NOQA
    qs = Election.objects.filter(state__gte=Election.State.OPEN)
    fields = {
        'proposition_begin_date': {
            'subject': 'Notificação eVotUM - Período de apresentação de' +
                       ' listas candidatas | eVotUM notification - Period' +
                       ' for the submission of applications',
            'type_pt': 'período de apresentação de listas candidatas',
            'type': 'period for the submission of applications',
            'kind_pt': 'inicia-se',
            'kind': 'begins'
        },
        'proposition_end_date': {
            'subject': 'Notificação eVotUM - Período de apresentação de' +
                       ' listas candidatas | eVotUM notification - Period' +
                       ' for the submission of applications',
            'type_pt': 'período de apresentação de listas candidatas',
            'type': 'period for the submission of applications',
            'kind_pt': 'termina',
            'kind': 'ends'
        },
        'minute_end_date': {
            'subject': 'Notificação eVotUM - Período de homologação | ' +
                       'eVotUM notification - Approval period',
            'type_pt': 'período de homologação',
            'type': 'approval period',
            'kind_pt': 'termina',
            'kind': 'ends'
        },
        'voting_begin_date': {
            'subject': 'Notificação eVotUM - Período de votação | ' +
                       'eVotUM notification - Voting time',
            'type_pt': 'período de votação',
            'type': 'voting time',
            'kind_pt': 'inicia-se',
            'kind': 'begins'
        },
        'roll_begin_date': {
            'subject': 'Notificação eVotUM - Período de publicação dos ' +
                       'cadernos eleitorais provisórios | eVotUM ' +
                       'notification - Publication of the provisional ' +
                       'electoral roll',
            'type_pt': 'período de publicação dos cadernos eleitorais'
                       ' provisórios',
            'type': 'publication of the provisional electoral roll',
            'kind_pt': 'termina',
            'kind': 'ends'
        },
        'roll_end_date': {
            'subject': 'Notificação eVotUM - Período de publicação dos ' +
                       'cadernos eleitorais definitivos | eVotUM ' +
                       'notification - Publication of the definitive ' +
                       'electoral roll',
            'type_pt': 'período de publicação dos cadernos eleitorais'
                       ' definitivos',
            'type': 'publication of the definitive electoral roll',
            'kind_pt': 'termina',
            'kind': 'ends'
        }
    }
    days = conf.get('notification_days_before')
    for election in qs:
        for key in fields:
            date = getattr(election, key)
            for day in days:
                notify_date = date - timezone.timedelta(days=day)
                if notify_date.date() == timezone.now().date():
                    ElectionNotificationDateMail(
                        subject=fields[key]['subject'],
                        context={
                            'election_id': election.identifier,
                            'process_id':
                                election.electoral_process.identifier,
                            'days': day,
                            'type_pt': fields[key]['type_pt'],
                            'type': fields[key]['type'],
                            'kind_pt': fields[key]['kind_pt'],
                            'kind': fields[key]['kind']
                        })\
                        .send(election.electoral_process)
                    sent += 1
                    logger.info(
                        "Notification sended: subject='%s'"
                        "electoral_process='%s'", fields[key]['subject'],
                        election.electoral_process)
    return sent


@app.task(ignore_result=True)
def generate_election_pdfs(election_id, total, total_weight, data, extra_info):
    """Task to generate pdf in all languages """
    languages = getattr(settings, 'LANGUAGES', None)
    for lang in languages:
        translation.activate(lang[0])
        election = get_election(election_id)
        election.generate_pdf(total, total_weight, data, extra_info)
        election.generate_voters_pdf()
    return election
