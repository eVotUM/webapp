# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.db import models

from guardian.shortcuts import get_objects_for_user
from evotecore.utils import compare_data


class ElectionQuerySet(models.QuerySet):

    def closed(self):
        """Return elections on state CLOSED"""
        return self.filter(state=self.model.State.CLOSED)

    def archived(self):
        """Return elections on state ARCHIVED"""
        return self.filter(state=self.model.State.ARCHIVED)

    def in_to_vote(self):
        """Return elections on state IN_TO_VOTE"""
        return self.filter(state=self.model.State.IN_TO_VOTE)

    def eligible(self, user):
        """Return elections eligible for the user"""
        return get_objects_for_user(
            user, "vote_election", self.all(), use_groups=False,
            with_superuser=False)


class CandidateQuerySet(models.QuerySet):

    def write_in(self):
        """ """
        return self.filter(write_in=True).order_by("designation")

    def not_write_in(self):
        """ """
        return self.filter(write_in=False)


class CandidateBaseManager(models.Manager):
    """ """

    def bulk_import(self, election, data):
        """ """
        candidates = []
        candidates_to_create = []
        for row in data:
            try:
                candidate = self.get(election=election,
                                     personnel_number=row['personnel_number'])
                self.efficient_update(candidate, row, [
                    "designation"
                ])
                candidates.append(candidate)
            except self.model.DoesNotExist:
                candidates_to_create.append(self.model(
                    write_in=True,
                    election=election,
                    designation=row["designation"],
                    personnel_number=row['personnel_number']
                ))
        candidates += self.bulk_create(candidates_to_create)
        # before write in imported candidates cleared
        candidate_ids = [c.pk for c in candidates]
        self.write_in().filter(election=election)\
            .exclude(pk__in=candidate_ids).delete()
        return candidates

    def efficient_update(self, voter, data, keys):
        """Only update if data changed, otherwise do nothing"""
        need_update = not compare_data(voter, data, keys)
        if need_update:
            for key in keys:
                setattr(voter, key, data[key])
            voter.save(update_fields=keys)

    def representation_values(self, is_write_in):
        if is_write_in:
            qs = self.write_in()
        else:
            qs = self.not_write_in()

        return {str(candidate.pk): candidate.__str__()
                for candidate in qs}


CandidateManager = CandidateBaseManager.from_queryset(CandidateQuerySet)


class CategoryQuerySet(models.QuerySet):

    def not_default(self):
        """ """
        return self.filter(default=False)

    def default(self):
        """ """
        return self.filter(default=True).first()


class CategoryBaseManager(models.Manager):

    def weight_values(self):
        """Return dictionary with slug as key and weight as value"""
        weights = self.values_list('slug', 'weight')
        return dict((str(slug), weight) for slug, weight in weights)


CategoryManager = CategoryBaseManager.from_queryset(CategoryQuerySet)
