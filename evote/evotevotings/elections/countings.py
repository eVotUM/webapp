# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals


class Vote(object):
    """ """
    _weight = 1
    total = 0
    total_weight = 0

    def __init__(self, weight, total):
        """ """
        self._weight = weight
        self.total = total
        self.total_weight = weight * total


class ListVote(list):
    """ """

    def __init__(self, weights, votes):
        """ """
        self._set_votes(weights, votes)

    def _set_votes(self, weights, data):
        """ """
        for weight_id, vote in data.items():
            weight = weights.get(weight_id, None)
            if weight:
                self.append(Vote(weight, vote))
            else:
                raise ValueError("Weight '{}' don't exists".format(weight_id))

    @property
    def total(self):
        """ """
        total = 0
        for vote in self:
            total += vote.total
        return total

    @property
    def total_weight(self):
        """ """
        total = 0
        for vote in self:
            total += vote.total_weight
        return total


class VoteResult(object):

    def __init__(self, cid, name, total, perc, seats=0):
        """ """
        self.cid = cid
        self.name = name
        self.total = total
        self.perc = perc
        self.seats = seats

    def __repr__(self):
        return "{0}: {1}/{2} ({3:.2f})".format(
            self.cid, self.total, self.seats, self.perc)


class CountingMethods:
    """ """
    DHONDT = 0
    PROPORTIONAL = 1


class VoteCounting(list):
    """ """
    candidates = dict()
    blank_votes = None
    _weights = None
    _data = None
    invalid_votes = 0
    total_votes = 0

    def __init__(self, weights, candidates_name, data):
        """ """
        self._data = data
        self._weights = weights
        self.total_votes = data['total_votes']
        self.invalid_votes = data['invalid_votes']
        self.candidates = self.set_candidates(data['candidates'])
        self.candidates_name = candidates_name
        self.blank_votes = self.set_blank_votes(data['blank_votes'])

    def set_candidates(self, data):
        """ """
        candidates = {}
        for cid, results in data.items():
            candidates[cid] = ListVote(self._weights, results)
        return candidates

    def set_blank_votes(self, data):
        """ """
        return ListVote(self._weights, data)

    def set_seats(self, results):
        """Set seats on vote results for each candidate"""
        for cid, seats in results.items():
            result = self.filter(cid)
            result.seats = seats

    def perc(self, value, total):
        """ """
        if total > 0:
            return float(value) / float(total) * 100
        return 0

    @property
    def total(self):
        """ """
        total = self.blank_votes.total
        for result in self.candidates.values():
            total += result.total
        return total

    @property
    def total_weight(self):
        """ """
        total = self.blank_votes.total_weight
        for result in self.candidates.values():
            total += result.total_weight
        return total

    @property
    def data(self):
        """Return a list of dictionaries with all results"""
        return [r.__dict__ for r in self]

    def filter(self, cid):
        """Search for results by candidate id"""
        for result in self:
            if result.cid == cid:
                return result
        return None

    def count(self, method=CountingMethods.PROPORTIONAL, **kwargs):
        """Calculate the results for each candidate and populate the object
        with vote results ordered, including blank votes as last item"""
        results = []
        for cid, result in self.candidates.items():
            results.append(VoteResult(
                cid=cid,
                name=self.candidates_name[cid],
                total=result.total_weight,
                perc=self.perc(result.total_weight, self.total_weight)
            ))
        # reset previous coutings
        self[:] = []
        self += sorted(results, key=lambda p: p.total, reverse=True)
        # calculate and set seats, if applicable
        if method == CountingMethods.DHONDT:
            nseats = kwargs.get('seats')
            candidates = dict((r.cid, r.total) for r in self)
            results = VoteCounting.dhondt(nseats, candidates)
            self.set_seats(results)
        # add the candidates with no votes
        for cid, name in self.candidates_name.items():
            if cid not in self.candidates:
                self.append(VoteResult(cid=cid, name=name, total=0, perc=0.0))
        # set, as last element, blank votes
        self.append(VoteResult(
            cid='blank',
            name='blank',
            total=self.blank_votes.total_weight,
            perc=self.perc(self.blank_votes.total_weight, self.total_weight)
        ))

    @staticmethod
    def dhondt(nseats, candidates):
        """Distribute seats between all candidates"""
        seats = {cand: 0 for cand in candidates.keys()}
        quotients = {cand: float(V) for cand, V in candidates.items()}
        if candidates:
            for seat in xrange(nseats):
                assigned = max(quotients, key=quotients.get)
                seats[assigned] += 1
                quotients[assigned] = candidates[assigned] / \
                    float(seats[assigned] + 1)
        return seats
