# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json
import logging

from django.utils import timezone
from django.conf import settings
from django.apps import apps

from eVotUM.Cripto import pkiutils, eccblind
from evotevotings.admin.models import SecretEntry
from evotevotings.admin.utils import (get_secret_entry,
                                      get_private_key_password)


logger = logging.getLogger('evote.crypto')


def get_electionvoter_model():
    return apps.get_model("evotevoters.ElectionVoter")  # NOQA


def initialize_blind_signatures():
    (init_component, pr_component) = eccblind.initSigner()
    return (init_component, pr_component)


def get_extra_election_data(election_id, user_id):
    """Used on voting step 1"""
    # ElectionVoter to fetch the category
    ElectionVoter = get_electionvoter_model()  # NOQA
    electionvoter_obj = ElectionVoter.objects.get(
        election_id=election_id,
        user_id=user_id
    )

    # Validator Service Certificate
    vs_cert = get_secret_entry(
        SecretEntry.Identifier.VALIDATOR_CERT
    )
    if not vs_cert:
        logger.critical("No Validator Service Certificate found")
        return False

    # Voting System Private Key
    votesys_private_key = get_secret_entry(
        SecretEntry.Identifier.VOTING_PVT_KEY
    )
    if not votesys_private_key:
        logger.critical("No Voting System Private Key found")
        return False

    # Voting System Private Key password
    pvtkey_password = get_private_key_password()
    if not pvtkey_password:
        logger.critical("No Voting System Private Key password found")
        return False
    # User/Election combo_id {user + election_id}
    combo_id = json.dumps({
        'election_id': str(election_id),
        'user_id': str(user_id)
    })

    # Sign the needed objects using the system private key
    # Attempt to sign the combo_id {user + election_id}sv
    errorCode, signed_combo_id = pkiutils.signObject(
        combo_id,
        votesys_private_key,
        pvtkey_password
    )
    if errorCode:
        logger.critical(
            "Could not sign election+user ID pkiutils.signObject=%r",
            errorCode)
        return False

    # Attempt to sign the election_id {election_id}sv
    errorCode, signed_election_id = pkiutils.signObject(
        election_id,
        votesys_private_key,
        pvtkey_password
    )
    if errorCode:
        logger.critical("Could not sign election ID pkiutils.signObject=%r",
                        errorCode)
        return False

    return {
        'combo_id': signed_combo_id,
        'vs_cert': vs_cert,
        'election_id': signed_election_id,
        'category_slug': electionvoter_obj.category.slug
    }


def calc_archive_date(date):
    days = settings.EVOTE_ELECTIONS.get('archive_after')
    return date + timezone.timedelta(days=days)
