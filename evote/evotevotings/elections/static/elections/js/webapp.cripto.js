// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/*
 * eVote - Elections
 * Author: Eurotux S.A.
 *
 */

/* globals _, $, SRVDATA */

"use strict";

/* Expose desired objects globally */
if( typeof CRIPTO === 'undefined') {
    var CRIPTO = {};
}

(function() {

    /*
     * Main Object to wrap criptographic methods
     * Example usage:
     */
    CRIPTO = (function() {
        var self = {

            /* Flag to set the object as initialized */
            initialized: false,
            /* Main data selector */
            data_selector: 'election-id',
            /* Data attr selector for each form inputs */
            step_selector: 'step',
            /* Data attr selector to submit the form */
            next_selector: 'next-step',
            /* Data attr for independent vote buttons */
            vote_btn_selector: 'vote-button',
            /* Data attr selector for the form data */
            form_selector: 'server-form',
            /* Data attr selector for the unique vote reference */
            reference_selector: 'unique-reference',
            /* Hidden form object handler */
            form_object: null,
            /* Current vote holder */
            stored_vote: null,
            /* Hash to store Local artifacts */
            local_components: {},
            /* AJAX Deferred requests handler */
            jqxhr: null,


            /* Method to bootstrap the object */
            initialize: function() {
                /* Avoid duplicate initialization */
                if(self.initialized)
                    return false;

                /* Sanity check */
                var data_obj = $('[data-'+self.data_selector+']');
                if(!data_obj.length)
                    return false;

                /* Set the object as initialized */
                self.initialized = true;

                /* Initialize the Storage handler object */
                STORAGE.initialize();

                /* Apply logic per form step */
                self._loadStep();

            },
            /* Helper function to set the correct vote format:
             *
             *  [
             *    {
             *       'blank' : bool,
             *       'candidates' : [
             *         {
             *            'id': string,
             *            'vote': integer
             *         }
             *       ],
             *       'category': string,
             *    }
             *    .
             *    .
             *    .
             *  ]
             *
             * */
            _formatVotes: function(vote_list, category) {
                /* Sanitization */
                vote_list = (typeof(vote_list) == "undefined") ? [] : vote_list;
                /* Vote format prototype */
                var vote_proto = {'blank': true , 'candidates': [], 'category': category};
                /* Iterate through each vote and transform it */
                _.each(vote_list, function(elem, i, list) {
                    try {
                        /* Populate the candidate ID for each vote */
                        var candidate_dict = {'id': elem.id, 'vote': elem.vote}
                        vote_proto.candidates.push(candidate_dict);
                    }
                    catch(err) {
                        /* LOG: Invalid vote structure */
                    }
                });

                /* Check if it is a blank vote */
                if(!_.isEmpty(vote_proto.candidates)) {
                    vote_proto.blank = false;
                }
                return vote_proto;
            },
            customReset: function(election_id) {
               if(typeof(election_id) == "undefined")
                   return false;
               var key = SRVDATA.key_prefix + election_id;
               STORAGE.reset(key);
            },
            /* Wrapper AJAX method to handle deferred POST requests */
            _ajaxRequest: function(endpoint, post_data, callback) {
               var csrf_token = self.getCookie('csrftoken');
               self.jqxhr = $.ajax({
                   url: endpoint,
                   cache: false,
                   dataType: "json",
                   type: "POST",
                   data: post_data,
                   beforeSend: function(xhr, settings){
                       xhr.setRequestHeader("X-CSRFToken", csrf_token);
                   }
               });
               /* Callback on success */
               self.jqxhr.done(callback);

               /* Callback on fail */
               self.jqxhr.fail(self._failedRequest);

               /* Function triggered o every request */
               self.jqxhr.always(self._finishedRequest);

            },
            /* Method to handle failed AJAX requests
            /* Function argument prototype from:
             *    https://github.com/jquery/api.jquery.com/issues/49
             */
            _failedRequest: function(xhr, errorMsg, error) {
                var response = $.parseJSON(xhr.responseText);
                if ('pin_value' in response) {
                    var errors = response['pin_value'],
                        field = $('input[data-pin-validation]'),
                        error_info = field.parent('.field-wrapper'),
                        error_str = '<ul class="errorlist">';

                    error_info.remove(".errorlist"); // clean errors
                    field.parents('.pure-field').addClass('error');

                    $.each(errors, function(index, error) {
                        error_str += "<li>"+error+"</li>";
                    });
                    error_str += "</ul>";
                    $(error_str).insertAfter(error_info);
                } else {
                    noty({
                        text: django.gettext("Could not finish the vote process. Please try again later"),
                        type: "error"
                    });
                }
            },
            /* Method to handle completed AJAX requests
            /* Function argument prototype from:
             *    https://github.com/jquery/api.jquery.com/issues/49
             */
            _finishedRequest: function(xhr, errorMsg, error) {
                self.jqxhr = null;
            },
            /* Method to check which form step
             * is being shown, applying all
             * necessary actions.
             * */
            _loadStep: function() {
                /* Abort if the object is not initialized */
                if(!self.initialized)
                    return false;

                /* Step value holder sanity check */
                var step_elem = $('[data-'+self.step_selector+']');
                if(typeof(step_elem) == "undefined")
                    return false;

                /* Get the current step */
                var current_step = step_elem.data(self.step_selector);
                current_step = parseInt(current_step);

                /* If in step 1 or 2 check for stored votes */
                if(current_step == 1 ||
                   current_step == 2 ||
                   current_step == 4
                   ) {
                    /* Initialize the Form handler object */
                    SRVDATA.initialize();
                    /* Check if there are votes on the storage */
                    var votes = SRVDATA.getVote();
                    if(!votes)
                        votes = [];
                    else
                        votes = JSON.parse(votes);

                    /* Save into the object (to be used later) */
                    self.stored_vote = votes;
                    /* Set chosen candidate list */
                    VOTE_POPULATE.init(current_step, self.stored_vote);
                }
                /* Step 1 related operations */
                if(current_step == 1) {
                    /* Step 1 - Select candidates */

                    /* Reset the stored data */
                    /* It will be updated on form submit */
                    SRVDATA.resetStorage();

                    /* Bind the necessary events */
                    self._bindStepOneEvents();
                }
                /* Step 2 related operations */
                if(current_step == 2) {
                    /* Generate the combo_id hash */
                    self._bindStepTwoEvents();
                }
                /* Step 4 related operations */
                if(current_step == 4) {
                    /* Format the votes */
                    var category = SRVDATA._getField('category_slug');
                    self.stored_vote = self._formatVotes(votes, category);
                    /* Generate the combo_id hash */
                    self._bindStepFourEvents();
                }
                /* Step 5 (Final) related operations */
                if(current_step == 5) {
                    /* Initialize the Form handler object */
                    SRVDATA.initialize();
                    /* Populate the unique vote bulletin */
                    var bulletin = SRVDATA.getUniqueBulletin();
                    var ref_obj = $('[data-'+self.reference_selector+']');
                    ref_obj.html(bulletin);
                    /* Clear the storage */
                    SRVDATA.resetStorage();
                }

            },
            /* Method to bind all events related to Step 1 */
            _bindStepOneEvents: function() {
                /* Sanity Check */
                var form_obj = $('[data-'+self.form_selector+']');
                if(!form_obj.length)
                    return false;
                /* Save the found form object */
                self.form_object = form_obj;

                /* Bind events */
                self.form_object.on('submit', function (e) {
                    e.preventDefault();
                });
                /* On 'next' button submit */
                self.form_object.find("[data-"+self.next_selector+"]").on('click', function (e) {
                    /* Operate before redirecting */
                    e.preventDefault();
                    /* Load the form data into the storage */
                    SRVDATA.updateStorage();
                    /* Get the chosen votes */
                    var votes = VOTING.getSelectedVotes();
                    /* Update the storage */
                    SRVDATA.storeVote(JSON.stringify(votes));
                    /* Proceed to the next step */
                    window.location = $(this).attr('href');
                });

            },
            /* Method to bind all events related to Step 2 */
            _bindStepTwoEvents: function() {
                /* Sanity Check */
                var btn_obj = $('[data-'+self.vote_btn_selector+']');
                if(!btn_obj.length)
                    return false;

                btn_obj.on('click', function(e) {
                    /* Operate before redirecting */
                    e.preventDefault();

                    try {
                        /* Get the combo ID from the storage */
                        var combo = SRVDATA._getField('combo_id');

                        /* Hash the election+user combo */
                        var hashed_combo = evotum.hashfunctions.generateSHA256Hash(combo);

                        /* Save into the storage */
                        SRVDATA.storeHash(hashed_combo);

                        /* Proceed to the next step */
                        window.location = $(this).attr('href');

                    }
                    catch(err) {
                        /* Handle exception */
                        console.log(err.message);
                    }
                });
            },
            /* Method to bind all events related to Step 4 */
            _bindStepFourEvents: function() {
                /* Sanity Check */
                var btn_obj = $('[data-'+self.vote_btn_selector+']');
                if(!btn_obj.length)
                    return false;

                /* Store into the local components array */
                self.local_components['submit_btn'] = btn_obj;

                btn_obj.on('click', function(e) {
                    /* Operate before redirecting */
                    e.preventDefault();

                    try {
                        /* Get Vote from the Storage */
                        var votes = self.stored_vote;
                        /* Get the public key from the certificate */
                        /* Cipher the vote */
                        var election_cert = SRVDATA.getElectionCertificate();
                        var json_str_votes = JSON.stringify(votes);
                        var ciphered_vote = evotum.pkiutils.encryptWithPublicKey(json_str_votes, election_cert);

                        /* Exception handling */
                        if( ciphered_vote == null)
                             throw "Could not cipher the vote";

                        /* Save into the storage */
                        SRVDATA.storeVote(ciphered_vote, true)

                        /* Hash the ciphered vote */
                        var cv_hash = evotum.hashfunctions.generateSHA256Hash(ciphered_vote);

                        /* Get the combo hash from the storage */
                        var combo_hash = SRVDATA.getHash();

                        /* Concatenate both hashes */
                        var final_hash = cv_hash + combo_hash;

                        /* Hash the result */
                        final_hash = evotum.hashfunctions.generateSHA256Hash(final_hash);

                        /* Save into the storage */
                        SRVDATA.storeUniqueBulletin(final_hash);

                        /* Generate the blind vote */
                        /* Get the prDashComponents */
                        var dash_components = SRVDATA.getBlindComponents();
                        var blind_data = evotum.eccblind.blindData(dash_components, ciphered_vote);

                        /* Exception handling */
                        if(typeof(blind_data) == 'undefined' ||
                           blind_data['errorCode'] == 1) {
                               throw "Could not generate blind signature";
                        }

                        var blindComponents = blind_data['blindComponents'];
                        var newPrComponents = blind_data['pRComponents'];
                        var blind_vote = blind_data['blindM'];

                        /* Save into the temp storage */
                        self.local_components['blindComponents'] = blindComponents;
                        self.local_components['pRDashComponents'] = newPrComponents;

                        var combo_id = SRVDATA._getField('combo_id');
                        var pin_value = $('input[data-pin-validation]').val();

                        /* Send the combo_id and blind_vote
                         * to the Validator Service */
                        var data_to_validate = {'signed_token': combo_id,
                                                'blind_hash': blind_vote,
                                                'pin_value': pin_value};

                        var vs_endpoint = configuration['genericservice']['endpoints']['validateVote'];

                        /* Pass the request onto the callback function */
                        self._ajaxRequest(vs_endpoint, data_to_validate, self._handleValidateVote);


                    }
                    catch(err) {
                        /* Handle exception */
                        console.log(err);
                    }
                });
            },
            /* Method to handle server requests from the validateVote endpoint
             *
             * Since this is a callback function, arguments are
             * prototyped to the $.ajax.done() method
             */
            _handleValidateVote: function(json_data, textStatus, jqXHR) {
                /* Try to get the JSON object */
                try {
                    /* If the error code is not 0, raise exception */
                    if(json_data.return_code != 0) {
                        throw "("+String(json_data.return_code)+") Could not validate request";
                    }
                    /* Hash the received blind signature */
                    var blind_signature = json_data.data;
                    var hashed_blind = evotum.hashfunctions.generateSHA256Hash(blind_signature);

                    /* Get the server generated blind prDashComponents from the storage */
                    var server_pr_components = SRVDATA.getBlindComponents();

                    /* Get the blindComponents generated client-side */
                    var client_blind_components = self.local_components['blindComponents'];

                    /* Unblind the blind signature */
                    var unblind_obj = evotum.eccblind.unblindSignature(blind_signature,
                                                                       server_pr_components,
                                                                       client_blind_components)

                    if(unblind_obj.errorCode != null)
                        throw "("+String(json_data.return_code)+") Could not unblind the signature";

                    /* Get the unblind signature result
                     * The signature contains the Vote signed by the Validato Service
                     */
                    var signature = unblind_obj.signature;

                    /* Fetch the VS certificate from the storage */
                    var vs_cert = SRVDATA.getVSCertificate();

                    /* Get the Public Key from the (ECC) Validator Service certificate */
                    var vs_pub = evotum.eccblind.getEccPublicKeyFromCertificate(vs_cert)
                    if( typeof(vs_pub.errorCode) === 'undefined' ) {
                        throw "Could not get the public key from the certificate";
                    }
                    var ecc_pubkey = vs_pub.publicKey

                    /* Get the blindComponents generated client-side */
                    var client_pr_components = self.local_components['pRDashComponents'];

                    /* Get the ciphered vote from the storage */
                    var ciphered_vote = SRVDATA.getVote(true)

                    /* Verifiy if signature is properly signed
                       and contains the ciphered vote */
                    var valid_signature = evotum.eccblind.verifySignature(ecc_pubkey,
                                                                          signature,
                                                                          client_blind_components,
                                                                          client_pr_components,
                                                                          ciphered_vote);

                    if(valid_signature.errorCode != null) {
                        throw "("+String(valid_signature.errorCode)+") Invalid signature";
                    }

                    /* Validate result */
                    if(! valid_signature.result) {
                        throw "Unxpected invalid signature";
                    }

                    /* Send to the Filter service:
                     *
                     */

                    /* Get the election ID from the storage */
                    var electid = SRVDATA.getSignedElectionID();

                    /* Get the Unique vote bulletin from the storage */
                    var final_hash = SRVDATA.getUniqueBulletin();

                    /* Build the data object */
                    var data_to_deliver = {'election_id': electid,
                                           'blind_hash': hashed_blind,
                                           'ciphered_vote': ciphered_vote,
                                           'signed_vote': signature,
                                           'blind_components': client_blind_components,
                                           'pr_components': client_pr_components,
                                           'unique_bulletin': final_hash};

                    var fs_endpoint = configuration['genericservice']['endpoints']['filterSignature'];

                    /* Pass the request onto the callback function */
                    self._ajaxRequest(fs_endpoint, data_to_deliver, self._handleFinalRequest);

                }
                catch(err) {
                     /* Handle exception */
                     console.log(err);
                }

            },
            /* Method to handle server requests from the filterSignature endpoint
             *
             * Expected result is a boolean which allows the voter to proceed
             * into the final view where the unique vote bulletin is displayed.
             *
             * Since this is a callback function, arguments are
             * prototyped to the $.ajax.done() method
             */
            _handleFinalRequest: function(json_data, textStatus, jqXHR) {
                var btn_obj = self.local_components['submit_btn'];
                if(!btn_obj.length)
                    /* TODO: Lost the button handler */
                    return false;

                if(json_data.result === true)
                    window.location = btn_obj.attr('href');

                /* Proceed to the next step */
                window.location = btn_obj.attr('href');
            },
            /* Auxiliary function to get a cookie value by name */
            getCookie: function(name) {
                var cookieValue = null;
                if (document.cookie && document.cookie !== '') {
                     var cookies = document.cookie.split(';');
                     for (var i = 0; i < cookies.length; i++) {
                         var cookie = jQuery.trim(cookies[i]);
                         // Does this cookie string begin with the name we want?
                         if (cookie.substring(0, name.length + 1) === (name + '=')) {
                             cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                              break;
                         }
                     }
                }
                return cookieValue;
           },

        }

        return self;

    })();

    /*
     * Object to handle browser data storage
     * Example usage:
     */
    var STORAGE = (function() {

        var self = {

            /* Flag to set the object as initialized */
            initialized: false,
            handler: false,

            /* Method to bootstrap the object [usually on page load] */
            initialize: function() {
                /* Avoid duplicate initialization */
                if(self.initialized) {
                    return false;
                }

                /* Set the desired storage handler:
                 *
                 * sessionStorage <
                 * localStorage
                 *
                 * */
                self.handler = window['sessionStorage'];

                /* Check for browser compatibility */
                if( typeof(self.handler) == 'undefined' ) {

                    /* No sessionStorage support */
                    return false;

                }

                /* Mark the object as initialized */
                self.initialized = true;


            },

            /* Method to save a key => value pair into the storage
             *
             * Expects a $ object
             * */
            set_item: function(key, value) {
                /* Sanity checks */
                if(!self.initialized) {
                    return false;
                }
                if(typeof(key) == 'undefined' ) {
                    return false;
                }
                try {
                    /* Save the data into the storage */
                    self.handler.setItem(key, value);
                }
                catch(err) {
                     /* Handle exception */
                     console.log(err.message);
                }

            },
            /* Method to get a stored value from the storage
             *
             * Expects a key identifier
             * */
            get_item: function(key) {
                /* Sanity checks */
                if(!self.initialized) {
                    return false;
                }
                if(typeof(key) == 'undefined' ) {
                    return false;
                }
                try {
                    /* Save the data into the storage */
                    if(self.handler.getItem(key))
                        return self.handler.getItem(key);

                    return null;
                }
                catch(err) {
                     /* Handle exception */
                     console.log(err.message);
                }

            },
            /* Method to reset a storage key
             *
             * Expects a key identifier
             * */
            reset: function(key) {
                /* Sanity checks */
                if(!self.initialized) {
                    self.initialize();
                }
                if(typeof(key) == 'undefined' ) {
                    return false;
                }
                try {
                    self.handler.removeItem(key);
                    return true;
                }
                catch(err) {
                     /* Handle exception */
                     console.log(err.message);
                     return false;
                }

            }

        }

        return self;

    })();

    /*
     * Object to handle server
     * proxied data
     *
     * Stored keys:
     *  - pr_components
     *  - election_cert
     *  - combo_id
     *  - election_id
     *  - vs_cert
     */
    var SRVDATA = (function() {

        var self = {

            /* Flag to set the object as initialized */
            initialized: false,
            /* Election storage key prefix */
            key_prefix: 'evoteelection_',
            /* Data attr selector for the form data */
            form_selector: 'server-form',
            /* Data attr selector for each form inputs */
            wrapper_selector: 'session-wrapper',
            /* Data attr selector to get the election id */
            election_selector: 'election-id',
            /* Vote JSON key */
            raw_vote_key: 'raw_vote',
            /* Ciphered Vote JSON key */
            ciphered_vote_key: 'ciphered_vote',
            /* SHA256(combo_id) JSON key */
            hash_key: 'combo_hash',
            /* SHA256(SHA256(combo_id) + SHA256([Vote])) JSON key */
            bulletin_key: 'unique_bulletin_hash',
            /* ElectioVoter category identifier */
            category_key: 'category_slug',
            /* Hidden form object handler */
            form_object: null,
            /* Election ID */
            election_id: null,


            /* Method to bootstrap the object [usually on page load] */
            initialize: function() {
                /* Avoid duplicate initialization */
                if(self.initialized) {
                    return false;
                }
                /* Get the election ID */
                var elect_obj = $('[data-'+self.election_selector+']');
                if(elect_obj.length) {
                    /* Save the form object pointer */
                    self.election_id = elect_obj.data(self.election_selector);
                }
                else {
                    return false;
                }

                /* Check if the form exists */
                var form_obj = $('[data-'+self.form_selector+']');
                if(form_obj.length) {
                    /* Save the form object pointer */
                    self.form_object = form_obj;
                }

                /* Mark the object as initialized */
                self.initialized = true;
            },
            /* Generate the storage key */
            _genStorageKey: function() {
                if(!self.initialized) {
                    self.initialize();
                }
                var elect_id = self.election_id;
                var key_prefix = self.key_prefix;
                return key_prefix + elect_id;
            },
            /* Method to reset the storage data
             *
             * Resets data per election
             * */
            resetStorage: function() {
                /* Sanity Check */
                if(!self.initialized) {
                    return false;
                }
                /* Initialize the storage system if needed */
                STORAGE.initialize();

                /* Generate the storage key for this election */
                var key = self._genStorageKey();

                STORAGE.reset(key);
            },
            /* Method to update the storage data when the
             * Step 1 form is present.
             *
             * Stores data per election
             * */
            updateStorage: function() {
                /* Sanity Check */
                if(!self.initialized) {
                    return false;
                }
                var formObj = self.form_object;
                /* Sanity check */
                if(formObj.length) {
                    var election_data = {};
                    /* Fetch each input and its value */
                    var inputs = formObj.find('[data-'+self.wrapper_selector+'] > input');
                    /* If there are inputs, we need to save the data into the storage */
                    if(inputs.length) {
                           inputs.each(function(idx,elem) {
                                 /* Get the $ object */
                                var obj = $(elem);
                                var name = obj.prop('name');
                                var value = obj.val();
                                election_data[name] = value;
                                /* Delete the input HTML */
                                obj.remove();
                           });

                           /* Save into the storage */
                           self.saveData(election_data);
                    }
                }

            },
            /* Method to update/insert a field into the
             * current storage structured data
             *
             * Expects a key:value pair
             * */
            _updateField: function(key, value) {
                /* Sanity check */
                if(typeof(key) == 'undefined' ||
                   typeof(value) == 'undefined'
                   ) {
                    return false;
                }
                if(!self.initialized) {
                    return false;
                }
                /* Load the current storage data */
                var data = self.loadData();

                /* Set the key value pair */
                data[key] = value;

                /* Update the storage */
                self.saveData(data);

            },
            /* Method to get a field from
             * current storage structured data
             *
             * Expects a key value
             * */
            _getField: function(key) {
                /* Sanity check */
                if(typeof(key) == 'undefined') {
                    return null;
                }
                if(!self.initialized) {
                    return false;
                }
                /* Load the current storage data */
                var data = self.loadData();

                /* No data loaded */
                if(!data)
                    return null;

                /* Set the key value pair */
                if(typeof data[key] != 'undefined')
                    return data[key];

                return null;

            },
            /* Method to store the currently selected vote
             * Proxies to _updateField method
             *
             * Expects a JSON Object
             * */
            storeVote: function(Vote, ciphered) {
                /* Storing the raw or ciphered vote? */
                var key = self.raw_vote_key;
                if(typeof(ciphered) != 'undefined') {
                    key = self.ciphered_vote_key;
                }
                return self._updateField(key, Vote);
            },
            /* Method to store the user/election Hash
             * Proxies to _updateField method
             *
             * Expects a JSON Object
             * */
            storeHash: function(Combo) {
                var key = self.hash_key;
                return self._updateField(key, Combo);
            },
            /* Method to store the unique vote bulletin
             * Proxies to _updateField method
             *
             * Expects a JSON Object
             * */
            storeUniqueBulletin: function(Combo) {
                var key = self.bulletin_key;
                return self._updateField(key, Combo);
            },
            /* Method to get the currently selected vote
             * Proxies to _getField method
             *
             * */
            getVote: function(ciphered) {
                /* Sanity check */
                /* Storing the raw or ciphered vote? */
                var key = self.raw_vote_key;
                if(typeof(ciphered) != 'undefined') {
                    key = self.ciphered_vote_key;
                }
                return self._getField(key);
            },
            /* Method to get the currently selected vote
             * Proxies to _getField method
             *
             * */
            getHash: function() {
                /* Sanity check */
                var key = self.hash_key;
                return self._getField(key);
            },
            /* Method to get the unique vote bulletin
             * Proxies to _getField method
             *
             * */
            getUniqueBulletin: function() {
                /* Sanity check */
                var key = self.bulletin_key;
                return self._getField(key);
            },
            /* Method to get the stored
             * election certificate
             *
             * */
            getElectionCertificate: function() {
                return self._getField('election_cert');
            },
            /* Method to get the stored
             * VS (validator service) certificate
             *
             * */
            getVSCertificate: function() {
                return self._getField('vs_cert');
            },
            /* Method to get the stored
             * prDashComponents for the blind signature
             *
             * */
            getBlindComponents: function() {
                return self._getField('pr_components');
            },
            /* Method to get the stored
             * Voting system signed election ID
             *
             * */
            getSignedElectionID: function() {
                return self._getField('election_id');
            },
            /* Method to load election data from the storage
             *
             * Expects a $ object to the form
             * */
            loadData: function() {
                /* Sanity check */
                if(!self.initialized) {
                    return false;
                }
                try {
                    /* Generate the storage key for this election */
                    var key = self._genStorageKey();
                    /* Initialize the storage system if needed */
                    STORAGE.initialize();
                    /* Save the instance into the storage */
                    if(typeof(JSON.stringify) == 'undefined')
                        throw "Cannot serialize object";

                    var raw_data = STORAGE.get_item(key);
                    return JSON.parse(raw_data);
                }
                catch(err) {
                    /* Handle exception */
                    console.log(err.message);
                }

                return false;

            },
            /* Method to proxy data from the server to the browser
             *
             * Expects a key:value pair
             * */
            saveData: function(value) {
                /* Sanity check */
                if(typeof(value) == 'undefined') {
                    return false;
                }
                try {
                    /* Initialize the storage system if needed */
                    STORAGE.initialize();
                    /* Save the instance into the storage */
                    if(typeof(JSON.stringify) == 'undefined')
                        throw "Cannot serialize object";

                    /* Generate the storage key for this election */
                    var key = self._genStorageKey();

                    STORAGE.set_item(key, JSON.stringify(value));
                }
                catch(err) {
                    /* Handle exception */
                    console.log(err.message);
                }


            }

        }

        return self;

    })();



    $(document).ready(function() {
        CRIPTO.initialize();
    });

})();
