# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django import forms

from evotecore import widgets

from .models import Election


class ElectoralCalendarForm(forms.ModelForm):
    """ """

    error_messages = {
        'proposition_end_date_invalid':
            _("Date must be after the begin of the period for the submission "
              "of applications."),
        'complaint_begin_date_invalid':
            _("Date must be between the publication of provisional and "
              "definitive electoral roll."),
        'complaint_end_date_invalid':
            _("Date must be after complaint period start."),
        'complaint_end_date_interval':
            _("Date must be before the publication of definitive "
              "electoral roll."),
        'roll_end_date_invalid':
            _("Date must be after the publication of the provisional "
              "electoral roll."),
        'voting_begin_date_invalid':
            _("Date must be after the publication of the definitive "
              "electoral roll."),
        'voting_end_date_invalid':
            _("Date must be after the begin of the voting time."),
        'minute_publication_date_invalid':
            _("Date must be after the end of the voting time."),
        'minute_begin_date_invalid':
            _("Date must be after the end of the voting time."),
        'minute_end_date_invalid':
            _("Date must be after the start of the approval period.")
    }

    class Meta:
        model = Election
        fields = [
            'proposition_begin_date', 'proposition_end_date',
            'complaint_begin_date', 'complaint_end_date', 'roll_begin_date',
            'roll_end_date', 'voting_begin_date', 'voting_end_date',
            'minute_publication_date', 'minute_begin_date', 'minute_end_date'
        ]
        widgets = {
            'proposition_begin_date': widgets.DateTimeInput(),
            'proposition_end_date': widgets.DateTimeInput(),
            'complaint_begin_date': widgets.DateTimeInput(),
            'complaint_end_date': widgets.DateTimeInput(),
            'roll_begin_date': widgets.DateTimeInput(),
            'roll_end_date': widgets.DateTimeInput(),
            'voting_begin_date': widgets.DateTimeInput(),
            'voting_end_date': widgets.DateTimeInput(),
            'minute_publication_date': widgets.DateTimeInput(),
            'minute_begin_date': widgets.DateTimeInput(),
            'minute_end_date': widgets.DateTimeInput()
        }

    def __init__(self, *args, **kwargs):
        super(ElectoralCalendarForm, self).__init__(*args, **kwargs)
        # disable date field if date already surpassed
        if self.instance and self.instance.state > self.instance.State.OPEN:
            now = timezone.now()
            for field_name in self.fields.keys():
                value = getattr(self.instance, field_name)
                self.fields[field_name].disabled = bool(value < now)

    def clean(self):
        cleaned_data = super(ElectoralCalendarForm, self).clean()
        self.validate_proposition_end_date(cleaned_data)
        self.validate_roll_end_date(cleaned_data)
        self.validate_complaint_begin_date(cleaned_data)
        self.validate_complaint_end_date(cleaned_data)
        self.validate_voting_begin_date(cleaned_data)
        self.validate_voting_end_date(cleaned_data)
        self.validate_minute_publication_date(cleaned_data)
        self.validate_minute_begin_date(cleaned_data)
        self.validate_minute_end_date(cleaned_data)
        return cleaned_data

    def validate_proposition_end_date(self, cleaned_data):
        proposition_end_date = cleaned_data.get('proposition_end_date')
        proposition_begin_date = cleaned_data.get(
            'proposition_begin_date', None)
        if proposition_begin_date and \
                proposition_end_date <= proposition_begin_date:
            self.add_error('proposition_end_date',
                           self.error_messages['proposition_end_date_invalid'])

    def validate_roll_end_date(self, cleaned_data):
        roll_end_date = cleaned_data.get('roll_end_date')
        roll_begin_date = cleaned_data.get('roll_begin_date', None)
        if roll_end_date <= roll_begin_date:
            self.add_error('roll_end_date',
                           self.error_messages['roll_end_date_invalid'])

    def validate_complaint_begin_date(self, cleaned_data):
        complaint_begin_date = cleaned_data.get('complaint_begin_date')
        roll_begin_date = cleaned_data.get('roll_begin_date', None)
        roll_end_date = cleaned_data.get('roll_end_date', None)
        if roll_begin_date and roll_end_date and \
                (complaint_begin_date < roll_begin_date or
                 complaint_begin_date > roll_end_date):
            self.add_error('complaint_begin_date',
                           self.error_messages['complaint_begin_date_invalid'])

    def validate_complaint_end_date(self, cleaned_data):
        complaint_end_date = cleaned_data.get('complaint_end_date')
        complaint_begin_date = cleaned_data.get('complaint_begin_date', None)
        roll_end_date = cleaned_data.get('roll_end_date', None)
        if complaint_begin_date and roll_end_date:
            if complaint_begin_date > complaint_end_date:
                self.add_error(
                    'complaint_end_date',
                    self.error_messages['complaint_end_date_invalid'])
            if complaint_end_date > roll_end_date:
                self.add_error(
                    'complaint_end_date',
                    self.error_messages['complaint_end_date_interval'])

    def validate_voting_begin_date(self, cleaned_data):
        voting_begin_date = cleaned_data.get('voting_begin_date')
        proposition_end_date = cleaned_data.get('proposition_end_date', None)
        roll_end_date = cleaned_data.get('roll_end_date', None)
        if proposition_end_date and roll_end_date and \
                (voting_begin_date < roll_end_date or
                 voting_begin_date < proposition_end_date):
            self.add_error('voting_begin_date',
                           self.error_messages['voting_begin_date_invalid'])

    def validate_voting_end_date(self, cleaned_data):
        voting_end_date = cleaned_data.get('voting_end_date')
        voting_begin_date = cleaned_data.get('voting_begin_date', None)
        if voting_begin_date and voting_end_date <= voting_begin_date:
            self.add_error('voting_end_date',
                           self.error_messages['voting_end_date_invalid'])

    def validate_minute_publication_date(self, cleaned_data):
        minute_publication_date = cleaned_data.get('minute_publication_date')
        voting_end_date = cleaned_data.get('voting_end_date', None)
        if voting_end_date and minute_publication_date < voting_end_date:
            self.add_error(
                'minute_publication_date',
                self.error_messages['minute_publication_date_invalid'])

    def validate_minute_begin_date(self, cleaned_data):
        minute_begin_date = cleaned_data.get('minute_begin_date')
        voting_end_date = cleaned_data.get('voting_end_date', None)
        if voting_end_date and minute_begin_date < voting_end_date:
            self.add_error('minute_begin_date',
                           self.error_messages['minute_begin_date_invalid'])

    def validate_minute_end_date(self, cleaned_data):
        minute_end_date = cleaned_data.get('minute_end_date')
        minute_begin_date = cleaned_data.get('minute_begin_date', None)
        if minute_begin_date and minute_end_date <= minute_begin_date:
            self.add_error('minute_end_date',
                           self.error_messages['minute_end_date_invalid'])
