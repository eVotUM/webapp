# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db.models import Q

from evotemessages.decorators import recipients
from evotemessages.recipients import BaseRecipientsGroup

from .models import Category, Election


@recipients
class CategoryRecipientsGroup(BaseRecipientsGroup):
    """
    Group of voters of election category
    """
    model = Category
    prefix = _("Category:")
    namespace = "voters"

    def search(self, query, group_object_id, **kwargs):
        if not group_object_id:  # if no group provided, return empty
            return self.model.objects.none()
        return self.get_queryset()\
            .select_related('election')\
            .filter(Q(designation__icontains=query) |
                    Q(election__identifier__icontains=query),
                    election__electoral_process_id=group_object_id)\
            .order_by()

    def get_user_ids(self, obj):
        if obj:
            return obj.election_voters.active()\
                .values_list("user_id", flat=True).order_by()
        return []

    def get_group_object(self, obj):
        return obj.election.electoral_process

    def get_label(self, obj):
        return "{} ({})".format(obj.designation, obj.election)

    def check_permission(self, user):
        return getattr(user, "is_electoral_member", False)


@recipients
class ElectionRecipientsGroup(BaseRecipientsGroup):
    """"""
    model = Election
    prefix = _("Election:")
    namespace = "voters"

    def search(self, query, group_object_id, **kwargs):
        if not group_object_id:  # if no group provided, return empty
            return self.model.objects.none()
        return self.get_queryset()\
            .filter(identifier__icontains=query,
                    electoral_process_id=group_object_id)\
            .order_by()

    def get_user_ids(self, obj):
        if obj:
            return obj.election_voters.active()\
                .values_list("user_id", flat=True).order_by()
        return []

    def get_group_object(self, obj):
        return obj.electoral_process

    def check_permission(self, user):
        return getattr(user, "is_electoral_member", False)
