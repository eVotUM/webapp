# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotevotings.voters.models import ElectionVoter
from modeltranslation.admin import TabbedTranslationAdmin
from guardian.admin import GuardedModelAdmin
from django.contrib import admin

from .models import Election, Candidate, Category


class CategoryInline(admin.TabularInline):
    model = Category


class CandidateInline(admin.TabularInline):
    model = Candidate


class ElectionVoterInline(admin.TabularInline):
    model = ElectionVoter


class ElectionAdmin(GuardedModelAdmin, TabbedTranslationAdmin):
    list_display = ('identifier', 'state', 'write_in', 'ordered_voting',
                    'weight_votes')
    inlines = [
        CategoryInline,
        CandidateInline,
        ElectionVoterInline
    ]


admin.site.register(Election, ElectionAdmin)
