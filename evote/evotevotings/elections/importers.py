# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.utils.translation import ugettext_lazy as _
from django.apps import apps
from django.db import transaction

from evotecore.importers import BaseImporter


logger = logging.getLogger('evote.importers')


class CandidateImporter(BaseImporter):
    """ """
    field_names = ('personnel_number', 'designation')
    BaseImporter.error_messages.update({
        'unique_personnel_number': _("Personnel number most be unique")
    })

    def __init__(self, election, delimiter=None):
        """ """
        super(CandidateImporter, self).__init__(delimiter)
        self.election = election

    @transaction.atomic
    def run(self, document):
        """ """
        data = self.get_csv_data(document, True)
        logger.info("Election %d: importing candidates", self.election.pk)
        Candidate = apps.get_model("evoteelections.Candidate")  # noqa
        return Candidate.objects.bulk_import(self.election, data)

    def get_validator(self):
        """ """
        validator = super(CandidateImporter, self).get_validator()
        validator.add_unique_check(
            'personnel_number',
            message=self.error_messages['unique_personnel_number'])
        return validator
