# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.db.models.signals import post_save, post_init, pre_save
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import pgettext
from django.core.validators import MinValueValidator
from django.core.files.base import ContentFile
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django.conf import settings
from django.urls import reverse
from django.db import models


from django.contrib.contenttypes.models import ContentType
from evotevotings.elections.documents import (ElectionMinutePDF,
                                              ElectionVotersPDF)
from evotevotings.processes.models import ElectoralProcess
from evotevotings.services.routers import ASrouter, CSrouter
from django_extensions.db.models import TimeStampedModel
from evotesearch.decorators import searchable
from evotelogs.decorators import auditable
from evotecore.mixins import ModelFieldChangedMixin
from evotedocs.models import Document
from evotecore.utils import images_upload_to
from sorl.thumbnail import ImageField
from django_fsm import FSMIntegerField, transition
from autoslug import AutoSlugField

from .managers import ElectionQuerySet, CandidateManager, CategoryManager
from .tasks import (publish_provisional_roll, publish_definitive_roll,
                    begin_minute_approval, close_election, archive_election)
from .utils import initialize_blind_signatures, calc_archive_date


class ElectionCalendarMixin(models.Model):
    """Mixin with electoral calendar for code responsability separation
    proposes
    """
    proposition_begin_date = models.DateTimeField(
        _("begin date"))
    proposition_end_date = models.DateTimeField(
        _("end date"))
    complaint_begin_date = models.DateTimeField(
        _("complaint period - start"))
    complaint_end_date = models.DateTimeField(
        _("complaint period - end"))
    roll_begin_date = models.DateTimeField(
        _("publication of the provisional electoral roll"))
    roll_end_date = models.DateTimeField(
        _("publication of the definitive electoral roll"))
    voting_begin_date = models.DateTimeField(
        _("begin date"))
    voting_end_date = models.DateTimeField(
        _("end date"))
    minute_publication_date = models.DateTimeField(
        _("date of publication"))
    minute_begin_date = models.DateTimeField(
        _("approval period - start"))
    minute_end_date = models.DateTimeField(
        _("approval period - end"))

    class Meta:
        abstract = True

    @property
    def is_within_complaint_period(self):
        now = timezone.now()
        return bool(self.complaint_begin_date <= now and
                    self.complaint_end_date >= now)

    @property
    def is_complaint_period_ended(self):
        now = timezone.now()
        return bool(self.complaint_end_date <= now)

    @property
    def is_voting_ended(self):
        now = timezone.now()
        return bool(self.voting_end_date <= now)


class ElectionBulletinMixin(models.Model):
    """Mixin with bulletin info for code responsability separation proposes
    """

    class PresentationMode:
        LIST = 0
        DROPDOWN = 1

        choices = (
            (LIST, _("List")),
            (DROPDOWN, _("Dropdown")),
        )

    presentation_mode = models.PositiveSmallIntegerField(
        _("presentation mode"), choices=PresentationMode.choices,
        default=PresentationMode.LIST)
    instructions = models.CharField(_("instructions"), max_length=256,
                                    blank=True)

    class Meta:
        abstract = True

    def is_list_mode(self):
        return self.presentation_mode == self.PresentationMode.LIST

    def is_dropdown_mode(self):
        return self.presentation_mode == self.PresentationMode.DROPDOWN


class ElectionManageMixin(models.Model):
    """ """
    configs_filled = models.BooleanField(editable=False, default=False)
    calendar_filled = models.BooleanField(editable=False, default=False)
    bulletin_filled = models.BooleanField(editable=False, default=False)

    class Meta:
        abstract = True

    def can_manage_operations(self):
        """
        """
        # in days
        if self.voting_begin_date:
            days = settings.EVOTE_ELECTIONS.get('manage_operations_after')
            delta = timezone.now() + timezone.timedelta(days=days)
            return bool(delta >= self.voting_begin_date and
                        self.electoral_process.actions_configured)
        return False

    def can_manage_open(self):
        """Only can manage critical operation open if election state is in
        defenitive electoral roll state"""
        return self.state == self.State.DEFINITIVE_ELECTORAL_ROLL

    def can_manage_close(self):
        """Only can manage critical operation close if election state is in
        defenitive electoral roll or in to vote state"""
        return self.state == self.State.DEFINITIVE_ELECTORAL_ROLL or \
            self.state == self.State.IN_TO_VOTE

    def can_manage_counting(self):
        """Only can manage critical operation counting after election voting
        has finished"""
        return self.state >= self.State.IN_COUNTING

    def can_change(self):
        """Only can change election if state is lower than definitive electoral
        roll and electoral process actions has been configured"""
        return self.state <= self.State.DEFINITIVE_ELECTORAL_ROLL and \
            self.electoral_process.actions_configured

    def can_change_configs(self):
        """ """
        return self.can_change()

    def can_change_calendar(self):
        """ """
        return self.electoral_process.actions_configured

    def can_change_categories(self):
        """Only can change categories if election has weighted votes and the
        definitive electoral roll is not published yet."""
        return self.can_change() and self.weight_votes and \
            self.state < self.State.DEFINITIVE_ELECTORAL_ROLL

    def can_change_rolls(self):
        """ """
        return self.can_change() and \
            self.state < self.State.DEFINITIVE_ELECTORAL_ROLL

    def can_change_candidates(self):
        """Only can change candidates if period for the submission of
        applications have been reached
        """
        return self.can_change() and self.proposition_begin_date and \
            self.proposition_begin_date <= timezone.now()

    def can_change_bulletin(self):
        """Only can change bulletin info if election configurations has been
        filled and at least one candidate have been created"""
        return self.can_change() and self.configs_filled and \
            self.candidates.exists()


class ElectionCriptoMixin(models.Model):
    """ Mixin to extend the Election with criptography
        related fields"""
    certificate = models.TextField(_("election certificate"), null=True)
    cert_private_key = models.TextField(
        _("election certificate private key"), null=True)
    blindsig_pr_dash_components = models.TextField(
        _("blind signature pr dash components"),
        editable=False, default="")
    blindsig_init_components = models.TextField(
        _("blind signature init components"),
        editable=False, default="")

    class Meta:
        abstract = True


class ElectionStatesMixin(models.Model):

    class State:
        NEW = 0
        OPEN = 1
        PROVISIONAL_ELECTORAL_ROLL = 2
        DEFINITIVE_ELECTORAL_ROLL = 3
        IN_TO_VOTE = 4
        IN_COUNTING = 5
        VOTES_SCRUTINIZED = 6
        MINUTE_IN_APPROVAL = 7
        CLOSED = 8
        ARCHIVED = 9

        choices = (
            (NEW, pgettext("election", "New")),
            (OPEN, _("Open ")),
            (PROVISIONAL_ELECTORAL_ROLL, _("Provisional electoral roll")),
            (DEFINITIVE_ELECTORAL_ROLL,
                _("Definitive electoral roll published")),
            (IN_TO_VOTE, _("Voting time")),
            (IN_COUNTING, _("Voting time ended")),
            (VOTES_SCRUTINIZED, _("Voting results published")),
            (MINUTE_IN_APPROVAL, _("Minute in approval")),
            (CLOSED, _("Closed")),
            (ARCHIVED, _("Archived")),
        )

    state = FSMIntegerField(
        _("state"), choices=State.choices, default=State.NEW)

    @property
    def is_closed(self):
        """Check if the election is closed"""
        return self.state == self.State.CLOSED

    @property
    def is_in_definitive_eletoral_roll(self):
        return self.state == self.State.DEFINITIVE_ELECTORAL_ROLL

    @property
    def is_definitive_eletoral_roll(self):
        return self.state >= self.State.DEFINITIVE_ELECTORAL_ROLL

    @property
    def is_in_to_vote(self):
        return self.state == self.State.IN_TO_VOTE

    def can_publish(self):
        return self.electoral_process.can_publish()

    def can_publish_provisional_roll(self):
        return self.roll_begin_date <= timezone.now()

    def can_publish_definitive_roll(self):
        return self.roll_end_date <= timezone.now()

    def can_begin_voting(self):
        return self.voting_begin_date <= timezone.now()

    def can_end_voting(self):
        return self.voting_end_date <= timezone.now()

    def can_begin_approval(self):
        return self.minute_begin_date <= timezone.now()

    def can_close(self):
        return self.minute_end_date <= timezone.now()

    def can_archive(self):
        return calc_archive_date(self.minute_end_date) <= timezone.now()

    @transition(state, source=State.NEW, target=State.OPEN,
                conditions=[can_publish])
    def publish(self):
        """Publish the election accordingly to the electoral process
        publication date. This action is triggered by the electoral process
        """

    @transition(state, source=State.OPEN,
                target=State.PROVISIONAL_ELECTORAL_ROLL,
                conditions=[can_publish_provisional_roll])
    def publish_provisional_roll(self):
        """Transition only called when roll_begin_date is reached"""

    @transition(state, source=State.PROVISIONAL_ELECTORAL_ROLL,
                target=State.DEFINITIVE_ELECTORAL_ROLL,
                conditions=[can_publish_definitive_roll])
    def publish_definitive_roll(self):
        """Transition only called when roll_end_date is reached"""
        # Get the final electoral roll cardinality
        voters = self.election_voters.active().count()
        # Connect to the Anonymizer Service
        ASrouter().initialize_election(self.pk, voters)

    @transition(state, source=State.DEFINITIVE_ELECTORAL_ROLL,
                target=State.IN_TO_VOTE, conditions=[can_begin_voting])
    def voting(self):
        """Transition to a in to vote state"""

    @transition(state, source=State.IN_TO_VOTE, target=State.IN_COUNTING,
                conditions=[can_end_voting])
    def counting(self):
        """Transition to a in counting state"""

    @transition(state, source=State.IN_COUNTING,
                target=State.VOTES_SCRUTINIZED)
    def scrutinized(self):
        """Transition to votes scrutinized"""

    @transition(state, source=State.VOTES_SCRUTINIZED,
                target=State.MINUTE_IN_APPROVAL,
                conditions=[can_begin_approval])
    def begin_approval(self):
        """Transition to minute in approval"""

    @transition(state, source=State.MINUTE_IN_APPROVAL, target=State.CLOSED,
                conditions=[can_close])
    def close(self):
        """Transition when minute_end_date is reached"""
        archive_date = calc_archive_date(self.minute_end_date)
        archive_election.si(self.pk).apply_async(eta=archive_date)

    @transition(state, source=State.CLOSED, target=State.ARCHIVED,
                conditions=[can_archive])
    def archive(self):
        """Transition when minute_end_date is reached"""
        ASrouter().close_election(self.pk)
        CSrouter().clean_results(self.pk)

    class Meta:
        abstract = True


@auditable
@searchable(title='identifier', content='description',
            extra={'electoral_process__is_test': False})
@python_2_unicode_compatible
class Election(
        ModelFieldChangedMixin, ElectionBulletinMixin, ElectionManageMixin,
        ElectionCalendarMixin, ElectionCriptoMixin, ElectionStatesMixin,
        TimeStampedModel):
    """ """
    REMARKABLE_CHOICES = [(n, n) for n in range(1, 21)]

    electoral_process = models.ForeignKey(
        ElectoralProcess, verbose_name=_("electoral process"),
        on_delete=models.CASCADE, related_name='elections')
    identifier = models.CharField(
        _("election identifier"), max_length=128,
        help_text=_("Insert the election designation"))
    slug = AutoSlugField(
        populate_from='identifier', unique=True, editable=False)
    description = models.CharField(
        _("election description"), max_length=512,
        help_text=_("brief description text about election"))
    limit_remarkable_choices = models.PositiveSmallIntegerField(
        _("maximum number of candidates to choose on the ballot"),
        choices=REMARKABLE_CHOICES, default=1)
    write_in = models.BooleanField(_("ballot with write-in?"), default=True)
    write_in_import = models.ForeignKey(
        Document, verbose_name=_("import file"), related_name="+", null=True)
    ordered_voting = models.BooleanField(
        _("ballot allows to sort candidates?"), default=True)
    weight_votes = models.BooleanField(
        _("Votes can have different weights?"), default=True)
    hondt_method = models.BooleanField(
        _("Use Hondt method?"), default=True)
    hondt_total_seats = models.PositiveSmallIntegerField(
        _("hondt total seats"), validators=[MinValueValidator(1)], null=True,
        blank=True)
    pdf_document = models.ForeignKey(
        Document, verbose_name=_("PDF document"), related_name="+",
        null=True, default=None)
    pdf_voters_document = models.ForeignKey(
        Document, verbose_name=_("PDF document of voters"), related_name="+",
        null=True, default=None)

    objects = ElectionQuerySet.as_manager()

    class Meta:
        verbose_name = _("election")
        verbose_name_plural = _("elections")
        permissions = (
            ('vote_election', 'Vote election'),
            ('view_election', 'View election'),
            # change_election already exists
        )

    def __str__(self):
        return self.identifier

    def get_absolute_url(self):
        return reverse('voters:electoralprocess-detail', kwargs={
            'slug': self.electoral_process.slug
        })

    @property
    def audit_group(self):
        elec_proc = self.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return group_type.id, elec_proc.pk

    def get_last_published_roll(self):
        """ Returns the electoral roll published of election """
        return self.electoral_rolls.published(self.pk)

    def generate_pdf(self, total, total_weight, data, extra_info):
        """ Generates the minute PDF """
        pdf = ElectionMinutePDF(
            self, total, total_weight, data, extra_info).render()
        file = Document(designation='election_minute')
        file_name = "{}.pdf".format(self.identifier_en)
        file.document.save(file_name, ContentFile(pdf), False)
        file.save()
        self.pdf_document = file
        self.save(update_fields=[
            'pdf_document', 'pdf_document_en', 'pdf_document_pt'])

    def generate_voters_pdf(self):
        """ Generates the voters list PDF """
        pdf = ElectionVotersPDF(self).render()
        file = Document(designation='election_voters')
        file_name = "{}_voters.pdf".format(self.identifier_en)
        file.document.save(file_name, ContentFile(pdf), False)
        file.save()
        self.pdf_voters_document = file
        self.save(update_fields=[
            'pdf_voters_document', 'pdf_voters_document_en',
            'pdf_voters_document_pt'])

    @staticmethod
    def post_init(sender, instance, **kwargs):
        """Callback staticmethod for post_init signal"""
        instance.track_changes([
            'roll_begin_date', 'roll_end_date', 'minute_begin_date',
            'minute_end_date'])

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        """Callback staticmethod for pre_save signal"""
        # On creating the object, initialize
        # the blind signature components
        if instance._state.adding is True:
            # Creating the object
            (init_component, pr_component) = initialize_blind_signatures()
            instance.blindsig_init_components = init_component
            instance.blindsig_pr_dash_components = pr_component

    @staticmethod
    def post_save_changes(sender, instance, **kwargs):
        """Callback staticmethod for post_save signal"""
        if instance.has_changed('roll_begin_date'):
            publish_provisional_roll.si(instance.pk)\
                .apply_async(eta=instance.roll_begin_date)
        if instance.has_changed('roll_end_date'):
            publish_definitive_roll.si(instance.pk)\
                .apply_async(eta=instance.roll_end_date)
        if instance.has_changed('minute_begin_date'):
            begin_minute_approval.si(instance.pk)\
                .apply_async(eta=instance.minute_begin_date)
        if instance.has_changed('minute_end_date'):
            close_election.si(instance.pk)\
                .apply_async(eta=instance.minute_end_date)

    @staticmethod
    def post_save_create_category(sender, instance, **kwargs):
        """create a default category"""
        if kwargs.get('created'):
            # TODO: pass designations to settings.
            instance.categories.create(
                designation_en="One", designation_pt="Um", default=True)


post_init.connect(Election.post_init, sender=Election)
pre_save.connect(Election.pre_save, sender=Election)
post_save.connect(Election.post_save_changes, sender=Election)
post_save.connect(Election.post_save_create_category, sender=Election)


@python_2_unicode_compatible
class Category(TimeStampedModel):
    """ """
    election = models.ForeignKey(
        Election, verbose_name=_("election"), related_name="categories",
        on_delete=models.CASCADE)
    designation = models.CharField(_("category designation"), max_length=128)
    slug = AutoSlugField(
        populate_from='designation', editable=False)
    weight = models.PositiveSmallIntegerField(
        _("category weight"), validators=[MinValueValidator(1)], default=1)
    default = models.BooleanField(
        _('is default?'), default=False, editable=False)
    objects = CategoryManager()

    class Meta:
        unique_together = ('election', 'designation')
        verbose_name = _("category")
        verbose_name_plural = _("categories")

    def __str__(self):
        return str(self.weight)


@auditable
@python_2_unicode_compatible
class Candidate(TimeStampedModel):
    """ """
    election = models.ForeignKey(
        Election, verbose_name=_("election"), related_name="candidates",
        on_delete=models.CASCADE)
    logo = ImageField(_("logo"), upload_to=images_upload_to, blank=True)
    personnel_number = models.CharField(_('personnel number'), max_length=64,
                                        db_index=True)
    designation = models.CharField(_("designation"), max_length=128)
    documents = models.ManyToManyField(Document, verbose_name=_("documents"),
                                       blank=True)
    write_in = models.BooleanField(default=False)
    order = models.PositiveSmallIntegerField(_("order"), default=0)
    objects = CandidateManager()

    class Meta:
        ordering = ("order",)
        verbose_name = _("candidate")
        verbose_name_plural = _("candidates")

    def __str__(self):
        return self.designation

    @property
    def audit_group(self):
        elec_proc = self.election.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)


@auditable
@python_2_unicode_compatible
class ElectionCountQuery(TimeStampedModel):
    """ """
    election = models.ForeignKey(
        Election, verbose_name=_("election"), related_name="count_queries",
        on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("user"),
        on_delete=models.CASCADE, related_name="+")
    total_count = models.PositiveSmallIntegerField(
        _("# Ballot papers"), null=True)

    class Meta:
        ordering = ("-created",)
        verbose_name = _("Count query")
        verbose_name_plural = _("Count queries")

    def __str__(self):
        return "{} ({}) - {}".format(self.election, self.user,
                                     self.total_count)

    @property
    def audit_group(self):
        elec_proc = self.election.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)
