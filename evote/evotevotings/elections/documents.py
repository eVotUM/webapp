# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.db.models import F
from django.utils import timezone, formats
from django.apps import apps

from reportlab.platypus import Indenter, Table, TableStyle
from reportlab.lib import colors

from evotedocs.documents import PdfDocument


class BaseElectionPdf(PdfDocument):

    def __init__(self, election):
        super(BaseElectionPdf, self).__init__()
        self.election = election

    def votes_table(self, data):
        """ Returns table with styles of votes counting """
        table_style = TableStyle()
        data, widths = self.add_col_space(data, col_widths=[150, 150, 150])
        table_style.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
        table_style.add('ALIGN', (0, 0), (-1, -1), 'CENTER')
        table_style.add('BOTTOMPADDING', (0, 0), (-1, 0), 5)
        table_style.add('FONTSIZE', (0, 1), (-1, -1), 15)
        table_style.add('LEADING', (0, 1), (-1, -1), 18)
        table_style.add('COLBACKGROUNDS', (0, 1), (-1, -1),
                        (colors.HexColor(0xeeeeee), None))

        table = Table(data, spaceBefore=17, spaceAfter=17, colWidths=widths)
        table.setStyle(table_style)
        return table


class ElectionMinutePDF(BaseElectionPdf):

    def __init__(self, election, total_votes, total_votes_weight, data, extra):
        super(ElectionMinutePDF, self).__init__(election)
        self.total_votes = total_votes
        self.total_votes_weight = total_votes_weight
        self.data = data
        self.extra = extra

    def set_story(self):
        now = formats.date_format(timezone.now(), 'DATETIME_FORMAT')
        self.story.append(self.title(self.get_title()))
        self.add_field_description(
            _("Electoral process"), str(self.election.electoral_process))
        self.add_field_description(_("Election"), str(self.election))
        self.add_field_description(_("Date"), now)
        self.story.append(self.space(height=20))

        self.story.append(Indenter(-15, 0))
        self.story.append(self.hr())
        self.story.append(Indenter(0, 0))

        self.add_votes_count()

        self.story.append(Indenter(15, 0))
        self.story.append(self.space(height=5))
        self.add_table(_("Results"), self.get_results_table())
        self.story.append(self.space(height=20))
        self.add_table(_("Adicional info"), self.get_results_extra_table(), 30)

    def add_field_description(self, label, value):
        """ Adds the block of pure-sec-title and field """
        self.story.append(self.pure_sec_title(label))
        self.story.append(self.space(height=5))
        self.story.append(Indenter(15, 0))
        self.story.append(self.body(value))
        self.story.append(Indenter(-15, 0))

    def add_table(self, label, table, space=10):
        """ Adds the block of pure-sec-title and table """
        self.story.append(self.pure_sec_title(label))
        self.story.append(self.space(height=space))
        self.story.append(Indenter(0, 15))
        self.story.append(table)
        self.story.append(Indenter(0, -15))

    def add_votes_count(self):
        """ Adds table with count of votes """
        if self.election.weight_votes:
            self.story.append(self.votes_table(
                [[_('Total votes with weight:'),
                  _('Total votes without weight:')],
                 [self.total_votes_weight, self.total_votes]]
            ))
        else:
            self.story.append(self.votes_table(
                [[_('Total votes in ballot box:')], [self.total_votes]]
            ))

    def get_results_table(self):
        """ Returns the table of results """
        table = []
        table.append([
            _('Candidate/List of candidates'),
            _('Votes'),
            _('Percentage')
        ])

        col_percs = [0.65, 0.15, 0.20]
        if self.election.hondt_method:
            table[0].append(_('Seats number'))
            col_percs = [0.45, 0.15, 0.20, 0.20]

        for elem in self.data:
            cid = _('Blank votes')
            if elem['cid'] != 'blank':
                Candidate = apps.get_model("evoteelections.Candidate")  # noqa
                cid = Candidate.objects.get(pk=elem['cid'])
            line = [
                self.tdcell(str(cid)),
                str(elem['total']),
                "{0:.2f}%".format(elem['perc']),
            ]
            if self.election.hondt_method:
                line.append(str(elem['seats']))
            table.append(line)
        return self.long_table(table, col_percs=col_percs)

    def get_results_extra_table(self):
        """ Returns the table of extra info """
        table = []
        table.append([
            self.thcell(_('Invalid Ballot Paper')),
            self.thcell(_('Submitted Ballot Paper')),
            self.thcell(_('Ballot papers with incomplete submission')),
            self.thcell(_('Voters that have not voted'))
        ])
        table.append([
            str(self.extra['invalid']),
            str(self.extra['submitted']),
            str(self.extra['submitting']),
            str(self.extra['unsubmitted'])
        ])
        return self.long_table(table, col_percs=[0.25, 0.25, 0.25, 0.25])

    def get_title(self):
        return _("{} - Election minute").format(self.election)


class ElectionVotersPDF(BaseElectionPdf):

    def set_story(self):
        self.story.append(self.title(self.get_title()))
        self.story.append(self.hr(space_after=20))
        self.story.append(self.get_voters_table())

    def get_title(self):
        return _("{} - Election voters list").format(self.election)

    def get_voters_table(self):
        """ """
        table = []
        table.append([_('Name'), _('Number'), _('Status')])
        for elem in self.election.election_voters.active().annotate(
                username=F("user__username")):
            table.append([
                self.tdcell(elem.name),
                elem.username,
                self.get_state_label(elem.vote_status)
            ])
        return self.long_table(table, col_percs=[0.65, 0.12, 0.23])

    def get_state_label(self, status):
        """ Returns label for state """
        if status == 2:
            return _("Didn't submitted vote")
        elif status == 1:
            return _("Voted")
        return _("Didn't voted")


class ElectionCountingsPdf(BaseElectionPdf):

    def set_story(self):
        total_count = 0
        last_count = self.election.count_queries.first()
        if last_count:
            total_count = last_count.total_count
        self.story.append(self.title(self.get_title()))
        self.story.append(self.hr(space_after=20))
        self.story.append(self.votes_table(
            [[_('Number of ballot papers in ballot box:')], [total_count]]))
        self.story.append(self.get_ballot_paper_table())

    def get_ballot_paper_table(self):
        table = []
        table.append([_('Requested By'), _('Date'), _('# Ballot papers')])
        for elem in self.election.count_queries.all():
            table.append([
                self.tdcell(elem.user.name),
                self.get_formated_date(elem.created),
                str(elem.total_count)
            ])
        return self.long_table(table, col_percs=[0.55, 0.27, 0.18])

    def set_footer(self):
        self.footer.append(self.hr(space_after=0))
        now = timezone.now().strftime('%d-%M-%Y %H:%M:%S')
        text = "{}: {}".format(_('Generated in'), now)
        self.footer.append(self.body(text))

    def get_title(self):
        return "{} - {}".format(
            self.election, _("Total ballot papers in ballot box"))
