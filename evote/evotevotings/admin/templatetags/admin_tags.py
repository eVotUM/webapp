# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals
from django import template
from evotevotings.admin.models import SecretEntry, Page


register = template.Library()

# This PK is associated with the fixture of pages (pages.json) and related to
# the page that stores the global messages
GLOBAL_ALERT_PAGE_PK = 10


@register.simple_tag
def certificate_identifier(value):
    """
        This tag receives an integer identifier and returns its counterpart
        string to the template
    """
    identifier = None

    if any(value in key for key in SecretEntry.Identifier.choices):
        identifier = SecretEntry.Identifier.choices[value][1]

    return identifier


@register.assignment_tag(takes_context=False)
def is_private_key(value):
    """
        This tag receives a certificate field identifier and returns
        a boolean depending on if that value is or not a private key
    """
    return any(value in key for key in SecretEntry.Identifier.private_keys)


@register.inclusion_tag('evoteadmin/includes/global_alert.html')
def global_alert():
    obj = Page.objects.filter(pk=GLOBAL_ALERT_PAGE_PK).first()
    if obj:
        return {'message': obj.body}
    return {'message': None}
