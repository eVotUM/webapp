# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.contrib import admin

from modeltranslation.admin import TabbedTranslationAdmin

from .models import SecretEntry, Page


class SecretEntryAdmin(admin.ModelAdmin):
    model = SecretEntry

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class PageAdmin(TabbedTranslationAdmin):
    model = Page

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


admin.site.register(SecretEntry, SecretEntryAdmin)
admin.site.register(Page, PageAdmin)
