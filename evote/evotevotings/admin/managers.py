# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.db import models


logger = logging.getLogger('evote.crypto')


class SecretEntryManager(models.Manager):
    """ """

    def check_integrity(self):
        """Checks if all entries are submitted"""
        valid = True
        identifier_list = [a for (a, b) in self.model.Identifier.choices]
        identifier_names = [b for (a, b) in self.model.Identifier.choices]
        entries = self.filter(identifier__in=identifier_list)\
            .only('identifier', 'body')
        if not entries.exists() or entries.count() != len(identifier_list):
            valid = False
        for entry in entries:
            if entry.body is None or entry.body == '':
                valid = False
                logger.error("Missing secret entry=%s",
                             identifier_names[entry.identifier])
        return valid
