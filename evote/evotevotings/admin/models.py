# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings
from django.urls import reverse
from django.db import models

from django_extensions.db.models import TimeStampedModel
from evotesearch.decorators import searchable
from autoslug import AutoSlugField

from .managers import SecretEntryManager


@python_2_unicode_compatible
class SecretEntry(TimeStampedModel):
    """ """
    class Identifier:

        ROOT_CERT = 0
        ROOT_PVT_KEY = 1
        VALIDATOR_CERT = 2
        VALIDATOR_PVT_KEY = 3
        COUNTER_CERT = 4
        COUNTER_PVT_KEY = 5
        VOTING_CERT = 6
        VOTING_PVT_KEY = 7
        MANAGEMENT_CERT = 8
        MANAGEMENT_PVT_KEY = 9

        private_keys = (
            (VOTING_PVT_KEY, _("Voting System Private key")),
            (VALIDATOR_PVT_KEY, _("Validator Service Private key")),
            (COUNTER_PVT_KEY, _("Counter Service Private key")),
            (MANAGEMENT_PVT_KEY, _("Election Management System Private key"))
        )

        certificates = (
            (VOTING_CERT, _("Voting System Certificate")),
            (VALIDATOR_CERT, _("Validator Service Certificate")),
            (COUNTER_CERT, _("Counter Service Certificate")),
            (ROOT_CERT, _("Root self-signed Certificate")),
            (ROOT_PVT_KEY, _("Root Private key")),
            (MANAGEMENT_CERT, _("Election Management System Certificate")),
        )

        pairs = [
            (ROOT_CERT, ROOT_PVT_KEY),
            (VOTING_CERT, VOTING_PVT_KEY),
            (VALIDATOR_CERT, VALIDATOR_PVT_KEY),
            (COUNTER_CERT, COUNTER_PVT_KEY),
            (MANAGEMENT_CERT, MANAGEMENT_PVT_KEY),
        ]

        # Sorted flat list of options
        choices = sorted(private_keys + certificates, key=lambda x: x[0])

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='+', editable=False)
    identifier = models.PositiveSmallIntegerField(
        _("identifier"), choices=Identifier.choices, unique=False)

    body = models.TextField(
        _("body"),
        blank=True,
        null=True
    )

    objects = SecretEntryManager()

    class Meta:
        verbose_name = _("Secret entry")
        verbose_name_plural = _("Secret entries")
        ordering = ['identifier']

    def __str__(self):
        return self.get_identifier_display()

    @property
    def is_valid(self):
        return self.body is not None and self.body.strip() != ""


@searchable(title='title', content='body')
@python_2_unicode_compatible
class Page(TimeStampedModel):
    """
    The class responsible for modeling the Page representation
    """
    title = models.CharField(_("title"), max_length=128)
    slug = AutoSlugField(
        populate_from='title_en',
        unique=True,
        editable=False
    )
    body = models.TextField(_("body"), blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='+',
        editable=False,
        verbose_name=_("User"),
        on_delete=models.CASCADE,
        null=True
    )

    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse(
            'voters:page-detail', kwargs={'slug': self.slug}
        )
