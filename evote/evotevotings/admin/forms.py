# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext_lazy as _

from evoteusers.forms import EvoteAuthenticationForm

from eVotUM.Cripto import pkiutils, cautils, eccblind

from .models import Page, SecretEntry
from .templatetags.admin_tags import is_private_key
from .utils import get_private_key_password
from evotecore.widgets import TinymceTextarea


class AdminAuthenticationForm(EvoteAuthenticationForm):
    """Admin user must have is_staff=True"""

    def confirm_login_allowed(self, user, cleaned_data=None):
        super(AdminAuthenticationForm, self).confirm_login_allowed(user)
        if not user.is_staff:
            raise forms.ValidationError(
                self.error_messages['invalid_login'],
                code='invalid_login',
            )


class PageForm(forms.ModelForm):
    """ """

    class Meta:
        model = Page
        fields = ['title_pt', 'title_en', 'body_pt', 'body_en']
        widgets = {
            'body_pt': TinymceTextarea,
            'body_en': TinymceTextarea
        }


class SecretEntryForm(forms.ModelForm):
    """
    The form for a single certificate entry
    """
    document = forms.FileField(required=False)
    password = forms.CharField(
        label="password",
        widget=forms.PasswordInput(
            attrs={
                'placeholder': _(u'Insert the private key password')
            }
        ),
        required=False
    )

    class Meta:
        model = SecretEntry
        fields = ('identifier', 'body', 'password')

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(SecretEntryForm, self).__init__(*args, **kwargs)
        # Making identifier disabled
        self.fields['identifier'].disabled = True

    def clean_body(self):

        body = self.cleaned_data.get('body', None)

        # Different behaviour depending on being on a formset or a simple form
        if self.prefix:
            document_key = self.prefix + '-document'
        else:
            document_key = 'document'

        document = self.files.get(document_key, None)

        if document is not None:
            body = document.read()

        return body

    def clean(self):
        cleaned_data = super(SecretEntryForm, self).clean()
        password = cleaned_data.get('password', None)
        identifier = cleaned_data.get('identifier', None)
        pvt_key_pem = cleaned_data.get('body', None)
        # Only validate this field if we are dealing with a private key
        if identifier and is_private_key(identifier) and pvt_key_pem:
            (valid, key_pair) = cautils.pemPrivateKeyToPKey(
                str(pvt_key_pem),
                str(password)
            )
            if valid is not None:
                self.add_error('password', _("Invalid private key password"))
            else:
                (error, new_pvt_key) = cautils.pKeyToPEMPrivateKey(
                    key_pair,
                    get_private_key_password()
                )
                if error is None:
                    cleaned_data['body'] = new_pvt_key

        return cleaned_data

    def save(self, commit=True):
        """ Overriding the ModelForm save method """
        # Only save if a file was submitted
        if not self.cleaned_data['body']:
            commit = False

        return super(SecretEntryForm, self).save(commit)


class SecretEntryBaseFormSet(forms.BaseModelFormSet):
    def clean(self):
        super(SecretEntryBaseFormSet, self).clean()
        # Process the root certificate
        # Defaults to the BD record
        root_cert_choice = SecretEntry.Identifier.ROOT_CERT
        root_pvt_choice = SecretEntry.Identifier.ROOT_PVT_KEY
        certificates = {}
        private_keys = {}
        # Validate remaining certificates
        for form in self.forms:
            entry_identifier = form.cleaned_data.get('identifier', None)
            # Get the updated entry body
            entry_body = self.get_updated_entry(
                entry_identifier,
                form.cleaned_data
            )
            # Process certificates only
            if is_private_key(entry_identifier):
                # Append to the list of private keys
                private_keys[entry_identifier] = {
                    'body': entry_body,
                    'form': form,
                }
                continue

            # Skip for the root private key
            if entry_identifier == root_pvt_choice:
                continue

            # Special case for the root certificate
            if entry_identifier == root_cert_choice:
                root_cert = entry_body
                if not root_cert:
                    form.add_error(
                        'document',
                        _("No Root certificate uploaded")
                    )
            else:
                if entry_body is not None:
                    # Append to the list of certificate to validate chain
                    certificates[entry_identifier] = {
                        'body': entry_body,
                        'form': form,
                    }

        # If there is no Root certificate abort
        if not root_cert:
            return False

        global_valid = True
        # Validate certificates
        for cert_identifier, cert_dict in certificates.iteritems():
            # Get the corresponding pair
            pvt_identifier = self.get_entry_pair(cert_identifier)
            key_dict = private_keys[pvt_identifier]
            key_body = key_dict.get('body', None)
            key_form = key_dict.get('form')
            valid = False
            # If no private key was submitted to
            if not key_body:
                key_form.add_error(
                    'document',
                    _("Must fill private key for certificate")
                )
            else:
                # Try to validate the certificate/key pair
                cert_body = cert_dict.get('body')
                cert_form = cert_dict.get('form')
                # Sign dummy object using the private key
                pvt_key_pass = get_private_key_password()
                object_to_sign = cert_body
                if cert_identifier ==\
                   SecretEntry.Identifier.VALIDATOR_CERT:
                    (prc, initc) = eccblind.initSigner()
                    (err, blindM) = eccblind.blindData(
                        prc,
                        str(object_to_sign)
                    )
                    (err, blind_sig) = eccblind.generateBlindSignature(
                        key_body,
                        pvt_key_pass,
                        blindM[2],
                        initc
                    )
                    (err, signature) = eccblind.unblindSignature(
                        blind_sig,
                        prc,
                        blindM[0]
                    )
                    (err, verify) = eccblind.verifySignature(
                        cert_body,
                        signature,
                        blindM[0],
                        blindM[1],
                        str(object_to_sign)
                    )
                    if err is None:
                        valid = True
                else:
                    (err, signed_obj) = pkiutils.signObject(
                        object_to_sign,
                        key_body,
                        pvt_key_pass
                    )
                    # Verify object using the certificate
                    (err, obj) = pkiutils.verifyObjectSignature(
                        signed_obj,
                        cert_body
                    )
                    if err is None and obj == object_to_sign:
                        valid = True

                if valid:
                    # Validate certificate chain
                    chain = pkiutils.verifyCertificateChain(
                        cert_body,
                        root_cert
                    )
                    if not chain:
                        valid = False

                if not valid:
                    error_msg = _(
                        "Certificate and Private key pair is invalid"
                    )
                    cert_form.add_error(
                        'document',
                        error_msg
                    )
                    key_form.add_error(
                        'document',
                        error_msg
                    )

            global_valid &= valid

        return global_valid

    def get_entry_pair(self, identifier):
        entry_pair = False
        for (cert, pvt_key) in SecretEntry.Identifier.pairs:
            if identifier == cert:
                entry_pair = pvt_key
            if identifier == pvt_key:
                entry_pair = cert
        return entry_pair

    def get_updated_entry(self, identifier, clean_data):
        # Try to get the entry body from the submitted form
        entry_body = clean_data.get('body', None)
        if entry_body:
            return entry_body
        # Otherwise get from the Database
        try:
            entry = SecretEntry.objects.get(identifier=identifier)
            entry_body = entry.body
        except SecretEntry.DoesNotExist:
            entry_body = None

        return entry_body


SecretEntryFormset = forms.modelformset_factory(
    SecretEntry,
    extra=0,
    formset=SecretEntryBaseFormSet,
    form=SecretEntryForm,
    min_num=len(SecretEntry.Identifier.choices),
    max_num=len(SecretEntry.Identifier.choices),
)
