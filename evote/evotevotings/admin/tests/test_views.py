# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from django.conf import settings

from evotevotings.admin.models import SecretEntry
from evotevotings.admin.forms import (AdminAuthenticationForm,
                                      SecretEntryFormset)


from .utils import PageTestUtils
from . import AdminTestCase


class LoginViewTest(AdminTestCase):
    """ """

    def setUp(self):
        self.credentials = {
            "username": "dev",
            "password": "password"
        }
        self.user = self.create_user(is_staff=True, **self.credentials)

    def test_login_view(self):
        """ """
        response = self.client.get(reverse('evoteadmin:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'evoteadmin/login.html')

    def test_login_staff_user(self):
        """ """
        response = self.client.post(reverse('evoteadmin:login'), follow=True,
                                    data=self.credentials)
        self.assertRedirects(response, reverse('evoteadmin:page-list'))
        self.assertEqual(response.context[0]['user'], self.user)

    def test_login_non_staff_user(self):
        """ """
        self.user.is_staff = False
        self.user.save()
        response = self.client.post(reverse('evoteadmin:login'), follow=True,
                                    data=self.credentials)
        error_message = AdminAuthenticationForm.error_messages['invalid_login']
        self.assertFormError(response, "form", None, error_message)

    def test_login_with_wrong_credentials(self):
        self.credentials['password'] = "wrong_password"
        response = self.client.post(reverse('evoteadmin:login'),
                                    data=self.credentials)
        self.assertFalse(response.context['form'].is_valid())
        self.assertIn('__all__', response.context['form'].errors)


class LogoutViewTest(AdminTestCase):

    def setUp(self):
        # prepare a logged in voter
        self.login_as_admin()

    def test_logout_view(self):
        """ """
        response = self.client.get(reverse('evoteadmin:logout'), follow=True)
        self.assertRedirects(response, settings.LOGIN_URL)
        self.assertIsInstance(response.context[0]['user'], AnonymousUser)


class PageListViewTest(AdminTestCase):

    def setUp(self):
        self.login_as_admin()
        self.url = reverse("evoteadmin:page-list")

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            "evoteadmin/page_list.html"
        )

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class PageDetailViewTest(PageTestUtils, AdminTestCase):

    def setUp(self):
        self.login_as_admin()
        instance = self.create_page(
            created_by=self.user
        )
        instance.save()
        self.url = reverse(
            "evoteadmin:page-update",
            kwargs={'slug': instance.slug}
        )

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            "evoteadmin/page_update.html"
        )

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class SecretEntryFormSetViewTest(AdminTestCase):

    fixtures = ['certs']

    def setUp(self):
        self.login_as_admin()
        self.url = reverse(
            "evoteadmin:secretentry"
        )

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response,
            "evoteadmin/secretentry.html"
        )

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_formset_forms_number(self):
        self.assertEqual(
            SecretEntryFormset.__dict__['max_num'],
            len(SecretEntry.Identifier.choices)
        )
