# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from StringIO import StringIO
from evotevotings.admin.forms import AdminAuthenticationForm

from . import AdminTestCase

from evotevotings.admin.forms import PageForm, SecretEntryForm


class AdminAuthenticationFormTest(AdminTestCase):
    """ """

    def setUp(self):
        self.credentials = {
            'username': 'member',
            'password': 'password'
        }
        self.user = self.create_user(is_staff=True, **self.credentials)

    def test_admin_user(self):
        form = AdminAuthenticationForm(data=self.credentials)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.get_user(), self.user)

    def test_non_admin_user(self):
        self.user.is_staff = False
        self.user.save()
        form = AdminAuthenticationForm(data=self.credentials)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors.values()[0][0], form.error_messages['invalid_login'])


class PageFormTest(AdminTestCase):
    """ """

    def setUp(self):
        self.data = {
            'title_pt': 'Teste Titulo',
            'title_en': 'Test Title',
            'body_pt': 'Teste corpo de texto',
            'body_en': 'Content body test'
        }

    def test_page_form(self):
        form = PageForm(data=self.data)
        self.assertTrue(form.is_valid())
        self.assertEqual(
            form.cleaned_data['title_pt'],
            self.data['title_pt']
        )

    def test_invalid_page_form(self):
        invalid_data = self.data
        invalid_data['title_pt'] = ''
        form = PageForm(invalid_data)
        self.assertFalse(form.is_valid())


class SecretEntryFormTest(AdminTestCase):

    fixtures = ['certs']

    def setUp(self):
        self.credentials = {
            'username': 'member',
            'password': 'password'
        }
        self.user = self.create_user(is_staff=True, **self.credentials)

        self.invalid_data = {
            'identifier': 1,
            'document': StringIO(
                'This is a certificate file mock!'
            ),
            'body': 'This is a certificate file mock!'
        }

    def test_disabled_identifier(self):
        form = SecretEntryForm()
        self.assertTrue(form.fields['identifier'].disabled)

    def test_invalid_form(self):
        form = SecretEntryForm(data=self.invalid_data)
        self.assertFalse(form.is_valid())

    def test_invalid_form_with_prefix(self):
        form = SecretEntryForm(data=self.invalid_data, prefix="test")
        self.assertFalse(form.is_valid())
