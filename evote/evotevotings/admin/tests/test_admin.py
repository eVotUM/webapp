# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from mock import Mock

from django.contrib.admin.sites import AdminSite

from evotevotings.admin.models import SecretEntry
from evotevotings.admin.admin import SecretEntryAdmin

from . import AdminTestCase


class SecretEntryAdminTest(AdminTestCase):
    """ """
    def setUp(self):
        self.site = AdminSite()

    def test_save_model_with_user(self):
        request = Mock()
        request.user = self.create_user()
        secret_entry = SecretEntry(identifier=0, body="dummy body")
        admin = SecretEntryAdmin(SecretEntry, self.site)
        admin.save_model(request, secret_entry, None, None)
        self.assertEqual(secret_entry.user, request.user)
