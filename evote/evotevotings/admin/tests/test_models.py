# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from unittest import skip

from django.core.urlresolvers import reverse
from django.test import SimpleTestCase
from . import AdminTestCase
from .utils import PageTestUtils, SecretEntryTestUtils
from evotevotings.admin.models import SecretEntry


class SecretEntryTest(SecretEntryTestUtils, SimpleTestCase):

    def test_str_equal_to_identifier(self):
        entry = SecretEntry(
            identifier=1,
            body=None
        )
        self.assertEqual(
            str(entry),
            entry.get_identifier_display()
        )

    def test_invalid_certificate(self):
        entry = SecretEntry(
            identifier=1,
            body=None
        )
        self.assertFalse(bool(entry.is_valid))


class PageTest(PageTestUtils, AdminTestCase):

    def setUp(self):
        self.credentials = {
            "username": "dev",
            "password": "password"
        }
        self.user = self.create_user(is_staff=True, **self.credentials)
        self.instance = self.create_page(created_by=self.user)
        self.client.post(reverse('evoteadmin:login'), follow=True,
                         data=self.credentials)

    def test_str_equal_to_title(self):
        self.assertEqual(str(self.instance), self.instance.title)

    @skip("Trying to call an unregistered namespace for this app")
    def test_get_absolute_url(self):
        response = self.client.get(self.instance.get_absolute_url())
        self.assertEqual(response.status_code, 200)
