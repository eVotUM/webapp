# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.urlresolvers import reverse_lazy
from django.test import TestCase, override_settings

from evoteusers.tests.utils import UserTestUtils


@override_settings(
    ROOT_URLCONF='evote.urls.admin',
    LOGIN_REDIRECT_URL=reverse_lazy("evoteadmin:page-list"),
    LOGIN_URL=reverse_lazy("evoteadmin:login"),
    JAVASCRIPT_SETTINGS_SCAN_MODULES={}
)
class AdminTestCase(UserTestUtils, TestCase):

    def login_as_admin(self):
        credentials = {
            "username": "dev1",
            "password": "password"
        }
        self.user = self.create_user(is_staff=True, **credentials)
        self.client.login(**credentials)
        return credentials
