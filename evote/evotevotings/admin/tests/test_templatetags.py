# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.template import Context, Template
from django.test import TestCase

from evotevotings.admin.models import Page


class TemplateTagsTest(TestCase):

    def render_template(self, string, context=None, request=None):
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


class AdminTagsTest(TemplateTagsTest):

    def test_global_alert_no_message(self):
        """ """
        template = "{% load admin_tags %}{% global_alert %}"
        rendered = self.render_template(template, {})
        self.assertInHTML("", rendered)

    def test_global_alert_with_html_message(self):
        """ """
        obj = Page.objects.get(pk=10)
        obj.body = "<b>This is an test message"
        obj.save()
        template = "{% load admin_tags %}{% global_alert %}"
        expected = (
            '<div class="globalalert">'
            '    <div class="wrap">This is an test message</div>'
            '</div>'
        )
        rendered = self.render_template(template, {})
        self.assertInHTML(expected, rendered)
