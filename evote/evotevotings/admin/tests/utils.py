# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotevotings.admin.models import Page, SecretEntry

from autofixture import create_one, create


class PageTestUtils(object):

    def create_page(self, **field_values):
        return create_one(
            Page,
            field_values=field_values
        )

    def create_pages(self, num, **field_values):
        return create(
            Page,
            num,
            field_values=field_values
        )


class SecretEntryTestUtils(object):

    def mock_secretentry(self, **field_values):
        return SecretEntry(
            field_values
        )
