# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.shortcuts import redirect
from django.conf import settings

from evotecore.mixins import (DjangoTablesFilterMixin, SelectedMenuMixin,
                              FeedbackMessageMixin)
from evoteusers.views import EvoteLoginView
from evotecore.views import ErrorBaseView

from .models import Page, SecretEntry
from .tables import PageTable
from .forms import (AdminAuthenticationForm, PageForm,
                    SecretEntryFormset)

from evoteajaxforms.views import FormsetJsonSimpleView
from .filters import PageListFilter
from .menus import Menu


class TitledViewMixin(object):

    title = None

    def get_context_data(self, **kwargs):
        context = super(TitledViewMixin, self).get_context_data(**kwargs)
        if self.title:
            context.update({
                'title': self.title
            })
        return context


class LoginView(EvoteLoginView):
    """Provides user the ability to login"""
    use_adfs = getattr(settings, "ADFS_WSFED", False)
    form_class = AdminAuthenticationForm
    template_name = "evoteadmin/login.html"

    def dispatch(self, request, *args, **kwargs):
        """ """
        if self.use_adfs and request.user.is_authenticated():
            return redirect(settings.LOGIN_REDIRECT_URL)
        return super(LoginView, self).dispatch(request, *args, **kwargs)


@method_decorator(login_required, name="dispatch")
class PageListView(
        SelectedMenuMixin, TitledViewMixin, DjangoTablesFilterMixin, ListView):
    model = Page
    template_name = "evoteadmin/page_list.html"
    title = _("Page Management")
    table_class = PageTable
    table_pagination = {'per_page': 5}
    filter_class = PageListFilter
    selected_menu = Menu.PAGEMANAGE


@method_decorator(login_required, name="dispatch")
class PageUpdateView(
        SelectedMenuMixin, TitledViewMixin, FeedbackMessageMixin, UpdateView):
    model = Page
    form_class = PageForm
    template_name_suffix = '_update'
    title = _("Update Page")
    success_url = reverse_lazy('evoteadmin:page-list')
    selected_menu = Menu.PAGEMANAGE
    success_message = _("Page updated successfully")


class PageDetailView(SelectedMenuMixin, DetailView):
    model = Page
    template_name = "voters/page_detail.html"
    selected_menu = Menu.PAGEMANAGE


@method_decorator(login_required, name="dispatch")
class PagePreviewView(SelectedMenuMixin, TitledViewMixin, DetailView):
    model = Page
    template_name = "evoteadmin/page_preview.html"
    title = _("Preview")
    selected_menu = Menu.PAGEMANAGE


@method_decorator(login_required, name="dispatch")
class SecretEntryFormSetView(
        SelectedMenuMixin, TitledViewMixin, FormsetJsonSimpleView):

    # TODO
    # permission_required = '???'
    template_name = "evoteadmin/secretentry.html"
    form_class = SecretEntryFormset
    prefix = 'secretentry'
    title = _("Certificate Upload")
    success_message = _("Certificates updated successfully")
    success_url = reverse_lazy('evoteadmin:secretentry')
    selected_menu = Menu.CERTPAGE

    def get(self, request):

        # 1st. Iterate over the Identifier choices
        #   -> Try to load an already created SecretEntry.
        #   -> Create an empty one if it does not exist.
        for secret_entry_type in SecretEntry.Identifier.choices:
            try:
                SecretEntry.objects.get(identifier=secret_entry_type[0])
            except SecretEntry.DoesNotExist:
                SecretEntry.objects.create(
                    identifier=secret_entry_type[0],
                    user=request.user
                )

        return super(SecretEntryFormSetView, self).get(request)

    def get_form_kwargs(self):
        """
            Overriding the form queryset through kwargs.
            This rule ensures only currently valid certificates end up in
            the formset
        """

        kwargs = super(SecretEntryFormSetView, self).get_form_kwargs()
        identifier_list = [a for (a, b) in SecretEntry.Identifier.choices]
        kwargs['queryset'] = SecretEntry.objects.filter(
            identifier__in=identifier_list
        )
        return kwargs


# Error views
class AdminErrorView(ErrorBaseView):
    template_name = 'evoteadmin/errors/base.html'
