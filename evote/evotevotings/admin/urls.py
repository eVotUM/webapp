# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.views.generic.base import RedirectView
from django.conf.urls import url
from django.conf import settings

from evoteusers.views import EvoteLogoutView

from .views import (
    LoginView,
    PageListView,
    PageUpdateView,
    PagePreviewView,
    PageDetailView,
    SecretEntryFormSetView
)

app_name = "evoteadmin"

urlpatterns = [
    url(
        regex=r'^$',
        view=RedirectView.as_view(url=settings.LOGIN_REDIRECT_URL),
        name="index"
    ),
    url(
        regex=r'^page/$',
        view=PageListView.as_view(),
        name="page-list"
    ),
    url(
        regex=r'^page/(?P<slug>[-\w]+)/$',
        view=PageUpdateView.as_view(),
        name="page-update"
    ),
    url(
        regex=r'^page/(?P<slug>[-\w]+)/preview/$',
        view=PagePreviewView.as_view(),
        name="page-preview"
    ),
    url(
        regex=r'^page/(?P<slug>[-\w]+)/$',
        view=PageDetailView.as_view(),
        name="page-detail"
    ),
    url(
        regex=r'^secretentry/$',
        view=SecretEntryFormSetView.as_view(),
        name="secretentry"
    ),
    url(
        regex=r'^login/$',
        view=LoginView.as_view(),
        name="login"
    ),
    url(
        regex=r'^logout/$',
        view=EvoteLogoutView.as_view(),
        name="logout"
    ),
]
