# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


# Generated by Django 1.10.4 on 2016-12-20 10:46
from __future__ import unicode_literals

import autoslug.fields
from django.db import migrations, models
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('title', models.CharField(max_length=128, verbose_name='title')),
                ('title_en', models.CharField(max_length=128, null=True, verbose_name='title')),
                ('title_pt', models.CharField(max_length=128, null=True, verbose_name='title')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='title_en', unique=True)),
                ('body', models.TextField(verbose_name='body')),
                ('body_en', models.TextField(null=True, verbose_name='body')),
                ('body_pt', models.TextField(null=True, verbose_name='body')),
            ],
            options={
                'verbose_name': 'Page',
                'verbose_name_plural': 'Pages',
            },
        ),
        migrations.CreateModel(
            name='SecretEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('identifier', models.PositiveSmallIntegerField(choices=[(0, 'Voting Certificate'), (1, 'Voting Private key'), (2, 'Validator Certificate'), (3, 'Validator Private key'), (4, 'Counter Certificate'), (5, 'Counter Private key'), (6, 'Root Certificate'), (7, 'Root Private key'), (8, 'Management Certificate'), (9, 'Management Private key')], verbose_name='identifier')),
                ('body', models.TextField(blank=True, null=True, verbose_name='body')),
            ],
            options={
                'ordering': ['identifier'],
                'verbose_name': 'Secret entry',
                'verbose_name_plural': 'Secret entries',
            },
        ),
    ]
