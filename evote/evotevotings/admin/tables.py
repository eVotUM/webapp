# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.core.urlresolvers import reverse_lazy

import django_tables2 as tables
from .models import Page


class PageTable(tables.Table):
    """
    Base Institutional Pages List table with common fields
    """

    title = tables.Column(attrs={'th': {'class': 's-15'}})
    created_by = tables.Column(
        attrs={'th': {'class': 's-20'}}
    )
    modified = tables.Column(attrs={'th': {'class': 's-15'}})

    class Meta:
        model = Page
        empty_text = _(u'No institutional pages exist.')
        orderable = False
        attrs = {
            "class": "pure-table pure-table-striped"
            " page-list"
        }
        fields = ('title', 'created_by', 'modified')

    def render_title(self, record):
        # [WIP] Title should link to page content (Still does not exist)
        url = reverse_lazy(
            'evoteadmin:page-update',
            kwargs={'slug': record.slug}
        )
        view_url = reverse_lazy(
            'evoteadmin:page-preview',
            kwargs={'slug': record.slug})

        return format_html(u"{}<br>"
                           '<span><a href="{}">{}</a> | '
                           '<a href="{}">{}</a></span>',
                           record.title, url, _(u'Edit'), view_url, _(u'View'))
