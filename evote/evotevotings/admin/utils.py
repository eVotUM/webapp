# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.apps import apps

from eVotUM.Cripto import pkiutils
from .models import SecretEntry


def get_election_model():
    return apps.get_model("evoteelections.Election")  # NOQA


def get_certificates(election_id=None):
    """ Get all certificates stored in the SecretEntry model.
        If an election ID is given, get the certificate and
        private key from the Election model """
    certificates = {}

    # Check for election ID
    if election_id is not None:
        try:
            Election = get_election_model()  # NOQA
            election = Election.objects.get(pk=election_id)
            elec_pvt_key = election.cert_private_key
            elec_cert = election.certificate
        except ObjectDoesNotExist:
            elec_pvt_key = None
            elec_cert = None

        certificates['election'] = elec_cert
        certificates['election_pvt_key'] = elec_pvt_key

    # Validator Service certificate (ECC PEM)
    certificates['vs'] = get_secret_entry(
        SecretEntry.Identifier.VALIDATOR_CERT
    )
    # Validator Service Public key (RSA PEM)
    (err, pem_vs_pub) = pkiutils.getPublicKeyFromCertificate(
        certificates['vs']
    )
    certificates['vs_pub'] = pem_vs_pub

    # Validator Service Private Key (ECC PEM)
    certificates['vs_pvt_key'] = get_secret_entry(
        SecretEntry.Identifier.VALIDATOR_PVT_KEY
    )

    # Root certificate (CA) Certificate (RSA PEM)
    certificates['root'] = get_secret_entry(
        SecretEntry.Identifier.ROOT_CERT
    )

    # Root certificate (CA) Private Key (RSA PEM)
    certificates['root_pvt_key'] = get_secret_entry(
        SecretEntry.Identifier.ROOT_PVT_KEY
    )

    # Voting System Certificate (RSA PEM)
    certificates['votesys'] = get_secret_entry(
        SecretEntry.Identifier.VOTING_CERT
    )

    # Voting System Private Key (RSA PEM)
    certificates['votesys_pvt_key'] = get_secret_entry(
        SecretEntry.Identifier.VOTING_PVT_KEY
    )

    # Counter Service Certificate (RSA PEM)
    certificates['cs'] = get_secret_entry(
        SecretEntry.Identifier.COUNTER_CERT
    )

    # Counter Service Private Key (RSA PEM)
    certificates['cs_pvt_key'] = get_secret_entry(
        SecretEntry.Identifier.COUNTER_PVT_KEY
    )

    # Election Management Certificate (RSA PEM)
    certificates['sge'] = get_secret_entry(
        SecretEntry.Identifier.MANAGEMENT_CERT
    )

    # Election Management Private Key (RSA PEM)
    certificates['sge_pvt_key'] = get_secret_entry(
        SecretEntry.Identifier.MANAGEMENT_PVT_KEY
    )

    return certificates


def get_secret_entry(entry_name):
    entry = SecretEntry.objects.filter(identifier=entry_name)
    if entry.exists():
        entryobj = entry.first()
        return entryobj.body
    else:
        return False


def get_private_key_password():
    return settings.SECRET_KEY


def not_expired_certificate(cert):
    # Validate certificate expiration
    (error_code, pubkey) = pkiutils.getPublicKeyFromCertificate(cert)
    if error_code is not None:
        return False
    return True


def validate_certificate(cert, root_cert):
    """ Checks if a given certificate is chained to a root certificate
        and still before expiration period """
    # Check for chain certificates
    chain = pkiutils.verifyCertificateChain(cert, root_cert)
    if not chain:
        return -1
    # Check for expired certificate
    expired = not_expired_certificate(root_cert)
    if not expired:
        return -2
    return True
