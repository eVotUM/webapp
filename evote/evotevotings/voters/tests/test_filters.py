# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.




from django.test import TestCase
from django.utils import timezone
from django.db.models import Q

from evotevotings.processes.models import ElectoralProcess
from evotevotings.elections.models import Election
from evotevotings.voters.filters import (
    YearFilter, VotingEndYearFilter, RollVoterFilter)
from evotevotings.processes.tests.utils import ProcessesTestUtils
from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.voters.models import ElectionVoter


class VotersFiltersTest(ProcessesTestUtils, TestCase):

    def test_yearfilter(self):
        """
        Test if year filter is done
        """
        self.create_electoral_process()
        qs = ElectoralProcess.objects.all()
        now = timezone.now()
        expected = qs.filter(publication_date__year=now.year)
        content = YearFilter(qs).filter(qs, now.year)
        self.assertEqual(content.count(), expected.count())


class VotingEndYearTest(ElectionsTestUtils, TestCase):

    def test_voting_end_year_filter(self):
        """
        Test if year filter is done
        """
        self.create_election()
        qs = Election.objects.all()
        now = timezone.now()
        expected = qs.filter(voting_end_date__year=now.year)
        content = VotingEndYearFilter(qs).filter(qs, now.year)
        self.assertEqual(content.count(), expected.count())


class RollVoterFilterTest(ElectionsTestUtils, TestCase):

    def test_search_voter_filter(self):
        """
        Test if filter is done
        """
        voter = self.create_voter()
        qs = ElectionVoter.objects.all()
        expected = qs.filter(Q(name__icontains=voter.name) |
                             Q(user__username__icontains=voter.name))
        content = RollVoterFilter(qs).filter(qs, voter.name)
        self.assertEqual(content.count(), expected.count())
