# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json

from django.test import TestCase
from django.core.urlresolvers import reverse


class ContactsFormTest(TestCase):
    """ """
    def setUp(self):
        self.url = reverse("voters:contacts")

    def test_form(self):
        data = {
            'contacts-name': 'Unit test robot',
            'contacts-email': 'unit@unit.com',
            'contacts-subject': 'Unit subject',
            'contacts-message': 'Unit robot message',
            'g-recaptcha-response': 'PASSED',
            'recaptcha_response_field': 'PASSED'
        }
        response = self.client.post(self.url, data=data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('errors_list', content)
