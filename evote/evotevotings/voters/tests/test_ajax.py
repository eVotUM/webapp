# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.urlresolvers import reverse
from django.test import TestCase

from mock import patch

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evoteusers.tests.utils import UserTestUtils


class VoteReferencesAjaxViewTest(UserTestUtils, ElectionsTestUtils, TestCase):

    def setUp(self):
        self.login()
        self.election = self.create_election()
        self.references = ["REF1", "REF2", ]
        self.url = reverse("voters:vote-reference-next-page", kwargs={
            "pk": self.election.electoral_process_id,
            "pk_election": self.election.pk
        })

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch("evotevotings.voters.ajax.get_references", return_value=([], False))
    def test_used_template(self, mock_fn):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, 'voters/includes/vote_reference_list.html')

    @patch("evotevotings.voters.ajax.get_references")
    def test_fetch_references(self, mock_fn):
        """ """
        mock_fn.return_value = (self.references, False)
        response = self.client.get(self.url)
        context = response.context
        self.assertIn("references", context)

    @patch("evotevotings.voters.ajax.get_references")
    def test_paginated_references(self, mock_fn):
        """ """
        mock_fn.return_value = (self.references, True)
        response = self.client.get(self.url)
        context = response.context
        self.assertIn("next_page", context)
        self.assertIn("more", context)
        self.assertTrue(context['more'])
