# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import TestCase

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.voters.models import ElectionVoter, ElectoralRoll

from .utils import VotersTestUtils


class ElectoralRollQuerySetTest(VotersTestUtils, ElectionsTestUtils, TestCase):
    """ """
    def setUp(self):
        self.election = self.create_election()

    def test_published_with_published_rolls(self):
        """ """
        electoral_roll = self.create_electoral_roll(
            election=self.election, published=True)
        published = ElectoralRoll.objects.published(self.election.pk)
        self.assertIsNotNone(published)
        self.assertEqual(electoral_roll, published)

    def test_published_with_unpublished_rolls(self):
        """ """
        self.create_electoral_roll(election=self.election)
        published = ElectoralRoll.objects.published(self.election.pk)
        self.assertIsNone(published)

    def test_published_return_most_recent_created(self):
        """ """
        electoral_rolls = self.create_electoral_rolls(
            2, election=self.election, published=True)
        published = ElectoralRoll.objects.published(self.election.pk)
        self.assertEqual(electoral_rolls[-1], published)

    def test_to_publish_with_unpublished_rolls(self):
        """ """
        electoral_rolls = self.create_electoral_rolls(
            2, election=self.election, imported=True)
        # must return the newest unpublished electoral roll
        to_publish = ElectoralRoll.objects.to_publish(self.election.pk)
        self.assertEqual(electoral_rolls[-1], to_publish)

    def test_to_publish_with_most_recent_roll_published(self):
        """ """
        self.create_electoral_roll(election=self.election)
        self.create_electoral_roll(election=self.election, published=True)
        # only can publish if there are electoral rolls unpublished after
        # a published one
        to_publish = ElectoralRoll.objects.to_publish(self.election.pk)
        self.assertIsNone(to_publish)


class ElectionVoterManagerTestCase(
        VotersTestUtils, ElectionsTestUtils, TestCase):
    """ """

    def setUp(self):
        self.electoral_roll = self.create_electoral_roll()

    def test_bulk_import(self):
        """ """
        data = [{
            'personnel_number': 'pg21200',
            'name': "test",
            'category': "one"
        }, {
            'personnel_number': 'pg21201',
            'name': "test",
            'category': "one"
        }]
        imported = ElectionVoter.objects.bulk_import(self.electoral_roll, data)
        found = ElectionVoter.objects.filter(
            election_id=self.electoral_roll.election_id)
        self.assertQuerysetEqual(found, map(repr, imported))

    def test_bulk_import_with_category_empty(self):
        """ """
        data = [{
            'personnel_number': 'pg21200',
            'name': "test",
            'category': ""
        }]
        imported = ElectionVoter.objects.bulk_import(self.electoral_roll, data)
        found = ElectionVoter.objects.filter(
            election_id=self.electoral_roll.election_id)
        self.assertQuerysetEqual(found, map(repr, imported))

    def test_bulk_import_update_existing_voters(self):
        """ """
        category = self.create_category(election=self.electoral_roll.election)
        voter = self.create_election_voter(
            category=category, election=self.electoral_roll.election)
        data = [{
            'personnel_number': voter.user.username,
            'name': 'test',
            'category': voter.category.slug
        }]
        imported = ElectionVoter.objects.bulk_import(self.electoral_roll, data)
        self.assertEqual(len(imported), 1)
        voter.refresh_from_db()
        self.assertEqual(voter.name, "test")
