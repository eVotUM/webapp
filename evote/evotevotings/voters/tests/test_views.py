# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from unittest import skip
from mock import patch, Mock, MagicMock
import json

from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from django.views.generic import View, TemplateView
from django.test import SimpleTestCase, RequestFactory
from django.conf import settings

from evotevotings.voters.models import ElectionVoter
from evotevotings.voters.views import (ElectionVotingStep3View,
                                       ElectionVotingStep3PinView,
                                       SecureStepsMixin, InvalidStepPath,
                                       NoRefererDefined,
                                       ElectionVotingStepMixin)

from . import VotersTestCase


class LoginViewTest(VotersTestCase):
    """ """

    def setUp(self):
        self.credentials = {
            "username": "dev",
            "password": "password"
        }
        self.user = self.create_user(**self.credentials)
        self.url = reverse('voters:login')

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/login.html')

    def test_login_valid_user(self):
        """ """
        response = self.client.post(self.url, follow=True,
                                    data=self.credentials)
        self.assertRedirects(response, settings.LOGIN_REDIRECT_URL)
        self.assertEqual(response.context[0]['user'], self.user)

    def test_login_with_wrong_credentials(self):
        self.credentials['password'] = "wrong_password"
        response = self.client.post(self.url, data=self.credentials)
        error_message = "Please enter a correct username and password. " +\
            "Note that both fields may be case-sensitive."
        self.assertFormError(response, "form", None, error_message)


class LogoutViewTest(VotersTestCase):

    def setUp(self):
        self.login()

    def test_logout_view(self):
        """ """
        response = self.client.get(reverse('voters:logout'), follow=True)
        self.assertRedirects(response, reverse('evotecore:index'))
        self.assertIsInstance(response.context[0]['user'], AnonymousUser)


class ElectoralProcessListViewTest(VotersTestCase):

    def setUp(self):
        # create and login user
        self.login()
        self.url = reverse("voters:electoralprocess-list")

    def test_used_template_to_unauthenticated(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "voters/electoralprocess_list.html")

    def test_used_template_to_authenticated(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "voters/electoralprocess_mylist.html")

    @skip("to implement")
    def test_context_table_class_to_unauthenticated(self):
        pass

    @skip("to implement")
    def test_context_table_class_to_authenticated(self):
        pass

    @skip("to implement")
    def test_context_queryset_to_unauthenticated(self):
        pass

    @skip("to implement")
    def test_context_queryset_to_authenticated(self):
        pass


class ElectoralProcessDetailViewTest(VotersTestCase):

    def setUp(self):
        self.login_as_voter()
        self.url = reverse("voters:electoralprocess-detail", kwargs={
            "slug": self.voter.election.electoral_process.slug
        })

    def test_used_template_to_authenticated(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "voters/electoralprocess_mydetail.html")

    def test_used_template_to_unauthenticated(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "voters/electoralprocess_detail.html")


class ElectoralProcessArchiveViewTest(VotersTestCase):

    def test_used_template(self):
        """ """
        response = self.client.get(reverse("voters:electoralprocess-archive"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "voters/electoralprocess_archive.html")

    @skip("to implement")
    def test_context_table_class(self):
        """TODO: implement test"""
        pass

    @skip("to implement")
    def test_context_queryset(self):
        """TODO: implement test"""
        pass


class ContactsViewTest(VotersTestCase):

    def setUp(self):
        self.url = reverse("voters:contacts")

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/contacts.html')


def raise_valerror(self, *args, **kwargs):
    self.add_error('notice_email', ('Please provide the notice email'))


class PersonalDataViewTest(VotersTestCase):
    """ """
    def setUp(self):
        self.login()
        self.url = reverse("voters:personal-data")

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/personal_data.html')

    def test_form_submit_valid(self):
        data = {
            'personal-photo': 'Photo',
            'personal-name': 'Name',
            'personal-email': 'test@test.com',
            'personal-primary_phone': '+351911111111',
            'personal-notice_method': 1,
            'personal-notice_phone': '',
            'personal-notice_email': 'test@test.com',
        }
        response = self.client.post(self.url, data=data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('errors_list', content)

    @patch("evoteusers.forms.PersonalDataForm.clean", new=raise_valerror)
    def test_form_submit_fail_mail(self):
        data = {
            'personal-photo': 'Photo',
            'personal-name': 'Name',
            'personal-primary_phone': '+351911111111',
            'personal-notice_method': 1,
            'personal-notice_phone': '',
        }
        response = self.client.post(self.url, data=data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('errors_list', content)

    def test_form_submit_fail_phone(self):
        data = {
            'personal-photo': 'Photo',
            'personal-name': 'Name',
            'personal-email': 'test@test.com',
            'personal-primary_phone': '',
            'personal-notice_method': 0,
        }
        response = self.client.post(self.url, data=data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('errors_list', content)

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ResendPendingTokenMailViewTest(VotersTestCase):
    def setUp(self):
        self.login()
        self.url = reverse("voters:resend-email")

    def test_get(self):
        """ """
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse("voters:personal-data"))

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class HowitWorksViewTest(VotersTestCase):
    def setUp(self):
        self.url = reverse("voters:howitworks")

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/howitworks.html')


class ElectionHistoryViewTest(VotersTestCase):

    def setUp(self):
        self.login()
        self.url = reverse("voters:election-history")

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "voters/election_history.html")

    def test_get_to_unauthenticated_user(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @skip("to implement")
    def test_context_table_class(self):
        """TODO: implement test"""
        pass

    @skip("to implement")
    def test_context_queryset(self):
        """TODO: implement test"""
        pass


"""
VOTING VIEWS
"""


class ElectoralProcessVotingViewTest(VotersTestCase):

    def setUp(self):
        self.login_as_voter()
        self.url = reverse("voters:electoralprocess-voting", kwargs={
            "slug": self.voter.election.electoral_process.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, 'voters/electoralprocess_voting.html')

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class SecureStepsMixinTest(SimpleTestCase):

    class SecuredTestView(SecureStepsMixin, View):
        """ """
        steps = ['1', '2']

        def reverse_step_url(self, stepid):
            return "/step{}/".format(stepid)

    def setUp(self):
        self.view = self.SecuredTestView()
        self.view.request = RequestFactory().get('/test/')

    def test_process_steps_define_steps_urls(self):
        self.view.process_steps()
        self.assertEqual(self.view.steps_urls, ['/step1/', '/step2/'])

    @patch.object(SecuredTestView, 'get_referer', side_effect=NoRefererDefined)
    def test_process_steps_with_no_referer_defined(self, get_referer):
        """"if referer is not defined, must return to first step"""
        redirect_to = self.view.process_steps()
        self.assertEqual(redirect_to, '/step1/')

    @patch.object(SecuredTestView, 'get_referer', return_value=None)
    @patch.object(SecuredTestView, 'get_step_index',
                  side_effect=InvalidStepPath)
    def test_process_steps_with_request_path_not_in_steps(
            self, get_step_index, get_referer):
        """request.path not in valid steps, redirect to first step"""
        redirect_to = self.view.process_steps()
        self.assertEqual(redirect_to, '/step1/')

    @patch.object(SecuredTestView, 'get_referer', return_value=None)
    @patch.object(SecuredTestView, 'get_step_index', return_value=0)
    def test_process_steps_with_request_path_as_first_step(
            self, get_step_index, get_referer):
        """request.path equal to first step url, do nothing"""
        redirect_to = self.view.process_steps()
        self.assertIsNone(redirect_to)

    @patch.object(SecuredTestView, 'get_referer', return_value=Mock())
    @patch.object(SecuredTestView, 'get_step_index',
                  side_effect=[1, InvalidStepPath])
    def test_process_steps_with_invalid_referer_path(
            self, get_step_index, get_referer):
        """referer.path not in valid steps, redirect to first step"""
        redirect_to = self.view.process_steps()
        self.assertEqual(redirect_to, '/step1/')
        get_step_index.assert_any_call('/test/')
        get_step_index.assert_any_call(get_referer().path)

    @patch.object(SecuredTestView, 'get_referer', return_value=Mock())
    @patch.object(SecuredTestView, 'get_step_index',
                  side_effect=[1, 3])
    def test_process_steps_with_invalid_step_jump_next(
            self, get_step_index, get_referer):
        """"""
        redirect_to = self.view.process_steps()
        self.assertEqual(redirect_to, get_referer().path)
        get_step_index.assert_any_call('/test/')
        get_step_index.assert_any_call(get_referer().path)

    @patch.object(SecuredTestView, 'get_referer', return_value=Mock())
    @patch.object(SecuredTestView, 'get_step_index',
                  side_effect=[3, 1])
    def test_process_steps_with_invalid_step_jump_previous(
            self, get_step_index, get_referer):
        """"""
        redirect_to = self.view.process_steps()
        self.assertEqual(redirect_to, get_referer().path)
        get_step_index.assert_any_call('/test/')
        get_step_index.assert_any_call(get_referer().path)

    @patch.object(SecuredTestView, 'get_referer', return_value=Mock())
    @patch.object(SecuredTestView, 'get_step_index',
                  side_effect=[2, 3])
    def test_process_steps_with_valid_step_jump_next(
            self, get_step_index, get_referer):
        """"""
        redirect_to = self.view.process_steps()
        self.assertIsNone(redirect_to)
        get_step_index.assert_any_call('/test/')
        get_step_index.assert_any_call(get_referer().path)

    @patch.object(SecuredTestView, 'get_referer', return_value=Mock())
    @patch.object(SecuredTestView, 'get_step_index',
                  side_effect=[3, 2])
    def test_process_steps_with_valid_step_jump_previous(
            self, get_step_index, get_referer):
        """"""
        redirect_to = self.view.process_steps()
        self.assertIsNone(redirect_to)
        get_step_index.assert_any_call('/test/')
        get_step_index.assert_any_call(get_referer().path)

    def test_get_step_index_valid_path(self):
        """"""
        self.view.steps_urls = ['/step1/', '/step2/']
        index = self.view.get_step_index("/step1/")
        self.assertEqual(index, 0)

    def test_get_step_index_invalid_path(self):
        """"""
        self.view.steps_urls = ['/step1/', '/step2/']
        with self.assertRaisesRegexp(InvalidStepPath,
                                     "The step path '/test/' is invalid"):
            self.view.get_step_index("/test/")

    def test_get_referer_not_defined(self):
        """"""
        with self.assertRaises(NoRefererDefined):
            self.view.get_referer()

    def test_get_referer_defined(self):
        """"""
        self.view.request.META['HTTP_REFERER'] = "/test/"
        self.assertEqual(self.view.get_referer().path, "/test/")


class ElectionVotingStepMixinTest(SimpleTestCase):
    """ """

    class StepTestView(ElectionVotingStepMixin, TemplateView):
        """ """
        steps = ['1', '2']
        template_name = "test.html"

        def reverse_step_url(self, stepid):
            return "/step{}/".format(stepid)

        def get_object(self):
            return MagicMock()

        def get_election_voter_object(self, election_id):
            return MagicMock()

    def setUp(self):
        self.factory = RequestFactory()
        self.view = self.StepTestView()
        self.view.request = self.factory.get('/test/')
        self.view.request.user = Mock(is_authenticated=True)

    @patch.object(StepTestView, 'process_steps', return_value='/redirect/')
    def test_dispatch_redirect_to_process_steps_return(self, process_steps):
        response = self.view.dispatch(self.view.request)
        self.assertRedirects(response, "/redirect/",
                             fetch_redirect_response=False)

    @patch.object(StepTestView, 'process_steps', return_value="/step1/")
    def test_dispatch_redirect_dont_loop(self, process_steps):
        self.view.request = self.factory.get('/step1/')
        self.view.request.user = Mock(is_authenticated=True)
        response = self.view.dispatch(self.view.request)
        self.assertEqual(response.status_code, 200)


class ElectionVotingStep1ViewTest(VotersTestCase):

    def setUp(self):
        self.login_as_voter()
        self.url = reverse("voters:election-voting-step1", kwargs={
            "slug": self.voter.election.electoral_process.slug,
            "slug_election": self.voter.election.slug
        })
        self.mocked_context = {
            'combo_id': 'dummy',
            'vs_cert': 'dummy',
            'election_id': 'dummy',
            'category_slug': 'dummy'
        }

    @patch("evotevotings.voters.views.get_extra_election_data")
    def test_used_template(self, mock_fn):
        """ """
        mock_fn.return_value = self.mocked_context
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/election_voting_step1.html')

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch("evotevotings.voters.views.get_extra_election_data")
    def test_invalid_context_data(self, mock_fn):
        """ """
        mock_fn.return_value = False
        response = self.client.get(self.url)
        context = response.context[-1]
        self.assertIn("is_valid", context)
        self.assertFalse(context['is_valid'])

    @patch("evotevotings.voters.views.get_extra_election_data")
    def test_context_data(self, mock_fn):
        """ """
        mock_fn.return_value = self.mocked_context
        response = self.client.get(self.url)
        context = response.context[-1]
        self.assertIn("is_valid", context)
        self.assertTrue(context['is_valid'])


class ElectionVotingStep2ViewTest(VotersTestCase):

    def setUp(self):
        self.login_as_voter()
        self.url = reverse("voters:election-voting-step2", kwargs={
            "slug": self.voter.election.electoral_process.slug,
            "slug_election": self.voter.election.slug
        })
        self.referer = reverse("voters:election-voting-step1", kwargs={
            "slug": self.voter.election.electoral_process.slug,
            "slug_election": self.voter.election.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url, HTTP_REFERER=self.referer)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/election_voting_step2.html')

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


# to avoid steps secure validation
@patch.object(ElectionVotingStep3View, 'process_steps',
              MagicMock(return_value=None))
@patch.object(ElectionVotingStep3View, 'get_object', MagicMock())
class ElectionVotingStep3ViewTest(SimpleTestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.view = ElectionVotingStep3View.as_view()

    def test_login_required(self):
        """ """
        request = self.factory.get('/election-voting-step3/')
        request.user = Mock(is_authenticated=False)
        response = self.view(request)
        self.assertEqual(response.status_code, 302)

    @patch.object(ElectionVotingStep3View, 'get_election_voter_object',
                  MagicMock())
    def test_used_template(self):
        """ """
        request = self.factory.get('/election-voting-step3/')
        request.user = Mock(is_authenticated=True)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        self.assertIn("voters/election_voting_step3.html",
                      response.template_name)

    @patch.object(ElectionVotingStep3View, 'get_election_voter_object')
    @patch.object(ElectionVotingStep3View, 'set_success_message')
    def test_post_send_pin_through_email(self, set_success_message,
                                         get_election_voter_object):
        data = {
            'send_through': 'email'
        }
        request = self.factory.post('/election-voting-step3/', data)
        request.user = Mock(is_authenticated=True)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        election_voter = get_election_voter_object()
        election_voter.send_pin.assert_called_with('email')

    @patch.object(ElectionVotingStep3View, 'get_election_voter_object')
    @patch.object(ElectionVotingStep3View, 'set_success_message')
    def test_post_send_pin_through_sms(self, set_success_message,
                                       get_election_voter_object):
        data = {
            'send_through': 'sms'
        }
        request = self.factory.post('/election-voting-step3/', data)
        request.user = Mock(is_authenticated=True)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        election_voter = get_election_voter_object()
        election_voter.send_pin.assert_called_with('sms')

    @patch.object(ElectionVotingStep3View, 'get_election_voter_object',
                  MagicMock())
    def test_post_send_pin_invalid(self):
        data = {
            'send_through': 'invalid'
        }
        request = self.factory.post('/election-voting-step3/', data)
        request.user = Mock(is_authenticated=True)
        response = self.view(request)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertIn('errors_list', content)


# to bypass steps secure validation
@patch.object(ElectionVotingStep3PinView, 'process_steps',
              MagicMock(return_value=None))
@patch.object(ElectionVotingStep3PinView, 'get_election_voter_object')
@patch.object(ElectionVotingStep3PinView, 'get_object')
class ElectionVotingStep3PinViewTest(VotersTestCase):

    def setUp(self):
        self.request = RequestFactory().get('/election-voting-step3-pin/')
        self.request.user = Mock(is_authenticated=True)
        self.request.session = {}
        self.view = ElectionVotingStep3PinView.as_view()
        self.election = Mock(slug='test', electoral_process=Mock(slug='test'))

    def test_login_required(self, get_object, get_election_voter_object):
        """ """
        self.request.user.is_authenticated = False
        response = self.view(self.request)
        self.assertEqual(response.status_code, 302)

    def test_used_template(self, get_object, get_election_voter_object):
        """ """
        response = self.view(self.request)
        self.assertEqual(response.status_code, 200)
        self.assertIn("voters/election_voting_step3_pin.html",
                      response.template_name)

    def test_get_context_data(self, get_object, get_election_voter_object):
        """ """
        get_object.return_value = self.election
        response = self.view(self.request)
        self.assertIn("from_ama", response.context_data)
        self.assertIn("validated", response.context_data)
        self.assertFalse(response.context_data['from_ama'])
        self.assertFalse(response.context_data['validated'])
        response.render()
        self.assertContains(response, "voting-action")

    def test_get_context_data_with_invalid_ama_attributes(
            self, get_object, get_election_voter_object):
        """ """
        get_object.return_value = self.election
        self.request.session = {'authgov': {'test': 'test'}}
        response = self.view(self.request)
        self.assertIn("from_ama", response.context_data)
        self.assertIn("validated", response.context_data)
        self.assertFalse(response.context_data['from_ama'])
        self.assertFalse(response.context_data['validated'])
        response.render()
        self.assertContains(response, "voting-action")

    def test_get_context_data_with_invalid_ama_nic(
            self, get_object, get_election_voter_object):
        """ """
        get_object.return_value = self.election
        self.request.user.nic = '1234567'
        self.request.session = {'authgov': {'attributes': {'NIC': '7654321'}}}
        response = self.view(self.request)
        self.assertIn("from_ama", response.context_data)
        self.assertIn("validated", response.context_data)
        self.assertTrue(response.context_data['from_ama'])
        self.assertFalse(response.context_data['validated'])
        response.render()
        self.assertNotContains(response, "voting-action")

    def test_get_context_data_with_valid_ama_nic(
            self, get_object, get_election_voter_object):
        """ """
        get_object.return_value = self.election
        self.request.user.nic = '1234567'
        self.request.session = {'authgov': {'attributes': {'NIC': '1234567'}}}
        response = self.view(self.request)
        self.assertIn("from_ama", response.context_data)
        self.assertIn("validated", response.context_data)
        self.assertTrue(response.context_data['from_ama'])
        self.assertTrue(response.context_data['validated'])
        response.render()
        self.assertContains(response, "voting-action")
        election_voter = get_election_voter_object()
        self.assertEqual(election_voter.token_send_through,
                         ElectionVoter.SendThrough.AMA)
        election_voter.save.assert_called_with(update_fields=[
            'token_send_through',
            'token_expiration_date'
        ])


class ElectionVotingStep4ViewTest(VotersTestCase):

    def setUp(self):
        self.login_as_voter()
        self.url = reverse("voters:election-voting-step4", kwargs={
            "slug": self.voter.election.electoral_process.slug,
            "slug_election": self.voter.election.slug
        })
        self.referer = reverse("voters:election-voting-step3-pin", kwargs={
            "slug": self.voter.election.electoral_process.slug,
            "slug_election": self.voter.election.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url, HTTP_REFERER=self.referer)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/election_voting_step4.html')

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectionSearchVoteViewTest(VotersTestCase):

    def setUp(self):
        self.login_as_voter()
        self.election = self.voter.election
        self.references = [
            'REF#1',
            'UREF#2',
            'TREF#3',
            'REF#4',
        ]
        kwargs = {
            "slug": self.election.electoral_process.slug,
            "slug_election": self.election.slug
        }
        self.url = reverse("voters:election-search-vote",
                           kwargs=kwargs)

    @patch("evotevotings.voters.views.get_references",
           return_value=([], False))
    def test_used_template(self, mock_fn):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/election_search_vote.html')

    @patch("evotevotings.voters.views.get_references")
    def test_reference_data(self, mock_fn):
        """ """
        mock_fn.return_value = (self.references, False)
        response = self.client.get(self.url)
        context = response.context[-1]
        self.assertIn("references", context)
        self.assertEqual(context['references'], self.references)

    @patch("evotevotings.voters.views.get_references")
    def test_reference_search(self, mock_fn):
        """ """
        mock_fn.return_value = ([self.references[1], ], False)
        search_data = {'search': self.references[1]}
        response = self.client.get(self.url, search_data)
        context = response.context[-1]
        self.assertIn("references", context)
        self.assertEqual(context['references'], [self.references[1], ])

    @patch("evotevotings.voters.views.get_references")
    def test_reference_pagination(self, mock_fn):
        """ """
        mock_fn.return_value = (self.references, True)
        response = self.client.get(self.url)
        context = response.context[-1]
        self.assertIn("references", context)
        self.assertIn("more", context)
        self.assertTrue(context['more'])
        self.assertIn("next_page", context)


class ElectoralRollDetailViewTest(VotersTestCase):

    def setUp(self):
        self.roll = self.create_electoral_roll()
        self.url = reverse("voters:electoralroll-detail", kwargs={
            "slug": self.roll.election.electoral_process.slug,
            "slug_roll": self.roll.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/electoralroll_detail.html')


class ValidateEmailViewTest(VotersTestCase):
    def setUp(self):
        self.login_as_voter()
        self.url = "{}?token={}".format(reverse("voters:validate-email"),
                                        self.voter.user.pending_email_token)

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'voters/validate_email.html')

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)
