# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import TestCase
from django.core.urlresolvers import reverse
from django.template.defaultfilters import truncatechars

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.processes.models import ElectoralProcess
from evotevotings.voters.tables import ElectoralProcessArchiveTable


class ElectoralProcessArchiveTableTest(ElectionsTestUtils, TestCase):
    """
    Test custom fields output of Archive table
    """
    def setUp(self):
        self.table = ElectoralProcessArchiveTable({})

    def test_render_elections_valid(self):
        election = self.create_election()
        base_url = reverse(
            "voters:electoralprocess-detail",
            args=[election.electoral_process.slug]
        )
        url = base_url + "#election-" + str(election.pk)
        expected = "<ul><li><a href='{}'>{}</a></li></ul>"\
            .format(url, truncatechars(election.identifier, 20))
        rendered = self.table.render_elections(election.electoral_process)
        self.assertInHTML(expected, rendered)

    def test_render_elections_empty_valid(self):
        process = ElectoralProcess()
        expected = "<ul><li>No Elections</li></ul>"
        rendered = self.table.render_elections(process)
        self.assertInHTML(expected, rendered)
