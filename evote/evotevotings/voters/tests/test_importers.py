# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from StringIO import StringIO
from csvvalidator import VALUE_PREDICATE_FALSE, UNIQUE_CHECK_FAILED

from django.test import TestCase

from evotevotings.voters.tests.utils import VotersTestUtils
from evotevotings.voters.importers import ElectoralRollImporter


class ElectoralRollImporterTestCase(VotersTestUtils, TestCase):
    """ """
    def setUp(self):
        """ """
        self.electoral_roll = self.create_electoral_roll()
        self.importer = ElectoralRollImporter(self.electoral_roll.election)
        self.header = "personnel_number;name;category\n"

    def _get_csv(self, lines=None):
        """ """
        csv = self.header
        if lines:
            csv += "\n".join(lines)
        csv = csv.encode('utf-8')
        return StringIO(csv)

    def test_validate_with_valid_header_fields(self):
        """ """
        csv = self._get_csv()
        result = self.importer.validate(csv)
        self.assertEqual(result, [])

    def test_validate_with_invalid_header_fields(self):
        """ """
        invalid_csv_header = StringIO("personnel_number;names;category")
        result = self.importer.validate(invalid_csv_header)
        self.assertEqual(result[0]['unexpected'], set(["names"]))
        self.assertEqual(result[0]['missing'], set(["name"]))

    def test_validate_unique_personnel_number(self):
        """ """
        csv = self._get_csv([
            "123;test name;one",
            "123;test name;one"
        ])
        result = self.importer.validate(csv)
        self.assertEqual(result[0]['code'], UNIQUE_CHECK_FAILED)
        self.assertEqual(result[0]['key'], "personnel_number")

    def test_validate_category(self):
        """ """
        csv = self._get_csv([
            "123;test name;two",
        ])
        result = self.importer.validate(csv)
        self.assertEqual(result[0]['code'], VALUE_PREDICATE_FALSE)
        self.assertEqual(result[0]['field'], "category")

    def test_validate_category_not_required(self):
        """ """
        csv = self._get_csv([
            "123;test name;",
        ])
        result = self.importer.validate(csv)
        self.assertEqual(result, [])

    def test_run(self):
        csv = self._get_csv([
            "1;test name;one",
            "2;test name;one",
        ])
        voters = self.importer.run(self.electoral_roll, csv)
        self.assertIsNotNone(voters)
        self.assertEqual(len(voters), 2)

    def test_run_with_special_characters(self):
        csv = self._get_csv([
            "1;test name é;one",
            "2;test name ó;one",
        ])
        voters = self.importer.run(self.electoral_roll, csv)
        self.assertIsNotNone(voters)
        self.assertEqual(len(voters), 2)

    def test_run_with_already_existent_data(self):
        """Test update and new insertion"""
        content = [
            "1;test name;one",
            "2;test name;one",
        ]
        # insert new voters
        self.importer.run(self.electoral_roll, self._get_csv(content))
        content.append("3;test name 3;one")
        # insert new voter and update 2
        voters = self.importer.run(self.electoral_roll, self._get_csv(content))
        self.assertEqual(voters.count(),
                         self.electoral_roll.election_voters.count())
