# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from mock import patch

from django.contrib.auth import get_user_model
from django.utils import timezone
from django.test import SimpleTestCase
from django.conf import settings

from evotevotings.voters.models import ElectoralRoll, ElectionVoter
from evotedocs.models import Document


class ElectoralRollTest(SimpleTestCase):
    """ """

    def test_str_equal_to_designation(self):
        document = Document(designation="designation")
        instance = ElectoralRoll(document=document)
        self.assertEqual(str(instance), instance.document.designation)


class ElectionVoterTest(SimpleTestCase):

    def test_str(self):
        ev_obj = ElectionVoter(name='Dev Test')
        self.assertEqual(str(ev_obj), 'Dev Test')

    def test_is_vote_available(self):
        modified = timezone.now() - timezone.timedelta(days=1)
        ev_obj = ElectionVoter(vote_status=1, modified=modified)
        self.assertEqual(ev_obj.is_vote_available, (1, False))

    @patch.object(ElectionVoter, 'save')
    def test_is_token_valid(self, save):
        expiration_date = timezone.now() + timezone.timedelta(seconds=10)
        election_voter = ElectionVoter(
            pin_value='QWERTY', token_expiration_date=expiration_date)
        self.assertTrue(election_voter.is_token_valid('QWERTY'))
        save.assert_called_with(update_fields=['token_validated'])

    def test_is_token_valid_with_wrong_token(self):
        expiration_date = timezone.now() + timezone.timedelta(seconds=10)
        election_voter = ElectionVoter(
            pin_value='QWERTY', token_expiration_date=expiration_date)
        self.assertFalse(election_voter.is_token_valid('WRONG'))

    def test_is_token_valid_with_expiration_date_surpassed(self):
        expiration_date = timezone.now() - timezone.timedelta(seconds=10)
        election_voter = ElectionVoter(
            pin_value='QWERTY', token_expiration_date=expiration_date)
        self.assertFalse(election_voter.is_token_valid('QWERTY'))

    @patch.object(ElectionVoter, 'save')
    @patch("evotevotings.voters.models.sms_service.send_message")
    def test_send_pin_through_sms(self, send_message, save):
        User = get_user_model()  # NOQA
        election_voter = ElectionVoter()
        election_voter.user = User(primary_phone="000000000")
        election_voter.send_pin(ElectionVoter.SendThrough.SMS)
        sms_text = settings.PIN_SMS_CONTENT.format(
            pin=election_voter.pin_value)
        send_message.assert_called_with("000000000", sms_text)
        save.assert_called_with(update_fields=[
            'pin_value',
            'token_send_through',
            'token_expiration_date',
            'token_validated'
        ])

    @patch.object(ElectionVoter, 'save')
    @patch("evotevotings.voters.models.SendPinMail")
    def test_send_pin_through_email(self, send_pin_mail, save):
        User = get_user_model()  # NOQA
        election_voter = ElectionVoter()
        election_voter.user = User(name="test", email="dev@eurotux.com")
        election_voter.send_pin(ElectionVoter.SendThrough.EMAIL)
        send_pin_mail.assert_called_with({
            'pin_value': election_voter.pin_value,
            'pin_expiration_date': election_voter.token_expiration_date
        })
        send_pin_mail.return_value.send.assert_called_with(
            [(election_voter.user.name, election_voter.user.get_email())])
        save.assert_called_with(update_fields=[
            'pin_value',
            'token_send_through',
            'token_expiration_date',
            'token_validated'
        ])

    def test_send_pin_through_invalid_method(self):
        election_voter = ElectionVoter()
        with self.assertRaisesRegexp(
                ValueError, "Can't send pin, 'invalid' is not a valid method"):
            election_voter.send_pin('invalid')
