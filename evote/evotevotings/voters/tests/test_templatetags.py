# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import Mock

from django.template import Context, Template
from django.test import TestCase

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.admin.models import Page


class TemplateTagsTest(object):

    def render_template(self, string, context=None, request=None):
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


class VotersTagsTest(TemplateTagsTest, ElectionsTestUtils, TestCase):
    """ """

    def create_candidate_list_field(self, choices=[]):
        """ """
        candidate_list = Mock()
        candidate_list.attrs.id = 'testid'
        candidate_list.attrs.name = 'testname'
        candidate_list.field.disabled = True
        candidate_list.field.required = True
        candidate_list.field.choices = choices
        candidate_list.auto_id = 1
        candidate_list.name = 'test'
        return candidate_list

    def test_candidate_list_no_candidates(self):
        """ """
        context = {'candidates': []}
        template = "{% load voters_tags %}{% candidate_list candidates %}"
        expected = ""
        rendered = self.render_template(template, context)
        self.assertInHTML(expected, rendered)

    def test_candidate_list_with_candidates(self):
        """ """
        candidate = self.create_candidate(**{'logo': None,
                                             'designation': 'test'})
        candidate_list = self.create_candidate_list_field()
        candidate_list.choice_value = candidate.id
        context = {'candidates': [candidate_list]}
        template = "{% load voters_tags %}{% candidate_list candidates %}"
        expected = (
            '<div data-vote-option class="candidate-box">'
            '    <img data-candidate-logo class="candidate-logo" '
            'src="/static/images/avatar.png" width="85" height="85" '
            'alt="candidate">'
            '    <div class="candidate-designation">'
            '        <p data-candidate-designation>test</p>'
            '    </div>'
            '    <label for="testid" class="pure-checkbox">'
            '        <input data-vote-checkbox id="testid" name="test" '
            'value="1" type="checkbox">'
            '        <span></span>'
            '    </label>'
            '</div>'
        )
        rendered = self.render_template(template, context)
        self.assertInHTML(expected, rendered)

    def test_candidate_dropdown(self):
        """ """
        candidate_list = self.create_candidate_list_field()
        context = {'candidates': candidate_list}
        template = "{% load voters_tags %}{% candidate_dropdown candidates %}"
        expected = (
            '<div class="candidate-box">'
            '    <img data-candidate-logo class="candidate-logo" '
            'src="/static/images/avatar.png" width="85" height="85" '
            'alt="candidate">'
            '    <div class="candidate-designation">'
            '        <select data-select-vote data-style-select id="1" '
            'name="test" class="pure-select" required disabled>'
            '        </select>'
            '    </div>'
            '    <label for="field" class="pure-checkbox no-selectable">'
            '        <input data-vote-simulation name="vote" value="" '
            'type="checkbox">'
            '        <span></span>'
            '    </label>'
            '</div>'
        )
        rendered = self.render_template(template, context)
        self.assertInHTML(expected, rendered)

    def test_candidate_dropdown_with_candidates(self):
        """ """
        candidate = self.create_candidate(**{'logo': None})
        choices = [(candidate.id, 'label')]
        candidate_list = self.create_candidate_list_field(choices)
        context = {'candidates': candidate_list}
        template = "{% load voters_tags %}{% candidate_dropdown candidates %}"
        expected = (
            '<div class="candidate-box">'
            '    <img data-candidate-logo class="candidate-logo" '
            'src="/static/images/avatar.png" width="85" height="85" '
            'alt="candidate">'
            '    <div class="candidate-designation">'
            '        <select data-select-vote data-style-select id="1" '
            'name="test" class="pure-select" required disabled>'
            '            <option data-logo="/static/images/avatar.png" '
            'value="1">label</option>'
            '        </select>'
            '    </div>'
            '    <label for="field" class="pure-checkbox no-selectable">'
            '        <input data-vote-simulation name="vote" value="" '
            'type="checkbox">'
            '        <span></span>'
            '    </label>'
            '</div>'
        )
        rendered = self.render_template(template, context)
        self.assertInHTML(expected, rendered)

    def test_candidate_dropdown_with_candidate_not_found_in_db(self):
        """ """
        choices = [(None, 'label')]
        candidate_list = self.create_candidate_list_field(choices)
        context = {'candidates': candidate_list}
        template = "{% load voters_tags %}{% candidate_dropdown candidates %}"
        expected = (
            '<div class="candidate-box">'
            '    <img data-candidate-logo class="candidate-logo" '
            'src="/static/images/avatar.png" width="85" height="85" '
            'alt="candidate">'
            '    <div class="candidate-designation">'
            '        <select data-select-vote data-style-select id="1" '
            'name="test" class="pure-select" required disabled>'
            '            <option data-logo="/static/images/avatar.png" '
            'value="">label</option>            '
            '        </select>'
            '    </div>'
            '    <label for="field" class="pure-checkbox no-selectable">'
            '        <input data-vote-simulation name="vote" value="" '
            'type="checkbox">'
            '        <span></span>'
            '    </label>'
            '</div>'
        )
        rendered = self.render_template(template, context)
        self.assertInHTML(expected, rendered)

    def test_voting_faqs(self):
        """ """
        template = "{% load voters_tags %}{% voting_faqs %}"
        expected = ""
        rendered = self.render_template(template, {})
        self.assertInHTML(expected, rendered)

    def test_voting_faqs_with_html(self):
        """ """
        expected = "<h1>Unit Test</h1>"
        obj = Page.objects.get(pk=11)
        obj.body = expected
        obj.save()
        template = "{% load voters_tags %}{% voting_faqs %}"
        rendered = self.render_template(template, {})
        self.assertInHTML(expected, rendered)
