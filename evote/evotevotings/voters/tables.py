# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import django_tables2 as tables

from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.core.urlresolvers import reverse_lazy
from django.template.defaultfilters import truncatechars

from evotevotings.processes.models import ElectoralProcess
from evotevotings.elections.models import Election
from evotecore.tables import ImageColumn, DateTimeColumn
from evotevotings.processes.tables import StateColumn
from evotevotings.voters.models import ElectionVoter


class ElectoralProcessTable(tables.Table):
    """ """
    logo = ImageColumn('', attrs={'th': {'class': 's-10'}})
    identifier = tables.Column(attrs={'th': {'class': 's-50'}},
                               verbose_name=_('Identifier'))
    state = StateColumn(attrs={'th': {'class': 's-15'}})
    link = tables.LinkColumn(
        viewname='voters:electoralprocess-detail',
        args=[tables.A('slug')],
        orderable=False,
        attrs={'a':
               {'class': 'pure-button pure-button-primary pure-button-round\
                pull-right'},
               'th': {'class': 's-20'}},
        text=_('Browse'),
        verbose_name=''
    )

    class Meta:
        model = ElectoralProcess
        empty_text = _('No electoral processes.')
        orderable = False
        attrs = {"class": "pure-table pure-table-line"}
        fields = ('logo', 'identifier', 'state', 'link')


class ElectoralProcessArchiveTable(tables.Table):
    """ """
    logo = ImageColumn('', attrs={'th': {'class': 's-10'}}, orderable=False)
    identifier = tables.Column(attrs={'th': {'class': 's-50'}},
                               verbose_name=_('Identifier'))
    elections = tables.Column(_('Elections'), empty_values=(),
                              attrs={'th': {'class': 's-15'}})
    state = StateColumn(attrs={'th': {'class': 's-15'}})
    link = tables.LinkColumn(
        viewname='voters:electoralprocess-detail',
        args=[tables.A('slug')],
        orderable=False,
        attrs={'a':
               {'class': 'pure-button pure-button-primary pure-button-round\
                pull-right'},
               'th': {'class': 's-10'}},
        text=_('Consult'),
        verbose_name=''
    )

    class Meta:
        model = ElectoralProcess
        empty_text = _('No electoral processes.')
        attrs = {"class": "pure-table pure-table-line"}
        fields = ('logo', 'identifier', 'elections', 'state', 'link')

    def render_elections(self, record):
        base_url = reverse_lazy("voters:electoralprocess-detail",
                                args=[record.slug])
        res = "<ul>"
        if record.elections.exists():
            for election in record.elections.all():
                url = base_url + "#election-" + str(election.pk)
                res += format_html(
                    "<li><a href='{}'>{}</a></li>", url,
                    truncatechars(election.identifier, 20))
        else:
            res += format_html("<li>{}</li>", _("No Elections"))
        res += "</ul>"
        return format_html(res)


class ElectoralProcessMyTable(tables.Table):
    """ """
    logo = ImageColumn('', attrs={'th': {'class': 's-10'}})
    identifier = tables.Column(attrs={'th': {'class': 's-50'}},
                               verbose_name=_('Identifier'))
    state = StateColumn(attrs={'th': {'class': 's-15'}})
    consult = tables.LinkColumn(
        viewname='voters:electoralprocess-detail', args=[tables.A('slug')],
        orderable=False, text=_('Consult'), verbose_name='',
        attrs={
            'a': {
                'class': 'pure-button pure-button-primary pure-button-round'},
            'th': {'class': 's-10'}
        },
    )
    vote = tables.LinkColumn(
        viewname='voters:electoralprocess-voting', args=[tables.A('slug')],
        orderable=False, text=_('Vote'), verbose_name='',
        attrs={
            'a': {
                'class': 'pure-button pure-button-primary pure-button-round'},
            'th': {'class': 's-10'}
        },
    )

    class Meta:
        model = ElectoralProcess
        empty_text = _('No electoral processes found.')
        orderable = False
        attrs = {"class": "pure-table pure-table-line"}
        fields = ('logo', 'identifier', 'state', 'vote', 'consult')

    def render_vote(self, column, value, record, bound_column):
        """Only render vote column if user has eligible elections"""
        user = self.context.get('request').user
        if record.elections.in_to_vote().eligible(user).exists():
            return column.render(value, record, bound_column)
        return ""


class ElectionHistoryTable(tables.Table):
    """ """
    logo = ImageColumn(
        '', attrs={'th': {'class': 's-10'}}, accessor="electoral_process.logo",
        orderable=False)
    electoral_process_identifier = tables.LinkColumn(
        attrs={'th': {'class': 's-20'}}, orderable=False,
        viewname='voters:electoralprocess-detail',
        args=[tables.A('electoral_process.slug')],
        accessor='electoral_process.identifier')
    identifier = tables.LinkColumn(
        attrs={'th': {'class': 's-20'}}, orderable=False,
        viewname='voters:electoralprocess-detail',
        args=[tables.A('electoral_process.slug')])
    voting_begin_date = DateTimeColumn(attrs={'th': {'class': 's-15'}})
    voting_end_date = DateTimeColumn(attrs={'th': {'class': 's-15'}})

    class Meta:
        model = Election
        empty_text = _('No elections in history')
        attrs = {"class": "pure-table pure-table-line"}
        fields = ('logo', 'electoral_process_identifier', 'identifier',
                  'voting_begin_date', 'voting_end_date')

    def render_identifier(self, column, value, record, bound_column):
        return column.render_link(
            "{}#{}".format(
                column.compose_url(record, bound_column), record.slug),
            record=record,
            value=value
        )


class ElectionVoterTable(tables.Table):
    """
    Table for electoral rolls to list the imported voters
    """
    name = tables.Column(verbose_name=_('name'))
    username = tables.Column(verbose_name=_('personnel number'),
                             accessor='user.username')
    category = tables.Column(verbose_name=_('weight'))

    class Meta:
        model = ElectionVoter
        fields = ('name', 'username', 'category')
        attrs = {"class": "pure-table pure-table-striped"}
        empty_text = _(u'No voters in the electoral roll')
