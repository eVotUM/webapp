# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals


class VotersMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        assert hasattr(request, 'user') and hasattr(request, 'session'), (
            "The Voters middleware requires authentication middleware and "
            "session middleware to be installed. Edit your MIDDLEWARE%s "
            "setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' and "
            "'django.contrib.auth.middleware.AuthenticationMiddleware' before "
            "'evotevotings.voters.middleware.VotersMiddleware'.")

        if request.user.is_authenticated():
            setattr(request.user, 'is_election_voter', True)

        return self.get_response(request)
