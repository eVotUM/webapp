// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, django, noty, Ladda */

/******************************************************************
 * Site Name: eVote
 * Author: Luis Silva
 *
 * JavaScript: Evote Votings App JavaScript (Initializations, etc...)
 *
 *****************************************************************/

"use strict";

/* Expose desired objects globally */
if( typeof VOTE_POPULATE === 'undefined') {
    var VOTE_POPULATE = {};
}
if( typeof VOTING === 'undefined') {
    var VOTING = {};
}

(function($) {
    /* pure window help page */
    var PW_HELP = (function() {

        var self = {
            help_btn  : 'a[data-pw-help-btn]',
            help_page : 'div[data-pw-help-page]',
            close     : 'a[data-pw-close]',
            click     : 'a[data-pw-help-btn], a[data-pw-help-close]',

            bindEvents: function bindEvents() {
                $(self.click).on('click', function (e) {
                    e.preventDefault();
                    $(self.help_btn).toggleClass('selected');
                    $(self.close).toggle();
                    $(self.help_page).toggle();
                });
            },

            initialize: function initialize() {
                self.bindEvents();
            }
        };
        return self;
    })();
    /* Step 3: vote authentication */
    var VOTE_AUTH = (function() {

        var self = {
            pin_type : 'select[data-pin-type]',
            send_pin : 'input[data-send-pin-to]',
            send_pin_label: 'label[data-send-pin-to-label]',

            init: function init() {
                $(self.pin_type).trigger('change');
            },

            bindEvents: function bindEvents() {
                $(self.pin_type).on('change', function (e) {
                    var option = $(this).val(),
                        send_pin = $(self.send_pin),
                        type = send_pin.data(option),
                        send_pin_label = $(self.send_pin_label),
                        label = send_pin_label.data(option);
                    send_pin_label.text(label);
                    send_pin.val(type);
                });
            },

            initialize: function initialize() {
                self.bindEvents();
                self.init();
            }
        };
        return self;
    })();
    /* poplate votes list based ond session storage vote */
    VOTE_POPULATE = (function() {
        var self = {
            list_vote_mark : 'input[data-vote-checkbox]',
            select_vote    : 'select[data-select-vote]',
            candidate_designation : '[data-candidate-designation]',
            candidate_logo : 'img[data-candidate-logo]',
            candidate_vote : 'input[data-candidate-vote]',
            candidate_box  : '[data-vote-option-selected]',

            init: function init(step, votes) {
                step = typeof(step) == "undefined" ? 1 : step;
                votes = typeof(votes) == "undefined" ? [] : votes;
                if (step == '1') {
                    /* Case in step1: populate de edit page (List or Dropdown) */
                    if (VOTING._votingType() == 'dropdown') {
                        self._populateDropdown(votes);
                    } else {
                        self._populateList(votes);
                    }
                }
                if (step == '2') {
                    /* Case in step2: populate de confirmation vote page */
                    self._createCandidateBox(votes);
                    $(votes).each(function(index, el) {
                        self._addVotes(el, index);
                    });
                    $(self.candidate_box).show();
                }
            },
            /* Step 1: if Already vote, populate the edit form,
                       candidate list (dropdown).
             */
            _populateDropdown: function _populateDropdown(votes) {
                var item = null;
                if (votes.length > 0) {
                    $(votes).each(function(index, el) {
                        $($(self.select_vote)[index]).val(el.id).change();
                    });
                }
            },
            /* Step 1: if Already vote, populate the edit form,
                       candidate list.
             */
            _populateList: function _populateList(votes) {
                var item = null;
                $(votes).each(function(index, el) {
                    item = $(self.list_vote_mark+'[value="'+el.id+'"]');
                    item.prop('checked', true).click();
                });
            },
            /* Step 2: Add candidates based on number of votes */
            _createCandidateBox: function _createCandidateBox(votes) {
                var i = 0,
                    candidate_box = $(self.candidate_box);
                for (i; i < (votes.length-1); i++) {
                    candidate_box
                        .clone()
                        .attr('data-vote-option-idx', i+1)
                        .insertAfter(self.candidate_box+':last-child');
                };
            },
            /* Step 2: Insert the candidate info in the list */
            _addVotes: function _addVotes(vote, index) {
                var candidate = $('[data-vote-option-idx="'+index+'"]');
                candidate.find(self.candidate_logo).attr('src', vote.logo);
                candidate.find(self.candidate_designation).text(vote.designation);
                candidate.find(self.candidate_vote).prop('checked', true);
            }
        };
        return self;
    })();
    /* Voting validations: Storage voting example:

        vote = [{'id': '1',
                 'designation': 'Candidate A',
                 'logo': '/media/cache/7e/c4/7ec49be0bc86b4d26bcd1a2d0f8da32c.png',
                 'vote': 1
                },
                {
                 'id': '2',
                 'designation': 'Candidate B',
                 'logo': '/static/images/avatar.png',
                 'vote': 1
                }]
     */
    VOTING = (function() {

        var self = {
            // Election
            select_election: 'a[data-election]',
            // List options
            list_vote      : 'div[data-vote-option]',
            list_vote_mark : 'input[data-vote-checkbox]',
            // Dropdown options
            select_vote     : 'select[data-select-vote]',
            select_vote_mark: 'input[data-vote-simulation]',
            // Vote form and candidate info (storage info)
            candidate_designation : '[data-candidate-designation]',
            candidate_logo  : 'img[data-candidate-logo]',
            allowed_votes: $('form[data-vote-choices]').data('vote-choices'),
            form : 'form[data-js-submit]',
            submit: '[data-next-step]',
            is_ordered_voting: ($('form[data-ordered-voting]').length>0 ? true : false),
            storage: null,

            bindEvents: function bindEvents() {
                $(self.list_vote).on('click', function (e) {
                    e.preventDefault();
                    self._voteList(this);
                });
                $(self.select_vote).on('change', function (e) {
                    self._voteDropdown(this);
                });
                $(self.form).on('submit', function (e) {
                    e.preventDefault();
                });
                $(self.submit).on('click', function (e) {
                    // otherwise do nothing
                });
                $(self.select_election).on('click', function (e) {
                    if(typeof CRIPTO !== 'undefined') {
                        var election_id = $(this).data('election');
                        CRIPTO.customReset(election_id);
                    }
                });
            },
            prepareOrderedVoting: function prepareOrderedVoting() {
                if (self.is_ordered_voting) {
                    self._dropdownDisabledAllVotes($(self.select_vote).first());
                }
            },
            /* return the votation type */
            _votingType: function _votingType() {
                if ($(self.select_vote).length > 0) {
                    return 'dropdown';
                }
                return 'list';
            },
            /* save vote in user session storage */
            _setVote: function _setVote(value) {
                self.storage.storeVote(JSON.stringify(value));
            },
            /* return vote from user session storage */
            getSelectedVotes: function() {
                if (self._votingType() == 'dropdown') {
                    var votes = self._getDropdownVotes();
                }
                if (self._votingType() == 'list') {
                    var votes = self._getListVotes();
                }
                return votes;
            },
            /* List votes methods
             */
            /* validate vote */
            _validListVote: function _validListVote() {
                var current_votes = $(self.list_vote_mark).filter(':checked').length;
                if (self.allowed_votes > current_votes) {
                    return true;
                }
                return false;
            },
            /* add vote, if possible */
            _voteList: function _voteList(elem) {
                if (self._validListVote() || $(elem).hasClass('selected')) {
                    $(elem).toggleClass('selected');
                    var $checkbox = $(elem).find(':checkbox');
                    $checkbox.prop('checked', !$checkbox[0].checked);
                } else {
                    noty({
                        text: django.gettext("Maximum number of candidates to choose exceeded"),
                        type: "error"
                    });
                }
            },
            /* return votes in JSON format */
            _getListVotes: function _getListVotes() {
                var votes = new Array(),
                    candidate = null,
                    elem = {};
                $($(self.list_vote_mark).filter(':checked')).each(function(index, el) {
                    candidate = $($(el).parents('.candidate-box'));
                    elem = {
                        'id': el.value,
                        'designation': candidate.find(self.candidate_designation).text(),
                        'logo': candidate.find(self.candidate_logo).attr('src'),
                        'vote': 1
                    };
                    votes.push(elem);
                });
                return votes;
            },
            /* Dropdown votes methods
             */
            _validDropdownVote: function _validDropdownVote() {
                var votes = $.map($(self.select_vote), function(vote) {
                                return vote.value;
                            });
                votes = votes.filter(function(n){ return n != '' });
                if ( votes.length != _.uniq(votes).length ) {
                    noty({
                        text: django.gettext("You already voted for this candidate."),
                        type: "error"
                    });
                    return false;
                }
                return true
            },
            _dropdownDisabledAllVotes: function _dropdownDisabledAllVotes(elem) {
                var candidates = $($(elem).parents('.candidate-box')).nextAll();
                $(candidates).each(function(index, el) {
                    self._dropdownResetVote($(el).find('select'), true);
                });
            },
            _dropdownEnableVote: function _dropdownEnableVote(elem) {
                var candidate = $($(elem).parents('.candidate-box')).next();
                candidate.removeClass('disabled');
                candidate.find('select').attr("disabled", false);
            },
            _dropdownResetVote: function _dropdownResetVote(elem, disabled) {
                $(elem).val('').trigger('change.select2');
                var candidate = $($(elem).parents('.candidate-box'));
                var img_path  = candidate.find('option:selected').data('logo');
                candidate.removeClass('selected');
                candidate.find(self.candidate_logo).attr('src', img_path);
                candidate.find(self.select_vote_mark).prop('checked', false);
                if (disabled) {
                    candidate.addClass('disabled');
                    $(elem).attr("disabled", true);
                }
            },
            _dropdownMarkVote: function _dropdownMarkVote(elem) {
                var candidate = $($(elem).parents('.candidate-box'));
                var img_path  = candidate.find('option:selected').data('logo');
                candidate.addClass('selected');
                candidate.find(self.candidate_logo).attr('src', img_path );
                candidate.find(self.select_vote_mark).prop('checked', true);
            },
            _voteDropdown: function _voteDropdown(elem) {
                if (self._validDropdownVote() && elem.value != '') {
                    self._dropdownMarkVote(elem);
                    if (self.is_ordered_voting) {
                        self._dropdownEnableVote(elem);
                    }
                } else {
                    // reset selected option when the vote is invalid
                    // or remove vote
                    self._dropdownResetVote(elem, false);
                    if (self.is_ordered_voting) {
                        self._dropdownDisabledAllVotes(elem);
                    }
                }
            },
            /* return votes in JSON format */
            _getDropdownVotes: function _getDropdownVotes() {
                var votes = new Array(),
                    option = null,
                    elem = {};
                $(self.select_vote).each(function(index, el) {
                    if (el.value != '') {
                        option = $(el).find('option:selected');
                        elem = {
                            'id': el.value,
                            'designation': option.text(),
                            'logo': option.data('logo'),
                            'vote': 1
                        };
                        if (self.is_ordered_voting) {
                            elem['vote'] = self.allowed_votes - index;
                        }
                        votes.push(elem);
                    }
                });
                return votes;
            },
            /* initialize functions */
            initialize: function initialize() {
                if(typeof SRVDATA !== 'undefined')
                    self.storage = SRVDATA;
                self.bindEvents();
                self.prepareOrderedVoting();
            }
        };
        return self;
    })();

    $(document).ready(function() {
        PW_HELP.initialize();
        VOTING.initialize();
        VOTE_AUTH.initialize();
    });

})(jQuery);

function initGMaps(elem, location, zoom){
    var map = new google.maps.Map(elem, {
        zoom: zoom,
        center: location
    });
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
}

// Google maps init
function contactsInitMap() {
    var mapdiv = document.getElementById('contacts-map');
    var address = $(mapdiv).data('address');
    var gps = $(mapdiv).data('gps').split(',');
    var use_address = typeof address !== 'undefined' ? true : false;
    var location = {'lat': parseFloat(gps[0]), 'lng': parseFloat(gps[1])}
    if (use_address) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                location = results[0].geometry.location
            }
            initGMaps(mapdiv, location, 15);
        });
    } else {
        initGMaps(mapdiv, location, 15);
    }
}
