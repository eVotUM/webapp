# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django import forms

from evotevotings.elections.models import Candidate
from evotemessages.forms import MessageBaseForm
from captcha.fields import ReCaptchaField
from evotecore import widgets


class PerPageFiltersForm(forms.Form):
    """ """

    PER_PAGE_CHOICES = [(5, 5), (10, 10)]

    per_page = forms.ChoiceField(choices=PER_PAGE_CHOICES,
                                 label=_('See by page'), required=False,
                                 widget=widgets.Select)

    class Meta:
        fields = ('per_page')


class IndexFiltersForm(forms.Form):
    """ """

    PER_PAGE_CHOICES = [(5, 5), (10, 10)]

    per_page = forms.ChoiceField(choices=PER_PAGE_CHOICES,
                                 label=_('See by page'), required=False,
                                 widget=widgets.Select)

    class Meta:
        fields = ('per_page')


class ElectionVotingStep1Form(forms.Form):
    """ """
    # Hidden session fields
    session_fields = ['combo_id', 'election_id', 'pr_components',
                      'election_cert', 'vs_cert', 'category_slug', ]
    candidates_list_0 = forms.ModelMultipleChoiceField(
        required=True,
        queryset=Candidate.objects.none(),
        widget=forms.CheckboxSelectMultiple)

    # {User + Election ID}sv
    combo_id = forms.CharField(widget=forms.HiddenInput)
    # {Election ID}sv
    election_id = forms.CharField(widget=forms.HiddenInput)
    # prDashComponents
    pr_components = forms.CharField(widget=forms.HiddenInput)
    # Election Certificate
    election_cert = forms.CharField(widget=forms.HiddenInput)
    # Validator Service Certificate
    vs_cert = forms.CharField(widget=forms.HiddenInput)
    # Voter category identifier (for vote weight purposes)
    category_slug = forms.CharField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        """" """
        election = kwargs.pop('instance')
        kw_combo_id = kwargs.pop('combo_id', None)
        kw_election_id = kwargs.pop('election_id', None)
        kw_vs_cert = kwargs.pop('vs_cert', None)
        kw_category_slug = kwargs.pop('category_slug', None)
        super(ElectionVotingStep1Form, self).__init__(*args, **kwargs)

        # Election Model data
        self.initial['pr_components'] = election.blindsig_pr_dash_components
        self.initial['election_cert'] = election.certificate
        # Election extra data
        self.initial['combo_id'] = kw_combo_id
        self.initial['election_id'] = kw_election_id
        self.initial['vs_cert'] = kw_vs_cert
        self.initial['category_slug'] = kw_category_slug

        qs = election.candidates.write_in()
        if not election.write_in:
            qs = election.candidates.not_write_in()

        if election.is_list_mode():
            self.fields['candidates_list_0'].queryset = qs

        if not election.is_list_mode():
            choices = election.limit_remarkable_choices
            for n in range(0, choices):
                self.fields["candidates_list_%d" % n] = forms.ModelChoiceField(
                    empty_label=_('Select candidate/list of candidates'),
                    required=True,
                    queryset=qs,
                    widget=widgets.Select)

    def get_session_fields(self):
        """ """
        return [field for field in self
                if field.name in self.session_fields]

    def get_nonsession_fields(self):
        """ """
        return [field for field in self
                if field.name not in self.session_fields]


class ElectionVotingStep3Form(forms.Form):
    """ """
    SEND_TO_CHOICES = [('sms', 'SMS'), ('email', 'E-mail')]

    send_through = forms.ChoiceField(
        choices=SEND_TO_CHOICES, initial='sms',
        label=_('The voting code will be sent to (or select the CMD'
                ' Authentication button below)') + ":",
        required=False, widget=widgets.Select(attrs={'data-pin-type': ''},))

    send_pin_to = forms.CharField(label=_('Send PIN to') + ":",
                                  required=False,
                                  widget=forms.TextInput)

    def __init__(self, *args, **kwargs):
        """" """
        self.election = kwargs.pop('instance')
        user = kwargs.pop('user')
        super(ElectionVotingStep3Form, self).__init__(*args, **kwargs)

        self.initial['send_pin_to'] = user.get_phone()
        self.fields['send_pin_to'].widget = forms.TextInput(attrs={
            'data-send-pin-to': '',
            'disabled': '',
            'data-sms': user.get_phone(),
            'data-email': user.get_email(),
        })

        if user.notice_method == user.NoticeMethod.EMAIL:
            self.initial['send_through'] = 'email'
            self.initial['send_pin_to'] = user.get_email()


class ElectionVotingStep3PinForm(forms.Form):
    """ """
    pin_validation = forms.CharField(label=_('Insert the voting code') + ":",
                                     required=False,
                                     widget=forms.TextInput)

    def __init__(self, *args, **kwargs):
        """" """
        self.election = kwargs.pop('instance')
        super(ElectionVotingStep3PinForm, self).__init__(*args, **kwargs)

        self.fields['pin_validation'].widget = forms.TextInput(
            attrs={'data-pin-validation': ''})


class ElectionSearchVoteForm(forms.Form):
    """ """

    search = forms.CharField(
        required=False, widget=forms.TextInput(attrs={
            'placeholder': _('Insert your voting reference here')}))

    class Meta:
        fields = ('search',)


class ContactsForm(forms.Form):
    """ """

    name = forms.CharField(
        label=_("Name"), max_length=254,
        widget=forms.TextInput(attrs={
            'autofocus': True,
            'placeholder': _("Name"),
            'autocomplete': 'off'
        }))
    email = forms.EmailField(
        label=_("Email"), max_length=254, widget=forms.EmailInput(attrs={
            'autofocus': True,
            'placeholder': _("Your email address"),
            'autocomplete': 'off'
        }))
    subject = forms.CharField(
        label=_("Subject"), max_length=254, widget=forms.TextInput(attrs={
            'autofocus': True,
            'placeholder': _("Subject"),
            'autocomplete': 'off'
        }))
    message = forms.CharField(
        label=_("Message"), widget=forms.Textarea(attrs={
            'autofocus': True,
            'placeholder': _("Message"),
            'autocomplete': 'off',
            'rows': 4
        }))
    captcha = ReCaptchaField()


class ElectionClaimMessageForm(MessageBaseForm):
    """ """
    recipients = forms.CharField(disabled=True)
    captcha = ReCaptchaField()

    class Meta(MessageBaseForm.Meta):
        fields = MessageBaseForm.Meta.fields + ('recipients', 'captcha',)

    def __init__(self, *args, **kwargs):
        super(ElectionClaimMessageForm, self).__init__(*args, **kwargs)
        self.fields['subject'].disabled = True
        if self.user.is_authenticated():
            del self.fields['captcha']
