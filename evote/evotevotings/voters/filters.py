# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import django_filters

from django.db.models import Q

from evotevotings.processes.models import ElectoralProcess
from evotevotings.voters.forms import PerPageFiltersForm, IndexFiltersForm
from evotevotings.elections.models import Election
from evotecore import widgets

from .models import ElectionVoter


def wrapper_choices(queryset):
    def choices():
        return [(obj.year, obj.year) for obj in queryset]
    return choices


class YearFilter(django_filters.ChoiceFilter):
    """
    Custom filter for the year
    """

    def __init__(self, queryset, *args, **kwargs):
        super(YearFilter, self).__init__(
            choices=wrapper_choices(queryset), *args, **kwargs)

    def filter(self, qs, value):
        if value:
            return qs.filter(publication_date__year=value)
        return qs


class VotingEndYearFilter(django_filters.ChoiceFilter):
    """
    Custom filter for the year
    """

    def __init__(self, queryset, *args, **kwargs):
        super(VotingEndYearFilter, self).__init__(
            choices=wrapper_choices(queryset), *args, **kwargs)

    def filter(self, qs, value):
        if value:
            return qs.filter(voting_end_date__year=value)
        return qs


class ArchiveFilter(django_filters.FilterSet):
    """
    Filter of Archives
    """
    publication_date = YearFilter(
        label='',
        widget=widgets.Select,
        queryset=ElectoralProcess.objects.closed().dates(
            'publication_date', 'year'))

    class Meta:
        model = ElectoralProcess
        form = PerPageFiltersForm
        fields = ['publication_date']


class ElectionHistoryFilter(django_filters.FilterSet):
    """
    Filter of History
    """
    voting_end_date = VotingEndYearFilter(
        label='',
        widget=widgets.Select,
        queryset=Election.objects.all().dates(
            'voting_end_date', 'year'))

    class Meta:
        model = Election
        form = PerPageFiltersForm
        fields = ['voting_end_date']


class IndexFilter(django_filters.FilterSet):
    """
    Filter of index page
    """

    class Meta:
        model = ElectoralProcess
        form = IndexFiltersForm


class RollVoterFilter(django_filters.CharFilter):
    """
    Custom filter for the year
    """

    def filter(self, qs, value):
        if value:
            return qs.filter(Q(name__icontains=value) |
                             Q(user__username__icontains=value))
        return qs


class VoterFilter(django_filters.FilterSet):
    """
    Filter of voters of electoral roll
    """

    search = RollVoterFilter()

    class Meta:
        model = ElectionVoter
        fields = ['search']
