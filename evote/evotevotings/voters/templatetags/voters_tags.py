# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django import template
from django.utils.safestring import mark_safe

from evotevotings.elections.models import Candidate
from evotevotings.admin.models import Page


register = template.Library()

# This PK is associated with the fixture of pages (pages.json) and related to
# the page that stores the voting faqs
VOTING_FAQS_PAGE_PK = 11


@register.inclusion_tag('voters/includes/candidate_list.html')
def candidate_list(candidates_list, **kwargs):
    """ """
    candidates = []
    for candidate in candidates_list:
        candidates.append((candidate_obj(candidate.choice_value), candidate))
    return {
        'candidate_list': candidates,
    }


@register.inclusion_tag('voters/includes/candidate_dropdown.html')
def candidate_dropdown(candidates_list, **kwargs):
    """ """
    candidates = []
    extra_attrs = []
    if candidates_list.field.required:
        extra_attrs.append('required')
    if candidates_list.field.disabled:
        extra_attrs.append('disabled')

    for candidate in candidates_list.field.choices:
        if candidate[0]:
            candidates.append((candidate_obj(candidate[0]), candidate[1]))
        else:
            candidates.append(('', candidate[1]))
    return {
        'candidate_list_attr': candidates_list,
        'candidate_list': candidates,
        'extra_attrs': ' '.join(extra_attrs)
    }


@register.simple_tag
def candidate_obj(candidate_id):
    """ """
    return Candidate.objects.get(pk=candidate_id)


@register.simple_tag
def election_closed_for_voter(user_id, election):
    """ Return False if the election is not closed and the user did not
        submit his vote. """
    if not election.is_in_to_vote:
        return True
    election_voter = election.election_voters.filter(user=user_id).first()
    return not bool(election_voter and election_voter.can_vote())


@register.simple_tag
def voting_faqs():
    obj = Page.objects.filter(pk=VOTING_FAQS_PAGE_PK).first()
    if obj:
        return mark_safe(obj.body)
    return ""
