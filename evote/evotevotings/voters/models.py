# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django.conf import settings
from django.db import models

from evotevotings.elections.models import Category, Election
from django_extensions.db.models import TimeStampedModel
from evotelogs.decorators import auditable
from guardian.shortcuts import assign_perm, remove_perm
from contrib.umws.sms import sms_service
from evotedocs.models import Document
from autoslug import AutoSlugField

from .managers import ElectionVoterManager, ElectoralRollQuerySet
from .mailings import SendPinMail
from .utils import generate_pin, pin_expiration_date
from .tasks import (update_electoral_roll_permissions,
                    generate_electoral_roll_pdf)


secutiry_logger = logging.getLogger("evote.security")
logger = logging.getLogger("evote.core")


def populate_electoral_roll(instance):
    return instance.document.designation


@auditable
@python_2_unicode_compatible
class ElectoralRoll(TimeStampedModel):
    """ """
    election = models.ForeignKey(
        Election, verbose_name=_("election"), related_name="electoral_rolls",
        on_delete=models.CASCADE)
    document = models.ForeignKey(
        Document, verbose_name=_("document"), related_name="+",
        on_delete=models.PROTECT)
    pdf_document = models.ForeignKey(
        Document, verbose_name=_("PDF document"), related_name="+",
        null=True, default=None)
    slug = AutoSlugField(
        populate_from=populate_electoral_roll,
        unique=True, editable=False)
    imported = models.NullBooleanField(default=None)
    published_on = models.DateTimeField(
        _("published on"), null=True, default=None)
    published = models.BooleanField(default=False)
    objects = ElectoralRollQuerySet.as_manager()

    class Meta:
        ordering = ("-created",)
        verbose_name = _("electoral roll")
        verbose_name_plural = _("electoral rolls")

    def __str__(self):
        return self.document.designation

    @property
    def audit_group(self):
        elec_proc = self.election.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)

    @property
    def version(self):
        return self.created.strftime('%Y%m%d%H%M')

    def publish(self):
        """Update date of publication, mark voters as active and inactive, and
        update all permissions"""
        # mark new voters as active
        self.election_voters.update(is_active=True)
        # unmark voters wich are not listed as inactive
        voter_ids = list(self.election_voters.values_list('id', flat=True))
        inactivate_voters = self.election.election_voters\
            .exclude(pk__in=voter_ids)
        if inactivate_voters.exists():
            inactivate_voters.update(is_active=False)
        # update published date
        self.published_on = timezone.now()
        self.published = True
        self.save(update_fields=['published_on', 'published'])
        # change previous published rolls to unpublished
        previous_published = ElectoralRoll.objects\
            .filter(election_id=self.election_id, published=True)\
            .exclude(pk=self.pk)
        if previous_published.exists():
            previous_published.update(published=False)
        # launch tasks to assign permissions and generate pdf file
        update_electoral_roll_permissions.si(self.pk).delay()
        generate_electoral_roll_pdf.si(self.pk).delay()


class VoteValidationMixin(models.Model):
    """Mixin with vote validation fields
    """
    class SendThrough:
        SMS = 'sms'
        EMAIL = 'email'
        AMA = 'ama'

        choices = (
            (SMS, 'SMS'),
            (EMAIL, "Email"),
            (AMA, "AMA"),
        )

    pin_value = models.CharField(
        _("PIN"),
        max_length=64,
        editable=False,
        null=True)
    token_send_through = models.CharField(
        _("Authenticate using PIN (Email/SMS) or .Gov"),
        max_length=32,
        editable=False,
        null=True)
    token_expiration_date = models.DateTimeField(
        _("Authentication token validation date"),
        editable=False,
        null=True)
    token_validated = models.BooleanField(
        _("Valid token"),
        default=False)

    class Meta:
        abstract = True

    def is_token_valid(self, pin_value=None):
        """ """
        if self.token_send_through != self.SendThrough.AMA and \
                pin_value != self.pin_value:
            secutiry_logger.warning(
                "Pin validation: invalid pin '%s'", pin_value)
            return False
        if timezone.now() > self.token_expiration_date:
            secutiry_logger.warning(
                "Pin validation: pin expired token_expiration_date=%s",
                self.token_expiration_date)
            return False
        self.token_validated = True
        self.save(update_fields=['token_validated'])
        secutiry_logger.info("Pin validation: pin valid")
        return True

    def send_pin(self, send_through):
        self.pin_value = generate_pin()
        self.token_send_through = send_through
        self.token_expiration_date = pin_expiration_date()
        self.token_validated = False
        if send_through == self.SendThrough.SMS:
            send_to = self.user.get_phone()
            sms_text = settings.PIN_SMS_CONTENT.format(pin=self.pin_value)
            sms_service.send_message(send_to, sms_text)
        elif send_through == self.SendThrough.EMAIL:
            send_to = [(self.user.name, self.user.get_email())]
            mail_info = {
                'pin_value': self.pin_value,
                'pin_expiration_date': self.token_expiration_date
            }
            SendPinMail(mail_info).send(send_to)
        else:
            errmsg = "Can't send pin, '{}' is not a valid method"\
                .format(send_through)
            logger.error(errmsg)
            raise ValueError(errmsg)
        self.save(update_fields=[
            'pin_value',
            'token_send_through',
            'token_expiration_date',
            'token_validated'
        ])


@auditable
@python_2_unicode_compatible
class ElectionVoter(TimeStampedModel, VoteValidationMixin):
    """ """
    class VoteStatus:
        NO = 0
        YES = 1
        PENDING = 2

        choices = (
            (NO, _("Unsubmitted")),
            (YES, _("Submitted")),
            (PENDING, _("Submitting")),
        )
        names = {
            NO: 'unsubmitted',
            YES: 'submitted',
            PENDING: 'submitting',
        }

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("user"),
        on_delete=models.CASCADE, related_name="election_voters")
    election = models.ForeignKey(
        Election, verbose_name=_("election"), related_name="election_voters",
        on_delete=models.CASCADE)
    category = models.ForeignKey(
        Category, verbose_name=_("category"), related_name="election_voters",
        on_delete=models.PROTECT)
    electoral_rolls = models.ManyToManyField(
        ElectoralRoll, verbose_name=_("electoral rolls"),
        related_name="election_voters")
    name = models.CharField(_("name"), max_length=255, db_index=True)
    is_active = models.BooleanField(_("is active"), default=False)
    blind_vote_hash = models.CharField(
        _("blind user cyphered vote hash"), max_length=255, editable=False,
        db_index=True, null=True)
    vote_status = models.PositiveSmallIntegerField(
        _("vote status"), choices=VoteStatus.choices, default=VoteStatus.NO)
    objects = ElectionVoterManager()

    class Meta:
        ordering = ('name',)
        verbose_name = _("voter")
        verbose_name_plural = _("voters")
        index_together = ['election', 'vote_status']

    def __str__(self):
        return self.name

    @property
    def audit_group(self):
        elec_proc = self.election.electoral_process
        group_type = ContentType.objects.get_for_model(elec_proc)
        return (group_type.id, elec_proc.pk)

    @property
    def is_vote_available(self):
        """Checks if this ElectionVoter vote is still pending
           Can either be in PENDING after 2 minutes or
           in the NO status"""
        if(self.vote_status == self.VoteStatus.NO):
            return (0, True)
        if(self.vote_status == self.VoteStatus.YES):
            return (1, False)

        current_date = timezone.now()
        ttl = settings.PIN_TIMETOLIVE
        threshold = current_date - timezone.timedelta(seconds=ttl)
        available = self.modified < threshold

        if(available):
            return (0, True)
        else:
            return (2, False)

    def can_vote(self):
        return self.vote_status != self.VoteStatus.YES

    def assign_perm(self):
        """"""
        assign_perm("view_election", self.user, self.election)
        assign_perm("vote_election", self.user, self.election)
        assign_perm("view_electoralprocess", self.user,
                    self.election.electoral_process)

    def remove_perm(self):
        """ """
        remove_perm("view_election", self.user, self.election)
        remove_perm("vote_election", self.user, self.election)
        # check if this user has elections active in the electoral process
        election_ids = Election.objects\
            .filter(electoral_process=self.election.electoral_process_id)\
            .values_list('pk', flat=True)
        has_active_elections = ElectionVoter.objects\
            .filter(election_id__in=election_ids, user=self.user,
                    is_active=True).exists()
        if not has_active_elections:
            remove_perm("view_electoralprocess", self.user,
                        self.election.electoral_process)
