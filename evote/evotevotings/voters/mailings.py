# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.conf import settings

from evotevotings.elections.models import Election
from evotemailings.mailings import BaseMailing


class ContactMail(BaseMailing):
    """ """
    template_name = 'voters/mails/contact_mail.html'

    def get_mail_to(self):
        name = getattr(settings, "EVOTE_CONTACT_NAME")
        email = getattr(settings, "EVOTE_CONTACT_MAIL")
        return [(name, email)]

    def get_mail_reply_to(self):
        return [self.get_context_data().get('from')]

    def get_subject(self):
        name = self.get_context_data().get('name')
        msg = "eVotUM - Contacto de " + name + " | Contact from " + name
        subject = msg
        return subject


class SendPinMail(BaseMailing):
    """ """
    template_name = 'voters/mails/pin_mail.html'

    def get_mail_to(self):
        name = getattr(settings, "EVOTE_CONTACT_NAME")
        email = getattr(settings, "EVOTE_CONTACT_MAIL")
        return [(name, email)]

    def get_mail_reply_to(self):
        return [self.get_context_data().get('from')]

    def get_subject(self):
        subject = "eVotUM - Código de votação | Voting code"
        return subject


class SendReferenceMail(BaseMailing):
    """ """
    template_name = 'voters/mails/reference_mail.html'

    def get_subject(self):
        subject = "eVotUM - Referência de voto | Vote reference"
        return subject

    def get_context_data(self):
        context = super(SendReferenceMail, self).get_context_data()
        election_id = context.get('election_id')
        context.update({
            'election': Election.objects.get(pk=election_id)
        })
        return context
