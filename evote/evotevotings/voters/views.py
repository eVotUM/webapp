# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.utils.six.moves.urllib.parse import urlparse
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import UpdateView
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.utils.encoding import force_text
from django.views.generic import ListView, DetailView, TemplateView
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from django.http import Http404

from evotevotings.processes.recipients import \
    ElectoralCommissionRecipientsGroup
from evotevotings.processes.models import ElectoralProcess
from evotevotings.elections.models import Election
from evotevotings.elections.utils import get_extra_election_data
from evoteajaxforms.views import FormJsonSimpleView, AjaxResponseAction
from evotemessages.views import (MessageReceivedListMixin,
                                 MessageSentListMixin, MessageDetailMixin,
                                 MessageArchivedListMixin, MessageCreateMixin,
                                 MessageArchiveMixin)
from evoteajaxforms.views import FormJsonView
from evotemessages.utils import gen_uid
from contrib.ama.utils import get_attributes
from evotesearch.views import SearchMixin
from evotecore.mixins import (DjangoTablesFilterMixin, PermissionListMixin,
                              PermissionRequiredMixin, SelectedMenuMixin)
from evoteusers.views import (EvoteLoginView, PersonalDataBaseView,
                              ValidateEmailBaseView,
                              ResendPendingTokenMailBaseView, EvoteLogoutView)
from evotecore.mixins import FeedbackMessageMixin
from evotecore.views import ErrorBaseView

from .mailings import ContactMail
from .filters import (ArchiveFilter, ElectionHistoryFilter, IndexFilter,
                      VoterFilter)
from .tables import (ElectoralProcessTable, ElectoralProcessArchiveTable,
                     ElectoralProcessMyTable, ElectionHistoryTable,
                     ElectionVoterTable)
from .models import ElectionVoter, ElectoralRoll
from .forms import (ElectionVotingStep1Form, ElectionVotingStep3Form,
                    ElectionVotingStep3PinForm, ElectionSearchVoteForm,
                    ContactsForm, ElectionClaimMessageForm)
from .utils import get_references, pin_expiration_date
from .menus import Menu


security_logger = logging.getLogger("evote.security")


class LoginView(EvoteLoginView):
    """Provides user the ability to login"""
    use_adfs = getattr(settings, "ADFS_WSFED", False)
    template_name = "voters/login.html"

    def dispatch(self, request, *args, **kwargs):
        """ """
        if self.use_adfs and request.user.is_authenticated():
            return redirect(settings.LOGIN_REDIRECT_URL)
        return super(LoginView, self).dispatch(request, *args, **kwargs)


class LogoutView(EvoteLogoutView):
    """Provides users the ability to logout"""

    def get_redirect_url(self):
        return reverse("evotecore:index")


class ElectoralProcessListView(
        SelectedMenuMixin, PermissionListMixin, DjangoTablesFilterMixin,
        ListView):
    """
    Shows the main view
    """
    model = ElectoralProcess
    queryset = ElectoralProcess.objects.published()
    permission_required = "evoteprocesses.view_electoralprocess"
    # tables
    table_pagination = {'per_page': 5}
    # filters
    filter_class = IndexFilter

    def get_table_class(self):
        if self.request.user.is_authenticated():
            return ElectoralProcessMyTable
        return ElectoralProcessTable

    def get_template_names(self):
        if self.request.user.is_authenticated():
            return ["voters/electoralprocess_mylist.html"]
        return ["voters/electoralprocess_list.html"]


class ElectoralProcessDetailView(
        SelectedMenuMixin, PermissionRequiredMixin, DetailView):
    """
    Shows the Electoral Process Detail view
    """
    model = ElectoralProcess
    permission_required = "evoteprocesses.view_electoralprocess"

    def get_template_names(self):
        if self.request.user.is_authenticated():
            return ["voters/electoralprocess_mydetail.html"]
        return ["voters/electoralprocess_detail.html"]

    def get_context_data(self, **kwargs):
        context = super(ElectoralProcessDetailView, self)\
            .get_context_data(**kwargs)
        context.update({
            'show_vote_link': self.object.elections.eligible(self.request.user)
            .in_to_vote().exists(),
            'state_scrutinized': Election.State.VOTES_SCRUTINIZED
        })
        return context


class ElectoralProcessArchiveView(
        SelectedMenuMixin, PermissionListMixin, DjangoTablesFilterMixin,
        ListView):
    """
    Shows the archive view
    """
    model = ElectoralProcess
    queryset = ElectoralProcess.objects.closed()
    permission_required = "evoteprocesses.view_electoralprocess"
    template_name = "voters/electoralprocess_archive.html"
    # tables
    table_class = ElectoralProcessArchiveTable
    table_pagination = {'per_page': 5}
    # filters
    filter_class = ArchiveFilter


class ContactsView(SelectedMenuMixin, FormJsonSimpleView):
    """ """
    prefix = 'contacts'
    form_class = ContactsForm
    template_name = "voters/contacts.html"
    action = AjaxResponseAction.REFRESH
    selected_menu = Menu.CONTACTS
    success_message = _("Your request was submitted successfully.")

    def form_valid(self, form):
        context = {
            'name': form.cleaned_data.get('name', ''),
            'from': form.cleaned_data.get('email', ''),
            'subject': form.cleaned_data.get('subject', ''),
            'message': form.cleaned_data.get('message', '')
        }
        ContactMail(context).send()
        return super(ContactsView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """ """
        context = super(ContactsView, self).get_context_data(**kwargs)
        context.update({
            'name': getattr(settings, "EVOTE_CONTACT_NAME"),
            'email': getattr(settings, "EVOTE_CONTACT_MAIL"),
            'phone': getattr(settings, "EVOTE_CONTACT_PHONE"),
            'address': getattr(settings, "EVOTE_CONTACT_ADDRESS"),
            'map_gps': getattr(settings, "EVOTE_CONTACT_GPS", None)
        })
        return context


@method_decorator(login_required, name="dispatch")
class PersonalDataView(SelectedMenuMixin, PersonalDataBaseView):
    """ """
    template_name = "voters/personal_data.html"
    selected_menu = Menu.PERSONALDATA

    def get_form_kwargs(self):
        """ """
        form_kwargs = super(PersonalDataView, self).get_form_kwargs()
        form_kwargs.update({
            'base_url': self.request.build_absolute_uri(
                reverse("voters:validate-email"))
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(PersonalDataView, self).get_context_data(**kwargs)
        context.update({
            'resend_url': reverse_lazy("voters:resend-email")
        })
        return context


class ValidateEmailView(ValidateEmailBaseView):
    """ """
    template_name = "voters/validate_email.html"


@method_decorator(login_required, name="dispatch")
class ResendPendingTokenMailView(ResendPendingTokenMailBaseView):
    """ Resends email with token """

    def get_redirect_url(self):
        return reverse_lazy("voters:personal-data")

    def get_base_validate_url(self):
        return reverse("voters:validate-email")


class SearchView(SearchMixin, TemplateView):
    """ View showing search results from the base search query """
    template_name = "voters/search.html"


class HowitworksView(SelectedMenuMixin, TemplateView):
    """ """
    template_name = "voters/howitworks.html"
    selected_menu = Menu.HOWDOESITWORK


@method_decorator(login_required, name="dispatch")
class ElectionHistoryView(
        SelectedMenuMixin, PermissionListMixin, DjangoTablesFilterMixin,
        ListView):
    """
    Shows the my elections history view
    """
    model = Election
    queryset = Election.objects.closed()
    permission_required = "evoteelections.view_election"
    template_name = "voters/election_history.html"
    # tables
    table_class = ElectionHistoryTable
    table_pagination = {'per_page': 5}
    # filters
    filter_class = ElectionHistoryFilter
    selected_menu = Menu.HISTORICAL


"""
VOTING VIEWS
"""


@method_decorator(login_required, name="dispatch")
class ElectoralProcessVotingView(
        SelectedMenuMixin, PermissionRequiredMixin, DetailView):
    """
    Shows the voting system
    """
    model = ElectoralProcess
    permission_required = "evoteprocesses.view_electoralprocess"
    template_name = "voters/electoralprocess_voting.html"

    def get_context_data(self, **kwargs):
        """ """
        context = super(ElectoralProcessVotingView, self)\
            .get_context_data(**kwargs)
        elections = self.object.elections.eligible(self.request.user)\
            .in_to_vote()
        context.update({
            'close_confirm': False,
            'elections': elections,
            'elections_active_count': elections.count(),
            'close_url': reverse_lazy(
                "voters:electoralprocess-detail", kwargs={
                    'slug': self.object.slug
                })
        })
        return context


class InvalidStepPath(Exception):
    """ """
    pass


class NoRefererDefined(Exception):
    """ """
    pass


class SecureStepsMixin(object):
    """Secure steps navigation checking the referer to validate from where
    the user came from and compare to where it's trying to go. If invalid
    navigation is detected, return the user to the first step of the form."""
    steps = []
    steps_urls = []

    def process_steps(self):
        self.steps_urls = []  # reset
        for step in self.steps:
            self.steps_urls.append(self.reverse_step_url(step))
        try:
            referer = self.get_referer()
            to_index = self.get_step_index(self.request.path)
            if to_index == 0:
                return None
            from_index = self.get_step_index(referer.path)
            if not (abs(to_index - from_index) <= 1):
                # redirect user to where it came from
                return referer.path
        except (InvalidStepPath, NoRefererDefined) as e:
            security_logger.warning(e, exc_info=True)
            return self.steps_urls[0]
        return None

    def get_step_index(self, path):
        try:
            return self.steps_urls.index(path)
        except ValueError:
            raise InvalidStepPath("The step path '{}' is invalid".format(path))

    def get_referer(self):
        referer = force_text(
            self.request.META.get('HTTP_REFERER'), strings_only=True,
            errors='replace')
        if not referer:
            raise NoRefererDefined()
        return urlparse(referer)

    def reverse_step_url(self, stepid):
        raise NotImplementedError()


class ElectionVotingStepMixin(SecureStepsMixin, PermissionRequiredMixin):
    """
    Generic class for all steps
    """
    model = Election
    steps = ['1', '2', '3', '3-pin', '4']  # order is important
    slug_url_kwarg = "slug_election"
    permission_required = "evoteelections.vote_election"

    def dispatch(self, request, *args, **kwargs):
        """ """
        self.object = self.get_object()
        self.election_voter = self.get_election_voter_object(self.object.pk)
        if self.secure_step():
            # check if voter has already submitted his vote
            if not self.election_voter.can_vote():
                raise Http404("You already voted on this election!")
            # check
            redirect_to = self.process_steps()
            # avoid loop redirects
            if redirect_to and redirect_to != request.path:
                return redirect(redirect_to)
        return super(ElectionVotingStepMixin, self)\
            .dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ElectionVotingStepMixin, self)\
            .get_context_data(**kwargs)
        context.update({
            'close_confirm': True,
            'close_url': reverse_lazy(
                "voters:electoralprocess-detail", kwargs={
                    'slug': self.object.electoral_process.slug
                })
        })
        return context

    def secure_step(self):
        return True

    def get_election_voter_object(self, election_id):
        return get_object_or_404(ElectionVoter, election_id=election_id,
                                 user=self.request.user)

    def reverse_step_url(self, stepid):
        viewname = "voters:election-voting-step{step}".format(step=stepid)
        return reverse(viewname, kwargs={
            'slug': self.kwargs.get('slug'),
            'slug_election': self.kwargs.get('slug_election')
        })


@method_decorator(login_required, name="dispatch")
class ElectionVotingStep1View(ElectionVotingStepMixin, DetailView):
    """
    Shows the voting system (step 1)
    """
    form_class = ElectionVotingStep1Form
    template_name = "voters/election_voting_step1.html"

    def get_context_data(self, **kwargs):
        context = super(ElectionVotingStep1View, self)\
            .get_context_data(**kwargs)

        form_kwargs = {'instance': self.object}
        election_data = get_extra_election_data(self.object.pk,
                                                self.request.user.pk)
        if election_data is not False:
            form_kwargs.update(election_data)
            combo_id = election_data['combo_id']
        else:
            combo_id = False

        context.update({
            'form': self.form_class(**form_kwargs),
            'is_valid': bool(election_data),
            'combo_id': combo_id
        })
        return context


@method_decorator(login_required, name="dispatch")
class ElectionVotingStep2View(ElectionVotingStepMixin, DetailView):
    """
    Shows the voting system (step 2)
    """
    template_name = "voters/election_voting_step2.html"


@method_decorator(login_required, name="dispatch")
class ElectionVotingStep3View(ElectionVotingStepMixin, FormJsonView):
    """
    Shows the voting system (step 3)
    """
    form_class = ElectionVotingStep3Form
    template_name = "voters/election_voting_step3.html"

    def get_context_data(self, **kwargs):
        context = super(ElectionVotingStep3View, self)\
            .get_context_data(**kwargs)
        context.update({
            'succ_url': self.get_success_url(),
            'error_url': self.request.get_full_path()
        })
        return context

    def get_form_kwargs(self):
        """ """
        form_kwargs = super(ElectionVotingStep3View, self).get_form_kwargs()
        form_kwargs.update({'user': self.request.user})
        return form_kwargs

    def form_valid(self, form):
        """ Save the PIN value, pin expiration date and send pin through info
            in election voter table """
        self.election_voter.send_pin(form.cleaned_data['send_through'])
        return super(ElectionVotingStep3View, self).form_valid(form)

    def get_success_url(self):
        """ On success redirect to next step """
        return reverse_lazy("voters:election-voting-step3-pin", kwargs={
            'slug': self.kwargs.get('slug'),
            'slug_election': self.kwargs.get('slug_election')
        })


@method_decorator(login_required, name="dispatch")
class ElectionVotingStep3PinView(ElectionVotingStepMixin, FormJsonView):
    """
    Shows the voting system (step 3 - PIN)
    """
    form_class = ElectionVotingStep3PinForm
    template_name = "voters/election_voting_step3_pin.html"

    def get_context_data(self, **kwargs):
        context = super(ElectionVotingStep3PinView, self)\
            .get_context_data(**kwargs)
        validated = False
        data_from_ama = get_attributes(self.request)
        if data_from_ama:
            nic = data_from_ama.get('NIC', None)
            validated = bool(nic and nic == self.request.user.nic)
            if validated:
                self.election_voter.token_send_through = \
                    ElectionVoter.SendThrough.AMA
                self.election_voter.token_expiration_date = \
                    pin_expiration_date()
                self.election_voter.save(update_fields=[
                    'token_send_through',
                    'token_expiration_date'
                ])
            else:
                security_logger.warning(
                    "User '%s' is trying to authenticate through ama with "
                    "another nic: %s != %s", self.request.user, nic,
                    self.request.user.nic)
        context.update({
            'from_ama': bool(data_from_ama),
            'validated': validated
        })
        return context

    def process_steps(self):
        data_from_ama = get_attributes(self.request)
        # if data_from_ama is defined, used ama to authenticate. in such cases,
        # avoid process_steps validation because referer is defined as ama
        if bool(data_from_ama):
            security_logger.warning(
                "Secure steps: skiping step blocking validation, user came "
                "from Autenticacao.gov")
            return None
        return super(ElectionVotingStep3PinView, self).process_steps()


@method_decorator(login_required, name="dispatch")
class ElectionVotingStep4View(ElectionVotingStepMixin, DetailView):
    """
    Shows the voting system (step 4)
    """
    template_name = "voters/election_voting_step4.html"

    def secure_step(self):
        return False


@method_decorator(login_required, name="dispatch")
class ElectionSearchVoteView(PermissionRequiredMixin, DetailView):
    """
    Seach vote in some election
    """
    model = Election
    form_class = ElectionSearchVoteForm
    permission_required = "evoteelections.vote_election"
    template_name = "voters/election_search_vote.html"
    slug_url_kwarg = "slug_election"

    def get_context_data(self, **kwargs):
        """ """
        context = super(ElectionSearchVoteView, self).\
            get_context_data(**kwargs)

        page = self.request.GET.get('page', 1)
        election_id = self.object.pk
        electoral_process_id = self.object.electoral_process_id
        search_text = self.request.GET.get('search') or ''

        (references, more) = get_references(election_id,
                                            page,
                                            search_text)

        context.update({
            'form': self.form_class(self.request.GET),
            'references': references,
            'search': search_text,
            'election_id': election_id,
            'electoral_process_id': electoral_process_id,
            'page': page,
            'next_page': int(page) + 1,
            'more': more
        })
        return context


class ElectoralRollDetailView(
        SelectedMenuMixin, DjangoTablesFilterMixin, DetailView):
    """ """
    model = ElectoralRoll
    table_class = ElectionVoterTable
    template_name = "voters/electoralroll_detail.html"
    slug_url_kwarg = "slug_roll"
    filter_class = VoterFilter

    def get_table_data(self):
        """ """
        table_filter = self.get_filter(self.object.election_voters.active())
        return table_filter.qs


# Error views
class VotersErrorView(ErrorBaseView):
    template_name = 'voters/errors/base.html'


"""
MESSAGES
"""


class MessageVotersMixin(SelectedMenuMixin):
    """ """
    selected_menu = Menu.MESSAGES
    contact_namespace = "voters"


class MessageVotersListMixin(MessageVotersMixin, PermissionListMixin):
    """ """
    template_name = "voters/message_list.html"
    permission_required = "evotemessages.view_message"


@method_decorator(login_required, name="dispatch")
class MessageReceivedListView(MessageVotersListMixin, MessageReceivedListMixin,
                              ListView):
    """ """


@method_decorator(login_required, name="dispatch")
class MessageSentListView(MessageVotersListMixin, MessageSentListMixin,
                          ListView):
    """ """


@method_decorator(login_required, name="dispatch")
class MessageArchivedListView(MessageVotersListMixin, MessageArchivedListMixin,
                              ListView):
    """ """


@method_decorator(login_required, name="dispatch")
class MessageDetailView(MessageVotersMixin, MessageDetailMixin,
                        PermissionRequiredMixin, DetailView):
    """ """
    template_name = "voters/message_detail.html"
    permission_required = "evotemessages.view_message"


class MessageFormVotersMixin(object):
    """ """

    def form_valid(self, form):
        super(MessageFormVotersMixin, self).form_valid(form)
        self.set_success_message()
        return self.json_to_response()


@method_decorator(login_required, name="dispatch")
class MessageCreateView(MessageVotersMixin, MessageFormVotersMixin,
                        MessageCreateMixin, FormJsonSimpleView):
    """ """
    template_name = "voters/message_form.html"
    success_url = reverse_lazy('voters:messages-received')
    success_message = _("The message was send successfully.")


@method_decorator(login_required, name="dispatch")
class MessageArchiveView(FeedbackMessageMixin, MessageArchiveMixin,
                         PermissionRequiredMixin, UpdateView):
    """ """
    success_url = reverse_lazy('voters:messages-received')
    success_message = _("The message was archived successfully.")
    permission_required = "evotemessages.view_message"


class ElectionClaimMessageView(MessageFormVotersMixin, MessageCreateMixin,
                               FormJsonSimpleView):
    """ """
    form_class = ElectionClaimMessageForm
    template_name = "voters/election_claim.html"
    success_message = _("Your claim was sent successfully.")
    contact_namespace = "voters"

    has_recipients = False

    def get_election(self):
        if hasattr(self, 'election'):
            return self.election
        self.election = get_object_or_404(
            Election, slug=self.kwargs['slug_election'],
            electoral_process__slug=self.kwargs['slug'])
        return self.election

    def get_group(self):
        return self.get_election().electoral_process

    def get_recipients(self, form):
        electoral_process_id = self.get_election().electoral_process_id
        return [gen_uid(ElectoralCommissionRecipientsGroup,
                        electoral_process_id)]

    def get_initial(self):
        initial = super(ElectionClaimMessageView, self).get_initial()
        recipient = ElectoralCommissionRecipientsGroup()
        election = self.get_election()
        initial.update({
            'recipients': recipient.format_label(election.electoral_process),
            'subject': _('Claim - Election: {}').format(election)
        })
        return initial

    def get_context_data(self, **kwargs):
        context = super(ElectionClaimMessageView, self).get_context_data(
            **kwargs)
        context.update({
            'election': self.get_election()
        })
        return context

    def get_success_url(self):
        return reverse_lazy(
            "voters:electoralprocess-detail", args=[self.kwargs['slug']])
