# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json
import requests
import base64
import csv
import timeit
from multiprocessing import Pool, cpu_count

from django.core.management.base import BaseCommand
from django.core.urlresolvers import reverse

from eVotUM.Cripto import hashfunctions, eccblind


# Helper functions
def filter_service(base_url, request_data):
    """Wrapper call to filter service"""
    url = base_url + reverse('genericservice:filtersignature')
    request_data = json.dumps(request_data)
    response = requests.post(
        url,
        data=request_data,
        headers={'content-type': 'application/json'},
        timeout=None
    )
    if response.status_code != 200:
        print('Could not insert vote')
        return None
    json_response = response.json()
    return json_response.pop("result", False)


def validator_service(base_url, request_data):
    """Wrapper call to validator service"""
    url = base_url + reverse('genericservice:validatevote')
    request_data = json.dumps(request_data)
    response = requests.post(
        url,
        data=request_data,
        headers={'content-type': 'application/json'},
        timeout=None
    )
    if response.status_code != 200:
        print('Could not validate vote request')
        return None
    json_response = response.json()
    return json_response.pop('data', None)


def async_vote(base_url, vote_dict):
    """Spawn a vote request workflow"""
    # Set run timer
    start_time = timeit.default_timer()
    # Parse requested data
    blind_hash_str = vote_dict['blind_hash']
    vote_dict['blind_hash'] = blind_hash_str.encode("hex")
    data_to_validate = {
        k: vote_dict[k] for k in (
            'signed_token',
            'blind_hash',
            'pin_value'
        )
    }
    pr_components = vote_dict['pr_components']

    new_blind_components = vote_dict['new_blind_components']

    new_pr_components = vote_dict['new_pr_components']

    vs_cert = base64.b64decode(vote_dict['vs_cert'])

    ciphered_vote = vote_dict['ciphered_vote']

    final_hash = vote_dict['unique_bulletin']

    election_id = vote_dict['election_id']

    # Time needed to prepare data
    parse_time = timeit.default_timer()
    # Send to validator
    blind_signature = validator_service(base_url, data_to_validate)

    # Time spent with validator service
    validator_time = timeit.default_timer()

    if blind_signature is None:
        return False

    hashed_blind = hashfunctions.generateSHA256Hash(
        str(blind_signature)
    )

    (err, signature) = eccblind.unblindSignature(
        str(blind_signature),
        str(pr_components),
        str(new_blind_components)
    )
    if err is not None:
        print("(%d) Could not unblind signature" % err)
        return False

    (err, valid) = eccblind.verifySignature(
        vs_cert,
        str(signature),
        new_blind_components,
        new_pr_components,
        ciphered_vote
    )
    if err is not None:
        print("(%d) Invalid blind signature" % err)
        return False

    data_to_filter = {
        'election_id': election_id,
        'blind_hash': hashed_blind,
        'ciphered_vote': ciphered_vote,
        'signed_vote': signature,
        'blind_components': new_blind_components,
        'pr_components': new_pr_components,
        'unique_bulletin': final_hash
    }

    # Time spent with blind signatures
    blind_time = timeit.default_timer()

    # Send to filter
    vote_veredict = filter_service(base_url, data_to_filter)

    # Time spent with filter service
    filter_time = timeit.default_timer()

    if vote_veredict:
        msg = "Success"
    else:
        msg = "Failed"

    # Print relevant data in CSV format
    print "{},{},{},{},{},{},{}".format(
        msg,
        final_hash,
        filter_time - start_time,
        parse_time - start_time,
        validator_time - parse_time,
        blind_time - validator_time,
        filter_time - blind_time
    )
    # All done
    return True


class Command(BaseCommand):
    help = 'Inserts votes into a local or remote election. Previously \
        initialized using `simulate_election_prepare`.'

    def add_arguments(self, parser):
        parser.add_argument(
            'votes_csv', nargs='?', help='Absolute path to CSV file',
        )
        parser.add_argument(
            'base_url', nargs='?', help='DNS to generate the request',
            default='http://localhost:8000'
        )
        parser.add_argument(
            'proc_limit', nargs='?', help='Maximum concurrent threads',
            default=cpu_count()
        )

    def handle(self, child=None, *args, **options):
        main_start_time = timeit.default_timer()
        # Set number of votes to simulate
        votes_csv = options.get('votes_csv')
        # Set the base DNS
        base_url = options.get('base_url')
        proc_limit = int(options.get('proc_limit'))
        # Print header for CSV output
        print "{},{},{},{},{},{},{}".format(
            "message",
            "vote_hash",
            "total_time",
            "parse_time",
            "validator_time",
            "blind_time",
            "filter_time"
        )
        total_votes = 0
        pool = Pool(proc_limit)
        # Read the CSV File
        with open(votes_csv) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                total_votes += 1
                pool.apply_async(
                    async_vote,
                    args=(base_url, row,)
                )
            pool.close()
            pool.join()

        main_total_time = timeit.default_timer() - main_start_time
        print "\n{}\n{}: {}\n{}: {}\n".format(
            "Inserted %d votes" % total_votes,
            "Total time",
            main_total_time,
            "Number of concurrent processes",
            proc_limit
        )

    def print_success(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def print_error(self, msg):
        self.stderr.write(self.style.ERROR(msg))
