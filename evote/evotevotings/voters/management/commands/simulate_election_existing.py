# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json
import csv
import base64
import math

from django.utils import timezone
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from evotevotings.elections.utils import get_extra_election_data
from evotevotings.voters.models import ElectionVoter
from evotevotings.elections.models import Election
from eVotUM.Cripto import hashfunctions, pkiutils, eccblind
from evotelogs.registers import audit_register

BULK_BATCH_LIMIT = 1500


class Command(BaseCommand):
    help = 'Generates a CSV file with votes from an existing election'
    # Class variables
    # self.election : Election created to simulate the votes

    def add_arguments(self, parser):
        parser.add_argument(
            'election_id', nargs='?', help='Election ID to generate votes',
        )

    def handle(self, child=None, *args, **options):
        audit_register.remove(ElectionVoter)
        audit_register.remove(get_user_model())
        # Set number of votes to simulate
        election_id = int(options.get('election_id', 1))
        self.election_id = election_id
        # Create the necessary data
        file_path = self.prepare_election(election_id)
        self.print_success(
            "Finished generating election. ID: %d\n" % election_id +
            "Votes CSV file: %s\n" % file_path
        )

    def generate_json_vote(self, blank_vote=False,
                           candidate_id=1, category_slug='one'):
        candidate = [{
            'id': candidate_id,
            'vote': 1
        }]
        if candidate_id is False:
            blank_vote = True
            candidate = []
        vote = [
            {
                'blank': blank_vote,
                'candidates': candidate,
                'category': category_slug
            }
        ]
        json_vote = json.dumps(vote)
        return json_vote

    def generate_vote_request(self, voter, election, candidate_id):
        request_dict = {}
        user_id = voter.user.pk

        step_zero_data = get_extra_election_data(
            election.pk,
            user_id
        )
        # SHA256 combo_id
        combo_hash = hashfunctions.generateSHA256Hash(
            step_zero_data['combo_id']
        )
        # Get Candidates
        category_slug = election.categories.only('slug').first().slug

        # Get the JSON vote
        json_vote = self.generate_json_vote(False, candidate_id, category_slug)
        # Encrypt with election certificate
        elect_cert = election.certificate
        ciphered_vote = pkiutils.encryptWithPublicKey(json_vote, elect_cert)
        if ciphered_vote is None:
            self.print_error("Could not cipher the vote")
            return False

        ciphered_vote_hash = hashfunctions.generateSHA256Hash(
            ciphered_vote
        )
        final_hash = combo_hash + ciphered_vote_hash
        final_hash = hashfunctions.generateSHA256Hash(final_hash)

        pr_components = election.blindsig_pr_dash_components
        (err, blind_data) = eccblind.blindData(pr_components, ciphered_vote)

        if err is not None:
            self.print_error("Could not generate blind vote data")
            return False

        new_blind_components = blind_data[0]
        new_pr_components = blind_data[1]
        blind_vote = blind_data[2]

        combo_id = step_zero_data['combo_id']

        # Create request for validator service
        request_dict = {
            'signed_token': combo_id,
            'blind_hash': blind_vote,
            'pin_value': 'abcd'
        }
        # Create incomplete for filter service
        request_dict['vs_cert'] = base64.b64encode(step_zero_data['vs_cert'])
        # Election ID is signed, does not stand for election.pk
        request_dict['election_id'] = step_zero_data['election_id']
        request_dict['ciphered_vote'] = ciphered_vote
        request_dict['signed_vote'] = 'to_fill'
        request_dict['pr_components'] = pr_components
        request_dict['new_blind_components'] = new_blind_components
        request_dict['new_pr_components'] = new_pr_components
        request_dict['unique_bulletin'] = final_hash

        return request_dict

    def prepare_election(self, election_id=1):
        """"""
        now = timezone.now()
        election = Election.objects.get(pk=election_id)
        # Set the pin authentication method
        voter_data = {
            'token_expiration_date': now + timezone.timedelta(days=10),
            'pin_value': 'abcd',
            'token_validated': True,

        }
        election.election_voters.update(**voter_data)
        filename = "/tmp/votes_%d.csv" % election.pk
        self.csv_header = True
        self.dict_writer = None
        with open(filename, 'wb') as csv_file:
            self.write_to_csv(election, csv_file)

        return filename

    def write_to_csv(self, election, csv_file):
        nr_voters = election.election_voters.count()
        nr_candidates = election.candidates.count()
        slices = int(math.ceil(nr_voters / nr_candidates)) + 1

        # Set number of votes per candidate
        candidates = []
        for candidate in election.candidates.all():
            candidates += [candidate.pk] * slices
        # Set blank votes
        candidates += [False] * slices

        for voter in election.election_voters.active():
            candidate_id = candidates.pop()
            vote_data = self.generate_vote_request(
                voter, election, candidate_id
            )
            if self.csv_header:
                columns = vote_data.keys()
                self.dict_writer = csv.DictWriter(csv_file, columns)
                self.dict_writer.writeheader()
                self.csv_header = False
            self.dict_writer.writerow(vote_data)

    def print_success(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def print_error(self, msg):
        self.stderr.write(self.style.ERROR(msg))
