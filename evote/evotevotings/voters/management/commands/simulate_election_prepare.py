# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json
import csv
import base64

from django.utils import timezone
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model

from evotevotings.elections.utils import (get_extra_election_data,
                                          initialize_blind_signatures)
from evotevotings.admin.models import SecretEntry
from evotevotings.voters.models import ElectionVoter
from evotevotings.elections.models import Election
from evotevotings.processes.models import ElectoralProcess
from evotevotings.elections.utils import get_secret_entry
from evotevotings.services.routers import ASrouter
from eVotUM.Cripto import hashfunctions, pkiutils, eccblind
from evotelogs.registers import audit_register

BULK_BATCH_LIMIT = 1500


class Command(BaseCommand):
    help = 'Prepares an arbitrary election generating a CSV file with votes'
    # Class variables
    # self.election : Election created to simulate the votes

    def add_arguments(self, parser):
        parser.add_argument(
            'num_voters', nargs='?', help='Number of votes to generate',
        )

    def handle(self, child=None, *args, **options):
        audit_register.remove(ElectionVoter)
        audit_register.remove(get_user_model())
        # Set number of votes to simulate
        num_voters = int(options.get('num_voters', 10))
        self.max_votes = num_voters
        # Create the necessary data
        (election_id, file_path) = self.generate_election(num_voters)
        self.print_success(
            "Finished generating election. ID: %d\n" % election_id +
            "Votes CSV file: %s\n" % file_path
        )

    def generate_json_vote(self):
        vote = [
            {
                'blank': False,
                'candidates': [
                    {
                        'id': 1,
                        'vote': 1
                    }
                ],
                'category': 'one'
            }
        ]
        json_vote = json.dumps(vote)
        return json_vote

    def generate_vote_request(self, user_id, election):
        request_dict = {}
        step_zero_data = get_extra_election_data(
            election.pk,
            user_id
        )
        # SHA256 combo_id
        combo_hash = hashfunctions.generateSHA256Hash(
            step_zero_data['combo_id']
        )
        # Get the JSON vote
        json_vote = self.generate_json_vote()
        # Encrypt with election certificate
        elect_cert = election.certificate
        ciphered_vote = pkiutils.encryptWithPublicKey(json_vote, elect_cert)
        if ciphered_vote is None:
            self.print_error("Could not cipher the vote")
            return False
        ciphered_vote_hash = hashfunctions.generateSHA256Hash(
            ciphered_vote
        )
        final_hash = combo_hash + ciphered_vote_hash
        final_hash = hashfunctions.generateSHA256Hash(final_hash)

        pr_components = election.blindsig_pr_dash_components
        (err, blind_data) = eccblind.blindData(pr_components, ciphered_vote)

        if err is not None:
            self.print_error("Could not generate blind vote data")
            return False

        new_blind_components = blind_data[0]
        new_pr_components = blind_data[1]
        blind_vote = blind_data[2]

        combo_id = step_zero_data['combo_id']

        # Create request for validator service
        request_dict = {
            'signed_token': combo_id,
            'blind_hash': blind_vote,
            'pin_value': 'abcd'
        }
        # Create incomplete for filter service
        request_dict['vs_cert'] = base64.b64encode(step_zero_data['vs_cert'])
        # Election ID is signed, does not stand for election.pk
        request_dict['election_id'] = step_zero_data['election_id']
        request_dict['ciphered_vote'] = ciphered_vote
        request_dict['signed_vote'] = 'to_fill'
        request_dict['pr_components'] = pr_components
        request_dict['new_blind_components'] = new_blind_components
        request_dict['new_pr_components'] = new_pr_components
        request_dict['unique_bulletin'] = final_hash

        return request_dict

    def generate_election(self, num_voters=10):
        """"""
        # Vote Status
        status = Election.State.IN_TO_VOTE

        now = timezone.now()

        self.electoral_process = ElectoralProcess(
            created=now,
            created_by_id=1,
            identifier='Dummy Electoral Process',
        )
        self.electoral_process.save()

        # Generate Election data
        # Using the voting system certificate as election certificate
        votesys_cert_id = SecretEntry.Identifier.VOTING_CERT
        # Blind signature components
        (init_component, pr_component) = initialize_blind_signatures()
        election = Election(
            blindsig_init_components=init_component,
            blindsig_pr_dash_components=pr_component,
            identifier='Dummy Election',
            created=now,
            state=status,
            certificate=get_secret_entry(votesys_cert_id),
            proposition_begin_date=now,
            proposition_end_date=now,
            roll_begin_date=now,
            roll_end_date=now,
            voting_begin_date=now,
            voting_end_date=now,
            minute_begin_date=now,
            minute_end_date=now,
            minute_publication_date=now,
            complaint_begin_date=now,
            complaint_end_date=now,
            electoral_process=self.electoral_process
        )
        election.save()
        voters = []
        users = []

        mail_suffix = "simulate_%d.vote.org" % election.pk
        category_id = election.categories.only('pk').first().pk

        # Iterate through the number of voters requested
        for i in range(num_voters):
            user = get_user_model()(
                created=now,
                email="dummy_%d@%s" % (i, mail_suffix),
                name="dummy_%d" % i,
            )
            users.append(user)

            if(len(users) > BULK_BATCH_LIMIT):
                get_user_model().objects.bulk_create(users)
                users = []

        get_user_model().objects.bulk_create(users)
        self.users = get_user_model().objects\
            .filter(email__contains=mail_suffix)

        filename = "/tmp/votes_%d.csv" % election.pk
        self.csv_header = True
        self.dict_writer = None
        with open(filename, 'wb') as csv_file:
            user_ids = []
            for user_id in self.users.values_list('pk', flat=True):
                user_ids.append(user_id)
                voter = ElectionVoter(
                    name=mail_suffix,
                    created=now,
                    is_active=True,
                    election=election,
                    user_id=user_id,
                    category_id=category_id,
                    token_expiration_date=now + timezone.timedelta(days=1),
                    pin_value='abcd',
                    token_validated=True
                )
                voters.append(voter)

                if(len(voters) > BULK_BATCH_LIMIT):
                    ElectionVoter.objects.bulk_create(voters)
                    self.write_to_csv(user_ids, election, csv_file)
                    voters = []
                    user_ids = []

            ElectionVoter.objects.bulk_create(voters)
            self.write_to_csv(user_ids, election, csv_file)

        # Initialize Election in the AS
        as_router = ASrouter()
        as_router.initialize_election(election.pk, num_voters)
        self.print_success("Initialized election with %d entries" % num_voters)

        return (election.pk, filename)

    def write_to_csv(self, uid_list, election, csv_file):
        for uid in uid_list:
            vote_data = self.generate_vote_request(uid, election)
            if self.csv_header:
                columns = vote_data.keys()
                self.dict_writer = csv.DictWriter(csv_file, columns)
                self.dict_writer.writeheader()
                self.csv_header = False
            self.dict_writer.writerow(vote_data)

    def print_success(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def print_error(self, msg):
        self.stderr.write(self.style.ERROR(msg))
