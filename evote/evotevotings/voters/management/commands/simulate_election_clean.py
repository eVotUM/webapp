# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


from evotevotings.voters.models import ElectionVoter
from evotevotings.elections.models import Election
from evotevotings.processes.models import ElectoralProcess
from evotevotings.services.routers import ASrouter
from evotelogs.registers import audit_register


class Command(BaseCommand):
    help = 'Cleanup election generated to swarm votes'
    # Class variables
    # self.election : Election created to simulate the votes

    def add_arguments(self, parser):
        parser.add_argument(
            'election_id', nargs='?', help='Election ID to cleanup',
        )

    def handle(self, child=None, *args, **options):
        audit_register.remove(ElectionVoter)
        audit_register.remove(get_user_model())
        # Get the election ID to cleanup
        election_id = int(options.get('election_id'))
        try:
            election = Election.objects.get(pk=election_id)
            electoral_process = ElectoralProcess.objects.get(
                pk=election.electoral_process.pk
            )
        except Exception:
            self.print_error("No election to cleanup.\n")
            return False

        # Cleanup
        mail_suffix = "simulate_%d.vote.org" % election_id
        ElectionVoter.objects.filter(
            election_id=election.pk,
            name=mail_suffix
        ).delete()
        get_user_model().objects.filter(
            email__contains=mail_suffix,
        ).delete()
        election.categories.all().delete()
        election.delete()
        electoral_process.delete()
        ASrouter().close_election(election_id)
        self.print_success("Cleanup complete\n")

    def print_success(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def print_error(self, msg):
        self.stderr.write(self.style.ERROR(msg))
