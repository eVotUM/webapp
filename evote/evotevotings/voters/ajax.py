# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404

from evotevotings.elections.models import Election

from .utils import get_references


@method_decorator(login_required, name="dispatch")
class VoteReferencesAjaxView(TemplateView):
    """ """
    template_name = 'voters/includes/vote_reference_list.html'

    def get_context_data(self, **kwargs):

        page = self.request.GET.get('page', 2)
        election_id = self.kwargs.get('pk_election')
        electoral_process_id = self.kwargs.get('pk')
        search_text = self.request.GET.get('search') or ''

        # Collect the references
        (references, more) = get_references(
            election_id=election_id, page=page, search=search_text)
        context = super(VoteReferencesAjaxView, self).\
            get_context_data(**kwargs)

        context.update({
            'page': page,
            'next_page': int(page) + 1,
            'electoral_process_id': electoral_process_id,
            'election_id': election_id,
            'references': references,
            'search': search_text,
            'more': more
        })
        return context

    def get(self, request, *args, **kwargs):

        election_id = self.kwargs.get('pk_election')
        electoral_process_id = self.kwargs.get('pk')

        get_object_or_404(
            Election, pk=election_id,
            electoral_process_id=electoral_process_id)

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)
