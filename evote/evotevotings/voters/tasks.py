# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os.path
import logging

from django.core.files.base import ContentFile
from django.utils import translation
from django.apps import apps
from django.conf import settings

from evotevotings.voters.importers import ElectoralRollImporter
from evotedocs.models import Document
from celery import shared_task

from .documents import ElectoralRollPdf


logger = logging.getLogger('evote.core')


@shared_task(ignore_result=True, bind=True)
def import_electoral_roll(self, electoral_roll_id):
    """ """
    ElectoralRoll = apps.get_model("evotevoters.ElectoralRoll")  # noqa
    # select_related to avoid to much queries on import
    roll = ElectoralRoll.objects.select_related('election')\
        .get(pk=electoral_roll_id)
    # check if file exists in file system, if not, wait 5 seconds and check
    # again. This is necessary due to replication lsyncd of data files
    if not os.path.exists(roll.document.document.path):
        # http://docs.celeryproject.org/en/latest/userguide/tasks.html#retrying
        raise self.retry(countdown=5, max_retries=3)
    try:
        importer = ElectoralRollImporter()
        importer.run(roll, roll.document.document)
    except:
        logger.exception("Error importing electoral roll")
        roll.imported = False
    else:
        roll.imported = True
    finally:
        roll.save(update_fields=['imported'])
    return roll.imported


@shared_task(ignore_result=True)
def update_electoral_roll_permissions(electoral_roll_id):
    """ """
    ElectoralRoll = apps.get_model("evotevoters.ElectoralRoll")  # noqa
    roll = ElectoralRoll.objects.get(pk=electoral_roll_id)
    active = roll.election.election_voters.active()
    active.assign_perms()
    inactive = roll.election.election_voters.inactive()
    inactive.remove_perms()
    return (active.count(), inactive.count())


@shared_task(ignore_result=True)
def generate_electoral_roll_pdf(electoral_roll_id):
    """ """
    ElectoralRoll = apps.get_model("evotevoters.ElectoralRoll")  # noqa
    electoral_roll = ElectoralRoll.objects.select_related('election')\
        .get(pk=electoral_roll_id)
    languages = getattr(settings, 'LANGUAGES', None)
    for lang in languages:
        translation.activate(lang[0])
        try:
            pdf = ElectoralRollPdf(electoral_roll).render()
            file = Document(designation='electoral_roll')
            file_name = "{}_{}.pdf".format(
                electoral_roll.election.identifier_en, electoral_roll.version)
            file.document.save(file_name, ContentFile(pdf), False)
            file.save()
            electoral_roll.pdf_document = file
            electoral_roll.save(update_fields=[
                'pdf_document', 'pdf_document_pt', 'pdf_document_en'])
        except:
            logger.exception("Error generating electoral roll pdf")
