# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.views.generic.base import RedirectView
from django.conf.urls import url
from django.conf import settings

from evotevotings.admin.views import PageDetailView
from .views import (LoginView, ElectoralProcessListView,
                    ElectoralProcessArchiveView, ElectoralProcessDetailView,
                    ElectionHistoryView, ElectoralProcessVotingView,
                    ElectionVotingStep1View, ElectionVotingStep2View,
                    ElectionVotingStep3View, ElectionVotingStep3PinView,
                    ElectionVotingStep4View, ElectionSearchVoteView,
                    ContactsView, PersonalDataView, ElectionClaimMessageView,
                    SearchView, HowitworksView, ElectoralRollDetailView,
                    ValidateEmailView, ResendPendingTokenMailView,
                    MessageReceivedListView, MessageSentListView,
                    MessageArchivedListView, MessageCreateView,
                    MessageDetailView, MessageArchiveView, LogoutView)

from .ajax import VoteReferencesAjaxView

app_name = "voters"

urlpatterns = [
    url(
        regex=r'^$',
        view=RedirectView.as_view(url=settings.LOGIN_REDIRECT_URL),
        name="index"
    ),
    url(
        regex=r'^login/$',
        view=LoginView.as_view(),
        name="login"
    ),
    url(
        regex=r'^logout/$',
        view=LogoutView.as_view(),
        name="logout"
    ),
    url(
        regex=r'^contacts/$',
        view=ContactsView.as_view(),
        name="contacts"
    ),
    url(
        regex=r'^page/(?P<slug>[-\w]+)/$',
        view=PageDetailView.as_view(),
        name="page-detail"
    ),
    url(
        regex=r'^search/$',
        view=SearchView.as_view(),
        name="search"
    ),
    url(
        regex=r'^howitworks/$',
        view=HowitworksView.as_view(),
        name="howitworks"
    ),
    url(
        regex=r'^personaldata/$',
        view=PersonalDataView.as_view(),
        name="personal-data"
    ),
    url(
        regex=r'^personaldata/validateemail/$',
        view=ValidateEmailView.as_view(),
        name="validate-email"
    ),
    url(
        regex=r'^personaldata/resendemail/$',
        view=ResendPendingTokenMailView.as_view(),
        name="resend-email"
    ),

    # messages
    url(
        regex=r'^messages/received/$',
        view=MessageReceivedListView.as_view(),
        name="messages-received"
    ),
    url(
        regex=r'^messages/sent/$',
        view=MessageSentListView.as_view(),
        name="messages-sent"
    ),
    url(
        regex=r'^messages/archived/$',
        view=MessageArchivedListView.as_view(),
        name="messages-archived"
    ),
    url(
        regex=r'^messages/new/$',
        view=MessageCreateView.as_view(),
        name="messages-create"
    ),
    url(
        regex=r'^messages/(?P<pk>[-\d]+)/$',
        view=MessageDetailView.as_view(),
        name="messages-detail"
    ),
    url(
        regex=r'^messages/(?P<pk>[-\d]+)/archive/$',
        view=MessageArchiveView.as_view(),
        name="messages-archive"
    ),

    # electoral process
    url(
        regex=r'^electoralprocess/$',
        view=ElectoralProcessListView.as_view(),
        name="electoralprocess-list"
    ),
    url(
        regex=r'^electoralprocess/archive/$',
        view=ElectoralProcessArchiveView.as_view(),
        name="electoralprocess-archive"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/$',
        view=ElectoralProcessDetailView.as_view(),
        name="electoralprocess-detail"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/' +
        'electoralroll/(?P<slug_roll>[-\w]+)/$',
        view=ElectoralRollDetailView.as_view(),
        name="electoralroll-detail"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/(?P<slug_election>[-\w]+)/'
              r'claim/$',
        view=ElectionClaimMessageView.as_view(),
        name="election-claim"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/(?P<slug_election>[-\w]+)/'
              r'search/$',
        view=ElectionSearchVoteView.as_view(),
        name="election-search-vote"
    ),
    url(
        regex=r'^election/history/$',
        view=ElectionHistoryView.as_view(),
        name="election-history"
    ),
    # slug rules are greedy, order of urls is important here
    url(
        regex=r'^(?P<slug>[-\w]+)/vote/$',
        view=ElectoralProcessVotingView.as_view(),
        name="electoralprocess-voting"
    ),
    url(
        regex=r'^(?P<slug>[-\w]+)/vote/(?P<slug_election>[-\w]+)/step1/$',
        view=ElectionVotingStep1View.as_view(),
        name="election-voting-step1"
    ),
    url(
        regex=r'^(?P<slug>[-\w]+)/vote/(?P<slug_election>[-\w]+)/step2/$',
        view=ElectionVotingStep2View.as_view(),
        name="election-voting-step2"
    ),
    url(
        regex=r'^(?P<slug>[-\w]+)/vote/(?P<slug_election>[-\w]+)/step3/$',
        view=ElectionVotingStep3View.as_view(),
        name="election-voting-step3"
    ),
    url(
        regex=r'^(?P<slug>[-\w]+)/vote/(?P<slug_election>[-\w]+)/step3/pin/$',
        view=ElectionVotingStep3PinView.as_view(),
        name="election-voting-step3-pin"
    ),
    url(
        regex=r'^(?P<slug>[-\w]+)/vote/(?P<slug_election>[-\w]+)/step4/$',
        view=ElectionVotingStep4View.as_view(),
        name="election-voting-step4"
    ),
]

# ajax urls
urlpatterns += [
    url(
        regex=r'^(?P<pk>[-\d]+)/vote/(?P<pk_election>[-\d]+)/search/next$',
        view=VoteReferencesAjaxView.as_view(),
        name="vote-reference-next-page"
    ),
]
