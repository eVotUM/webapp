# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from datetime import timedelta
from eVotUM.Cripto.shamirsecret import generateSecretTime

from django.utils import timezone
from django.conf import settings

from evotevotings.services.routers import ASrouter


def get_references(election_id, page=1, search=None):
    """ Method to search vote references through the Anonymizer Service """
    # Connect to the Anonymizer Service
    return ASrouter().list_references(election_id, page, search)


def pin_expiration_date():
    return timezone.now() + timedelta(seconds=settings.PIN_TIMETOLIVE)


def generate_pin():
    (secret, _) = generateSecretTime(settings.PIN_SIZE,
                                     settings.PIN_TIMETOLIVE)
    return secret
