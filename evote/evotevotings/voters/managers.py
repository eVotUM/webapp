# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.db.models import Count
from django.db import models

from evotecore.utils import list_portions, compare_data


class ElectoralRollQuerySet(models.QuerySet):

    def get_all_published(self):
        """Get published electoral roll on the process"""
        return self.filter(published=True).first()

    def published(self, election_id):
        """Get published electoral roll on the election"""
        return self.filter(election_id=election_id, published=True).first()

    def to_publish(self, election_id):
        """Get electoral roll that can be published"""
        to_publish = self.filter(election_id=election_id, published=False,
                                 imported=True)
        published = self.published(election_id)
        if published:
            to_publish = to_publish.filter(created__gt=published.created)
        return to_publish.first()

    def is_importing(self, election_id):
        """ """
        return self.filter(election_id=election_id, imported=None)


class ElectionVoterQuerySet(models.QuerySet):
    """ """

    def active(self):
        """Get published electoral roll on the process"""
        return self.filter(is_active=True)

    def inactive(self):
        """Get published electoral roll on the process"""
        return self.filter(is_active=False)

    def get_election_voter(self, user_id):
        return self.filter(user_id=user_id).first()

    def votes_status_count(self):
        """ """
        status_count = dict((name, 0) for status, name in
                            self.model.VoteStatus.names.items())
        # order_by empty to avoid group by name (default ordering)
        counts = self.active().values('vote_status')\
            .annotate(total=Count('vote_status')).order_by()
        for count in counts:
            name = self.model.VoteStatus.names[count['vote_status']]
            status_count[name] = count['total']
        return status_count

    def assign_perms(self):
        """TODO: optimize this method"""
        qs = self.select_related('user', 'election__electoral_process')\
            .order_by()
        for voter in qs:
            voter.assign_perm()

    def remove_perms(self):
        """TODO: optimize this method"""
        qs = self.select_related('user', 'election__electoral_process')\
            .order_by()
        for voter in qs:
            voter.remove_perm()


class ElectionVoterBaseManager(models.Manager):
    """ """

    def bulk_import(self, electoral_roll, data):
        """Import voter for determined election. Create associated user if
        necessary to allow the voter login in the system.
        Associate permissions to allow user view the election and associated
        electoral process."""
        voters_user_ids = []
        voters_to_create = []
        User = get_user_model()  # NOQA
        registered_users = User.objects.values_list("username", flat=True)
        registered_users = map(lambda x: x.lower(), registered_users)
        for row in data:
            voter_user_id = None
            username = row["personnel_number"].lower()
            row["category_id"] = self._get_category(
                electoral_roll.election.categories, row["category"])
            if username not in registered_users:
                voter_user_id = User.objects.create(
                    name=row["name"], username=username).pk
            else:
                # user already exists in database
                try:
                    voter = self.get(
                        election_id=electoral_roll.election_id,
                        user__username__iexact=username)
                    self.efficient_update(voter, row, [
                        "name",
                        "category_id"
                    ])
                    voters_user_ids.append(voter.user_id)
                except self.model.DoesNotExist:
                    voter_user_id = User.objects.only("id")\
                        .get(username__iexact=username).pk
            if voter_user_id:
                voters_user_ids.append(voter_user_id)
                voters_to_create.append(self.model(
                    name=row["name"],
                    user_id=voter_user_id,
                    election_id=electoral_roll.election_id,
                    category_id=row["category_id"],
                ))
        for portion in list_portions(voters_to_create, 3000):
            self.bulk_create(portion)
        election_voters = self.filter(election_id=electoral_roll.election_id,
                                      user_id__in=voters_user_ids)
        electoral_roll.election_voters.add(*election_voters)
        return election_voters

    def efficient_update(self, voter, data, keys):
        """Only update if data changed, otherwise do nothing"""
        need_update = not compare_data(voter, data, keys)
        if need_update:
            for key in keys:
                setattr(voter, key, data[key])
            voter.save(update_fields=keys)

    def _get_category(self, qs, slug=None):
        """ """
        if slug:
            return qs.only("id").get(slug=slug).pk
        return qs.only("id").get(default=True).pk


ElectionVoterManager = ElectionVoterBaseManager.from_queryset(
    ElectionVoterQuerySet)
