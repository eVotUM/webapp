# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.db.models import F

from reportlab.platypus import Table, TableStyle
from reportlab.lib import colors

from evotedocs.documents import PdfDocument


class ElectoralRollPdf(PdfDocument):

    def __init__(self, electoral_roll):
        super(ElectoralRollPdf, self).__init__()
        self.electoral_roll = electoral_roll

    def set_story(self):
        self.story.append(self.title(self.get_title()))
        self.story.append(self.header())
        self.story.append(self.voters_table())

    def get_calendar_icon(self):
        """ Returns the flowable with calendar icon """
        return self.static_image('/static/images/calendar.png', width=23)

    def get_reload_icon(self):
        """ Returns the flowable wit reload icon """
        return self.static_image('/static/images/reload.png', width=23)

    def header(self):
        """ Adds the header of page to story """
        table_data = []
        table_data.append([
            self.get_reload_icon(),
            _('Version: {}').format(self.electoral_roll.version),
            self.get_calendar_icon(),
            _('Publication date: {}').format(self.get_formated_date(
                self.electoral_roll.published_on))
        ])
        #
        table_style = TableStyle()
        table_style.add('FONTNAME', (0, 0), (-1, -1), 'RobotoRegular')
        table_style.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
        table_style.add('FONTSIZE', (0, 0), (-1, -1), 12)
        table_style.add('BACKGROUND', (0, 0), (-1, -1),
                        colors.HexColor(0x5f6e89))
        table_style.add('TEXTCOLOR', (0, 0), (-1, -1), colors.white)

        col_widths = self.calc_col_widths(0.05, 0.45, 0.05, 0.45)
        table = Table(table_data, colWidths=col_widths, rowHeights=32)
        table.setStyle(table_style)
        return table

    def voters_table(self):
        """ Adds table of voters to story """
        table_data = []
        table_data.append([_('Name'), _('Number'), _('Voting weight')])
        for voter in self.electoral_roll.election_voters.annotate(
                category_weight=F("category__weight"),
                username=F("user__username")):
            table_data.append([
                self.tdcell(voter.name),
                voter.username,
                voter.category_weight
            ])
        return self.long_table(table_data, col_percs=[0.70, 0.15, 0.15])

    def get_title(self):
        return _("{} - Electoral Roll").format(self.electoral_roll.election)
