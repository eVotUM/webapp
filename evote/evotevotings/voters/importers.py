# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.utils.translation import ugettext_lazy as _
from django.apps import apps
from django.db import transaction

from evotelogs.registers import audit_register
from evotecore.importers import BaseImporter


logger = logging.getLogger('evote.importers')


class ElectoralRollImporter(BaseImporter):
    """ """
    field_names = ('personnel_number', 'name', 'category')
    BaseImporter.error_messages.update({
        'unique_personnel_number': _("Personnel number must be unique"),
        'invalid_email': _("Email invalid format"),
        'invalid_category': _("Voting weight not defined"),
    })
    category_slugs = []

    def __init__(self, election=None, delimiter=None):
        """ """
        super(ElectoralRollImporter, self).__init__(delimiter)
        if election:
            self.category_slugs = election.categories\
                .values_list('slug', flat=True)

    @transaction.atomic
    def run(self, electoral_roll, document):
        """ """
        data = self.get_csv_data(document, True)
        ElectionVoter = apps.get_model("evotevoters.ElectionVoter")  # noqa
        User = apps.get_model("evoteusers.User")  # noqa
        # unregister ElectionVoter to dont log the imported info
        audit_register.remove(ElectionVoter)
        audit_register.remove(User)
        logger.info("Electoral roll %d: importing election voter",
                    electoral_roll.pk)
        # do the import
        result = ElectionVoter.objects.bulk_import(electoral_roll, data)
        # register ElectionVoter again
        audit_register.add(ElectionVoter)
        audit_register.add(User)
        return result

    def get_validator(self):
        """ """
        validator = super(ElectoralRollImporter, self).get_validator()
        validator.add_unique_check(
            'personnel_number',
            message=self.error_messages['unique_personnel_number'])
        validator.add_value_predicate(
            'category', self._check_category,
            message=self.error_messages['invalid_category'])
        return validator

    def _check_category(self, value):
        """ """
        if value:
            return value in self.category_slugs
        return True
