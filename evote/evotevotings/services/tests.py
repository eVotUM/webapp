# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import patch

from django.core.urlresolvers import reverse
from django.utils import timezone
from django.test import TestCase
from django.apps import apps

from eVotUM.Cripto import eccblind, pkiutils

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.elections.utils import get_extra_election_data
from evotevotings.admin.models import SecretEntry
from evotevotings.admin.utils import (get_certificates,
                                      not_expired_certificate,
                                      validate_certificate,
                                      get_secret_entry)
from evotevotings.services import jssettings


# Helper Class
class ServiceTestUtils(ElectionsTestUtils):
    """ Helper functions class """

    def get_election_model(self):
        return apps.get_model("evoteelections.Election")  # noqa

    def generate_fake_election(self):
        now = timezone.now()
        vbegin = now - timezone.timedelta(days=1)
        vend = now + timezone.timedelta(days=1)
        status = self.get_election_model().State.IN_TO_VOTE
        # Generate fixtures
        votesys_cert_id = SecretEntry.Identifier.VOTING_CERT
        self.election = self.create_election(
            voting_begin_date=vbegin, voting_end_date=vend,
            state=status,
            certificate=get_secret_entry(votesys_cert_id))
        self.create_candidates(4,
                               election=self.election)
        self.voter = self.create_voter(
            election=self.election
        )
        self.election_data = get_extra_election_data(
            self.election.pk,
            self.voter.user_id
        )
        self.certificates = get_certificates()
        # Initialize the blind signature
        (init_component, pr_component) = eccblind.initSigner()
        self.init_component = init_component
        self.pr_component = pr_component
        self.vote_bulletin = "DUMMY VOTE"


class ExpiredRootCertificateTestCase(TestCase, ServiceTestUtils):
    """ Class to test Expired Root certificate"""

    fixtures = ['users.json', 'certs_exp_root.json']
    # Setup the Test Case

    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

    def test_invalid_certificate(self):
        root_cert = self.certificates['root']
        expired = not_expired_certificate(root_cert)
        self.assertFalse(expired)


class ExpiredVotingCertificateTestCase(TestCase, ServiceTestUtils):
    """ Class to test Expired Root certificate"""

    fixtures = ['users.json', 'certs_exp_voting.json']

    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

    def test_invalid_certificate(self):
        cert = self.certificates['votesys']
        expired = not_expired_certificate(cert)
        self.assertFalse(expired)


class UnchainedVotingCertificateTestCase(TestCase, ServiceTestUtils):
    """ Class to test Expired Root certificate"""
    fixtures = ['users.json', 'certs_unchained_voting.json']

    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

    def test_invalid_certificate(self):
        cert = self.certificates['votesys']
        root_cert = self.certificates['root']
        chain = validate_certificate(cert, root_cert)
        self.assertEqual(chain, -1)


class ExpiredValidatorCertificateTestCase(TestCase, ServiceTestUtils):
    """ Class to test Expired Root certificate"""

    fixtures = ['users.json', 'certs_exp_vs.json']

    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

    def test_invalid_certificate(self):
        cert = self.certificates['vs']
        expired = not_expired_certificate(cert)
        self.assertFalse(expired)


class UnchainedValidorCertificateTestCase(TestCase, ServiceTestUtils):
    """ Class to test Expired Root certificate"""

    fixtures = ['users.json', 'certs_unchained_vs.json']

    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

    def test_invalid_certificate(self):
        cert = self.certificates['vs']
        root_cert = self.certificates['root']
        chain = validate_certificate(cert, root_cert)
        self.assertEqual(chain, -1)


class ValidCertificateTestCase(object):
    fixtures = ['users.json', 'certs.json']


class APIViewTest(TestCase):
    """ """
    def setUp(self):
        self.url = reverse('genericservice:swagger')

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'rest_framework_swagger/index.html')


class JavascriptSettingsTest(TestCase):
    """ """
    def setUp(self):
        self.jss = jssettings.javascript_settings()
        self.vvote_endpoint = self.jss['endpoints']['validateVote']
        self.fsig_endpoint = self.jss['endpoints']['filterSignature']

    def test_data_structure(self):
        self.assertIn('endpoints', self.jss)

    def test_filtersignature_url(self):
        validate_vote = reverse('genericservice:filtersignature')
        self.assertIn('filterSignature', self.jss['endpoints'])
        self.assertEquals(self.fsig_endpoint, validate_vote)

    def test_validatevote_url(self):
        validate_vote = reverse('genericservice:validatevote')
        self.assertIn('validateVote', self.jss['endpoints'])
        self.assertEquals(self.vvote_endpoint, validate_vote)


class StepZeroTest(ValidCertificateTestCase, ServiceTestUtils):
    """ Class to test the services workflow Step 0"""

    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

    # Test Units
    def test_dataintegrity(self):
        """ Check if election data was properly loaded """
        extra_data = get_extra_election_data(self.election.pk,
                                             self.voter.user_id)
        self.assertTrue(extra_data)

    def test_invalid_election_id(self):
        """ Check unexpected election id """
        extra_data = get_extra_election_data(-1,
                                             self.voter.user_id)
        self.assertFalse(extra_data)

    @patch("evotevotings.elections.utils.get_secret_entry", return_value=False)
    def test_invalid_certificate(self, mock_fn):
        """ Check for invalid certificates """
        extra_data = get_extra_election_data(self.election.pk,
                                             self.voter.user_id)
        self.assertFalse(extra_data)

    def test_valid_certificate(self):
        """ Check if election certificate is valid """
        extra_data = get_extra_election_data(self.election.pk,
                                             self.voter.user_id)
        cert = extra_data['vs_cert']
        self.assertTrue(pkiutils.verifyPEMCertificate(cert))

    @patch("evotevotings.elections.utils.get_secret_entry", return_value=False)
    def test_invalid_private_key(self, mock_fn):
        """ Check for invalid certificates """
        extra_data = get_extra_election_data(self.election.pk,
                                             self.voter.user_id)
        self.assertFalse(extra_data)

    @patch("evotevotings.elections.utils.pkiutils.signObject",
           return_value=(1, None))
    def test_unsinged_objects(self, mock_fn):
        """ Check for unsigned object """
        extra_data = get_extra_election_data(self.election.pk,
                                             self.voter.user_id)
        self.assertFalse(extra_data)


class StepTwoTest(ValidCertificateTestCase, ServiceTestUtils):
    """ Class to test the services workflow Step 2"""

    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()

        # Validate signature (test_validate_signature)

        # Validate Chain certificate (test_system_certificate)

        # Validate Voting System certificate
        # (test_valid_voting_system_certificate)

        # Validate Root certificate (test_valid_root_certificate)

        # Validate Election period (test_election_period)

        # Validate ElectionVoter permission to vote
        # (test_voter_permission)

        # Validate Voting Status (test_voter_status)

        # Sign the blind vote (test_blind_signature_signin)

    def test_system_certificate(self):
        """ Check if voting system certificate was issued
            with chain root certificate """
        election_id = self.election.id
        certs = get_certificates(election_id)
        vote_system_cert = certs['votesys']
        root_cert = certs['root']
        chain = pkiutils.verifyCertificateChain(
            vote_system_cert,
            root_cert
        )

        self.assertTrue(chain)

    def test_valid_root_certificate(self):
        """ Check if root certificate is within validation period """
        root_cert = self.certificates['root']
        not_expired = not_expired_certificate(root_cert)
        self.assertTrue(not_expired)

    def test_valid_voting_system_certificate(self):
        """ Check if certificate is within validation period """
        vote_system_cert = self.certificates['votesys']
        not_expired = not_expired_certificate(vote_system_cert)
        self.assertTrue(not_expired)

    def test_election_period(self):
        """ Check if election is in voting period """
        self.assertTrue(self.election.is_in_to_vote)

    def test_voter_permission(self):
        """ Check if election voter can vote in this election """
        self.assertIs(self.voter.election, self.election)

    def test_voter_status(self):
        """ Check if vote is available for the voter in the election"""
        self.assertTrue(self.voter.is_vote_available[1])

    def test_validate_signature(self):
        """ Check if the user + election signature is valid"""
        signature = self.election_data['combo_id']
        votesys_cert = self.certificates['votesys']
        (error_code, obj) = pkiutils.verifyObjectSignature(
            signature,
            votesys_cert
        )
        self.assertIsNone(error_code)
