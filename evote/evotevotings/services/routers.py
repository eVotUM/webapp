# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import requests
import logging
import base64
import json

from django.conf import settings

from evotevotings.admin.utils import get_certificates


logger = logging.getLogger('evote.services')


class BaseRouter(object):

    endpoint = None
    headers = {'content-type': 'application/json'}

    def __init__(self):
        # Get all endpoints from the settings
        services_endpoints = settings.EVOTE_SERVICES_ENDPOINTS
        # set the anonymizer service base url
        self.endpoint = services_endpoints.get(self.endpoint_name)


class ASrouter(BaseRouter):
    """Helper for Anonymizer Service communication handling"""
    endpoint_name = 'anonymizer'

    def build_url(self, pk, path):
        """Method to build base endpoint URL for a given election ID
        """
        return "{}election/{}/{}/".format(self.endpoint, pk, path)

    def anonymize_vote(self, election_id, data):
        """Method to insert a vote into the AS database"""
        # Build the endpoint URL
        url = self.build_url(election_id, "anonymizevote")

        # Connect to the endpoint
        response = requests.post(
            url, data=json.dumps(data), headers=self.headers, verify=False)

        if response.status_code == 200:
            # Parse the response
            json_response = response.json()
            # Expected boolean response
            return json_response
        else:
            logger.critical(
                "Anonymize vote: url=%s status_code=%s",
                url, response.status_code)
        return False

    def initialize_election(self, election_id, voters_nr):
        """Method to handle election initialization"""
        # Build the endpoint URL
        url = self.build_url(election_id, "initialize")
        # Connect to the endpoint
        response = requests.post(
            url, data=json.dumps({'voters_nr': voters_nr}),
            headers=self.headers, verify=False)
        # Parse the response
        json_response = response.json()
        # Error found
        if response.status_code != 200 or json_response['error'] is True:
            logger.critical(
                "Initialize election: url=%s status_code=%s response=%r",
                url, response.status_code, json_response)
            return False
        return True

    def close_election(self, election_id):
        """Method to handle election closing"""
        # Build the endpoint URL
        url = self.build_url(election_id, "close")

        # Connect to the endpoint
        response = requests.post(
            url, data=json.dumps({}), headers=self.headers, verify=False)

        # Parse the response
        json_response = response.json()

        # Error found, raise exception
        if response.status_code != 200:
            logger.error("Close election: url=%s status_code=%s response=%r",
                         url, response.status_code, json_response[0])
            return False
        return True

    def list_references(self, election_id, page=1, search=None):
        """ Method to handle references search and listing """
        # List to hold the returned references
        references = []
        more = False

        # Build the endpoint URL
        url = self.build_url(election_id, "references")
        # Create the required GET data
        request_data = {'page': page}
        if search is not None:
            request_data['unique_vote_bulletin'] = search

        # Connect to the endpoint
        response = requests.get(url, request_data, verify=False)
        reference_list = response.json()

        # Check for valid response
        if response.status_code == 200 and 'count' in reference_list:
            for reference in reference_list['results']:
                references.append(reference['unique_vote_bulletin'])

            # Check for next page
            if reference_list['next'] is not None:
                more = True
        else:
            logger.error("References: url=%s status_code=%d",
                         url, response.status_code)

        return (references, more)

    def partial_count(self, election_id):
        """ Method to get the current vote count """
        # Build the endpoint URL
        url = self.build_url(election_id, "count")

        count = 0
        # Connect to the endpoint
        response = requests.get(url, headers=self.headers, verify=False)

        # Parse the response
        json_response = response.json()

        # Check for valid response
        if response.status_code == 200 and 'count' in json_response:
            count = json_response['count']
        else:
            logger.error("Partial count: url=%s status_code %d",
                         url, response.status_code)
        return count


class CSrouter(BaseRouter):
    """ Helper for Counter Service communication handling """
    # Counter service endpoints
    endpoint_name = 'counter'

    def build_url(self, pk, path):
        """Method to build base endpoint URL for a given election ID
        """
        return "{}scrutiny/{}/{}/".format(self.endpoint, pk, path)

    def generate_results(self, election):
        """ Method to generate the election results """
        # Build the endpoint URL
        url = self.build_url(election.pk, "generate")

        # Fetch all data to be sent to the counter service
        secret_entries = get_certificates(election.pk)
        # Election private key (from Election)
        elec_pvt_key = secret_entries['election_pvt_key']
        # Counter Service private key
        cs_pvt_key = secret_entries['cs_pvt_key']
        # Election management certificate
        sge_cert = secret_entries['sge']
        # Validator Service certificate
        vs_cert = secret_entries['vs']
        # Root (CA) certificate
        root_cert = secret_entries['root']

        # List of members shared secrets
        secret_list = election.electoral_process.electoral_members\
            .values_list('private_key', flat=True)
        valid_candidate_ids = election.candidates\
            .filter(write_in=election.write_in).values_list('pk', flat=True)
        valid_category_slugs = election.categories.all()\
            .values_list('slug', flat=True)
        # Build the request POST data
        # Encoding all certificates and private keys
        request_data = {
            'election_pvt_key': base64.b64encode(elec_pvt_key),
            'cs_pvt_key': base64.b64encode(cs_pvt_key),
            'vs_cert': base64.b64encode(vs_cert),
            'root_cert': base64.b64encode(root_cert),
            'sge_cert': base64.b64encode(sge_cert),
            'secret_list': list(secret_list),
            'electoral_process_id': election.electoral_process_id,
            'election_info': {
                'limit_remarkable_choices': election.limit_remarkable_choices,
                'is_ordered_voting': election.ordered_voting,
                'valid_candidate_ids': list(valid_candidate_ids),
                'valid_category_slugs': list(valid_category_slugs)
            }
        }

        response = requests.post(
            url, json.dumps(request_data), headers=self.headers, verify=False)
        json_response = response.json()
        results = None

        if response.status_code == 200:
            # Check for errors
            if json_response['error']:
                logger.critical("Scrutiny results: error message=%s",
                                json_response['message'])
            elif json_response['message'] is not False:
                results = json_response['message']
        else:
            logger.critical("Scrutiny results: url=%s status_code=%d",
                            url, response.status_code)
            results = json_response
        return results

    def check_results_status(self, election_id):
        """ Method to check the current scrutiny status """

        # Build the endpoint URL
        url = self.build_url(election_id, "status")

        # Return value
        status = None
        # Connect to the endpoint
        response = requests.get(url, headers=self.headers, verify=False)

        # Parse the response
        json_response = response.json()

        # Check for valid response
        if response.status_code == 200 and \
                'result_status_choices' in json_response:
            status = int(json_response['result_status_choices'])
        else:
            logger.warning(
                "Check results status: url=%s status_code=%d",
                url, response.status_code)
        return status

    def clean_results(self, election_id):
        """ Method to clean all results associated to a given election ID.
        Returns boolean."""
        url = self.build_url(election_id, "clean")

        # Connect to the endpoint
        response = requests.delete(url, verify=False)

        # Set the response code to return
        if response.status_code != 204:
            logger.error("Clean results: url=%s status_code=%d",
                         url, response.status_code)
            return False
        return True
