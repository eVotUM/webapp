# eVote Services Workflow

Simple action/reaction table to handle all possible pitfalls throughout the service communication process:

| Stage          | Possible error           | Handler  | Platform    |
| -------------- |:------------------------:| --------:| -----------:|
| General | Undefined Cripto library functions | Django | Python |
| General | Undefined JavaScript Cripto library functions | Browser | JavaScript |
| Initialization | Missing Root Certificate | Webapp | Python |
| Initialization | Missing Validator Service Certificate | Webapp | Python |
| Initialization | Missing Voting System Certificate | Webapp | Python |
| Initialization | Election Certificate not properly generated | Cripto | Python |
| Initialization | Blind election components not generated | Webapp | Python |
| Step 0 | Missing Voting System private key | Webapp | Python |
| Step 0 | Unable to sign user + election ID | Cripto | Python |
| Step 0 | Unable to sign election ID | Cripto | Python |
| Step 0 | Missing Election blind components  | Webapp | Python |
| Step 0 | Missing Election Certificate  | Webapp | Python |
| Step 1 | Unable to initialize session storage | Browser | JavaScript |
| Step 1 | Unable to generate user + election ID Hash  | Browser | JavaScript |
| Step 1 | Unable to cipher the vote using the Election Certificate  | Cripto | JavaScript |
| Step 1 | Unable to hash unique vote reference | Browser | JavaScript |
| Step 1 | Unable to generate the blind vote data | Cripto | JavaScript |
| Step 1 | Unable to connect through the Validator Service Endpoint | Webapp | JavaScript |
| Step 2 | Data serialization errors | Django | Python |
| Step 2 | Unable to validate the user + election ID signature | Cripto | Python |
| Step 2 | Unchained Voting System certificate to Root Certificate | Cripto | Python |
| Step 2 | Expired Voting System certificate | Cripto | Python |
| Step 2 | Expired Root Certificate | Cripto | Python |
| Step 2 | Expired Root Certificate | Cripto | Python |
| Step 2 | Election not within voting period | Webapp | Python |
| Step 2 | User already voted in this election | Webapp | Python |
| Step 2 | User vote already pending for this election | Webapp | Python |
| Step 2 | Could not change user vote status to pending | Django | Python |
| Step 2 | Unable to sign the blind data received | Cripto | Python |
| Step 2 | Unable to hash the blind signature | Cripto | Python |
| Step 2 | Could not bind the signature hash to the voter | Django | Python |
| Step 3 | Unable to hash the blind signature | Browser | Javascript |
| Step 3 | Could not unblind the blind signature | Cripto | Javascript |
| Step 3 | Could verify the blind data consistency | Cripto | Javascript |
| Step 3 | Unable to connect through the Filter Service Endpoint | Webapp | JavaScript |
| Step 4 | Data serialization errors | Django | Python |
| Step 4 | Missing Voting System Certificate | Webapp | Python |
| Step 4 | Missing Root Certificate | Webapp | Python |
| Step 4 | Missing Validator Service Certificate | Webapp | Python |
| Step 4 | Expired Voting System Certificate | Webapp | Python |
| Step 4 | Unchained Voting System Certificate to Root Certificate | Webapp | Python |
| Step 4 | Expired Voting System Certificate | Webapp | Python |
| Step 4 | Unable to verify the ciphered vote signature | Cripto | Python |
| Step 4 | Invalid vote bind signature data | Cripto | Python |
| Step 4 | Could not match vote hash for the given election | Webapp | Python |
| Step 4 | Unable to connect through the Anonymizer Service Endpoint | Webapp | Python |
| Step 4 | Negative response from the Anonymizer Service | Webapp | Python |
| Step 5 | Data serialization errors | Anonymizer Service | Python |
| Step 5 | Election not initialized | Anonymizer Service | Python |
| Step 5 | Duplicate vote found | Anonymizer Service | Python |
| Step 5 | Could not insert vote into the database | Anonymizer Service | Python |
