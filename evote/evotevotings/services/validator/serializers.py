# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from rest_framework import serializers


class ValidateVoteSerializer(serializers.Serializer):
    signed_token = serializers.CharField(required=True,
                                         allow_blank=False,
                                         max_length=1024,
                                         help_text="Election ID " +
                                                   "+ user ID signed" +
                                                   " by the Voting System")
    blind_hash = serializers.CharField(required=True,
                                       allow_blank=False,
                                       max_length=1024,
                                       help_text="Blind hash containing " +
                                                 "the cyphered Vote")
    pin_value = serializers.CharField(required=False,
                                      allow_blank=True,
                                      max_length=128,
                                      help_text="PIN value to validate " +
                                                "vote")

    def validate(self, data):
        """
        Complete object validation.
        Access fields using array dictionary notation data['field']
        Errors should raise a ValidationError Exception
        """
        for key, value in data.iteritems():
            if key != 'pin_value':
                data[key] = value.encode()
        return data

    def validate_pin_value(self, value):
        """
        """
        if value == "":
            return None
        return value
