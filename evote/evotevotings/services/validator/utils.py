# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json
import logging

from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.serializers import ValidationError
from evotevotings.admin.models import SecretEntry
from evotevotings.admin.utils import (get_certificates, get_secret_entry,
                                      get_private_key_password)
from eVotUM.Cripto import pkiutils, eccblind, hashfunctions


logger = logging.getLogger('evote.crypto')
security_logger = logging.getLogger("evote.security")


def get_election_model():
    return apps.get_model("evoteelections.Election")  # noqa


def get_election_voter_model():
    return apps.get_model("evotevoters.ElectionVoter")  # noqa


def generate_blind_signature(signature, voter, election):
    """ Attempt to generate an hashed blind signature"""
    blind_vote = signature['blind_hash']
    # Get the election blind initComponents
    blind_components = election.blindsig_init_components
    # Generate the blind signature
    blind_signature = blind_vs_sign(blind_vote, blind_components)
    # Generate the blind signature hash
    blind_hash = hashfunctions.generateSHA256Hash(blind_signature)
    # Bind the blind signature hash to the ElectionVoter
    voter.blind_vote_hash = blind_hash
    # Set the voting status to PENDING
    voter.vote_status = voter.VoteStatus.PENDING
    # Save the model changes into the DB
    voter.save()
    # Return the blind signature to the requestor
    return blind_signature


def validate_vote_request(signature):
    """ Validate the necessary request data """
    Election = get_election_model()  # NOQA
    ElectionVoter = get_election_voter_model()  # NOQA

    # Validate the certificates
    certs = get_certificates()

    # Check if voting system certificate is chained to root
    chain = pkiutils.verifyCertificateChain(certs['votesys'], certs['root'])
    if not chain:
        errmsg = ('Unchained voting system certificate '
                  'pkiutils.verifyCertificateChain=False')
        logger.critical(errmsg)
        raise ValidationError(errmsg)

    # Validate voting system certificate expiration
    (error_code, pubkey) = pkiutils.getPublicKeyFromCertificate(certs['root'])
    if error_code is not None:
        errmsg = ("expired voting system certificate "
                  "pkiutils.getPublicKeyFromCertificate=%s" % str(error_code))
        logger.critical(errmsg)
        raise ValidationError(errmsg)

    # Avoid Base64 padding errors
    signature = str(signature)

    # Validate signed object
    (error_code, unsigned_obj) = pkiutils.verifyObjectSignature(
        signature, certs['votesys'])

    if (error_code is not None):
        errmsg = ("Could not validate object signature "
                  "verifyObjectSignature=%s" % str(error_code))
        logger.critical(errmsg)
        raise ValidationError(errmsg)

    unsigned_obj = json.loads(unsigned_obj)

    # Validate Election Period
    election_id = unsigned_obj['election_id']
    if election_id is None:
        errmsg = 'Unsigned object contains no election_id'
        logger.critical(errmsg)
        raise ValidationError(errmsg)

    election_obj = Election.objects.get(pk=election_id)

    if not election_obj.is_in_to_vote:
        errmsg = 'Election %r not in voting period' % election_id
        logger.critical(errmsg)
        raise ValidationError(errmsg)

    # Validate user vote capability
    user_id = unsigned_obj['user_id']
    if user_id is None:
        errmsg = 'Unsigned object contains no user_id'
        security_logger.warning(errmsg)
        raise ValidationError(errmsg)

    voter = ElectionVoter.objects.get(user_id=user_id, election_id=election_id)

    if(election_obj.pk != voter.election.pk):
        errmsg = "User can't vote in this election"
        security_logger.warning(errmsg)
        raise ValidationError(errmsg)

    (code, vote_available) = voter.is_vote_available
    if not vote_available:
        errmsg = 'Vote not available for this user code=%s' % code
        security_logger.warning(errmsg)
        raise ValidationError(errmsg)

    # Return the ElectionVoter and Election objects pair
    return (voter, election_obj)


def blind_vs_sign(blind_data, init_components):
    """ Sign blind data using the validator service private key """
    # Get the Validator Service Private Key
    vs_private_key = get_secret_entry(SecretEntry.Identifier.VALIDATOR_PVT_KEY)
    # Get the Validator Service Private Key Password
    pvt_key_password = get_private_key_password()
    # Atempt to generate the blind signature
    (error_code, signature) = eccblind.generateBlindSignature(
        vs_private_key, pvt_key_password, blind_data, init_components)

    # Check if blind_data was successfully signed
    if error_code is not None:
        errmsg = ("Could sign the blind data "
                  "eccblind.generateBlindSignature=%s" % str(error_code))
        logger.critical(errmsg)
        raise ValidationError(errmsg)

    # Return the blind signature
    return signature


def check_vote_status(blind_hash, election_id):
    ElectionVoter = get_election_voter_model()  # NOQA
    try:
        election_voter = ElectionVoter.objects.get(
            blind_vote_hash=blind_hash, election=election_id)
    except ObjectDoesNotExist:
        pass
    else:
        (status, avail) = election_voter.is_vote_available
        if status == ElectionVoter.VoteStatus.PENDING:
            election_voter.vote_status = ElectionVoter.VoteStatus.YES
            election_voter.save(update_fields=['vote_status'])
            return True
    return False
