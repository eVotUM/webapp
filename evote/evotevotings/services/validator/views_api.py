# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.utils.translation import ugettext as _

from rest_framework.response import Response
from rest_framework.generics import CreateAPIView
from rest_framework.serializers import ValidationError

from .serializers import ValidateVoteSerializer
from .utils import validate_vote_request, generate_blind_signature


logger = logging.getLogger('evote.crypto')


class ValidateVoteView(CreateAPIView):
    """
    Standard API view
    """
    permission_classes = []
    serializer_class = ValidateVoteSerializer

    def get_serializer_class(self):
        """
        Define the Serializer class to be interpreted
        by Django Swagger
        """
        return self.serializer_class

    def post(self, request, *args, **kwargs):
        """
        POST method docs
        Returns a JSON object with blind signature
        Return codes (0 < code < 10 for cripto errors):
            0 - No errors found
            1 - Empty data
            2 - Invalid data received
        """
        return_code = 0
        obj_data = {}
        return_data = {}
        serializer = ValidateVoteSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            signed_token = serializer.validated_data['signed_token']
            (voter, election) = validate_vote_request(signed_token)
            pin_value = serializer.validated_data.get('pin_value', None)
            if not voter.is_token_valid(pin_value):
                raise ValidationError(
                    {'pin_value': [_('Voting code not valid')]})
            blind_signature = generate_blind_signature(
                serializer.validated_data, voter, election)
            if blind_signature is None:
                return_code = 2
                logger.critical("generate_blind_signature failed return=None")
            else:
                obj_data = blind_signature
        else:
            return_code = 1
            logger.warning("Validator: data submitted is invalid")
        return_data['return_code'] = return_code
        return_data['data'] = obj_data
        return Response(return_data)
