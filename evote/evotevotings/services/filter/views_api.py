# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging
from eVotUM.Cripto import pkiutils, eccblind

from evotevotings.services.validator import utils
from evotevotings.services.routers import ASrouter
from evotevotings.voters.mailings import SendReferenceMail
from evotevotings.voters.models import ElectionVoter
from evotevotings.admin.utils import get_certificates, validate_certificate
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView

from .serializers import FilterSignatureSerializer


logger = logging.getLogger('evote.crypto')


class FilterSignatureView(CreateAPIView):
    """
    Standard API view
    """
    permission_classes = []
    serializer_class = FilterSignatureSerializer

    def get_serializer_class(self):
        """
        Define the Serializer class to be interpreted by Django Swagger
        """
        return self.serializer_class

    def post(self, request, *args, **kwargs):
        """
        POST method docs
        Returns either true or false
        """
        serializer = FilterSignatureSerializer(data=request.data)
        # Top-level validation through the serializer
        serializer.is_valid(raise_exception=True)
        sanitized_data = serializer.validated_data
        response_dict = {
            "result": False
        }
        # Get the necessary certificates
        certs = get_certificates()
        # Check if Voting System certificate is chained to Root
        valid = validate_certificate(certs['votesys'], certs['root'])

        # Not a chained certificate
        if valid == -1:
            logger.critical('Unchained Voting System certificate')
            return Response(response_dict)
        # Expired certificate
        if valid == -2:
            logger.critical('Expired Voting System certificate')
            return Response(response_dict)

        # Validate signed election_id object
        signed_obj = str(sanitized_data['election_id'])
        (error_code, election_id) = pkiutils.verifyObjectSignature(
            signed_obj, certs['votesys']
        )
        if error_code is not None:
            logger.critical('Invalid election+user signature '
                            'pkiutils.verifyObjectSignature=%d', error_code)
            return Response(response_dict)

        # Validate vote object blind signature
        (error_code, unsigned_vote) = eccblind.verifySignature(
            certs['vs_pub'],
            sanitized_data['signed_vote'],
            sanitized_data['blind_components'],
            sanitized_data['pr_components'],
            sanitized_data['ciphered_vote']
        )
        if error_code is not None:
            logger.critical('Invalid vote blind signature '
                            'eccblind.verifySignature=%d', error_code)
            return Response(response_dict)

        # Check if vote is available for this user
        vote_status = utils.check_vote_status(
            sanitized_data['blind_hash'],
            election_id
        )
        if not vote_status:
            logger.critical(
                "Vote not found for the given blind_hash: '%s' and "
                "election_id: '%s'", sanitized_data['blind_hash'], election_id)
            return Response(response_dict)

        return_data = vote_status

        # Remove the hash from the dictionary and send the
        # remaining fields to the anonymizer service
        blind_hash = sanitized_data.pop('blind_hash')
        # Remove the signed election ID aswell
        sanitized_data.pop('election_id')

        # Send to the Anonymizer Service
        if vote_status:
            return_data = ASrouter().anonymize_vote(
                election_id,
                sanitized_data
            )
            # If the response is True, send the reference email
            if return_data:
                try:
                    SendReferenceMail({
                        'reference': sanitized_data['unique_bulletin'],
                        'election_id': election_id
                    }).send([(
                        self.request.user.name,
                        self.request.user.get_email()
                    )])
                except AttributeError:
                    logger.exception("Could not send reference email to=%s",
                                     self.request.user.get_email())
            # Otherwise revert the vote status
            else:
                election_voter = ElectionVoter.objects.get(
                    blind_vote_hash=blind_hash,
                    election=election_id
                )
                election_voter.vote_status = ElectionVoter.VoteStatus.PENDING
                election_voter.save(update_fields=['vote_status'])

        response_dict['result'] = bool(return_data)

        return Response(response_dict)
