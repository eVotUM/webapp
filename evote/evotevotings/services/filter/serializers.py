# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from rest_framework import serializers


class FilterSignatureSerializer(serializers.Serializer):
    election_id = serializers.CharField(required=True,
                                        allow_blank=False,
                                        help_text="Election ID" +
                                                  " signed by" +
                                                  " the Voting System")
    blind_hash = serializers.CharField(required=True,
                                       allow_blank=False,
                                       help_text="Blind hash containing" +
                                                 " the ciphered Vote")
    ciphered_vote = serializers.CharField(required=True,
                                          allow_blank=False,
                                          help_text="Ciphered vote")
    signed_vote = serializers.CharField(required=True,
                                        allow_blank=False,
                                        help_text="Ciphered vote hash " +
                                                  " signed by" +
                                                  " the Validator Service")
    blind_components = serializers.CharField(required=True,
                                             allow_blank=False,
                                             help_text="Blind signature" +
                                                       " components")
    pr_components = serializers.CharField(required=True,
                                          allow_blank=False,
                                          help_text="Blind PR signature" +
                                                    " components")
    unique_bulletin = serializers.CharField(required=True,
                                            allow_blank=False,
                                            help_text="Unique bulletin number")

    def validate(self, data):
        """
        Complete object validation.
        Access fields using array dictionary notation data['field']
        Errors should raise a ValidationError Exception
        """
        data = super(FilterSignatureSerializer, self).validate(data)

        for key, value in data.iteritems():
            data[key] = value.encode()

        return data
