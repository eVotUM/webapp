# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json

from mock import patch
from django.core.urlresolvers import reverse
from django.test import TestCase

from evotevotings.admin.models import SecretEntry
from evotevotings.voters.models import ElectionVoter
from evotevotings.voters.tests import VotersTestCase
from evotevotings.admin.utils import get_certificates
from evotevotings.admin.utils import (get_secret_entry,
                                      get_private_key_password)
from evotevotings.services.tests import (
    ServiceTestUtils, ValidCertificateTestCase)
from eVotUM.Cripto import eccblind


class FilterServiceAPITest(TestCase):
    """ """
    # Setup the Test Case
    def setUp(self):
        self.url = reverse('genericservice:filtersignature')

    def test_api_call_fail(self):
        r = self.client.post(self.url)
        self.assertEqual(r.status_code, 400)


class FilterSignatureAPITest(ValidCertificateTestCase, VotersTestCase,
                             ServiceTestUtils):
    """Class to test the services workflow Step 4"""
    # Setup the Test Case
    def setUp(self):
        # Create Election
        # Create ElectionVoter
        # Generate Candidates
        # Generate Signature
        self.generate_fake_election()
        self.data = self.generate_fake_request_data()
        self.certificates = get_certificates()
        self.url = reverse('genericservice:filtersignature')

    def get_blind_signature(self, vs_private_key, pvtkey_password,
                            signed_combo_id, dash_components,
                            init_components):
        """ Function to blind data and generate signature """
        signature = None

        errorCode, blind_data = eccblind.blindData(
            dash_components, bytes(signed_combo_id))

        if(errorCode is None):
            errorCode, signature = eccblind.generateBlindSignature(
                vs_private_key, pvtkey_password, blind_data[2],
                init_components)

        return (errorCode, signature)

    def generate_fake_request_data(self):
        ciphered_vote = 'testcpheredvote'
        sha_cv = ("4b12333d0a1a5f55592678646c6b1e50b"
                  "4dd7ae9e37c7988a6b2d8a65fe8bb9b")

        vs_private_key = get_secret_entry(
            SecretEntry.Identifier.VALIDATOR_PVT_KEY)
        pvtkey_password = get_private_key_password()

        errorCode, signature = self.get_blind_signature(vs_private_key,
                                                        pvtkey_password,
                                                        sha_cv,
                                                        self.pr_component,
                                                        self.init_component)

        data = {'ciphered_vote': ciphered_vote,
                'election_id': self.election_data['election_id'],
                'signed_vote': signature,
                'unique_bulletin': self.vote_bulletin,
                'blind_components': self.init_component,
                'pr_components': self.pr_component,
                'blind_hash': sha_cv}

        return data

    @patch("evotevotings.services.filter.views_api.get_certificates")
    def test_api_missing_vs_cert(self, mock_fn):
        self.certificates.pop("vs")
        mock_fn.return_value = self.certificates
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api.get_certificates")
    @patch("evotevotings.services.filter.views_api.validate_certificate")
    def test_api_missing_root_cert(self, validate_certificate,
                                   get_certificates):
        self.certificates["root"] = None
        get_certificates.return_value = self.certificates
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        response = json.loads(r.content)
        self.assertFalse(response['result'])
        validate_certificate.assert_called_with(
            self.certificates['votesys'], None)

    @patch("evotevotings.services.filter.views_api.get_certificates")
    @patch("evotevotings.services.filter.views_api.validate_certificate")
    def test_api_missing_votesys_cert(self, validate_certificate,
                                      get_certificates):
        self.certificates["votesys"] = None
        get_certificates.return_value = self.certificates
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        response = json.loads(r.content)
        self.assertFalse(response['result'])
        validate_certificate.assert_called_with(
            None, self.certificates['root'])

    @patch("evotevotings.services.filter.views_api.validate_certificate")
    def test_api_unchained_votesys_cert(self, mock_fn):
        mock_fn.return_value = -1
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api.validate_certificate")
    def test_api_expired_votesys_cert(self, mock_fn):
        mock_fn.return_value = -2
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api" +
           ".pkiutils.verifyObjectSignature")
    def test_api_invalid_signature(self, mock_fn):
        mock_fn.return_value = (1, None)
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api.eccblind.verifySignature")
    def test_api_invalid_blind(self, mock_fn):
        mock_fn.return_value = (1, None)
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api.utils.check_vote_status")
    def test_api_missing_hash(self, mock_fn):
        mock_fn.return_value = None
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api.ASrouter")
    def test_failed_anonymize_vote(self, mock_fn):
        mock_fn().anonymize_vote.return_value = False
        self.voter.vote_status = ElectionVoter.VoteStatus.PENDING
        self.voter.blind_vote_hash = self.data['blind_hash']
        self.voter.save()
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        response = json.loads(r.content)
        self.assertFalse(response['result'])

    @patch("evotevotings.services.filter.views_api.utils.check_vote_status")
    @patch("evotevotings.services.filter.views_api.ASrouter")
    def test_api_call_ok(self, mock_router, mock_status):
        self.login_as_voter()
        mock_router.anonymize_vote.return_value = True
        mock_status.return_value = True
        self.voter.vote_status = ElectionVoter.VoteStatus.PENDING
        self.voter.blind_vote_hash = self.data['blind_hash']
        self.voter.save()
        r = self.client.post(self.url, self.data)
        self.assertEqual(r.status_code, 200)
        json_resp = r.content.decode('string-escape').strip('"')
        response = json.loads(json_resp)
        self.assertTrue(response['result'])
