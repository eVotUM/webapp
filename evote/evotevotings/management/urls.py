# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.views.generic.base import RedirectView
from django.conf.urls import url
from django.conf import settings

from evoteusers.views import EvoteLogoutView

from .views import (ElectoralProcessListView, ElectoralProcessDetailView,
                    ElectoralProcessActionsView, ElectionListView,
                    ElectionDetailConfigsView, ElectionDetailCalendarView,
                    ElectionDetailCategoriesView, ElectionDetailRollsView,
                    ElectionDetailCandidatesView, ElectionDetailBulletinView,
                    ElectionDetailBulletinPreviewView,
                    LoginView, ElectoralProcessFormView,
                    ElectoralProcessMembersView, ElectoralProcessSummaryView,
                    ElectoralProcessConfirmationView,
                    ElectoralProcessElectionsView,
                    ElectoralProcessCommunicationView,
                    ElectionDetailRollsPublishView,
                    PersonalDataView, ElectionManageView,
                    ElectionManageCloseView, ElectionManageScrutinizeView,
                    MessageReceivedListView, MessageSentListView,
                    MessageArchivedListView, MessageCreateView,
                    MessageDetailView, MessageResponseView,
                    MessageArchiveView, ElectoralProcessMembersEditView,
                    GenerateCertificateView, ElectoralProcessSummaryPdfView,
                    ValidateEmailView, ResendPendingTokenMailView,
                    ElectionCountingsPDFView)

from .ajax import (SecureActionAjaxView, ManageSecureActionAjaxView,
                   ElectoralMemberVerifyView, ElectionCountingsPartialView,
                   ElectionScrutinyStatusView, AuthAmaAjaxView,
                   HistoryAjaxView, HistoryNewAjaxView)

app_name = "management"


urlpatterns = [
    url(
        regex=r'^$',
        view=RedirectView.as_view(url=settings.LOGIN_REDIRECT_URL),
        name="index"
    ),
    url(
        regex=r'^login/$',
        view=LoginView.as_view(),
        name="login"
    ),
    url(
        regex=r'^logout/$',
        view=EvoteLogoutView.as_view(),
        name="logout"
    ),
    url(
        regex=r'^personaldata/$',
        view=PersonalDataView.as_view(),
        name="personal-data"
    ),
    url(
        regex=r'^personaldata/resendemail/$',
        view=ResendPendingTokenMailView.as_view(),
        name="resend-email"
    ),
    url(
        regex=r'^personaldata/validateemail/$',
        view=ValidateEmailView.as_view(),
        name="validate-email"
    ),
    url(
        regex=r'^electoralprocess/$',
        view=ElectoralProcessListView.as_view(),
        name="electoralprocess-list"
    ),
    url(
        regex=r'^electoralprocess/new/$',
        view=ElectoralProcessFormView.as_view(),
        name="electoralprocess-new"
    ),
    # slug rules are greedy, order of urls is important here
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/$',
        view=ElectoralProcessDetailView.as_view(),
        name="electoralprocess-detail"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/received/$',
        view=MessageReceivedListView.as_view(),
        name="messages-received"
    ),

    # messages
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/sent/$',
        view=MessageSentListView.as_view(),
        name="messages-sent"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/archived/$',
        view=MessageArchivedListView.as_view(),
        name="messages-archived"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/new/$',
        view=MessageCreateView.as_view(),
        name="messages-create"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/(?P<pk>[-\d]+)/$',
        view=MessageDetailView.as_view(),
        name="messages-detail"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/(?P<pk>[-\d]+)/response/$',
        view=MessageResponseView.as_view(),
        name="messages-response"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_slug>[-\w]+)/'
              'messages/(?P<pk>[-\d]+)/archive/$',
        view=MessageArchiveView.as_view(),
        name="messages-archive"
    ),

    # electoral process
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/edit/$',
        view=ElectoralProcessFormView.as_view(),
        name="electoralprocess-edit"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/edit/elections/$',
        view=ElectoralProcessElectionsView.as_view(),
        name="electoralprocess-elections"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/members/$',
        view=ElectoralProcessMembersView.as_view(),
        name="electoralprocess-members-view"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/members/edit/$',
        view=ElectoralProcessMembersEditView.as_view(),
        name="electoralprocess-members-edit"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/communication/$',
        view=ElectoralProcessCommunicationView.as_view(),
        name="electoralprocess-communication"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/summary/$',
        view=ElectoralProcessSummaryView.as_view(),
        name="electoralprocess-summary"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/summary/pdf/$',
        view=ElectoralProcessSummaryPdfView.as_view(),
        name="electoralprocess-pdf"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/generate/$',
        view=GenerateCertificateView.as_view(),
        name="electoralprocess-generate"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/confirmation/$',
        view=ElectoralProcessConfirmationView.as_view(),
        name="electoralprocess-confirmation"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/actions/$',
        view=ElectoralProcessActionsView.as_view(),
        name="electoralprocess-actions"
    ),

    # elections
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/$',
        view=ElectionListView.as_view(),
        name="election-list"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/manage/$',
        view=ElectionManageView.as_view(),
        name="election-manage"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/manage/countingspdf/$',
        view=ElectionCountingsPDFView.as_view(),
        name="election-manage-pdf"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/manage/close/$',
        view=ElectionManageCloseView.as_view(),
        name="election-manage-close"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/manage/scrutinize/$',
        view=ElectionManageScrutinizeView.as_view(),
        name="election-manage-scrutinize"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/configs/$',
        view=ElectionDetailConfigsView.as_view(),
        name="election-detail-configs"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/calendar/$',
        view=ElectionDetailCalendarView.as_view(),
        name="election-detail-calendar"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/categories/$',
        view=ElectionDetailCategoriesView.as_view(),
        name="election-detail-categories"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/rolls/$',
        view=ElectionDetailRollsView.as_view(),
        name="election-detail-rolls"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/rolls/publish/$',
        view=ElectionDetailRollsPublishView.as_view(),
        name="election-detail-rolls-publish"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/candidates/$',
        view=ElectionDetailCandidatesView.as_view(),
        name="election-detail-candidates"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/bulletin/$',
        view=ElectionDetailBulletinView.as_view(),
        name="election-detail-bulletin"
    ),
    url(
        regex=r'^electoralprocess/(?P<slug>[-\w]+)/elections/'
              r'(?P<slug_election>[-\w]+)/bulletin/preview$',
        view=ElectionDetailBulletinPreviewView.as_view(),
        name="election-detail-bulletin-preview"
    )
]

# ajax urls
urlpatterns += [
    url(
        regex=r'^electoralprocess/(?P<process_pk>\d+)/auth/ama/$',
        view=AuthAmaAjaxView.as_view(),
        name="auth-ama"
    ),
    url(
        regex=r'^(?P<process_pk>\d+)/electoral-action/manage/validate/$',
        view=ManageSecureActionAjaxView.as_view(),
        name="secureaction-manage"
    ),
    url(
        regex=r'^(?P<process_pk>\d+)/electoral-action/validate/(?P<pk>\d+)/$',
        view=SecureActionAjaxView.as_view(),
        name="secureaction-validate"
    ),
    url(
        regex=r'^members/(?P<process_pk>\d+)/verify/$',
        view=ElectoralMemberVerifyView.as_view(),
        name='electoralmember-verify'
    ),
    url(
        regex=r'^(?P<process_pk>\d+)/election/(?P<pk>\d+)/countings/$',
        view=ElectionCountingsPartialView.as_view(),
        name='election-countings'
    ),
    url(
        regex=r'^(?P<process_pk>\d+)/election/(?P<pk>\d+)/scrutiny-status/$',
        view=ElectionScrutinyStatusView.as_view(),
        name='election-scrutiny-status'
    ),
    url(
        regex=r'^electoralprocess/(?P<process_pk>\d+)/history/next/$',
        view=HistoryAjaxView.as_view(),
        name="electoralprocess-history-next"
    ),
    url(
        regex=r'^electoralprocess/(?P<process_pk>\d+)/history/new/$',
        view=HistoryNewAjaxView.as_view(),
        name="electoralprocess-history-count"
    ),
]
