# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.forms import (BaseInlineFormSet, inlineformset_factory,
                          modelformset_factory)
from django.utils import timezone
from django.conf import settings
from django import forms

from evotevotings.elections.importers import CandidateImporter
from evotevotings.processes.models import (ElectoralProcess, ElectoralAction,
                                           ElectoralMember, ElectoralDocument)
from evotevotings.elections.models import Election, Category, Candidate
from evotevotings.voters.importers import ElectoralRollImporter
from phonenumber_field.validators import validate_international_phonenumber
from evotevotings.elections.forms import ElectoralCalendarForm
from evotevotings.admin.models import SecretEntry
from evotevotings.admin.utils import get_secret_entry
from contrib.umws.members import members_service
from evoteusers.forms import EvoteAuthenticationForm
from evotecore.mixins import NestedFormsetFormMixin
from evotedocs.models import Document
from evotedocs.forms import DocumentForm, OrderedDocumentFormset
from eVotUM.Cripto import shamirsecret
from evotecore import widgets


class YesNo:
    choices = (
        (True, _('Yes')),
        (False, _('No'))
    )


class ManagementADFSAuthenticationForm(forms.Form):
    """ """

    class Profiles:
        ELECTORAL_MEMBER = 0
        INSTITUTIONAL_RESPONSIBLE = 1

        choices = (
            (ELECTORAL_MEMBER, _("Electoral Commission")),
            (INSTITUTIONAL_RESPONSIBLE, _("Election Corporate Manager")),
        )

    profile = forms.TypedChoiceField(
        label=_("Profile"), choices=Profiles.choices, coerce=int,
        initial=Profiles.ELECTORAL_MEMBER, widget=widgets.Select)


class ManagementAuthenticationForm(ManagementADFSAuthenticationForm,
                                   EvoteAuthenticationForm):
    """ """
    def confirm_login_allowed(self, user):
        super(ManagementAuthenticationForm, self).confirm_login_allowed(user)
        is_institutional_responsible = self.cleaned_data.get('profile') == \
            self.Profiles.INSTITUTIONAL_RESPONSIBLE and \
            user.is_institutional_responsible
        is_electoral_member = self.cleaned_data.get('profile') == \
            self.Profiles.ELECTORAL_MEMBER and user.electoral_members.exists()
        if not is_institutional_responsible and not is_electoral_member:
            raise forms.ValidationError(
                self.error_messages['invalid_login'],
                code='invalid_login',
            )


class SecureActionKeysForm(forms.Form):
    """Form to introduce all keys for action security validation.
    The keys shown by the form are dinamic, taking in account the number of
    electoral """

    def __init__(self, *args, **kwargs):
        self.num_members = kwargs.pop("num_members")
        self.strength = kwargs.pop("strength")
        self.elec_proc_id = kwargs.pop("elec_proc_id")
        super(SecureActionKeysForm, self).__init__(*args, **kwargs)
        # by default form has already a key field
        for num in range(1, (self.num_keys + 1)):
            fkwargs = {'label': _("Key") + " {}".format(num)}
            if settings.DEBUG:
                fkwargs.update({'required': False})
            self.fields["key%d" % num] = forms.FileField(**fkwargs)

    def clean(self):
        super(SecureActionKeysForm, self).clean()
        # Edge case for empty form submission
        # Return immediately to fire the form validation
        if not self.cleaned_data or settings.DEBUG:
            return self.cleaned_data
        # List of JWT components
        components = []
        # Management System Certificate
        sge_cert = get_secret_entry(SecretEntry.Identifier.MANAGEMENT_CERT)
        # Get all the secrets from the given files
        for key, field in self.cleaned_data.iteritems():
            # Sanitize newlines
            contents = str(field.read()).strip()
            components.append(contents)
        # Fuse the requested components
        (error, pwd) = shamirsecret.recoverSecretFromComponents(
            components, str(self.elec_proc_id), str(sge_cert))
        # Invalidate form if an error occurs
        if error is not None:
            raise forms.ValidationError(
                _("Invalid components of the secret submitted"))

    @property
    def num_keys(self):
        if self.strength == ElectoralAction.Strength.SIMPLE:
            return 1
        elif self.strength == ElectoralAction.Strength.STRONG:
            return (self.num_members / 2) + 1
        return self.num_members


ElectoralActionFormset = inlineformset_factory(
    ElectoralProcess, ElectoralAction, fk_name="electoral_process", extra=0,
    can_delete=False, fields=("action", "strength"), widgets={
        'action': forms.HiddenInput(),
        'strength': forms.RadioSelect()
    })


class ElectoralProcessBaseForm(forms.ModelForm):
    """ """

    class Meta:
        model = ElectoralProcess
        fields = ('logo', 'identifier_pt', 'identifier_en', 'description_pt',
                  'description_en')
        widgets = {
            'description_pt': forms.Textarea(attrs={'rows': 4}),
            'description_en': forms.Textarea(attrs={'rows': 4})
        }

    def __init__(self, *args, **kwargs):
        super(ElectoralProcessBaseForm, self).__init__(*args, **kwargs)
        self.fields['identifier_en'].widget.attrs['placeholder'] =\
            _('Please insert the designation of the electoral process '
              '(in English)')
        self.fields['identifier_pt'].widget.attrs['placeholder'] =\
            _('Please insert the designation of the electoral process '
              '(in Portuguese)')
        self.fields['description_pt'].widget.attrs['placeholder'] =\
            _('Please insert a short description about the electoral process'
              ' (in Portuguese)')
        self.fields['description_en'].widget.attrs['placeholder'] =\
            _('Please insert a short description about the electoral process'
              ' (in English)')


class ElectoralProcessAsInstitutionalForm(ElectoralProcessBaseForm):
    """ """

    class Meta(ElectoralProcessBaseForm.Meta):
        fields = ElectoralProcessBaseForm.Meta.fields + ('is_test',)
        process_widgets = ElectoralProcessBaseForm.Meta.widgets
        process_widgets.update({
            'is_test': forms.RadioSelect(choices=YesNo.choices)
        })
        widgets = process_widgets

    def __init__(self, *args, **kwargs):
        super(ElectoralProcessAsInstitutionalForm, self).__init__(
            *args, **kwargs)
        if not self.instance.is_draft():
            for field in self.fields:
                self.fields[field].widget.attrs['disabled'] = True


class ElectoralProcessAsMemberForm(ElectoralProcessBaseForm):
    """ """

    def __init__(self, *args, **kwargs):
        super(ElectoralProcessAsMemberForm, self).__init__(*args, **kwargs)
        if not self.instance.is_draft() and not self.instance.is_new():
            self.fields['publication_date'].widget.attrs['disabled'] = True

    class Meta(ElectoralProcessBaseForm.Meta):
        fields = ElectoralProcessBaseForm.Meta.fields + ('publication_date', )
        process_widgets = ElectoralProcessBaseForm.Meta.widgets
        process_widgets.update({
            'publication_date': widgets.DateTimeInput()
        })
        widgets = process_widgets

    def clean_publication_date(self):
        if not self.instance.is_draft() and not self.instance.is_new():
            return self.instance.publication_date

        publication_date = self.cleaned_data.get('publication_date', None)
        if publication_date and publication_date >= timezone.now():
            return publication_date

        raise ValidationError(_('The publication date must be after today'))


class ElectoralDocumentForm(NestedFormsetFormMixin, forms.ModelForm):

    formset_class = OrderedDocumentFormset
    formset_prefix = "documents"

    class Meta:
        model = ElectoralDocument
        fields = ('designation',)

    def get_queryset(self):
        """Return many to many queryset if candidate already exists on db"""
        if self.instance and self.instance.pk:
            return self.instance.documents.all()
        return Document.objects.none()

    def _save_m2m(self, *args, **kwargs):
        """Save and associates documents to the instance"""
        instances = self.formset.save(*args, **kwargs)
        self.instance.documents.add(*instances)
        super(ElectoralDocumentForm, self)._save_m2m(*args, **kwargs)


ElectoralDocumentFormset = inlineformset_factory(
    ElectoralProcess, ElectoralDocument, extra=0, min_num=1,
    form=ElectoralDocumentForm)


class ElectoralProcessCommunicationForm(forms.Form):
    """ """
    responsible_communication = forms.ModelChoiceField(
        required=True, empty_label=None,
        queryset=ElectoralMember.objects.none(),
        widget=widgets.SearchableSelect,
        label=_('Responsible for communication'))

    class Meta:
        fields = ('responsible_communication',)

    def __init__(self, *args, **kwargs):
        """" """
        electoral_process = kwargs.pop('instance')
        super(ElectoralProcessCommunicationForm, self).__init__(
            *args, **kwargs)
        self.fields['responsible_communication'].queryset = \
            electoral_process.electoral_members.all()


class ElectionForm(ElectoralCalendarForm):
    """ """

    class Meta(ElectoralCalendarForm.Meta):
        fields = ElectoralCalendarForm.Meta.fields + [
            'identifier_pt', 'identifier_en', 'description_pt',
            'description_en']
        election_widgets = ElectoralCalendarForm.Meta.widgets
        election_widgets.update({
            'description_pt': forms.Textarea(attrs={'rows': 4}),
            'description_en': forms.Textarea(attrs={'rows': 4})
        })
        widgets = election_widgets

    def __init__(self, *args, **kwargs):
        super(ElectionForm, self).__init__(*args, **kwargs)
        self.fields['identifier_en'].widget.attrs['placeholder'] =\
            _('Please insert the designation of the election (in English)')
        self.fields['identifier_pt'].widget.attrs['placeholder'] =\
            _('Please insert the designation of the election (in Portuguese)')
        self.fields['description_pt'].widget.attrs['placeholder'] =\
            _('Please insert a short description about the election '
              '(in Portuguese)')
        self.fields['description_en'].widget.attrs['placeholder'] =\
            _('Please insert a short description about the election '
              '(in English)')


class ElectionsInlineFormSet(BaseInlineFormSet):

    def __init__(self, *args, **kwargs):
        super(ElectionsInlineFormSet, self).__init__(*args, **kwargs)
        if not self.instance.is_draft():
            for form in self.forms:
                for field in form.fields:
                    form.fields[field].widget.attrs['disabled'] = True


ElectionFormset = inlineformset_factory(
    ElectoralProcess, Election, extra=0, form=ElectionForm, min_num=1,
    formset=ElectionsInlineFormSet)


class ElectionDetailConfigsForm(forms.ModelForm):
    """ """

    class Meta:
        model = Election
        fields = ('identifier_pt', 'identifier_en', 'description_pt',
                  'description_en', 'limit_remarkable_choices', 'write_in',
                  'ordered_voting', 'weight_votes', 'hondt_method',
                  'hondt_total_seats')
        widgets = {
            'description_en': forms.Textarea(attrs={'rows': 3}),
            'description_pt': forms.Textarea(attrs={'rows': 3}),
            'limit_remarkable_choices': widgets.Select(),
            'write_in': forms.RadioSelect(choices=YesNo.choices),
            'ordered_voting': forms.RadioSelect(choices=YesNo.choices),
            'weight_votes': forms.RadioSelect(choices=YesNo.choices),
            'hondt_method': forms.RadioSelect(choices=YesNo.choices)
        }

    def __init__(self, *args, **kwargs):
        super(ElectionDetailConfigsForm, self).__init__(*args, **kwargs)
        self.fields['identifier_en'].widget.attrs['placeholder'] =\
            _('Please insert the designation of the election (in English)')
        self.fields['identifier_pt'].widget.attrs['placeholder'] =\
            _('Please insert the designation of the election (in Portuguese)')
        self.fields['description_pt'].widget.attrs['placeholder'] =\
            _('Please insert a short description about the election ' +
              '(in Portuguese)')
        self.fields['description_en'].widget.attrs['placeholder'] =\
            _('Please insert a short description about the election ' +
              '(in English)')

    def clean(self):
        super(ElectionDetailConfigsForm, self).clean()
        hondt_method = self.cleaned_data.get('hondt_method')
        if hondt_method:
            hondt_total_seats = self.cleaned_data.get('hondt_total_seats')
            if hondt_total_seats is not None and (hondt_total_seats < 1):
                self.add_error('hondt_total_seats',
                               _('Please insert a value bigger than 0.'))
            elif hondt_total_seats is None:
                self.add_error('hondt_total_seats',
                               _('This field is required.'))


class CategoryForm(forms.ModelForm):
    """ """

    class Meta:
        model = Category
        fields = ('designation_pt', 'designation_en', 'weight')

    def validate_unique(self):
        # Must remove organization field from exclude in order
        # for the unique_together constraint to be enforced.
        exclude = self._get_validation_exclusions()
        exclude.remove('designation')
        try:
            self.instance.validate_unique(exclude=exclude)
        except ValidationError, e:
            self._update_errors(e.message_dict)


CategoryFormset = inlineformset_factory(
    Election, Category, extra=1, form=CategoryForm)


class ElectoralRollDocumentForm(DocumentForm):
    """ """

    def __init__(self, *args, **kwargs):
        """ """
        self.election = kwargs.pop('election')
        self.is_importing = kwargs.pop('is_importing')
        super(ElectoralRollDocumentForm, self).__init__(*args, **kwargs)
        if self.is_importing:
            for field in self.fields:
                self.fields[field].disabled = True
        self.fields['designation'].required = True

    def clean_document(self):
        """ """
        document = self.cleaned_data.get('document')
        importer = ElectoralRollImporter(self.election)
        errors = importer.validate(document, limit=5, verbose_errors=True)
        if errors:
            raise ValidationError(errors)
        return document


class CandidateForm(NestedFormsetFormMixin, forms.ModelForm):
    """ """
    formset_class = OrderedDocumentFormset
    formset_prefix = "documents"

    class Meta:
        model = Candidate
        fields = ('logo', 'designation')

    def get_queryset(self):
        """Return many to many queryset if candidate already exists on db"""
        if self.instance and self.instance.pk:
            return self.instance.documents.all()
        return Document.objects.none()

    def _save_m2m(self, *args, **kwargs):
        """Save and associates documents to the instance"""
        instances = self.formset.save(*args, **kwargs)
        self.instance.documents.add(*instances)
        super(CandidateForm, self)._save_m2m(*args, **kwargs)


CandidateFormset = inlineformset_factory(
    Election, Candidate, extra=0, min_num=1, form=CandidateForm)


class ElectionWriteInDocumentForm(DocumentForm):
    """ """

    class Meta(DocumentForm.Meta):
        fields = ('document',)

    def __init__(self, *args, **kwargs):
        """ """
        self.election = kwargs.pop('election')
        super(ElectionWriteInDocumentForm, self).__init__(*args, **kwargs)

    def clean_document(self):
        """ """
        document = self.cleaned_data.get('document')
        importer = CandidateImporter(self.election)
        errors = importer.validate(document, limit=5, verbose_errors=True)
        if errors:
            raise ValidationError(errors)
        return document


BulletinCandidateFormset = modelformset_factory(
    Candidate, extra=0, fields=("order",))


class ElectionBulletinForm(NestedFormsetFormMixin, forms.ModelForm):
    """ """
    formset_class = BulletinCandidateFormset
    formset_prefix = 'candidates'

    class Meta:
        model = Election
        fields = ("presentation_mode", "instructions_pt", "instructions_en")
        widgets = {
            "presentation_mode": forms.RadioSelect()
        }

    def __init__(self, *args, **kwargs):
        super(ElectionBulletinForm, self).__init__(*args, **kwargs)
        if self.instance.write_in or self.instance.ordered_voting:
            self.initial['presentation_mode'] =\
                Election.PresentationMode.DROPDOWN
            self.fields['presentation_mode'].widget.attrs['disabled'] = True
            self.fields['presentation_mode'].required = False

    def get_queryset(self):
        """ """
        if self.instance and self.instance.pk:
            if self.instance.write_in:
                return self.instance.candidates.write_in()
            elif not self.instance.write_in:
                return self.instance.candidates.not_write_in()
        return Candidate.objects.none()

    def save(self, *args, **kwargs):
        """ """
        super(ElectionBulletinForm, self).save(*args, **kwargs)
        if not self.ignore():
            self.formset.save(*args, **kwargs)

    def ignore(self):
        """ignore formset validation and submition"""
        if self.instance and self.instance.write_in:
            return True
        return False

    def clean_presentation_mode(self):
        presentation_mode = self.cleaned_data.get('presentation_mode')
        if self.instance.write_in or self.instance.ordered_voting:
            return Election.PresentationMode.DROPDOWN
        return presentation_mode


class ElectoralMemberVerifyForm(forms.Form):
    """ """

    username = forms.CharField()

    class Meta:
        fields = ('username',)

    def clean_username(self):
        username = self.cleaned_data.get('username')
        self.set_user(username)
        return username

    def set_user(self, username):
        """ get the user from DTSI API """
        self.user = members_service.get_contact_info_by_login(username)
        if self.user and 'phone' in self.user and self.user['phone']:
            try:
                validate_international_phonenumber(self.user['phone'])
            except ValidationError:
                self.user['phone'] = ""
        if not self.user:
            raise ValidationError(_('The user does not exist'))

    def get_user(self):
        if hasattr(self, 'user'):
            return self.user
        return None


class ElectoralMemberForm(forms.ModelForm):
    """ """
    username = forms.CharField()
    email = forms.EmailField()
    name = forms.CharField()

    class Meta:
        model = ElectoralMember
        fields = ('email', 'name', 'username', 'role', 'electoral_process',
                  'designation', 'phone')

    def __init__(self, *args, **kwargs):
        super(ElectoralMemberForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.initial['name'] = self.instance.user.name
            self.initial['email'] = self.instance.user.email
            self.initial['username'] = self.instance.user.username
            self.fields['username'].widget.attrs['readonly'] = True
        else:
            self.fields['role'].widget.attrs['disabled'] = True
            self.fields['designation'].widget.attrs['readonly'] = True

        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['name'].widget.attrs['readonly'] = True
        self.fields['phone'].widget.attrs['readonly'] = True

    def save(self, commit=True):
        """ """
        member, created = ElectoralMember.objects.update_or_create_member(
            username=self.cleaned_data.get('username'),
            email=self.cleaned_data.get('email'),
            name=self.cleaned_data.get('name'),
            electoral_process=self.cleaned_data.get('electoral_process'),
            designation=self.cleaned_data.get('designation'),
            role=self.cleaned_data.get('role'),
            phone=self.cleaned_data.get('phone'))
        return member


class MembersFormSet(BaseInlineFormSet):
    """ """

    def clean(self):
        super(MembersFormSet, self).clean()
        chairman = False
        users = []
        forms = [form for form in self.forms if form not in self.deleted_forms]
        for form in forms:
            email = form.cleaned_data.get('email')
            if email not in users:
                users.append(email)
            else:
                form.add_error('email', _('Duplicate email address'))
            if form.cleaned_data.get('role') == ElectoralMember.Role.CHAIRMAN\
                    and not chairman:
                chairman = True
            elif form.cleaned_data.get('role') ==\
                    ElectoralMember.Role.CHAIRMAN:
                form.add_error('role', _('Only one chairman can be defined.'))


ElectoralMembersFormset = inlineformset_factory(
    ElectoralProcess, ElectoralMember, extra=0, min_num=3, max_num=24,
    validate_min=True, validate_max=True, form=ElectoralMemberForm,
    formset=MembersFormSet)


class ElectionManageOpenForm(forms.ModelForm):
    """ """
    error_messages = {
        'voting_begin_date_invalid':
            _("Must be higher than current date.")
    }

    class Meta:
        model = Election
        fields = ('voting_begin_date', )
        widgets = {
            'voting_begin_date': widgets.DateTimeInput(),
        }

    def __init__(self, *args, **kwargs):
        """ """
        super(ElectionManageOpenForm, self).__init__(*args, **kwargs)
        self.fields['voting_begin_date'].label =\
            _('Start of the voting time') + ":"
        if not self.instance.can_manage_open():
            for field in self.fields.values():
                field.disabled = True

    def clean_voting_begin_date(self):
        """ """
        voting_begin_date = self.cleaned_data.get('voting_begin_date')
        if voting_begin_date <= timezone.now():
            raise ValidationError(
                self.error_messages['voting_begin_date_invalid'])
        return voting_begin_date


class ElectionManageCloseForm(forms.ModelForm):
    """ """
    error_messages = {
        'voting_end_date_invalid': _("Must be higher than current date."),
        'voting_end_date_higher':
            _("Must be higher than 'voting begin date'."),
    }

    class Meta:
        model = Election
        fields = ('voting_end_date', )
        widgets = {
            'voting_end_date': widgets.DateTimeInput(),
        }

    def __init__(self, *args, **kwargs):
        super(ElectionManageCloseForm, self).__init__(*args, **kwargs)
        self.fields['voting_end_date'].label =\
            _('End of the voting time') + ":"
        if not self.instance.can_manage_close():
            for field in self.fields.values():
                field.disabled = True

    def clean_voting_end_date(self):
        """ """
        voting_end_date = self.cleaned_data.get('voting_end_date')
        if voting_end_date <= timezone.now():
            raise ValidationError(
                self.error_messages['voting_end_date_invalid'])
        elif voting_end_date <= self.instance.voting_begin_date:
            raise ValidationError(
                self.error_messages['voting_end_date_higher'])
        return voting_end_date


class ElectionManageScrutinizeForm(forms.ModelForm):
    """ """

    class Meta:
        model = Election
        fields = ('id', )

    def __init__(self, election=None, *args, **kwargs):
        super(ElectionManageScrutinizeForm, self).__init__(*args, **kwargs)

    def clean(self):
        """ """
        return {'id': self.instance.pk}


class CertificateGenerationBaseForm(forms.Form):
    """
    Base form for certificate sign operations
    """
    def __init__(self, *args, **kwargs):
        """ """
        self.elections = kwargs.pop("elections")
        super(CertificateGenerationBaseForm, self).__init__(*args, **kwargs)
        self.members_fields = []
        self.elections_fields = []

    def set_members_fields(self, components=None):
        """ """
        for member_pk in self.members_pk:
            member_field = forms.CharField(
                widget=forms.HiddenInput(attrs={'data-member-id': member_pk}),
            )
            if components:
                member_field.initial = components.pop()
            field_name = "secret_%d" % member_pk
            self.fields[field_name] = member_field
            self.members_fields.append(field_name)

    def set_elections_fields(self, csr_list=None, field_prefix="csr"):
        """ """
        for elec_pk, elec_expir in self.elections:
            attrs = {'data-election-id': elec_pk}
            if csr_list:
                attrs['data-expire-seconds'] = elec_expir
            election_field = forms.CharField(
                widget=forms.HiddenInput(attrs),
            )
            if csr_list:
                election_field.initial = csr_list.pop()
            field_name = "%s_%d" % (field_prefix, elec_pk)
            self.fields[field_name] = election_field
            self.elections_fields.append(field_name)

    def get_members_fields(self):
        """ """
        return [field for field in self
                if field.name in self.members_fields]

    def get_elections_fields(self):
        """ """
        return [field for field in self
                if field.name in self.elections_fields]

    def is_valid(self):
        return super(CertificateGenerationBaseForm, self).is_valid()


class CertificateGenerationRenderForm(CertificateGenerationBaseForm):
    """
    This form is used for presentation in browser only
    """
    password = forms.CharField(
        widget=forms.PasswordInput(),
        label=_('eVotUM root Certification Authority password'))
    ca_cert = forms.CharField(widget=forms.HiddenInput())
    ca_pvt_key = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        self.process = kwargs.pop("process")
        super(CertificateGenerationRenderForm, self).__init__(*args, **kwargs)
        self.initial_data = kwargs.pop("initial")
        self.fields['ca_cert'].initial = self.initial_data['ca_cert']
        self.fields['ca_pvt_key'].initial = self.initial_data['ca_pvt_key']
        self.set_elections_fields(self.initial_data['csrs'])


class CertificateGenerationForm(CertificateGenerationBaseForm):
    elec_pvt_key = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        """ """
        self.members_pk = kwargs.pop("members_pk")
        super(CertificateGenerationForm, self).__init__(*args, **kwargs)
        initial_data = kwargs["initial"]
        components = None
        if 'components' in initial_data:
            components = initial_data.pop('components')

        self.set_members_fields(components)
        self.set_elections_fields(None, "election")

    def clean(self):
        return super(CertificateGenerationForm, self).clean()
