# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.template.loader import get_template
from django.utils.html import format_html
from django.template import Context
from django.conf import settings

import django_tables2 as tables
from evotevotings.processes.models import ElectoralProcess, ElectoralMember

from evotevotings.elections.models import Election, ElectionCountQuery
from evotevotings.processes.tables import StateColumn
from evotevotings.voters.models import ElectoralRoll
from django_tables2.utils import AttributeDict
from evotecore.tables import ImageColumn, DateTimeColumn, CounterColumn


class BaseProcessTable(tables.Table):
    """
    Base table with common fields
    """
    logo = ImageColumn('', attrs={'th': {'class': 's-10'}})
    identifier = tables.Column(attrs={'th': {'class': 's-15'}},
                               verbose_name=_('Identifier'))
    description = tables.Column(attrs={'th': {'class': 's-50'}},
                                verbose_name=_('Description'))

    class Meta:
        model = ElectoralProcess
        empty_text = _('No electoral processes.')
        orderable = False
        attrs = {"class": "pure-table pure-table-line"}
        fields = ('logo', 'identifier', 'description')


class ElectoralProcessMemberTable(BaseProcessTable):
    """
    Table for Electoral Member
    """
    select_button = tables.LinkColumn(
        viewname='management:electoralprocess-detail',
        args=[tables.A('slug')],
        orderable=False,
        attrs={'a':
               {'class': 'pure-button pure-button-primary pure-button-round' +
                ' pull-right'},
               'th': {'class': 's-10'}},
        text=_('Select'),
        verbose_name=''
    )

    class Meta(BaseProcessTable.Meta):
        fields = BaseProcessTable.Meta.fields + ('select_button',)


class ElectoralProcessInstitutionalTable(BaseProcessTable):
    """
    Table for Institutional responsabile
    """
    state = StateColumn(attrs={'th': {'class': 's-15'}})
    select_button = tables.LinkColumn(
        viewname='management:electoralprocess-edit',
        args=[tables.A('slug')],
        orderable=False,
        attrs={'a':
               {'class': 'pure-button pure-button-primary pure-button-round' +
                ' pull-right'},
               'th': {'class': 's-10'}},
        text=_('Select'),
        verbose_name=''
    )

    class Meta(BaseProcessTable.Meta):
        fields = BaseProcessTable.Meta.fields + ('state', 'select_button',)


class ElectoralProcessDetailTable(tables.Table):
    """
    Table for Electoral process detail
    """
    logo = ImageColumn('', attrs={'th': {'class': 's-10'}})
    identifier = tables.Column(attrs={'th': {'class': 's-30'}},
                               verbose_name=_('Identifier'))
    description = tables.Column(attrs={'th': {'class': 's-40'}},
                                verbose_name=_('Description'))
    edit_button = tables.LinkColumn(
        viewname='management:electoralprocess-edit',
        args=[tables.A('slug')],
        text=_("Edit"),
        verbose_name='',
        attrs={'a': {
            'class': 'pure-button pure-button-secondary pure-button-round'
        }},
    )
    publication_date = DateTimeColumn(attrs={'th': {'class': 's-20'},
                                             'td': {'class': 'one-line'}},
                                      verbose_name=_('Publication date'))
    elections_button = tables.LinkColumn(
        viewname='management:election-list',
        args=[tables.A('slug')],
        attrs={'a': {
            'class': 'pure-button pure-button-primary pure-button-round ' +
                     'pull-right'
        }},
        text=_('Elections dashboard'),
        verbose_name=''
    )

    class Meta:
        model = ElectoralProcess
        orderable = False
        fields = ('logo', 'identifier', 'description', 'edit_button',
                  'publication_date', 'elections_button')
        empty_text = _('No electoral processes.')
        attrs = {"class": "pure-table pure-table-line"}

    def _wrapp_edit_link(self, column, value, record):
        rendered = column.render(value)
        if record.actions_configured:
            edit_url = reverse('management:electoralprocess-edit',
                               kwargs={'slug': record.slug})
            link = "<a href='{}' title='{}'>{}</a>".format(
                edit_url, record, rendered)
            return format_html(link)
        return rendered

    def render_logo(self, column, value, record):
        return self._wrapp_edit_link(column, value, record)

    def render_identifier(self, column, value, record):
        return self._wrapp_edit_link(column, value, record)

    def render_edit_button(self, column, value, record, bound_column):
        """ """
        if not record.actions_configured:
            title = _("Only can edit the electoral process after configuring "
                      "the security levels")
            column.attrs['a']['class'] += ' pure-button-disabled'
            column.attrs['a']['title'] = title
            return column.render_link("#", record=record, value=value)
        return column.render(value, record, bound_column)

    def render_electoral_documents(self, record):
        return record.electoral_documents.count()

    def render_elections(self, record):
        return record.elections.count()


class ElectionIconColumn(tables.Column):

    def render(self, filled=False, unlock=False, num=None, link=None):
        icon = "evi-b-lock"
        num = num if num is not None else ""
        if unlock:
            icon = "evi-b-success" if filled else "evi-noinfo"
        res = format_html("<i class='ev-icon evi-circle {}'></i><br>{}",
                          icon, num)
        if link and unlock:
            res = format_html("<a href={}>{}</a>", link, res)
        return res


class ElectionTable(tables.Table):
    """ """
    identifier = tables.LinkColumn(
        verbose_name=_("election"),
        viewname='management:election-detail-configs',
        kwargs={
            'slug': tables.A('electoral_process.slug'),
            'slug_election': tables.A('slug')
        }
    )
    operations_panel = tables.LinkColumn(
        verbose_name=_("election day dashboard"), text=_('Manage'),
        viewname='management:election-manage',
        kwargs={'slug': tables.A('electoral_process.slug'),
                'slug_election': tables.A('slug')},
        attrs={'a': {'class': 'pure-button pure-button-round'}}
    )
    configs = ElectionIconColumn(_("configuration"), empty_values=())
    calendar = ElectionIconColumn(_("timetable"), empty_values=())
    categories = ElectionIconColumn(_("voting weight"), empty_values=())
    electoral_rolls = ElectionIconColumn(_("electoral roll"), empty_values=())
    candidates = ElectionIconColumn(_("applications"), empty_values=())
    bulletin = ElectionIconColumn(_("ballot"), empty_values=())
    edit_button = tables.LinkColumn(
        viewname='management:election-detail-configs',
        kwargs={
            'slug': tables.A('electoral_process.slug'),
            'slug_election': tables.A('slug')
        },
        text=format_html("<i class='ev-icon evi-edit'></i>"),
        verbose_name='',
        attrs={
            'a': {'title': _('Edit election')}
        }
    )

    class Meta:
        model = Election
        fields = ("identifier", "operations_panel", "configs", "calendar",
                  "categories", "electoral_rolls", "candidates", "bulletin",
                  "edit_button")
        orderable = False
        empty_text = _(u"No elections available.")
        attrs = {"class": "pure-table"}

    def render_identifier(self, column, value, record, bound_column):
        """ """
        if not record.can_change():
            return value
        return column.render(value, record, bound_column)

    def render_operations_panel(self, column, value, record, bound_column):
        """ """
        if not record.can_manage_operations():
            open_in = getattr(settings, 'EVOTE_ELECTION_OPERATIONS_AFTER', 3)
            title = _("Only can manage the election {} days before the voting "
                      "begins").format(open_in)
            column.attrs['a']['class'] = 'pure-button pure-button-round pure-button-disabled'
            column.attrs['a']['title'] = title
            return column.render_link("#", record=record, value=value)
        else:
            column.attrs['a']['class'] = 'pure-button pure-button-round pure-button-primary'
            column.attrs['a']['title'] = ''
        return column.render(value, record, bound_column)

    def render_edit_button(self, column, value, record, bound_column):
        """ """
        if not record.can_change():
            return ""
        return column.render(value, record, bound_column)

    def render_configs(self, column, record):
        """ """
        return column.render(
            filled=record.configs_filled, unlock=record.can_change_configs(),
            link=reverse('management:election-detail-configs',
                         kwargs={'slug': record.electoral_process.slug,
                                 'slug_election': record.slug}
                         ))

    def render_calendar(self, column, record):
        """ """
        return column.render(
            filled=record.calendar_filled, unlock=record.can_change_calendar(),
            link=reverse('management:election-detail-calendar',
                         kwargs={'slug': record.electoral_process.slug,
                                 'slug_election': record.slug}
                         ))

    def render_categories(self, column, record):
        """ """
        return column.render(
            filled=record.categories.exists(), num=record.categories.count(),
            unlock=record.can_change_categories(),
            link=reverse('management:election-detail-categories',
                         kwargs={'slug': record.electoral_process.slug,
                                 'slug_election': record.slug}
                         ))

    def render_electoral_rolls(self, column, record):
        """ """
        return column.render(
            filled=record.election_voters.exists(),
            num=record.election_voters.active().count(),
            unlock=record.can_change_rolls(),
            link=reverse('management:election-detail-rolls',
                         kwargs={'slug': record.electoral_process.slug,
                                 'slug_election': record.slug}
                         ))

    def render_candidates(self, column, record):
        """ """
        num_candidates = record.candidates.filter(
            write_in=record.write_in).count()
        return column.render(
            filled=record.candidates.exists(), num=num_candidates,
            unlock=record.can_change_candidates(),
            link=reverse('management:election-detail-candidates',
                         kwargs={'slug': record.electoral_process.slug,
                                 'slug_election': record.slug}
                         ))

    def render_bulletin(self, column, record):
        """ """
        return column.render(
            filled=record.bulletin_filled, unlock=record.can_change_bulletin(),
            link=reverse('management:election-detail-bulletin',
                         kwargs={'slug': record.electoral_process.slug,
                                 'slug_election': record.slug}
                         ))


class ElectoralMemberSimpleTable(tables.Table):
    """
    Table for Electoral Members detail
    """
    counter = CounterColumn(empty_values=(), verbose_name='#')
    name = tables.Column(accessor='user.name', attrs={'th': {'class': 's-20'}})
    designation = tables.Column(attrs={'th': {'class': 's-20'}})
    email = tables.Column(accessor='user.email')

    class Meta:
        model = ElectoralMember
        empty_text = _('No electoral members.')
        orderable = False
        attrs = {"class": "pure-table pure-table-striped"}
        fields = ('counter', 'name', 'role', 'designation', 'email', 'phone')


def prefix_refresh(text):
    return format_html("&#x21bb; {}", text)


class ElectoralRollTable(tables.Table):
    """Table to list electoral rolls"""
    designation = tables.Column(accessor="document.designation")
    imported = tables.BooleanColumn(
        default=prefix_refresh(_("Importing...")), verbose_name=_('Uploaded'))
    published_on = tables.Column(default=_("Never published"))
    version = tables.Column(verbose_name=_("Version"))
    pdf_document = tables.Column(empty_values=())
    actions = tables.Column(verbose_name="", empty_values=(), attrs={
        "td": {"class": "text-right"}
    })

    class Meta:
        model = ElectoralRoll
        empty_text = _('No electoral roll uploaded.')
        attrs = {"class": "pure-table pure-table-alt"}
        fields = ('designation', 'version', 'published_on', 'imported',
                  'pdf_document', 'actions')
        orderable = False

    def __init__(self, *args, **kwargs):
        self.secure_action_url = kwargs.pop('secure_action_url')
        super(ElectoralRollTable, self).__init__(*args, **kwargs)

    def render_pdf_document(self, record):
        if record.pdf_document and record.pdf_document.document:
            link = record.pdf_document.document.url
            return format_html(
                "<a href='{}' target='_blank'><i class='ev-icon evi-pdf'>"
                "</i></a>", link)
        elif record.published:
            return prefix_refresh(_('Generating...'))
        return '---'

    def render_actions(self, record, table):
        """ """
        if record.published:
            attrs = {
                "href": "#",
                "class": "pure-button pure-button-disabled pure-button-round"
            }
            return format_html(
                "<a {}>{}</a>", AttributeDict(attrs).as_html(),
                _("Published electoral roll")
            )
        # render form to publish electoralroll
        to_publish = ElectoralRoll.objects.to_publish(record.election_id)
        if to_publish and to_publish.pk == record.pk:
            context = getattr(table, 'context', Context())
            context.update({
                'object': record,
                'secure_action_url': self.secure_action_url
            })
            return get_template(
                "management/includes/electoralroll_publish_form.html")\
                .render(context)
        return ""


class ElectionCountQueryTable(tables.Table):
    """ """
    name = tables.Column(_("Requested by"), accessor="user.name")
    created = tables.Column(_("Date"))
    total_count = tables.Column(_("# Ballot papers"))

    class Meta:
        model = ElectionCountQuery
        fields = ('name', 'created', 'total_count')
        attrs = {"class": "pure-table pure-table-striped"}
        orderable = False


class ElectionVoteCountingTable(tables.Table):
    """ """
    name = tables.Column(_("Candidate/List of candidates"))
    total = tables.Column(_("Votes"))
    perc = tables.Column(_("Percentage"))
    seats = tables.Column(_("Seats number"))

    class Meta:
        fields = ('name', 'total', 'perc', 'seats')
        attrs = {"class": "pure-table pure-table-striped"}
        orderable = False

    def __init__(self, *args, **kwargs):
        show_seats = kwargs.pop('show_seats')
        super(ElectionVoteCountingTable, self).__init__(*args, **kwargs)
        if not show_seats:
            self.exclude = ('seats',)

    def render_name(self, value):
        if value == 'blank':
            return _('Blank ballot')
        return value

    def render_perc(self, value):
        return "{0:.2f}%".format(value)


class ElectionVoteCountingExtraTable(tables.Table):
    """ """
    invalid = tables.Column(_("Invalid Ballot Paper"))
    submitted = tables.Column(_("Submitted Ballot Paper"))
    submitting = tables.Column(_("Ballot papers with incomplete submission"))
    unsubmitted = tables.Column(_("Voters that have not voted"))

    class Meta:
        fields = ('invalid', 'submitted', 'submitting', 'unsubmitted')
        attrs = {"class": "pure-table pure-table-striped"}
        orderable = False
