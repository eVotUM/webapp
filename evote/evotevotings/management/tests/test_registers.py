# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import Mock

from evotevotings.management.registers import secure_action

from . import ManagementTestCase


class SecureActionSessionRegisterTest(ManagementTestCase):

    def setUp(self):
        self.mock = Mock()
        self.mock.session = {}

    def test_prefix_key(self):
        key = secure_action.prefix_key("test")
        self.assertEqual(
            key, "{}_{}".format(secure_action.SESSION_PREFIX, "test"))

    def test_register(self):
        key = secure_action.prefix_key(1)
        secure_action.register(self.mock, 1, "test")
        self.assertIn(key, self.mock.session)
        self.assertEqual(self.mock.session[key], "test")

    def test_reset(self):
        key = secure_action.prefix_key(1)
        secure_action.register(self.mock, 1, "test")
        secure_action.reset(self.mock, 1)
        self.assertNotIn(key, self.mock.session)

    def test_reset_unexistent_key(self):
        with self.assertRaises(KeyError):
            secure_action.reset(self.mock, 1)

    def test_validate_registered_action(self):
        key = secure_action.prefix_key(1)
        secure_action.register(self.mock, 1, "test")
        is_valid = secure_action.validate(self.mock, 1, "test")
        self.assertTrue(is_valid)
        self.assertNotIn(key, self.mock.session)

    def test_validate_unregistered_action(self):
        is_valid = secure_action.validate(self.mock, 1, "test")
        self.assertFalse(is_valid)
