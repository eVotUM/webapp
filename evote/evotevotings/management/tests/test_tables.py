# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotevotings.management.tables import (ElectoralProcessDetailTable)

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.voters.tests.utils import VotersTestUtils

from . import ManagementTestCase


class ElectoralProcessDetailTableTest(ElectionsTestUtils, ManagementTestCase):
    """
    Test custom fields of electoral process detail table
    """

    def setUp(self):
        self.process = self.create_electoral_process()
        self.table = ElectoralProcessDetailTable({})

    def test_render_electoral_documents_empty(self):
        content = self.table.render_electoral_documents(self.process)
        self.assertEqual(content, 0)

    def test_render_elections_empty(self):
        content = self.table.render_elections(self.process)
        self.assertEqual(content, 0)

    def test_render_electoral_documents(self):
        self.create_electoral_document(electoral_process=self.process)
        self.create_electoral_document(electoral_process=self.process)
        content = self.table.render_electoral_documents(self.process)
        self.assertEqual(content, 2)

    def test_render_elections(self):
        self.create_election(electoral_process=self.process)
        content = self.table.render_elections(self.process)
        self.assertEqual(content, 1)


class ElectoralRollTableTest(VotersTestUtils, ManagementTestCase):
    """ """

    def setUp(self):
        pass
        # self.electoral_roll = self.create_electoral_roll()

    def test_render_created(self):
        pass

    def test_render_actions_with_published_rolls(self):
        pass

    def test_render_actions_with_to_publish_rolls(self):
        pass
