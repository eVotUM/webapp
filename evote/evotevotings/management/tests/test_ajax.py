# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from StringIO import StringIO
from mock import patch, Mock
import json

from django.core.urlresolvers import reverse
from django.test import RequestFactory
from django.http import Http404

from evotevotings.management.ajax import (ElectionCountingsPartialView,
                                          ElectionScrutinyStatusView)

from . import ManagementTestCase


class SecureActionAjaxViewTest(ManagementTestCase):
    """ """

    fixture = ['actions.json']

    def setUp(self):
        self.login_as_member()
        self.member.electoral_process.electoral_actions.prepopulate()
        self.url = reverse("management:secureaction-validate", kwargs={
            "process_pk": self.member.electoral_process.pk,
            "pk": 1
        })

    def test_get(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/partials/secure_action.html")

    def test_get_unexistent_action(self):
        """ """
        url = reverse("management:secureaction-validate", kwargs={
            "process_pk": self.member.electoral_process.pk,
            "pk": 100
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_get_unexistent_process(self):
        """ """
        url = reverse("management:secureaction-validate", kwargs={
            "process_pk": 3,
            "pk": 1
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_post(self):
        """ """
        file = StringIO(b"Dummy file")
        data = {'key1': file}
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.content)
        self.assertIn('errors_list', response_json)


class ManageSecureActionAjaxViewTest(ManagementTestCase):
    """ """

    fixture = ['actions.json']

    def setUp(self):
        # prepare a logged in member
        self.login_as_member()
        self.url = reverse("management:secureaction-manage", kwargs={
            "process_pk": self.member.electoral_process.pk,
        })

    def test_get(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/partials/secure_action.html")

    def test_get_unexistent_process(self):
        """ """
        url = reverse("management:secureaction-manage", kwargs={
            "process_pk": 3,
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_post(self):
        """ """
        file = StringIO(b"Dummy file")
        data = {"key1": file}
        response = self.client.post(self.url, data=data)
        response_json = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('errors_list', response_json)


class ElectoralMemberVerifyViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.dummy_data = {
            'username': 'dev',
            'email': 'dev',
            'phone': '+351912233441',
            'name': 'Dev Test'
        }
        self.login_as_institutional()
        self.url = reverse("management:electoralmember-verify", kwargs={
            "process_pk": self.member.electoral_process.pk}) +\
            "?username=tfb"

    @patch('evotevotings.management.forms.members_service.'
           'get_contact_info_by_login')
    def test_get(self, get_contact_info_by_login):
        """TODO: mock set_user of ElectoralMemberVerifyForm, instead of
        get_contact_info_by_login"""
        get_contact_info_by_login.return_value = self.dummy_data
        response = self.client.get(self.url)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('name', content)
        self.assertEqual(content['name'], 'Dev Test')

    @patch('evotevotings.management.forms.members_service.'
           'get_contact_info_by_login')
    def test_get_error(self, get_contact_info_by_login):
        """TODO: mock set_user of ElectoralMemberVerifyForm, instead of
        get_contact_info_by_login"""
        get_contact_info_by_login.return_value = {}
        response = self.client.get(self.url)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('error', content)


class AuthAmaAjaxViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_institutional()
        self.url = reverse("management:auth-ama", kwargs={
            "process_pk": self.member.electoral_process.pk
        })

    def test_template_used(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/partials/auth_ama.html")

    def test_login_required(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class HistoryAjaxViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        self.url = reverse("management:electoralprocess-history-next", kwargs={
            "process_pk": self.member.electoral_process.pk
        })

    def test_template_used(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/includes/history_list.html")

    def test_entries_rendered(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('No history', response.content)

    def test_login_required(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class HistoryNewAjaxViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        self.url = reverse(
            "management:electoralprocess-history-count", kwargs={
                "process_pk": self.member.electoral_process.pk
            })

    def test_value_returned(self):
        """ """
        response = self.client.get(self.url)
        response_json = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual({'new': 0}, response_json)

    def test_login_required(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectionCountingsPartialViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        # prepare a logged in member
        self.request = RequestFactory().get('/election-countings/')
        self.request.user = Mock(is_authenticated=True)

    @patch.object(ElectionCountingsPartialView, 'get_object')
    @patch.object(ElectionCountingsPartialView, 'get_total_votes',
                  return_value=0)
    @patch.object(ElectionCountingsPartialView, 'get_table_data',
                  return_value=[])
    def test_dispatch(self, get_table_data, get_total_votes, get_object):
        """ """
        election = Mock(pk=1, slug="test")
        election.electoral_process.slug = "test"
        election.can_manage_operations.return_value = True
        get_object.return_value = election
        response = ElectionCountingsPartialView.as_view()(self.request)
        self.assertEqual(response.status_code, 200)
        self.assertIn("Total ballot papers in ballot box", response.content)

    @patch.object(ElectionCountingsPartialView, 'get_object')
    def test_dispatch_can_manage_operations_false(self, get_object):
        """ """
        get_object().can_manage_operations.return_value = False
        with self.assertRaises(Http404):
            ElectionCountingsPartialView.as_view()(self.request)


class ElectionScrutinyStatusViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        # prepare a logged in member
        self.request = RequestFactory().get('/election-scrutiny-status/')
        self.request.user = Mock(is_authenticated=True)

    @patch("evotevotings.management.ajax.get_object_or_404")
    @patch("evotevotings.management.ajax.CSrouter.check_results_status")
    def test_get(self, check_results_status, get_object_or_404):
        """ """
        get_object_or_404.return_value = Mock(pk=1)
        check_results_status.return_value = 1
        response = ElectionScrutinyStatusView.as_view()(self.request)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(content, {'status': 1})
