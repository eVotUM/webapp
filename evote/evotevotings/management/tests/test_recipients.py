# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


# from evotemessages.templatetags.messages_tags import nested_url
# from evoteusers.tests.utils import UserTestUtils

# from . import ManagementTestCase


# class MessageRecipientsAjaxViewTest(ManagementTestCase, UserTestUtils):
#     """ """

#     def setUp(self):
#         self.login_as_member()
#         self.url = nested_url(
#             'message-recipients', slug=self.member.electoral_process.slug)

#     def test_get_with_slug(self):
#         """ """
#         url = self.url + "?q=test"
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)
