# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import json
from unittest import skip
from mock import MagicMock, patch

from django.contrib.auth.models import AnonymousUser
from django.core.urlresolvers import reverse
from django.forms.models import model_to_dict
from django.core.files import File
from django.utils import timezone, formats
from django.conf import settings

from evotevotings.elections.tests.utils import ElectionsTestUtils
from evotevotings.management.tables import (ElectoralProcessInstitutionalTable,
                                            ElectoralProcessMemberTable)
from evotevotings.management.forms import (ManagementAuthenticationForm,
                                           ElectoralProcessAsInstitutionalForm,
                                           ElectoralProcessAsMemberForm)
from evotevotings.processes.models import ElectoralMember, ElectoralProcess
from evotevotings.elections.models import Election
from evoteajaxforms.views import AjaxResponseAction
from guardian.shortcuts import assign_perm

from . import ManagementTestCase


def format_date(value):
    return formats.date_format(timezone.localtime(value), "DATETIME_FORMAT")


class LoginViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.credentials = {
            "username": "dev",
            "password": "password"
        }
        self.member = self.create_electoral_member(**self.credentials)
        self.url = settings.LOGIN_URL

    def test_login_view(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "management/login.html")

    def test_login_member(self):
        """ """
        self.credentials['profile'] = \
            ManagementAuthenticationForm.Profiles.ELECTORAL_MEMBER
        response = self.client.post(self.url, follow=True,
                                    data=self.credentials)
        self.assertRedirects(response, settings.LOGIN_REDIRECT_URL)
        self.assertEqual(response.context[0]["user"], self.member.user)

    def test_login_member_with_wrong_credentials(self):
        self.credentials["password"] = "wrong_password"
        self.credentials['profile'] = \
            ManagementAuthenticationForm.Profiles.ELECTORAL_MEMBER
        response = self.client.post(self.url, data=self.credentials)
        error_message = "Please enter a correct username and password. " +\
            "Note that both fields may be case-sensitive."
        self.assertFormError(response, "form", None, error_message)


class LogoutViewTest(ManagementTestCase):

    def setUp(self):
        self.login_as_member()

    def test_logout_view(self):
        """ """
        response = self.client.get(reverse("management:logout"), follow=True)
        self.assertRedirects(response, settings.LOGIN_URL)
        self.assertIsInstance(response.context[0]["user"], AnonymousUser)


class ElectoralProcessListViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.url = reverse("management:electoralprocess-list")

    def test_used_template(self):
        """ """
        self.login_as_member()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_list.html")

    def test_used_table_as_institutional(self):
        self.login_as_institutional()
        response = self.client.get(self.url)
        table_used = isinstance(
            response.context['table'], ElectoralProcessInstitutionalTable)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(table_used, True)

    def test_used_table_as_member(self):
        self.login_as_member()
        response = self.client.get(self.url)
        table_used = isinstance(
            response.context['table'], ElectoralProcessMemberTable)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(table_used, True)

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class DetailViewTest(ManagementTestCase):

    def setUp(self):
        self.login_as_member()
        self.url = reverse("management:electoralprocess-detail", kwargs={
            "slug": self.member.electoral_process.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"].pk,
                         self.member.electoral_process.pk)
        self.assertTemplateUsed(
            response, "management/electoralprocess_detail.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectoralActionViewTest(ManagementTestCase):

    fixture = ['actions.json']

    def setUp(self):
        self.login_as_member()
        self.url = reverse("management:electoralprocess-actions", kwargs={
            "slug": self.member.electoral_process.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"].pk,
                         self.member.electoral_process.pk)
        self.assertTemplateUsed(
            response, "management/electoralprocess_actions.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_post_with_action_validated(self):
        data = {
            'electoral_actions-TOTAL_FORMS': '2',
            'electoral_actions-INITIAL_FORMS': '2',
            'electoral_actions-0-action': '1',
            'electoral_actions-0-id': '1',
            'electoral_actions-0-strength': '2',
            'electoral_actions-1-action': '2',
            'electoral_actions-1-id': '2',
            'electoral_actions-1-strength': '1',
        }
        # register manage action as validated
        self.register_action(self.member.electoral_process.pk, "manage")
        self.member.electoral_process.electoral_actions.prepopulate()
        response = self.client.post(self.url, data=data)
        electoral_actions = self.member.electoral_process.electoral_actions
        self.assertEqual(response.status_code, 200)
        self.assertEqual(electoral_actions.get(pk=1).strength, 2)
        self.assertEqual(electoral_actions.get(pk=2).strength, 1)

    def test_post_with_action_not_validated(self):
        data = {
            'electoral_actions-TOTAL_FORMS': '1',
            'electoral_actions-INITIAL_FORMS': '1',
            'electoral_actions-0-action': '1',
            'electoral_actions-0-id': '1',
            'electoral_actions-0-strength': '2',
        }
        self.member.electoral_process.electoral_actions.prepopulate()
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertIn('action', content)
        self.assertIn(content['action'], "refresh")


class ElectionListViewTest(ElectionsTestUtils, ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        self.election = self.create_election(
            electoral_process=self.member.electoral_process)
        self.url = reverse("management:election-list", kwargs={
            "slug": self.member.electoral_process.slug
        })

    def test_get(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "management/election_list.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_queryset_filtered_by_electoral_process(self):
        """ """
        response = self.client.get(self.url)
        qs = response.context[0]['object_list']
        self.assertIn(self.election, list(qs))

    def test_view_extra_context_data(self):
        """ """
        response = self.client.get(self.url)
        self.assertIn('electoral_process', response.context[0])
        self.assertEqual(self.election.electoral_process_id,
                         response.context[0]['electoral_process'].pk)


class ElectoralProcessFormViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_institutional()
        self.process = self.create_electoral_process(
            created_by=self.member.user, actions_configured=True)
        self.url = reverse("management:electoralprocess-new")
        self.edit_url = reverse("management:electoralprocess-edit", kwargs={
            "slug": self.process.slug
        })

    def test_used_template_in_create_view(self):
        """ """
        assign_perm("evoteprocesses.add_electoralprocess", self.member.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_form.html")

    def test_used_template_in_edit_view(self):
        """ """
        assign_perm("evoteprocesses.change_electoralprocess", self.member.user,
                    self.process)
        response = self.client.get(self.edit_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_form.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_used_form_in_create_view_as_institutional(self):
        """ """
        assign_perm("evoteprocesses.add_electoralprocess", self.member.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context['form'],
                              ElectoralProcessAsInstitutionalForm)

    def test_used_form_in_edit_view_as_institutional(self):
        """ """
        assign_perm("evoteprocesses.change_electoralprocess", self.member.user,
                    self.process)
        response = self.client.get(self.edit_url)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context['form'],
                              ElectoralProcessAsInstitutionalForm)

    def test_used_form_as_member(self):
        """ """
        assign_perm("evoteprocesses.manage_electoralprocess", self.member.user,
                    self.process)
        self.set_as_member()
        response = self.client.get(self.edit_url)
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.context['form'],
                              ElectoralProcessAsMemberForm)

    def test_post_valid_as_responsible(self):
        """ """
        assign_perm("evoteprocesses.add_electoralprocess", self.member.user)
        data = {
            'process-identifier_pt': 'teste',
            'process-identifier_en': 'test',
            'process-description_pt': 'description in pt',
            'process-description_en': 'description in en',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertNotIn('errors_list', content)

    def test_post_valid_as_member(self):
        """ """
        assign_perm("evoteprocesses.manage_electoralprocess", self.member.user,
                    self.process)
        file_mock = MagicMock(spec=File, name='FileMock')
        file_mock.name = 'test1.jpg'
        date = formats.date_format(
            timezone.now() + timezone.timedelta(days=1),
            "DATETIME_FORMAT")
        data = {
            'process-identifier_pt': 'teste',
            'process-identifier_en': 'test',
            'process-description_pt': 'description in pt',
            'process-description_en': 'description in en',
            'process-publication_date': date,
            'electoral-documents-TOTAL_FORMS': 1,
            'electoral-documents-INITIAL_FORMS': 0,
            'electoral-documents-MIN_NUM_FORMS': 1,
            'electoral-documents-MAX_NUM_FORMS': 1000,
            'electoral-documents-0-designation': 'teste',
            'electoral-documents-0-documents-TOTAL_FORMS': 1,
            'electoral-documents-0-documents-INITIAL_FORMS': 0,
            'electoral-documents-0-documents-MIN_NUM_FORMS': 0,
            'electoral-documents-0-documents-MAX_NUM_FORMS': 1000,
            'electoral-documents-0-id': '',
            'electoral-documents-0-documents-0-designation': 'test',
            'electoral-documents-0-documents-0-id': '',
            'electoral-documents-0-documents-0-document': file_mock,

        }
        self.set_as_member()
        response = self.client.post(self.edit_url, data=data)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertNotIn('errors_list', content)

    def test_post_invalid_as_responsible(self):
        """ """
        assign_perm("evoteprocesses.add_electoralprocess", self.member.user)
        data = {
            'id_process-identifier_pt': 'teste',
            'id_process-identifier_en': 'test',
            'id_process-description_pt': 'description in pt',
            'id_process-description_en': 'description in en',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertIn('errors_list', content)

    @patch("evotevotings.management.views.ElectoralProcessFormView." +
           "is_action_secure", return_value=True)
    def test_post_invalid_as_member(self, mock_fn):
        """ """
        assign_perm("evoteprocesses.manage_electoralprocess", self.member.user,
                    self.process)
        date = formats.date_format(
            timezone.now() + timezone.timedelta(days=1),
            "DATETIME_FORMAT")
        data = {
            'process-identifier_pt': 'teste',
            'process-identifier_en': 'test',
            'process-description_pt': 'description in pt',
            'process-description_en': 'description in en',
            'process-publication_date': date,
            'electoral-documents-TOTAL_FORMS': 1,
            'electoral-documents-INITIAL_FORMS': 1,
            'electoral-documents-MIN_NUM_FORMS': 1,
            'electoral-documents-MAX_NUM_FORMS': 1000,
            'electoral-documents-0-designation': '',
            'electoral-documents-0-documents-TOTAL_FORMS': 2,
            'electoral-documents-0-documents-INITIAL_FORMS': 1,
            'electoral-documents-0-documents-MIN_NUM_FORMS': 0,
            'electoral-documents-0-documents-MAX_NUM_FORMS': 1000,
            'electoral-documents-0-id': 1,
            'electoral-documents-0-documents-0-designation': 'test',
            'electoral-documents-0-documents-0-id': 1,
        }
        self.set_as_member()
        response = self.client.post(self.edit_url, data=data)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertIn('errors_list', content)


class ElectoralProcessElectionsViewTest(ElectionsTestUtils,
                                        ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_institutional()
        self.process = self.create_electoral_process(
            created_by=self.member.user)
        self.election = self.create_election(electoral_process=self.process)
        self.url = reverse("management:electoralprocess-elections", kwargs={
            "slug": self.process.slug
        })
        self.data_valid = {
            'elections-TOTAL_FORMS': '1',
            'elections-INITIAL_FORMS': '0',
            'elections-MIN_NUM_FORMS': '1',
            'elections-MAX_NUM_FORMS': '1000',
            'elections-0-identifier_pt': 'election test in pt',
            'elections-0-identifier_en': 'election test in en',
            'elections-0-description_pt': 'description election in pt',
            'elections-0-description_en': 'description election in en',
            'elections-0-proposition_begin_date': '2016-10-26 15:00:00',
            'elections-0-proposition_end_date': '2016-10-27 15:00:00',
            'elections-0-complaint_begin_date': '2016-10-28 15:00:00',
            'elections-0-complaint_end_date': '2016-10-29 15:00:00',
            'elections-0-roll_begin_date': '2016-10-28 15:00:00',
            'elections-0-roll_end_date': '2016-10-29 15:00:00',
            'elections-0-voting_begin_date': '2016-10-30 15:00:00',
            'elections-0-voting_end_date': '2016-11-01 15:00:00',
            'elections-0-minute_publication_date': '2016-11-03 15:00:00',
            'elections-0-minute_begin_date': '2016-11-04 15:00:00',
            'elections-0-minute_end_date': '2016-11-05 15:00:00',
        }

    def test_get(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_elections.html")

    def test_post_valid(self):
        """ """
        response = self.client.post(self.url, data=self.data_valid)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('errors_list', content)

    def test_post_invalid(self):
        """ """
        data = {
            'elections-TOTAL_FORMS': '1',
            'elections-INITIAL_FORMS': '0',
            'elections-MIN_NUM_FORMS': '1',
            'elections-MAX_NUM_FORMS': '1000',
            'elections-0-identifier_pt': 'election test in pt',
            'elections-0-identifier_en': 'election test in en',
            'elections-0-description_pt': '',
            'elections-0-description_en': '',
        }
        response = self.client.post(self.url, data=data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('errors_list', content)

    def test_post_valid_as_process_new(self):
        """ """
        self.process.state = ElectoralProcess.State.NEW
        self.process.save()
        response = self.client.post(self.url, data=self.data_valid)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertIn('action', content)
        self.assertEqual(content['action'], "refresh")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectoralProcessMembersEditViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        """ """
        self.login_as_institutional()
        self.process = self.member.electoral_process
        self.process.state = ElectoralProcess.State.NEW
        self.url = reverse("management:electoralprocess-members-edit", kwargs={
            "slug": self.member.electoral_process.slug
        })
        self.valid_data = {
            'members-TOTAL_FORMS': 3,
            'members-INITIAL_FORMS': 1,
            'members-MIN_NUM_FORMS': 3,
            'members-MAX_NUM_FORMS': 24,
            'members-0-username': self.member.user.username,
            'members-0-email': self.member.user.email,
            'members-0-name': self.member.user.name,
            'members-0-role': self.member.role,
            'members-0-designation': self.member.designation,
            'members-0-phone': self.member.user.primary_phone,
            'members-0-id': self.member.pk,
            'members-0-electoral_process': self.process.pk,
            'members-1-username': 'dev2',
            'members-1-email': 'dev@eurotux.com',
            'members-1-name': 'Dev Test',
            'members-1-role': 1,
            'members-1-designation': 'Designation test dev',
            'members-1-phone': '+351922244665',
            'members-1-id': '',
            'members-1-electoral_process': self.process.pk,
            'members-2-username': 'dev3',
            'members-2-email': 'dev2@eurotux.com',
            'members-2-name': 'Dev Test 2',
            'members-2-role': 1,
            'members-2-designation': 'Designation test 2',
            'members-2-phone': '+351922244665',
            'members-2-id': '',
            'members-2-electoral_process': self.process.pk
        }
        self.usernames = [self.member.user.username, 'dev2', 'dev3']

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_members.html")

    def test_with_unexistent_object(self):
        """ """
        url = reverse("management:electoralprocess-members-edit", kwargs={
            "slug": "test"
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_form_valid(self):
        response = self.client.post(self.url, data=self.valid_data)
        created = ElectoralMember.objects.filter(
            user__username__in=self.usernames).count()
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        action_url = reverse("management:electoralprocess-summary",
                             kwargs={"slug": self.process.slug})
        self.assertIn('redirect', content['action'])
        self.assertIn(action_url, content['action_url'])
        self.assertEqual(created, 3)

    def test_form_valid_as_draft(self):
        self.process.state = ElectoralProcess.State.DRAFT
        self.process.save()
        response = self.client.post(self.url, data=self.valid_data)
        created = ElectoralMember.objects.filter(
            user__username__in=self.usernames).count()
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        action_url = reverse("management:electoralprocess-summary",
                             kwargs={"slug": self.process.slug})
        self.assertIn('redirect', content['action'])
        self.assertIn(action_url, content['action_url'])
        self.assertEqual(created, 3)

    def test_form_invalid_number_members(self):
        invalid_number_data = {
            'members-TOTAL_FORMS': 1,
            'members-INITIAL_FORMS': 1,
            'members-MIN_NUM_FORMS': 3,
            'members-MAX_NUM_FORMS': 24,
            'members-0-email': self.member.user.email,
            'members-0-name': self.member.user.name,
            'members-0-role': self.member.role,
            'members-0-designation': self.member.designation,
            'members-0-phone': self.member.user.primary_phone,
            'members-0-id': self.member.pk,
            'members-0-electoral_process': self.process.pk,
        }
        response = self.client.post(self.url, data=invalid_number_data)
        content = json.loads(response.content)
        self.assertIn('errors_list', content)

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectoralProcessMembersViewTest(ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        self.member.electoral_process.state = ElectoralProcess.State.NEW
        self.url = reverse(
            "management:electoralprocess-members-view", kwargs={
                "slug": self.member.electoral_process.slug
            })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_members.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectoralProcessSummaryViewTest(ElectionsTestUtils, ManagementTestCase):
    """ """

    def setUp(self):
        # TODO: improve the setUp
        self.login_as_institutional()
        self.process = self.create_electoral_process(
            created_by=self.member.user)
        self.member.electoral_process = self.process
        self.member.role = ElectoralMember.Role.CHAIRMAN
        self.member.save()
        self.url = reverse("management:electoralprocess-summary", kwargs={
            "slug": self.process.slug
        })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_summary.html")

    def test_redirect_when_no_chairman_associated(self):
        """ """
        self.member.role = ElectoralMember.Role.OTHER
        self.member.save()
        response = self.client.get(self.url)
        redirect_to = reverse("management:electoralprocess-members-edit",
                              kwargs={"slug": self.process.slug})
        self.assertRedirects(response, redirect_to)

    def test_redirect_when_no_members_associated(self):
        """ """
        self.member.delete()
        response = self.client.get(self.url)
        redirect_to = reverse("management:electoralprocess-members-edit",
                              kwargs={"slug": self.process.slug})
        self.assertRedirects(response, redirect_to)

    def test_with_unexistent_object(self):
        """ """
        url = reverse("management:electoralprocess-summary", kwargs={
            "slug": "test"
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class ElectoralProcessConfirmationViewTest(ManagementTestCase,
                                           ElectionsTestUtils):
    """ """

    def setUp(self):
        self.login_as_institutional()
        self.process = self.create_electoral_process(state=4)
        self.election = self.create_election(electoral_process=self.process)
        self.member = self.create_electoral_member(
            electoral_process=self.process, role=ElectoralMember.Role.CHAIRMAN)
        self.url = reverse(
            "management:electoralprocess-confirmation", kwargs={
                "slug": self.process.slug
            })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_confirmation.html")

    def test_cannot_proceed(self):
        """ """
        process = self.create_electoral_process(electoral_members=None)
        url = reverse(
            "management:electoralprocess-confirmation", kwargs={
                "slug": process.slug
            })
        response = self.client.get(url)
        message = list(response.context['messages'])[0]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(message), "Unable to execute the operation.")

    def test_with_unexistent_object(self):
        """ """
        url = reverse("management:electoralprocess-confirmation", kwargs={
            "slug": "test"
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class ElectoralProcessCommunicationViewTest(ManagementTestCase,
                                            ElectionsTestUtils):
    """ """

    def setUp(self):
        self.login_as_member()
        self.process = self.create_electoral_process(
            state=4, actions_configured=True)
        self.election = self.create_election(electoral_process=self.process)
        self.member = self.create_electoral_member(
            electoral_process=self.process, role=ElectoralMember.Role.CHAIRMAN,
            is_comunication_responsible=True)
        self.url = reverse(
            "management:electoralprocess-communication", kwargs={
                "slug": self.process.slug
            })

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/electoralprocess_communication.html")

    def test_change_responsible(self):
        data = {
            'responsible-responsible_communication': self.member.pk
        }
        response = self.client.post(self.url, data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('errors_list', content)

    def test_with_unexistent_object(self):
        """ """
        url = reverse("management:electoralprocess-communication", kwargs={
            "slug": "test"
        })
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class ElectionDetailConfigsViewTest(ManagementTestCase, ElectionsTestUtils):
    """ """

    def setUp(self):
        self.login_as_member()
        self.election = self.create_election(
            electoral_process=self.member.electoral_process)
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.url = reverse("management:election-detail-configs", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/election_detail_configs.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch("evotevotings.management.views.ElectionDetailConfigsView." +
           "is_action_secure", return_value=True)
    def test_success_url(self, mock_fn):
        data = model_to_dict(self.election)
        # garantee that election has hondt_total_seats correctly filled
        data.update({
            'hondt_total_seats': 1
        })
        response = self.client.post(self.url, data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(content['action'], AjaxResponseAction.REFRESH)


class ElectionDetailCalendarViewTest(ManagementTestCase, ElectionsTestUtils):
    """ """

    def setUp(self):
        self.login_as_member()
        self.election = self.create_election(
            electoral_process=self.member.electoral_process)
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.url = reverse("management:election-detail-calendar", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/election_detail_calendar.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch("evotevotings.management.views.ElectionDetailCalendarView." +
           "is_action_secure", return_value=True)
    def test_success_url(self, mock_fn):
        data = {
            'proposition_begin_date': '10-10-2016 10:00',
            'proposition_end_date': '11-10-2016 10:00',
            'complaint_begin_date': '12-10-2016 10:00',
            'complaint_end_date': '13-10-2016 10:00',
            'roll_begin_date': '12-10-2016 10:00',
            'roll_end_date': '13-10-2016 10:00',
            'voting_begin_date': '14-10-2016 10:00',
            'voting_end_date': '15-10-2016 10:00',
            'minute_publication_date': '16-10-2016 10:00',
            'minute_begin_date': '17-10-2016 10:00',
            'minute_end_date': '18-10-2016 10:00'
        }
        response = self.client.post(self.url, data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(content['action'], AjaxResponseAction.REFRESH)


class ElectionDetailCategoriesViewTest(ManagementTestCase, ElectionsTestUtils):
    """ """

    def setUp(self):
        self.login_as_member()
        self.election = self.create_election(
            electoral_process=self.member.electoral_process,
            weight_votes=True)
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.url = reverse("management:election-detail-categories", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })

    def test_used_template(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/election_detail_categories.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    @patch("evotevotings.management.views.ElectionDetailCategoriesView." +
           "is_action_secure", return_value=True)
    def test_success_url(self, mock_fn):
        data = {
            'categories-TOTAL_FORMS': '1',
            'categories-INITIAL_FORMS': '1',
            'categories-0-id': '1',
            'categories-0-election': self.election.pk,
            'categories-0-designation_pt': 'Dois',
            'categories-0-designation_en': 'Two',
            'categories-0-weight': '2'
        }
        response = self.client.post(self.url, data)
        content = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(content['action'], AjaxResponseAction.REFRESH)


class ElectionDetailCandidatesViewTest(ElectionsTestUtils, ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        self.election = self.create_election(
            electoral_process=self.member.electoral_process,
            proposition_end_date=timezone.now())
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.url = reverse("management:election-detail-candidates", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })

    def test_used_template(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/election_detail_candidates.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectionDetailRollsView(ElectionsTestUtils, ManagementTestCase):

    @skip("TO IMPLEMENT")
    def test_used_template(self):
        """ """

    @skip("TO IMPLEMENT")
    def test_login_required(self):
        """ """


class ElectionDetailRollsPublishView(ElectionsTestUtils, ManagementTestCase):

    @skip("TO IMPLEMENT")
    def test_used_template(self):
        """ """

    @skip("TO IMPLEMENT")
    def test_login_required(self):
        """ """


class ElectionDetailBulletinViewTest(ElectionsTestUtils, ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        self.election = self.create_election(
            electoral_process=self.member.electoral_process,
            configs_filled=True)
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.election.candidates.create()
        self.url = reverse("management:election-detail-bulletin", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })

    def test_used_template(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/election_detail_bulletin.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectionDetailBulletinPreviewViewTest(ElectionsTestUtils,
                                            ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_member()
        election = self.create_election(
            electoral_process=self.member.electoral_process)
        self.url = reverse("management:election-detail-bulletin-preview",
                           kwargs={"slug": self.member.electoral_process.slug,
                                   "slug_election": election.slug})

    def test_used_template(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(
            response, "management/election_detail_bulletin_preview.html")


class PersonalDataViewTest(ManagementTestCase):

    def setUp(self):
        self.login_as_member()
        self.url = reverse("management:personal-data")

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'management/personal_data.html')

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ResendPendingTokenMailViewTest(ManagementTestCase):
    def setUp(self):
        self.login_as_member()
        self.url = reverse("management:resend-email")

    def test_get(self):
        """ """
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse("management:personal-data"))

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectionManageViewTest(ElectionsTestUtils, ManagementTestCase):

    fixtures = ['actions.json', 'certs_incomplete.json']

    def setUp(self):
        self.login_as_member()
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.election = self.create_election(
            state=Election.State.DEFINITIVE_ELECTORAL_ROLL,
            electoral_process=self.member.electoral_process)
        self.url = reverse("management:election-manage", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })
        self.register_action(self.member.electoral_process.pk, 1)

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"].pk, self.election.pk)
        self.assertTemplateUsed(
            response, "management/election_manage.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_post_with_election_invalid_certificate_integrity(self):
        """ """
        expected = formats.date_format(
            timezone.now() + timezone.timedelta(days=1), "DATETIME_FORMAT")
        data = {
            'voting_begin_date': expected
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.election.refresh_from_db()
        returned = formats.date_format(
            self.election.voting_begin_date, "DATETIME_FORMAT")
        self.assertNotEqual(returned, expected)

    @patch("evotevotings.management.views.SecretEntry")
    def test_post_with_election_in_definitive_eletoral_roll(self, mock_fn):
        """ """
        mock_fn().objects.check_integrity.return_value = True
        expected = (timezone.now() + timezone.timedelta(days=3)).\
            strftime("%d-%m-%Y %H:%M")
        data = {
            'voting_begin_date': expected
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.election.refresh_from_db()
        returned = format_date(self.election.voting_begin_date)
        self.assertEqual(returned, expected)

    def test_post_with_election_not_in_definitive_eletoral_roll(self):
        """ """
        self.election.state = self.election.State.IN_TO_VOTE
        self.election.save()
        expected = (timezone.now() + timezone.timedelta(days=3)).\
            strftime("%d-%m-%Y %H:%M")
        data = {
            'voting_begin_date': expected
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.election.refresh_from_db()
        returned = format_date(self.election.voting_begin_date)
        self.assertNotEqual(returned, expected)


class ElectionManageCloseViewTest(ElectionsTestUtils, ManagementTestCase):

    fixture = ['actions.json']

    def setUp(self):
        voting_begin_date = timezone.now() - timezone.timedelta(days=1)
        self.login_as_member()
        electoral_process = self.member.electoral_process
        electoral_process.actions_configured = True
        electoral_process.save(update_fields=['actions_configured'])
        self.election = self.create_election(
            state=Election.State.IN_TO_VOTE,
            voting_begin_date=voting_begin_date,
            electoral_process=self.member.electoral_process)
        self.url = reverse("management:election-manage-close", kwargs={
            "slug": self.member.electoral_process.slug,
            "slug_election": self.election.slug
        })
        self.register_action(self.member.electoral_process.pk, 2)

    def test_used_template(self):
        """ """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["object"].pk, self.election.pk)
        self.assertTemplateUsed(
            response, "management/election_manage_close.html")

    def test_login_required(self):
        """ """
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_post_with_election_in_to_vote(self):
        """ """
        expected = (timezone.now() + timezone.timedelta(days=3)).\
            strftime("%d-%m-%Y %H:%M")
        data = {
            'voting_end_date': expected
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.election.refresh_from_db()
        returned = format_date(self.election.voting_end_date)
        self.assertEqual(returned, expected)

    def test_post_with_election_not_in_to_vote(self):
        """ """
        self.election.state = self.election.State.IN_COUNTING
        self.election.save()
        expected = (timezone.now() + timezone.timedelta(days=3)).\
            strftime("%d-%m-%Y %H:%M")
        data = {
            'voting_end_date': expected
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, 200)
        self.election.refresh_from_db()
        returned = format_date(self.election.voting_end_date)
        self.assertNotEqual(returned, expected)


class GenerateCertificateViewTest(ManagementTestCase):
    """ """
    fixtures = ['users.json', 'certs.json']

    def setUp(self):
        self.login_as_institutional()
        self.process = self.create_electoral_process(state=4)
        self.url = reverse("management:electoralprocess-generate", kwargs={
            "slug": self.process.slug,
        })
        saml_confs = getattr(settings, 'SAML', '')
        self.sess_key = saml_confs.get('session_dict', None)

    def test_get_with_nic(self):
        session = self.client.session
        session[self.sess_key] = {'attributes': {'NIC': self.member.user.nic}}
        session.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 400)

    def test_get_with_wrong_nic(self):
        session = self.client.session
        session[self.sess_key] = {'attributes': {'NIC': 'Unit test bad NIC'}}
        session.save()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

    def test_used_template(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)
        # self.assertTemplateUsed(
        #    response, 'management/generate_certificate.html')

    def test_login_required(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ElectoralProcessSummaryPdfView(ManagementTestCase):
    """ """

    def setUp(self):
        self.login_as_institutional()
        self.process = self.create_electoral_process(
            created_by=self.member.user)
        self.url = reverse(
            "management:electoralprocess-pdf", kwargs={
                "slug": self.process.slug,
            })

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_login_required(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)
