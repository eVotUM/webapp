# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase, RequestFactory
from django.views.generic import TemplateView

from evotevotings.management.mixins import HistoryMixin

from . import ManagementTestCase


class MixinsTestSimple(SimpleTestCase):

    class NokObject(HistoryMixin):
        """ """

    def test_mixin_nok(self):
        object = self.NokObject()
        self.assertRaises(NotImplementedError, object.get_electoral_process_pk)


class MixinsTest(ManagementTestCase):

    class DummyView(HistoryMixin, TemplateView):
        """ """
        process_pk = None

        def __init__(self, request, process_pk, *args, **kwargs):
            self.request = request
            self.process_pk = process_pk

        def get_electoral_process_pk(self):
            return self.process_pk

    def setUp(self):
        self.login_as_member()
        self.factory = RequestFactory()
        self.request = self.factory.get('/history/')
        setattr(self.member.user, 'is_electoral_member', True)
        self.request.user = self.member.user
        self.view = self.DummyView(
            self.request, self.member.electoral_process.pk)

    def test_context_data(self):
        kwargs = {}
        context = self.view.get_context_data(**kwargs)

        self.assertEqual(
            context['process_pk'], self.member.electoral_process.pk)
        self.assertEqual(context['next_page'], 1)
        self.assertEqual(context['show_history'], True)
        self.assertEqual(context['more'], True)
