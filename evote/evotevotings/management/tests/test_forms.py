# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils import timezone
from mock import patch

from evotevotings.processes.tests.utils import ProcessesTestUtils
from evotevotings.management.forms import (ManagementAuthenticationForm,
                                           SecureActionKeysForm,
                                           ElectoralMemberForm,
                                           ElectoralMemberVerifyForm,
                                           ElectoralProcessAsMemberForm,
                                           CertificateGenerationForm)
from evotevotings.processes.models import ElectoralAction, ElectoralMember

from . import ManagementTestCase


class ManagementAuthenticationFormTest(
        ManagementTestCase, ProcessesTestUtils):
    """ """

    def setUp(self):
        self.cred_member = {
            'username': 'member',
            'password': 'password'
        }
        self.cred_responsible = {
            'username': 'responsible',
            'password': 'password'
        }
        self.member = self.create_electoral_member(**self.cred_member)
        self.responsible = self.create_user(
            is_institutional_responsible=True, **self.cred_responsible)

    def test_member_user(self):
        self.cred_member['profile'] = \
            ManagementAuthenticationForm.Profiles.ELECTORAL_MEMBER
        form = ManagementAuthenticationForm(data=self.cred_member)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.get_user(), self.member.user)

    def test_responsible_user(self):
        self.cred_responsible['profile'] = \
            ManagementAuthenticationForm.Profiles.INSTITUTIONAL_RESPONSIBLE
        form = ManagementAuthenticationForm(data=self.cred_responsible)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.get_user(), self.responsible)

    def test_non_responsible_member_with_responsible_profile_selected(self):
        self.cred_member['profile'] = \
            ManagementAuthenticationForm.Profiles.INSTITUTIONAL_RESPONSIBLE
        form = ManagementAuthenticationForm(data=self.cred_member)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors.values()[0][0], form.error_messages['invalid_login'])

    def test_non_member_responsible_with_member_profile_selected(self):
        self.cred_responsible['profile'] = \
            ManagementAuthenticationForm.Profiles.ELECTORAL_MEMBER
        form = ManagementAuthenticationForm(data=self.cred_responsible)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors.values()[0][0], form.error_messages['invalid_login'])


class SecureActionKeysFormTest(ManagementTestCase):

    def test_num_keys_with_simple_strength(self):
        num_members = 10
        strength = ElectoralAction.Strength.SIMPLE
        form = SecureActionKeysForm(
            num_members=num_members, strength=strength,
            elec_proc_id=1
        )
        self.assertEqual(form.num_keys, 1)
        self.assertEqual(len(form.fields), 1)

    def test_num_keys_with_strong_strength(self):
        num_members = 10
        strength = ElectoralAction.Strength.STRONG
        form = SecureActionKeysForm(
            num_members=num_members, strength=strength,
            elec_proc_id=1
        )
        self.assertEqual(form.num_keys, (num_members / 2) + 1)
        self.assertEqual(len(form.fields), (num_members / 2) + 1)

    def test_num_keys_with_critical_strength(self):
        num_members = 10
        strength = ElectoralAction.Strength.CRITICAL
        form = SecureActionKeysForm(
            num_members=num_members, strength=strength,
            elec_proc_id=1
        )
        self.assertEqual(form.num_keys, num_members)
        self.assertEqual(len(form.fields), num_members)


class ElectoralMemberFormTest(ManagementTestCase):

    def setUp(self):
        self.process = self.create_electoral_process()
        self.data = {
            'name': 'test',
            'role': ElectoralMember.Role.CHAIRMAN,
            'designation': 'test',
            'username': 'dev',
            'email': 'dev@eurotux.com',
            'phone': '+351911111111',
            'electoral_process': self.process.pk
        }

    def test_add_chairman(self):
        form = ElectoralMemberForm(data=self.data)
        self.assertEqual(form.is_valid(), True)


class ElectoralMemberVerifyFormTest(ManagementTestCase):

    def setUp(self):
        self.data = {
            'username': 'dev'
        }
        self.data_invalid = {
            'username': 'unknown'
        }
        self.user_info = {
            'username': 'dev',
            'phone': '+351912233441',
            'name': 'Dev Test'
        }

    @patch('evotevotings.management.forms.members_service.'
           'get_contact_info_by_login')
    def test_get_user(self, get_contact_info_by_login):
        get_contact_info_by_login.return_value = self.user_info
        form = ElectoralMemberVerifyForm(data=self.data)
        form.is_valid()
        self.assertEqual(form.get_user(), self.user_info)

    @patch('evotevotings.management.forms.members_service.'
           'get_contact_info_by_login')
    def test_get_user_with_invalid_number(self, get_contact_info_by_login):
        self.user_info['phone'] = "93000"
        get_contact_info_by_login.return_value = self.user_info
        form = ElectoralMemberVerifyForm(data=self.data)
        form.is_valid()
        self.assertEqual(form.get_user()["phone"], "")

    def test_get_user_none(self):
        form = ElectoralMemberVerifyForm(data=self.data_invalid)
        self.assertEqual(form.get_user(), None)


class ElectoralProcessAsMemberFormTest(ManagementTestCase):

    def setUp(self):
        self.data = {
            'identifier_pt': 'test',
            'identifier_en': 'test',
            'description_pt': 'test',
            'description_en': 'test',
            'publication_date': timezone.now() + timezone.timedelta(days=1)
        }
        self.data_invalid = {
            'identifier_pt': 'test',
            'identifier_en': 'test',
            'description_pt': 'test',
            'description_en': 'test',
            'publication_date': timezone.now() - timezone.timedelta(days=1)
        }

    def test_publication_date_ok(self):
        form = ElectoralProcessAsMemberForm(data=self.data)
        self.assertEqual(form.is_valid(), True)

    def test_publication_date_nok(self):
        form = ElectoralProcessAsMemberForm(data=self.data_invalid)
        self.assertEqual(form.is_valid(), False)


class CertificateGenerationFormTest(ManagementTestCase):

    def setUp(self):
        self.data = {
            'elections': [],
            'members_pk': [],
            'initial': {},
            'data': {
                'elec_pvt_key': 'teste'
            }
        }
        self.data_invalid = {
            'elections': [],
            'members_pk': [],
            'initial': {},
            'data': {}
        }

    def test_form_valid(self):
        form = CertificateGenerationForm(
            **self.data
        )
        self.assertEqual(form.is_valid(), True)

    def test_form_invalid(self):
        form = CertificateGenerationForm(
            **self.data_invalid)
        self.assertEqual(form.is_valid(), False)
