# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.urlresolvers import reverse_lazy
from django.test import TestCase, override_settings, modify_settings

from evotevotings.processes.tests.utils import ProcessesTestUtils
from evotevotings.management.registers import secure_action
from evotevotings.management.forms import ManagementAuthenticationForm


@override_settings(
    ROOT_URLCONF='evote.urls.management',
    LOGIN_REDIRECT_URL=reverse_lazy("management:electoralprocess-list"),
    LOGIN_URL=reverse_lazy("management:login"),
    JAVASCRIPT_SETTINGS_SCAN_MODULES={},
    EVOTEMESSAGES_TEMPLATE_BASE_DIR='management/',
    EVOTEMESSAGES_BASE_APP_NAME='management'
)
@modify_settings(MIDDLEWARE={
    'append': 'evotevotings.management.middleware.ManagementMiddleware',
})
class ManagementTestCase(ProcessesTestUtils, TestCase):

    def login_as_member(self):
        credentials = {
            "username": "dev",
            "password": "password"
        }
        self.member = self.create_electoral_member(**credentials)
        self.client.login(**credentials)
        self.set_as_member()
        return credentials

    def login_as_institutional(self):
        credentials = {
            "username": "dev",
            "password": "password",
            "role": 1
        }
        self.member = self.create_institutional_member(**credentials)
        self.client.login(**credentials)
        self.set_as_institutional()
        return credentials

    def set_as_institutional(self):
        session = self.client.session
        session['user_profile'] = \
            ManagementAuthenticationForm.Profiles.INSTITUTIONAL_RESPONSIBLE
        session.save()

    def set_as_member(self):
        session = self.client.session
        session['user_profile'] = \
            ManagementAuthenticationForm.Profiles.ELECTORAL_MEMBER
        session.save()

    def register_action(self, electoral_process_id, action):
        """ """
        key = secure_action.prefix_key(electoral_process_id)
        session = self.client.session
        session[key] = action
        session.save()
