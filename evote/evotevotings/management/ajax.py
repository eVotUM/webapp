# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.shortcuts import get_object_or_404
from django.views import View
from django.http import JsonResponse, Http404
from django.core.urlresolvers import reverse
from django.views.generic.base import TemplateView
from django.core.paginator import Paginator

from evotevotings.processes.models import ElectoralProcess, ElectoralAction
from evotevotings.management.forms import ElectoralMemberVerifyForm
from evotevotings.elections.models import Election, ElectionCountQuery
from evotevotings.services.routers import ASrouter, CSrouter
from evoteajaxforms.views import (FormPartialJsonView, DetailPartialJsonView,
                                  PartialJsonMixin)
from evotelogs.models import AuditableEvent
from django_tables2 import SingleTableMixin
from django.conf import settings

from .registers import secure_action
from .tables import ElectionCountQueryTable
from .forms import SecureActionKeysForm


@method_decorator(login_required, name="dispatch")
class SecureActionAjaxView(FormPartialJsonView):
    """ """
    template_name = "management/partials/secure_action.html"
    form_class = SecureActionKeysForm
    success_message = None  # to avoid double success messages
    error_message = None  # to avoid double error messages

    def get_object(self):
        """ """
        return get_object_or_404(
            ElectoralAction, action_id=self.kwargs.get("pk", None),
            electoral_process=self.kwargs.get("process_pk"))

    def get_action_identifier(self):
        return self.kwargs.get("pk")

    def get_form_kwargs(self):
        """ """
        kwargs = super(SecureActionAjaxView, self).get_form_kwargs()
        electoral_action = self.get_object()
        electoral_process = electoral_action.electoral_process
        kwargs.update({
            'num_members': electoral_process.electoral_members.count(),
            'elec_proc_id': electoral_process.pk,
            'strength': electoral_action.strength
        })
        return kwargs

    def form_valid(self, form):
        secure_action.register(
            self.request, self.kwargs.get("process_pk"),
            self.get_action_identifier())
        return super(SecureActionAjaxView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
class ManageSecureActionAjaxView(SecureActionAjaxView):
    """ """

    def get_form_kwargs(self):
        """ """
        electoral_process = get_object_or_404(
            ElectoralProcess, pk=self.kwargs.get("process_pk"))
        kwargs = super(SecureActionAjaxView, self).get_form_kwargs()
        kwargs.update({
            'num_members': electoral_process.electoral_members.count(),
            'elec_proc_id': electoral_process.pk,
            'strength': ElectoralAction.Strength.CRITICAL
        })
        return kwargs

    def get_action_identifier(self):
        return "manage"


@method_decorator(login_required, name="dispatch")
class ElectoralMemberVerifyView(View):
    """ """

    def get(self, request, *args, **kwargs):
        form = ElectoralMemberVerifyForm(request.GET)

        response_data = None

        if form.is_valid():
            user = form.get_user()

            if user:
                response_data = {
                    'username': user['username'],
                    'email': user['email'],
                    'name': user['name'],
                    'phone': str(user['phone'] or ''),
                    'enable': ['role', 'designation']
                }
        else:
            response_data = {
                'error': form.errors['username']
            }

        return JsonResponse(response_data, status=200)


@method_decorator(login_required, name="dispatch")
class ElectionCountingsPartialView(SingleTableMixin, DetailPartialJsonView):
    """ """
    model = Election
    table_class = ElectionCountQueryTable
    template_name = "management/partials/election_countings.html"

    def dispatch(self, *args, **kwargs):
        """Raise 404 when elections is not within voting period"""
        self.object = self.get_object()
        if not self.object.can_manage_operations():
            raise Http404
        return super(ElectionCountingsPartialView, self)\
            .dispatch(*args, **kwargs)

    def get_table_data(self):
        """Return count queries queyset for this election"""
        return self.object.count_queries.all()

    def get_context_data(self, **kwargs):
        """Call api for total votes and add the value to the context"""
        # must be called before super() to include the new registered count
        total_votes = self.get_total_votes(self.request.user)
        context = super(ElectionCountingsPartialView, self)\
            .get_context_data(**kwargs)
        context.update({
            'total_votes': total_votes
        })
        return context

    def get_total_votes(self, user):
        """Request a votes counting and register the request"""
        # API call to get partial count
        total_votes = ASrouter().partial_count(self.object.pk)
        ElectionCountQuery.objects.create(
            election=self.object,
            user=user,
            total_count=total_votes
        )
        return total_votes


@method_decorator(login_required, name="dispatch")
class ElectionScrutinyStatusView(View):
    """ """

    def get(self, request, *args, **kwargs):
        election = get_object_or_404(
            Election, pk=kwargs.get('pk'),
            electoral_process_id=kwargs.get('process_pk'))
        data = {"status": CSrouter().check_results_status(election.pk)}
        return JsonResponse(data, status=200)


@method_decorator(login_required, name="dispatch")
class AuthAmaAjaxView(PartialJsonMixin):
    """ """
    template_name = "management/partials/auth_ama.html"

    def get(self, request, *args, **kwargs):
        self.object = get_object_or_404(
            ElectoralProcess, pk=kwargs['process_pk'])
        return super(AuthAmaAjaxView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AuthAmaAjaxView, self).get_context_data(**kwargs)
        context.update({
            'succ_url': reverse("management:electoralprocess-generate",
                                args=[self.object.slug]),
            'error_url': reverse("management:electoralprocess-summary",
                                 args=[self.object.slug]),
            'debug': settings.DEBUG  # to avoid authenticate on debug mode
        })
        return context


@method_decorator(login_required, name="dispatch")
class HistoryAjaxView(TemplateView):
    """ """
    template_name = 'management/includes/history_list.html'

    def get_context_data(self, **kwargs):
        process_pk = self.kwargs.get('process_pk')
        page_nr = self.request.GET.get('page', 1)
        context = {}

        qs = AuditableEvent.objects.get_history_of_model_obj(
            ElectoralProcess, process_pk)

        page = Paginator(qs, 10).page(page_nr)
        more = page.has_next()

        context.update({
            'next_page': int(page_nr) + 1,
            'process_pk': process_pk,
            'entries': page.object_list,
            'page': page_nr,
            'more': more
        })
        return context


@method_decorator(login_required, name="dispatch")
class HistoryNewAjaxView(View):
    """ """

    def get(self, request, *args, **kwargs):
        process_pk = self.kwargs.get('process_pk')
        last_login = self.request.user.last_login

        data = {"new": AuditableEvent.objects.get_history_new_count(
            ElectoralProcess, process_pk, last_login)}
        return JsonResponse(data, status=200)
