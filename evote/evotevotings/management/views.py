# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging
import string
import time

from django.contrib.auth.decorators import login_required, user_passes_test
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView
from django.views.generic.base import View
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages
from django.http import HttpResponse
from django.utils import timezone
from django.conf import settings

from evotevotings.elections.countings import VoteCounting, CountingMethods
from evotevotings.elections.documents import ElectionCountingsPdf
from evotevotings.processes.documents import ElectoralProcessPdf
from evotevotings.elections.importers import CandidateImporter
from evotevotings.processes.models import ElectoralProcess, ElectoralMember
from evotevotings.elections.models import Election
from evotevotings.services.routers import CSrouter
from evotevotings.elections.forms import ElectoralCalendarForm
from evotevotings.elections.tasks import (open_election_voting,
                                          close_election_voting,
                                          close_election_counting,
                                          generate_election_pdfs)
from evotevotings.voters.models import ElectoralRoll
from evotevotings.voters.views import ElectionVotingStep1View
from evotevotings.voters.tasks import import_electoral_roll
from evotevotings.admin.models import SecretEntry
from evoteajaxforms.views import (FormJsonView, FormsetJsonView,
                                  AjaxResponseAction, FormJsonSimpleView)
from evotemessages.views import (MessageReceivedListMixin,
                                 MessageSentListMixin, MessageDetailMixin,
                                 MessageArchivedListMixin, MessageCreateMixin,
                                 MessageResponseMixin, MessageArchiveMixin)
from contrib.ama.utils import get_attributes
from evoteusers.views import (EvoteLoginView, PersonalDataBaseView,
                              ValidateEmailBaseView,
                              ResendPendingTokenMailBaseView)
from evotecore.mixins import (PermissionListMixin, PermissionRequiredMixin,
                              SelectedMenuMixin, FeedbackMessageMixin)

from evotecore.views import ErrorBaseView
from django_tables2 import SingleTableMixin
from django_fsm import can_proceed

from .middleware import SESSION_USER_PROFILE
from .mailings import MembersKeysNotificationMail
from .mixins import (SecureActionMixin, HistoryMixin)
from .tables import (ElectoralProcessMemberTable, ElectoralProcessDetailTable,
                     ElectoralProcessInstitutionalTable, ElectionTable,
                     ElectoralMemberSimpleTable, ElectoralRollTable,
                     ElectionVoteCountingTable, ElectionVoteCountingExtraTable)
from .forms import (ManagementAuthenticationForm, ElectoralActionFormset,
                    ManagementADFSAuthenticationForm,
                    ElectionDetailConfigsForm, ElectionFormset,
                    CategoryFormset, ElectoralRollDocumentForm,
                    ElectoralProcessAsMemberForm,
                    ElectoralProcessCommunicationForm,
                    ElectoralProcessAsInstitutionalForm,
                    ElectoralDocumentFormset, CandidateFormset,
                    ElectionWriteInDocumentForm, ElectionBulletinForm,
                    ElectoralMembersFormset, CertificateGenerationForm,
                    CertificateGenerationRenderForm,
                    ElectionManageOpenForm, ElectionManageCloseForm,
                    ElectionManageScrutinizeForm)
from .utils import generateCSRs
from .menus import Menu


logger_crypto = logging.getLogger('evote.crypto')


class LoginView(EvoteLoginView):
    """Provides user the ability to login"""
    use_adfs = getattr(settings, "ADFS_WSFED", False)
    adfs_condig = getattr(settings, "ADFS_WSFED_CONFIG", {})
    template_name = "management/login.html"

    def get_form_class(self):
        if self.use_adfs:
            return ManagementADFSAuthenticationForm
        return ManagementAuthenticationForm

    def dispatch(self, request, *args, **kwargs):
        """ """
        if self.use_adfs and request.user.is_authenticated():
            if not request.user.is_institutional_responsible:
                self.request.session[SESSION_USER_PROFILE] = \
                    ManagementAuthenticationForm.Profiles.ELECTORAL_MEMBER
                return redirect(settings.LOGIN_REDIRECT_URL)
            elif not request.user.electoral_members.exists():
                self.request.session[SESSION_USER_PROFILE] = \
                    ManagementAuthenticationForm\
                            .Profiles.INSTITUTIONAL_RESPONSIBLE
                return redirect(settings.LOGIN_REDIRECT_URL)
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        self.request.session[SESSION_USER_PROFILE] = \
            form.cleaned_data.get('profile')
        if self.request.user.is_authenticated():
            return super(EvoteLoginView, self).form_valid(form)
        return super(LoginView, self).form_valid(form)


"""
View electoral process's
"""


def user_management_test(user):
    return hasattr(user, 'is_management_user')


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessListView(
        SelectedMenuMixin, PermissionListMixin, SingleTableMixin, ListView):
    """ """
    model = ElectoralProcess
    queryset = ElectoralProcess.objects.public()
    permission_required = "evoteprocesses.manage_electoralprocess"
    template_name = "management/electoralprocess_list.html"

    table_class = ElectoralProcessMemberTable
    table_pagination = {'per_page': 5}

    def get_required_permissions(self, request=None):
        """ """
        if self.request.user.is_institutional_responsible:
            return ['evoteprocesses.change_electoralprocess']
        return super(ElectoralProcessListView, self)\
            .get_required_permissions(request)

    def get_queryset(self, **kwargs):
        """ """
        if self.request.user.is_institutional_responsible:
            return ElectoralProcess.objects.all()
        return super(ElectoralProcessListView, self).get_queryset(**kwargs)

    def get_table_class(self):
        """ """
        if self.request.user.is_institutional_responsible:
            return ElectoralProcessInstitutionalTable
        return super(ElectoralProcessListView, self).get_table_class()


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessDetailView(
        SelectedMenuMixin, PermissionRequiredMixin, SingleTableMixin,
        HistoryMixin, DetailView):
    """ """
    model = ElectoralProcess
    template_name = "management/electoralprocess_detail.html"
    permission_required = "evoteprocesses.manage_electoralprocess"

    table_class = ElectoralProcessDetailTable
    table_pagination = False

    def get_table_data(self):
        return [self.object]

    def get_electoral_process_pk(self):
        return self.object.pk


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessActionsView(
        SelectedMenuMixin, PermissionRequiredMixin, SecureActionMixin,
        HistoryMixin, FormJsonView):
    """ """
    model = ElectoralProcess
    form_class = ElectoralActionFormset
    permission_required = "evoteprocesses.manage_electoralprocess"
    secure_action = "manage"
    template_name = "management/electoralprocess_actions.html"

    def get_success_url(self):
        """ """
        return reverse_lazy("management:electoralprocess-detail",
                            kwargs={'slug': self.kwargs.get('slug')})

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.electoral_actions.prepopulate()
        return super(ElectoralProcessActionsView, self)\
            .get(request, *args, **kwargs)

    def form_valid(self, form):
        """ """
        if not self.object.actions_configured:
            self.object.actions_configured = True
            self.object.save(update_fields=["actions_configured"])
        return super(ElectoralProcessActionsView, self).form_valid(form)

    def get_electoral_process_pk(self):
        return self.object.pk


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessFormView(
        SelectedMenuMixin, SecureActionMixin, PermissionRequiredMixin,
        HistoryMixin, FormJsonView):
    """ """
    model = ElectoralProcess
    prefix = 'process'
    formset = ElectoralDocumentFormset
    template_name = "management/electoralprocess_form.html"
    secure_action = 4
    formset_prefix = 'electoral-documents'
    accept_global_perms = True

    def get_required_permissions(self, request=None):
        """ """
        if not self.kwargs.get(self.slug_url_kwarg, None):
            return ['evoteprocesses.add_electoralprocess']
        elif self.request.user.is_institutional_responsible:
            return ['evoteprocesses.change_electoralprocess']
        return ['evoteprocesses.manage_electoralprocess']

    def get_form_class(self):
        """ """
        if self.request.user.is_institutional_responsible:
            return ElectoralProcessAsInstitutionalForm
        return ElectoralProcessAsMemberForm

    def form_valid(self, form):
        """ """
        if self.request.user.is_institutional_responsible:
            return self.handle_responsible_form(form)
        return self.handle_member_form(form)

    def handle_member_form(self, form):
        """ """
        documents_formset = self.formset(
            self.request.POST, self.request.FILES, prefix=self.formset_prefix,
            instance=self.object)
        if documents_formset.is_valid():
            form.save()
            documents_formset.save()
            self.set_success_message()
            return self.json_to_response(action=AjaxResponseAction.REFRESH)
        return self.formset_invalid(documents_formset, self.formset_prefix)

    def handle_responsible_form(self, form):
        process = form.save(commit=False)
        if not self.object:
            process.created_by = self.request.user
        process.save()
        self.set_success_message()
        if not self.object or process.is_draft():
            success_url = reverse_lazy("management:electoralprocess-elections",
                                       args=[process.slug])
            return self.json_to_response(success_url=success_url)
        return self.json_to_response(action=AjaxResponseAction.REFRESH)

    def get_context_data(self, **kwargs):
        """ """
        context = super(ElectoralProcessFormView, self)\
            .get_context_data(**kwargs)
        if self.request.user.is_electoral_member:
            context.update({
                'docformset': self.formset(
                    prefix=self.formset_prefix, instance=self.object),
            })
        return context

    def get_object(self):
        if self.kwargs.get(self.slug_url_kwarg, None):
            return super(ElectoralProcessFormView, self).get_object()
        return None

    def is_action_secure(self):
        if self.request.user.is_electoral_member:
            return super(ElectoralProcessFormView, self).is_action_secure()
        return True

    def get_electoral_action(self):
        if self.request.user.is_electoral_member:
            return super(ElectoralProcessFormView, self).get_electoral_action()
        return None

    def get_secure_action_url(self):
        if self.request.user.is_electoral_member:
            return super(ElectoralProcessFormView, self).\
                get_secure_action_url()
        return ''

    def get_to_secure_object(self):
        if self.request.user.is_electoral_member:
            return super(ElectoralProcessFormView, self).get_to_secure_object()
        return None

    def get_electoral_process_pk(self):
        return self.object.pk


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessElectionsView(
        SelectedMenuMixin, PermissionRequiredMixin, FormsetJsonView):
    """ """
    model = ElectoralProcess
    permission_required = 'evoteprocesses.change_electoralprocess'
    template_name = "management/electoralprocess_elections.html"
    form_class = ElectionFormset
    prefix = 'elections'

    def form_valid(self, form):
        if not self.object.is_draft():
            self.action = AjaxResponseAction.REFRESH
        return super(ElectoralProcessElectionsView, self).form_valid(form)

    def get_success_url(self):
        """ """
        return reverse_lazy(
            "management:electoralprocess-members-edit", kwargs={
                'slug': self.kwargs.get(self.slug_url_kwarg)
            })


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessMembersView(
        SelectedMenuMixin, PermissionRequiredMixin, SingleTableMixin,
        HistoryMixin, DetailView):
    """ """
    model = ElectoralProcess
    permission_required = 'evoteprocesses.manage_electoralprocess'
    template_name = "management/electoralprocess_members.html"
    table_class = ElectoralMemberSimpleTable

    def get_required_permissions(self, request=None):
        """ """
        if self.request.user.is_institutional_responsible:
            return ['evoteprocesses.change_electoralprocess']
        return super(ElectoralProcessMembersView, self)\
            .get_required_permissions(request)

    def get_table_data(self):
        return self.object.electoral_members.all()

    def get_electoral_process_pk(self):
        return self.object.pk


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessMembersEditView(SelectedMenuMixin, FormsetJsonView):
    model = ElectoralProcess
    template_name = "management/electoralprocess_members.html"
    form_class = ElectoralMembersFormset
    prefix = 'members'

    def get_success_url(self):
        return reverse_lazy(
            "management:electoralprocess-summary", kwargs={
                'slug': self.kwargs.get('slug')
            })

    def get_form_kwargs(self):
        form_kwargs = super(
            ElectoralProcessMembersEditView, self).get_form_kwargs()
        form_kwargs.update(
            {'form_kwargs': {'initial': {
                'electoral_process_id': self.object.pk}}}
        )
        return form_kwargs

    def form_valid(self, form):
        if not self.get_object().is_draft():
            self.action = AjaxResponseAction.NOTHING
        return super(ElectoralProcessMembersEditView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessSummaryView(
        FeedbackMessageMixin, SelectedMenuMixin, PermissionRequiredMixin,
        SingleTableMixin, DetailView):
    """ """
    model = ElectoralProcess
    permission_required = 'evoteprocesses.change_electoralprocess'
    template_name = "management/electoralprocess_summary.html"

    table_class = ElectoralMemberSimpleTable
    table_pagination = False
    error_message = _("Insert at least one chairman")

    def get_table_data(self):
        return self.object.electoral_members.all()

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.electoral_members.filter(
                role=ElectoralMember.Role.CHAIRMAN).exists():
            self.set_error_message()
            return redirect('management:electoralprocess-members-edit',
                            slug=self.kwargs['slug'])
        return super(ElectoralProcessSummaryView, self)\
            .dispatch(request, *args, **kwargs)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessSummaryPdfView(View):

    def get(self, request, *args, **kwargs):
        electoral_process = get_object_or_404(
            ElectoralProcess, slug=kwargs['slug'])
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="summary.pdf"'
        pdf = ElectoralProcessPdf(electoral_process).render()
        response.write(pdf)
        return response


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessConfirmationView(FeedbackMessageMixin, SelectedMenuMixin,
                                       DetailView):
    """ """
    model = ElectoralProcess
    template_name = "management/electoralprocess_confirmation.html"
    error_message = _("Unable to execute the operation.")

    def get(self, request, *args, **kwargs):
        response = super(ElectoralProcessConfirmationView, self).get(
            request, *args, **kwargs)
        if self.object:
            if not can_proceed(self.object.submit_new):
                self.set_error_message()
                return response
            self.object.submit_new()
            self.object.save()
        return response


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectoralProcessCommunicationView(
        SelectedMenuMixin, SecureActionMixin, HistoryMixin, FormJsonView):
    """ """
    model = ElectoralProcess
    template_name = "management/electoralprocess_communication.html"
    form_class = ElectoralProcessCommunicationForm
    prefix = 'responsible'
    action = AjaxResponseAction.REFRESH
    secure_action = 5
    success_message = _("Responsible of communication updated successfully")

    def get_form_kwargs(self):
        form_kwargs = super(ElectoralProcessCommunicationView, self)\
            .get_form_kwargs()
        chairmans = self.object.electoral_members.filter(
            is_comunication_responsible=True)
        if chairmans.exists():
            form_kwargs.update({
                'initial': {
                    'responsible_communication': chairmans.first()
                }
            })
        return form_kwargs

    def form_valid(self, form):
        member = form.cleaned_data['responsible_communication']
        ElectoralMember.objects.mark_as_responsible(self.object, member)
        return super(ElectoralProcessCommunicationView, self)\
            .form_valid(form)

    def get_electoral_process_pk(self):
        return self.object.pk


"""
Views elections
"""


class ElectionManagePermissionMixin(PermissionRequiredMixin, HistoryMixin):
    """ """
    permission_required = 'evoteprocesses.manage_electoralprocess'

    def get_electoral_process(self):
        """ """
        if not hasattr(self, 'electoral_process'):
            # defined here to be used on context_data and avoid more queries
            self.electoral_process = get_object_or_404(
                ElectoralProcess, slug=self.kwargs.get('slug'))
        return self.electoral_process

    def get_permission_object(self):
        return self.get_electoral_process()

    def get_electoral_process_pk(self):
        return self.get_electoral_process().pk


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionListView(
        SelectedMenuMixin, ElectionManagePermissionMixin, SingleTableMixin,
        ListView):
    """ """
    model = Election
    table_class = ElectionTable
    template_name = "management/election_list.html"

    def get_queryset(self):
        return self.get_electoral_process().elections.all()

    def get_context_data(self, **kwargs):
        context = super(ElectionListView, self).get_context_data(**kwargs)
        context.update({
            "electoral_process": self.get_electoral_process()
        })
        return context


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionManageView(
        SelectedMenuMixin, ElectionManagePermissionMixin,
        SecureActionMixin, FormJsonView):
    """ """
    model = Election
    form_class = ElectionManageOpenForm
    slug_url_kwarg = "slug_election"
    template_name = "management/election_manage.html"
    action = AjaxResponseAction.REFRESH
    secure_action = 1

    def get_to_secure_object(self):
        return self.get_electoral_process()

    def form_valid(self, form):
        if not SecretEntry.objects.check_integrity():
            logger_crypto.critical("Certificate integrity failed")
            self.set_error_message(_("Invalid certificate integrity"))
            return self.json_to_response()
        if not self.object.is_in_definitive_eletoral_roll:
            return self.json_to_response()
        response = super(ElectionManageView, self).form_valid(form)
        open_election_voting.si(self.object.id).apply_async(
            eta=self.object.voting_begin_date)
        return response


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionManageCloseView(
        SelectedMenuMixin, ElectionManagePermissionMixin,
        SecureActionMixin, FormJsonView):
    """ """
    model = Election
    form_class = ElectionManageCloseForm
    slug_url_kwarg = "slug_election"
    secure_action = 2
    template_name = "management/election_manage_close.html"
    action = AjaxResponseAction.REFRESH

    def get_to_secure_object(self):
        return self.get_electoral_process()

    def form_valid(self, form):
        if not (self.object.is_in_definitive_eletoral_roll or
                self.object.is_in_to_vote):
            return self.json_to_response()
        response = super(ElectionManageCloseView, self).form_valid(form)
        close_election_voting.si(self.object.id).apply_async(
            eta=self.object.voting_end_date)
        return response


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionManageScrutinizeView(
        SelectedMenuMixin, ElectionManagePermissionMixin,
        SecureActionMixin, FormJsonView):
    """ """
    model = Election
    form_class = ElectionManageScrutinizeForm
    slug_url_kwarg = "slug_election"
    secure_action = 3
    template_name = "management/election_manage_scrutinize.html"
    action = AjaxResponseAction.REFRESH
    success_message = _("The election count has started")

    def get_to_secure_object(self):
        return self.get_electoral_process()

    def get_context_data(self, **kwargs):
        """ """
        context = super(ElectionManageScrutinizeView, self)\
            .get_context_data(**kwargs)
        conf = getattr(settings, "EVOTE_ELECTIONS")
        # API call to check the status
        status = CSrouter().check_results_status(self.object.pk)
        finished = status == conf.get('counting_finish_state')
        scrutinizing = bool(status) and status is not None and not finished
        context.update({
            'scrutinizing': scrutinizing,
            'finished': finished
        })
        if finished:
            # API call to get results
            results = CSrouter().generate_results(self.object)
            weights = self.object.categories.weight_values()
            candidates = self.object.candidates.representation_values(
                self.object.write_in)
            counting = VoteCounting(
                weights=weights, candidates_name=candidates, data=results)
            if self.object.hondt_method:
                counting.count(seats=self.object.hondt_total_seats,
                               method=CountingMethods.DHONDT)
            else:
                counting.count()
            # prepare results extra info table data
            extra_info = self.object.election_voters.votes_status_count()
            extra_info['invalid'] = results['invalid_votes']
            context.update({
                'total_votes': counting.total,
                'total_votes_weight': counting.total_weight,
                'results_table': ElectionVoteCountingTable(
                    counting.data, show_seats=self.object.hondt_method),
                'results_extra_table': ElectionVoteCountingExtraTable(
                    [extra_info])
            })
            # Generate election PDF
            if not self.object.pdf_voters_document or \
                    not self.object.pdf_document:
                generate_election_pdfs.si(
                    self.object.pk, counting.total, counting.total_weight,
                    counting.data, extra_info).delay()
        return context

    def form_valid(self, form):
        """ """
        # API call to get results
        CSrouter().generate_results(self.object)
        # launch task responsible to change the state of election when
        # counting as finished
        close_election_counting.si(self.object.pk).delay()
        return super(ElectionManageScrutinizeView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailConfigsView(SelectedMenuMixin, SecureActionMixin,
                                ElectionManagePermissionMixin, FormJsonView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    form_class = ElectionDetailConfigsForm
    slug_url_kwarg = "slug_election"
    template_name = "management/election_detail_configs.html"
    secure_action = 6

    def can_change(self):
        return self.object.can_change_configs()

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the
        object"""
        return self.get_object().electoral_process

    def form_valid(self, form):
        self.object.configs_filled = True
        if self.object.write_in or self.object.ordered_voting:
            self.object.presentation_mode = Election.PresentationMode.DROPDOWN
        self.object.save(update_fields=["configs_filled", "presentation_mode"])
        return super(ElectionDetailConfigsView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailCalendarView(SelectedMenuMixin, SecureActionMixin,
                                 ElectionManagePermissionMixin, FormJsonView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    form_class = ElectoralCalendarForm
    slug_url_kwarg = "slug_election"
    template_name = "management/election_detail_calendar.html"
    secure_action = 7

    def can_change(self):
        return self.object.can_change_calendar()

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the
        object"""
        return self.get_object().electoral_process

    def form_valid(self, form):
        self.object.calendar_filled = True
        self.object.save(update_fields=["calendar_filled"])
        return super(ElectionDetailCalendarView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailCategoriesView(
        SelectedMenuMixin, SecureActionMixin,
        ElectionManagePermissionMixin, FormsetJsonView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    prefix = "categories"
    form_class = CategoryFormset
    slug_url_kwarg = "slug_election"
    template_name = "management/election_detail_categories.html"
    secure_action = 7

    def can_change(self):
        return self.object.can_change_categories()

    def get_form_kwargs(self):
        kwargs = super(ElectionDetailCategoriesView, self).get_form_kwargs()
        kwargs.update({
            "queryset": self.object.categories.not_default(),
        })
        return kwargs

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the
        object"""
        return self.get_object().electoral_process


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailRollsView(SelectedMenuMixin, SecureActionMixin,
                              SingleTableMixin, ElectionManagePermissionMixin,
                              FormJsonView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    form_class = ElectoralRollDocumentForm
    slug_url_kwarg = "slug_election"
    template_name = "management/election_detail_rolls.html"
    # must reflect the secure_action of ElectoralRollPublishView,
    # not used on import, only on publish
    secure_action = 7

    table_class = ElectoralRollTable
    table_pagination = {'per_page': 15}

    def can_change(self):
        return self.object.can_change_rolls()

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the
        object"""
        return self.get_object().electoral_process

    def is_action_secure(self):
        """The action validation is not used on this view, only used to
        calculate the action validation url for ElectoralRollPublishView"""
        return True

    def form_valid(self, form):
        """ """
        document = form.save()
        electoral_roll = ElectoralRoll.objects.create(
            election=self.object, document=document)
        import_electoral_roll.delay(electoral_roll.pk)
        return self.json_to_response()

    def get_form_kwargs(self):
        kwargs = super(ElectionDetailRollsView, self).get_form_kwargs()
        kwargs.update({
            "election": self.object,
            "is_importing": ElectoralRoll.objects.is_importing(self.object.pk)
        })
        return kwargs

    def get_table_data(self):
        return self.object.electoral_rolls.all()

    def get_table_kwargs(self):
        kwargs = super(ElectionDetailRollsView, self).get_table_kwargs()
        kwargs.update({
            'secure_action_url': self.get_secure_action_url()
        })
        return kwargs


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailRollsPublishView(
        SecureActionMixin, ElectionManagePermissionMixin, SingleObjectMixin,
        FormJsonSimpleView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    secure_action = 7
    slug_url_kwarg = "slug_election"
    success_message = _("The electoral roll has been published successfully")

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the
        object"""
        return self.get_object().electoral_process

    def post(self, request, *args, **kwargs):
        election = self.get_object()
        electoral_roll = ElectoralRoll.objects.to_publish(election.pk)
        if electoral_roll:
            electoral_roll.publish()
            self.set_success_message()
        return self.json_to_response()


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailCandidatesView(
        SelectedMenuMixin, SecureActionMixin,
        ElectionManagePermissionMixin, FormJsonView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    slug_url_kwarg = "slug_election"
    template_name = "management/election_detail_candidates.html"
    secure_action = 7

    def can_change(self):
        return self.object.can_change_candidates()

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the object
        """
        return self.get_object().electoral_process

    def get_prefix(self):
        """ """
        if not self.object.write_in:
            return "candidates"
        return "document"

    def get_form_class(self):
        """ """
        if not self.object.write_in:
            return CandidateFormset
        return ElectionWriteInDocumentForm

    def get_form_kwargs(self):
        """ """
        kwargs = super(ElectionDetailCandidatesView, self).get_form_kwargs()
        if self.object.write_in:
            kwargs.update({
                "election": self.object,
                "instance": self.object.write_in_import
            })
        else:
            kwargs.update({
                "queryset":
                    self.object.candidates.not_write_in().order_by('created')
            })
        return kwargs

    def form_valid(self, form):
        """ """
        instance = form.save()
        if self.object.write_in:
            if not self.object.write_in_import:
                self.object.write_in_import = instance
                self.object.save()
            importer = CandidateImporter(self.object)
            importer.run(instance.document)
        return self.json_to_response()

    def form_invalid(self, form, prefix=None):
        """ """
        if not self.object.write_in:
            return self.formset_invalid(form, prefix)
        return super(ElectionDetailCandidatesView, self)\
            .form_invalid(form, prefix)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailBulletinView(SelectedMenuMixin, SecureActionMixin,
                                 ElectionManagePermissionMixin, FormJsonView):
    """ """
    model = Election
    action = AjaxResponseAction.REFRESH
    prefix = "bulletin"
    form_class = ElectionBulletinForm
    slug_url_kwarg = "slug_election"
    template_name = "management/election_detail_bulletin.html"
    secure_action = 7

    def can_change(self):
        return self.object.can_change_bulletin()

    def get_to_secure_object(self):
        """TODO: remove get_object to avoid making two queries to get the object
        """
        return self.get_object().electoral_process

    def form_valid(self, form):
        self.object.bulletin_filled = True
        self.object.save(update_fields=["bulletin_filled"])
        return super(ElectionDetailBulletinView, self).form_valid(form)


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionDetailBulletinPreviewView(
        ElectionManagePermissionMixin, ElectionVotingStep1View):
    """ """
    template_name = "management/election_detail_bulletin_preview.html"

    def get_context_data(self, **kwargs):
        context = super(ElectionVotingStep1View, self)\
            .get_context_data(**kwargs)
        form_kwargs = {'instance': self.object}
        context.update({
            'form': self.form_class(**form_kwargs),
            'is_valid': True,
            'combo_id': False,
            'close_url': reverse_lazy(
                "management:election-detail-bulletin", kwargs={
                    'slug': self.kwargs.get('slug'),
                    'slug_election': self.kwargs.get('slug_election')
                })
        })
        return context

    def secure_step(self):
        return False

    def get_election_voter_object(self, election_id):
        return None


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class PersonalDataView(SelectedMenuMixin, PersonalDataBaseView):
    """ """
    template_name = "management/personal_data.html"
    selected_menu = Menu.PERSONALDATA

    def get_form_kwargs(self):
        """ """
        form_kwargs = super(PersonalDataView, self).get_form_kwargs()
        form_kwargs.update(
            {'base_url': self.request.build_absolute_uri(
                reverse("management:validate-email"))})
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(PersonalDataView, self).get_context_data(**kwargs)
        context.update({
            'resend_url': reverse_lazy("management:resend-email")
        })
        return context


class ValidateEmailView(ValidateEmailBaseView):
    """ """
    template_name = "management/validate_email.html"


class ResendPendingTokenMailView(ResendPendingTokenMailBaseView):
    """ Resends email with token """

    def get_redirect_url(self):
        return reverse_lazy("management:personal-data")

    def get_base_validate_url(self):
        return reverse("management:validate-email")


"""
MESSAGES
"""


class MessageManagementMixin(SelectedMenuMixin):
    """ """
    selected_menu = Menu.ELECPROCESS
    contact_namespace = "management"

    def get_group(self):
        slug = self.kwargs.get('process_slug')
        return get_object_or_404(ElectoralProcess, slug=slug)

    def get_member(self):
        return self.group_object.electoral_members.get(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(MessageManagementMixin, self).get_context_data(
            **kwargs)
        context.update({
            'is_comunication_responsible':
                self.get_member().is_comunication_responsible
        })
        return context


class MessageManagementListMixin(MessageManagementMixin, PermissionListMixin):
    """ """
    template_name = "management/message_list.html"
    permission_required = "evotemessages.view_message"


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageReceivedListView(MessageManagementListMixin,
                              MessageReceivedListMixin, ListView):
    """ """


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageSentListView(MessageManagementListMixin, MessageSentListMixin,
                          ListView):
    """ """


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageArchivedListView(MessageManagementListMixin,
                              MessageArchivedListMixin, ListView):
    """ """


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageDetailView(MessageManagementMixin, MessageDetailMixin,
                        PermissionRequiredMixin, DetailView):
    """ """
    template_name = "management/message_detail.html"
    permission_required = "evotemessages.view_message"


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageCreateView(MessageManagementMixin, MessageCreateMixin,
                        FormJsonSimpleView):
    """ """
    template_name = "management/message_form.html"
    success_message = _("The message was sent successfully.")
    anonymous_sender = True

    def get_initial(self):
        initial = super(MessageCreateView, self).get_initial()
        initial.update({
            "name": "{} - {}".format(self.group_object,
                                     self.request.user.name),
            "email": settings.EVOTE_MAILING_NOREPLY_DEFAULT
        })
        return initial

    def get_success_url(self):
        return reverse_lazy('management:messages-received', kwargs={
            'process_slug': self.kwargs.get('process_slug')
        })

    def form_valid(self, form):
        super(MessageCreateView, self).form_valid(form)
        self.set_success_message()
        return self.json_to_response()


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageResponseView(MessageManagementMixin, MessageResponseMixin,
                          PermissionRequiredMixin, FormJsonView):
    """ """
    template_name = "management/message_form.html"
    success_message = _("The message was sent successfully.")
    anonymous_sender = True
    permission_required = "evotemessages.view_message"

    def get_initial(self):
        initial = super(MessageResponseView, self).get_initial()
        initial.update({
            'name': "{} - {}".format(self.group_object,
                                     self.request.user.name),
            'email': settings.EVOTE_MAILING_NOREPLY_DEFAULT
        })
        return initial

    def get_success_url(self):
        return reverse_lazy('management:messages-received', kwargs={
            'process_slug': self.kwargs.get('process_slug')
        })

    def form_valid(self, form):
        super(MessageResponseView, self).form_valid(form)
        self.set_success_message()
        return self.json_to_response()


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class MessageArchiveView(FeedbackMessageMixin, MessageArchiveMixin,
                         PermissionRequiredMixin, UpdateView):
    """ """
    success_message = _("The message was archived successfully.")
    permission_required = "evotemessages.view_message"

    def get_success_url(self):
        return reverse_lazy('management:messages-received', kwargs={
            'process_slug': self.kwargs.get('process_slug')
        })


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class GenerateCertificateView(SelectedMenuMixin, FormJsonSimpleView):
    """ """
    form_class = CertificateGenerationForm
    form_render_class = CertificateGenerationRenderForm
    template_name = "management/generate_certificate.html"

    def get(self, request, *args, **kwargs):
        """
        Validate if AMA Authentication was made
        """
        nic = None
        attributes = get_attributes(request)
        if attributes:
            nic = attributes.get('NIC', None)
        # settings.DEBUG to avoid authenticate on debug mode
        if self.request.user.nic != nic and not settings.DEBUG:
            messages.error(self.request, _("Invalid Authentication"))
            obj = self.get_object()
            if obj.is_draft():
                return redirect("management:electoralprocess-summary",
                                slug=obj.slug)
            return redirect("management:electoralprocess-members-edit",
                            slug=obj.slug)
        return super(GenerateCertificateView, self).get(
            request, *args, **kwargs)

    def get_object(self):
        if not hasattr(self, 'object'):
            self.object = get_object_or_404(
                ElectoralProcess, slug=self.kwargs.get('slug'))
        return self.object

    def get_success_url(self):
        obj = self.get_object()
        if obj.is_draft():
            return reverse_lazy(
                "management:electoralprocess-confirmation", args=[obj.slug])
        return reverse_lazy(
            "management:electoralprocess-members-edit", args=[obj.slug])

    def get_cripto_data(self):
        process = self.get_object()
        if not hasattr(self, 'cripto_data'):
            self.cripto_data = generateCSRs(process)
        return self.cripto_data

    def get_form_kwargs(self):
        self.get_object()
        form_kwargs = super(
            GenerateCertificateView, self).get_form_kwargs()

        elections = []
        for election in self.object.elections.all():
            prop_date = election.proposition_begin_date
            expire = prop_date + timezone.timedelta(days=31 * 9)
            expire_secs = int(time.mktime(expire.timetuple()))
            expire_total = int(time.mktime(timezone.now().timetuple()))

            elections.append((election.pk, expire_secs - expire_total))

        members_pk = []
        for member in self.object.electoral_members.all():
            members_pk.append(member.pk)

        form_kwargs.update({
            'elections': elections,
            'members_pk': members_pk,
        })

        if self.request.method not in ('POST', 'PUT'):
            cripto_data = self.get_cripto_data()
            form_kwargs.update({'initial': {
                'components': cripto_data['components'],
                'elec_pvt_key': cripto_data['elec_pvt_key'],
            }})

        form_kwargs.pop("instance")  # HACK

        return form_kwargs

    def get_context_data(self, **kwargs):
        self.process = self.get_object()
        context = super(GenerateCertificateView, self).get_context_data(
            **kwargs)
        # TODO: Log suscipicious operations

        initial_data = self.get_cripto_data()
        form_kwargs = self.get_form_kwargs()
        elections = form_kwargs.pop('elections')

        context.update({
            'process': self.process,
            'form_render': self.form_render_class(
                process=self.process,
                elections=elections,
                initial=initial_data
            ),
        })
        return context

    def form_valid(self, form):
        obj = self.get_object()
        elec_pvt_key = form.cleaned_data.pop("elec_pvt_key")
        for field in form.cleaned_data:
            if "election_" in field:
                elec_id = string.replace(field, "election_", "")
                election = Election.objects.get(pk=elec_id)
                election.certificate = form.cleaned_data[field]
                election.cert_private_key = elec_pvt_key
                election.save()
            elif "secret_" in field:
                member_id = string.replace(field, "secret_", "")
                elec_member = ElectoralMember.objects.get(pk=member_id)
                elec_member.private_key = form.cleaned_data[field]
                elec_member.save()
            else:
                raise Exception("Error")

        for member in obj.electoral_members.all():
            MembersKeysNotificationMail({
                'key': member.private_key,
                'process_id': obj.identifier,
                'url': self.request.build_absolute_uri(
                    reverse("management:index"))
            }).send(
                [(member.user.get_full_name(), member.user.get_email())])
        self.set_success_message()
        return self.json_to_response()


@method_decorator(login_required, name="dispatch")
@method_decorator(user_passes_test(user_management_test), name="dispatch")
class ElectionCountingsPDFView(View):

    def get(self, request, *args, **kwargs):
        election = get_object_or_404(Election, slug=kwargs['slug_election'])
        pdf = ElectionCountingsPdf(election).render()
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] =\
            'attachment; filename="countings.pdf"'
        response.write(pdf)
        return response


# Error views
class ManagementErrorView(ErrorBaseView):
    template_name = 'management/errors/base.html'
