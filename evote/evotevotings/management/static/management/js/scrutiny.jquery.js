// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, django, configuration */

/******************************************************************
 * Site Name: eVote
 * Author: Sandro Rodrigues
 *
 * JavaScript: Management script
 *****************************************************************/

"use strict";

(function($) {

    $.fn.scrutiny = function (options) {

        var opts = $.extend({
            endpointAttr: 'scrutiny-endpoint',
            successUrlAttr: 'scrutiny-onsuccess-url',
            errorAttr: 'scrutiny-error',
            statusAttr: 'scrutiny-status',
            scrutinyFinishedClass: 'scrutiny-finished',
            scrutinyWorkingClass: 'scrutiny-working',
            scrutinyTextClass: 'scrutiny-text',
            scrutinyFinishedText: django.gettext("Finished"),
            scrutinyWorkingText: django.gettext("Working..."),
            interval: 3000,
        }, options);


        function Scrutiny($item) {
            this.$item = $item;
            this.$status = this.$item.find("[data-" + opts.statusAttr + "]");
            this.$errorStatus = this.$item.find("[data-" + opts.errorAttr + "]");
            this.errorCode = this.$errorStatus.data(opts.errorAttr);
            this.endpoint = this.$item.data(opts.endpointAttr);
            this.currentStatus = null;
            self.intervalID = null;

            this.init();
        }

        Scrutiny.prototype = {

            init: function ()
            {
                this.$errorStatus.hide();

                this.$status.each(function () {
                    $(this).append($("<span></span>").addClass(opts.scrutinyTextClass).hide());
                });

                this.setPolling();
            },

            getServerStatus: function() {
                return $.get(this.endpoint);
            },

            clearStatus: function (from, to)
            {
                from = from || 0;
                to = to || this.$status.length;

                if (from < 0)
                    from = 0;

                this.$status.removeClass(opts.scrutinyFinishedClass + " " + opts.scrutinyWorkingClass);
                this.$status.slice(from, to)
                    .find("." + opts.scrutinyTextClass)
                    .hide('fade', {}, 200);
            },

            updateStatus: function (status)
            {
                var $finished = null,
                    $working = null;

                // clear all status
                this.clearStatus(status);

                if (status === 0) {
                    $finished = null;
                    $working = this.$status.filter("[data-" + opts.statusAttr + "='1']");
                }
                else if (status >= 1) {
                    $finished = this.$status.slice(0, status);
                    $working = this.$status.filter("[data-" + opts.statusAttr + "='" + status + "']").next();
                }

                this.showStatus($finished, opts.scrutinyFinishedClass, opts.scrutinyFinishedText);
                this.showStatus($working, opts.scrutinyWorkingClass, opts.scrutinyWorkingText);
            },

            showStatus: function ($item, klass, text)
            {
                if ($item) {
                    $item.addClass(klass)
                        .find("." + opts.scrutinyTextClass)
                        .html(text)
                        .show('fade', {}, 500);
                }
            },

            showError: function () {
                this.$item.css('minHeight', this.$errorStatus.outerHeight()+'px');
                this.$status.hide(200);
                this.$errorStatus.show('fade', 400);
                clearInterval(this.intervalID);
            },

            setPolling: function ()
            {
                var self = this;

                var update = _.throttle(function() {
                    self.getServerStatus().done(function (data) {

                        if(self.errorCode === data.status){
                            self.showError();
                        }
                        // apply changes if status changed
                        else if (self.currentStatus !== data.status) {
                            self.updateStatus(data.status - 1);
                        }
                        // scrutiny ended condition
                        if (data.status > self.$status.length) {
                            var successUrl = self.$item.data(opts.successUrlAttr);

                            if (successUrl)
                                window.location = successUrl;
                            else
                                window.location.reload();
                        }

                        self.currentStatus = data.status;
                    });
                }, opts.interval);

                update();
                self.intervalID = setInterval(update, opts.interval);
            },
        };

        return this.each(function () {
            var scrutiny = new Scrutiny($(this))
        });
    };

    $(document).ready(function () {

        $('[data-scrutiny]').scrutiny();

    });

})(jQuery);
