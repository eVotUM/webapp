// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, _ */

/******************************************************************
 * Site Name: eVote
 * Author: Sandro Rodrigues
 *
 * JavaScript: Management script
 *****************************************************************/

"use strict";

(function($) {

    $.fn.shuffle = function (options)
    {
        var opts = $.extend({
            onShuffleFinish: null,
        }, options);

        return this.each(function ()
        {
            var itemsName = $(this).data('shuffle'),
                $parent = $("[data-shuffle-container='" + itemsName + "']");

            $(this).on('click', function (e) {
                e.preventDefault();

                var $items = $parent.find("> *");
                var $shuffled = _.shuffle($items.clone(true));

                $items.hide("fade", {}, 200).delay(200, function () {
                    $parent.empty().html($shuffled);

                    if ($.isFunction( opts.onShuffleFinish )) {
                        opts.onShuffleFinish($shuffled);
                    }
                });
            });
        });
    };

    $(document).ready(function () {

        $("[data-secure-action]").secureAction();

        $("[data-shuffle]").shuffle();

        // Copy dates of first form to the new form
        $("[data-formset][data-formset-prefix='elections']").on('formAdded', function(e){
            var $new_form = $(e.target);
            var dates_names = ['proposition_begin_date', 'proposition_end_date',
            'roll_begin_date', 'roll_end_date', 'complaint_begin_date', 'complaint_end_date',
            'voting_begin_date', 'voting_end_date', 'minute_publication_date',
            'minute_begin_date', 'minute_end_date'];

            var first_date = $new_form.find("[name$='proposition_begin_date']");

            if (!first_date.val()) {
                $.each(dates_names, function(index, elem){
                    var value = $(e.currentTarget).find("[name$='0-" + elem + "']").val();
                    $new_form.find("[name$='" + elem + "']").val(value);
                });
            }
        });

        $('#members-form').on('form:submit:success', function(){
            var $authform = $('#authform form');
            $authform.submit();
        });

        // Toogle sidebar
        $('.history-sidebar .toggle-sidebar, .overlay').on('click', function(){
            $('.history-sidebar').toggleClass('open');
            $('.overlay').toggleClass('active');
        });

        if ($('.history-sidebar').length) {
            var $boxnumber = $('.history-sidebar .boxnumber');
            var ajax_url = $boxnumber.data('count-url');
            $.ajax({
                type: "GET",
                url: ajax_url
            }).done(function(data) {
                $boxnumber.text(data['new']);
            });
        }

        // Toggle hondt seats input when hondt method is selected
        var hondt = $("[data-hondt]");

        if (hondt.length) {
            var totalSeats = hondt.find('#field_id_hondt_total_seats,label[for="id_hondt_total_seats"]');
            var $method = hondt.find('input[name="hondt_method"]');

            var checkHondt = function checkHondt(value) {
                if (value === 'True')
                    totalSeats.show('fade', {}, 150);
                else
                    totalSeats.hide('fade', {}, 150);
            };

            $method.on('change', function() {
                checkHondt($(this).val());
            });
            checkHondt($method.filter(':checked').val());
        }
    });

})(jQuery);
