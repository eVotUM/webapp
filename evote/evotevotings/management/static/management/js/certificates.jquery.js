// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, django */

/******************************************************************
 * Site Name: eVote
 * Author: André Rocha
 *
 * JavaScript: Management script
 *****************************************************************/

"use strict";

if( typeof CERTIFICATES === 'undefined') {
        var CERTIFICATES = {};
}

(function() {

    /*
     * Secret sharing and certificate management
     */
    CERTIFICATES = (function() {
        var self = {
            /* Selector to the real form */
            final_form: "[data-final-form]",
            /* Selector to the fake form */
            csr_form: "[data-csr-sign]",
            /* Hidden input with the CA Certificate */
            ca_cert: "id_ca_cert",
            /* Hidden input with the CA Private Key */
            ca_pvt_key: "id_ca_pvt_key",
            /* Hidden secret keys to be submitted */
            keys_fieldset: "[data-field-members]",
            /* List of CSR's to sign */
            csr_fieldset: "[data-field-csrs]",
            /* List of certificate holders */
            cert_fieldset: "[data-field-certs]",
            /* Form submit button */
            submit_button: "[data-csr-button]",

            initialize: function() {
                $(self.submit_button).on('click', function(e) {
                    e.preventDefault();
                    $(this).prop('disabled', true);
                    try {
                        var final_form = $(self.final_form);
                        var csr_form = $(self.csr_form);
                        var pwd = csr_form.find('input[type="password"]').val();
                        var ca_cert = csr_form.find('input#'+self.ca_cert).val();
                        var ca_pvt = csr_form.find('input#'+self.ca_pvt_key).val();
                        var keys_fieldset = csr_form.find(self.keys_fieldset);
                        var csr_fieldset = csr_form.find(self.csr_fieldset);
                        var cert_fieldset = final_form.find(self.cert_fieldset);

                        /* Check for empty password */
                        if(pwd === "")
                            throw "Please insert a password";
        
                        /* Try to validate the CA certificate password */
                        var result = evotum.cautils.pemPrivateKeyToPKey(ca_pvt, pwd);
                        var errorCode = result["errorCode"];
                        if(errorCode === 1)
                            throw "Incorrect password";
        
                        /* Fetch the CSR's from the form */
                        csr_fieldset.find('input').each(function(e) {
                            var csr = $(this).val();
                            var elec_id = $(this).data('election-id');
                            var exp_secs = $(this).data('expire-seconds');
                            var sign_result = evotum.cautils.signCSR(ca_cert, ca_pvt, pwd, csr, elec_id, 0, exp_secs);
                            cert_fieldset.find("[data-election-id='"+elec_id+"']").val(sign_result.pemCertificate)
                        });
                        
                        /* Submit the hidden form */
                        final_form.submit(); 
                    }
                    catch(e) {
                        noty({
                            text: django.gettext(e),
                            type: "error"
                        });
                        $(this).prop('disabled', false);
                    }
                });            
            }
        }

        return self;

    })();

    $(document).ready(function() {
        CERTIFICATES.initialize();
    });

})(jQuery);
