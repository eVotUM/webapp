// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, django */

/******************************************************************
 * Site Name: eVote
 * Author: Tiago Brito
 *
 * JavaScript: Validate email with DTSI
 *****************************************************************/

"use strict";

(function($) {

    $.fn.VerifyEmailWithDTSI = function(options) {

        var opts = $.extend({}, options);

        return this.each(function()
        {
            var $btn = $(this);
            var $form = $btn.closest('tr');

            $btn.on("click", function() {
                var field = $btn.attr('data-verify')
                var ajaxUrl = $btn.attr('data-ajax-url');
                var username = $("#" + field).val();
                var ladda = Ladda.create( $btn.get(0) ).start();

                $.ajax({
                    type: "GET",
                    url: ajaxUrl,
                    data: {
                        'username': username
                    }
                }).done(function(data) {
                    var $fieldDiv = $form.find("#field_" + field);

                    if (data['error']) {
                        $fieldDiv.addClass('error');
                        $fieldDiv.find('.errorlist').remove();
                        $fieldDiv.append("<ul class='errorlist'><li>" + data['error'] + "</li></ul>");
                        ladda.stop();
                    } else {
                        $.each(data, function(elem){
                            $form.find("input[name$='-" + elem + "']").val(data[elem]);
                        });
                        $.each(data['enable'], function(elem){
                            $form.find("[name$='-" + data['enable'][elem] + "']").prop("readonly", false).prop("disabled", false);
                        });
                        $fieldDiv.removeClass('error');
                        $fieldDiv.find('.errorlist').hide();
                        $fieldDiv.one('input.change', function(){
                            $.each(data, function(elem){
                                $form.find("input[name$='-" + elem + "']").val('');
                            });
                            $.each(data['enable'], function(elem){
                                $form.find("input[name$='-" + data['enable'][elem] + "']").prop("readonly", true).val('');
                                $form.find("select[name$='-" + data['enable'][elem] + "']").prop("disabled", true).val('');
                            });
                            $btn.removeAttr("disabled");
                        })
                        ladda.stop();
                        $btn.attr('disabled', 'disabled');
                    }
                });
            });
        });
    };

    $.fn.ReloadJSOnFormSet = function(options) {

        var opts = $.extend({}, options);

        return this.each(function()
        {
            var $form = $(this);

            $form.on('formAdded', function(event) {
                var $newForm = $(event.target);
                $newForm.find("[data-verify]").VerifyEmailWithDTSI();
            });
        });
    };

    $(document).ready(function () {
        $("[data-js-reload]").ReloadJSOnFormSet();
    });

})(jQuery);
