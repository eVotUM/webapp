// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery */

/******************************************************************
 * Site Name: eVote
 * Author: Sandro Rodrigues
 *
 * JavaScript: secure actions script
 *****************************************************************/

"use strict";

(function($) {

    $.fn.secureAction = function(options) {

        var opts = $.extend({
            flagWasValidatedAttr: "wasValidated",
        }, options);

        return this.each(function()
        {
            var $form = $(this);

            if ($form.attr('data-secure-action') !== '') {
                $form.data('needSecuredAction', true);

                $form.on("submit", function() {

                    var wasValidated = !! $form.data(opts.flagWasValidatedAttr);

                    if ( !wasValidated )
                    {
                        var modalOptions = {
                            remodal: {
                                hashTracking: false,
                                closeOnEscape: false,
                                closeOnOutsideClick: false
                            }
                        };

                        $.modalPartial( $form.data("secure-action"), modalOptions )
                            .done(function(modal)
                            {
                                var $innerForm = modal.find('form');

                                $innerForm.djangoAjaxForms({
                                    onSubmitSuccess: function()
                                    {
                                        modal.remodal().close();
                                        $form.data(opts.flagWasValidatedAttr, true);
                                        $form.trigger("submit");
                                    }
                                });
                            });
                    }

                    return wasValidated;
                });
            } else {
                return true;
            }
        });
    };

})(jQuery);
