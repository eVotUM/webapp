# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import math

from django.utils.text import Truncator
from django.core.exceptions import SuspiciousOperation
from django.conf import settings

from eVotUM.Cripto import shamirsecret, cautils

from evotevotings.admin.models import SecretEntry
from evotevotings.admin.utils import (get_secret_entry,
                                      get_private_key_password)


def generateCSRs(process):
    """ Generates all CSR's and shared secret keys
        for a given ElectoralProcess """
    csrs_list = []

    # Set the CA certificate and Private Key into the initial data
    ca_cert = get_secret_entry(
        SecretEntry.Identifier.ROOT_CERT
    )

    ca_pvt_key = get_secret_entry(
        SecretEntry.Identifier.ROOT_PVT_KEY
    )

    # Generate the private key password randomly
    secret_key = shamirsecret.generateSecret(64)

    # Generate the key pair
    (err, key_pair) = cautils.generateRSAKeyPair(2048)

    if err is not None:
        raise SuspiciousOperation

    # Protect the private key with the generated password
    (err, elec_pvt_key) = cautils.pKeyToPEMPrivateKey(
        key_pair,
        secret_key
    )

    if err is not None:
        raise SuspiciousOperation
    # Generate a CSR for each election
    cname = getattr(settings, 'EVOTE_CSR_COUNTRYNAME', '')
    spname = getattr(settings, 'EVOTE_CSR_STATENAME', '')
    lname = getattr(settings, 'EVOTE_CSR_LOCALITYNAME', '')
    oname = getattr(settings, 'EVOTE_CSR_ORGNAME', '')
    ouname = getattr(settings, 'EVOTE_CSR_ORGUNITNAME', '')
    for election in process.elections.all():
        # Election specific values
        str_elec_id = "%d" % election.pk
        common_name = Truncator(process.identifier_pt)\
            .chars(55) + " - " + str_elec_id
        (err, csr) = cautils.generateCSR(
            pemPrivateKey=elec_pvt_key,
            keyPassphrase=secret_key,
            commonName=common_name,
            countryName=cname,
            stateOrProvinceName=spname,
            localityName=lname,
            organizationName=oname,
            organizationalUnitName=ouname
        )
        # Set the csr to be sent into the form
        csrs_list.append(csr)

    # Get the Election management private key
    sge_pvt_key = get_secret_entry(
        SecretEntry.Identifier.MANAGEMENT_PVT_KEY
    )
    pvt_key_passwd = get_private_key_password()

    # Number of members
    n = process.electoral_members.count()
    # Generate the Shared secret components
    (err, components) = shamirsecret.createSharedSecretComponents(
        secret=secret_key,
        nShares=n,
        quorum=int(math.floor(n / 2) + 1),
        uid=str(process.pk),
        pemPrivateKey=sge_pvt_key,
        keyPassphrase=pvt_key_passwd,
    )

    if err is not None:
        raise SuspiciousOperation

    return_data = {
        'csrs': csrs_list,
        'components': components,
        'ca_cert': ca_cert,
        'ca_pvt_key': ca_pvt_key,
        'elec_pvt_key': elec_pvt_key
    }

    return return_data
