# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.contrib import messages
from django.conf import settings


from evoteajaxforms.views import AjaxResponseAction
from evotevotings.processes.models import (
    ElectoralProcess, ElectoralMember, ElectoralAction)

from .registers import secure_action
from .menus import Menu


class SecureActionMixin(object):
    """ """
    secure_action = None
    electoral_action = None

    def get_secure_action(self):
        return self.secure_action

    def get_to_secure_object(self):
        if hasattr(self, 'object') and self.object:
            return self.object
        return self.get_object()

    def is_action_secure(self):
        action = self.get_electoral_action()
        if action and action.strength == ElectoralAction.Strength.SIMPLE:
            return True
        return secure_action.validate(
            self.request, self.get_to_secure_object().pk,
            self.get_secure_action())

    def get_secure_action_url(self):
        action = self.get_electoral_action()
        if action and action.strength == ElectoralAction.Strength.SIMPLE:
            return ""
        return reverse_lazy("management:secureaction-validate", kwargs={
            'process_pk': self.get_to_secure_object().pk,
            'pk': self.get_secure_action()
        })

    def get_context_data(self, **kwargs):
        context = super(SecureActionMixin, self).get_context_data(**kwargs)
        context.update({
            'secure_action_url': self.get_secure_action_url()
        })
        return context

    def is_manage_action(self):
        return self.get_secure_action() == 'manage'

    def get_electoral_action(self):
        if self.is_manage_action():
            return None
        elif not self.electoral_action:
            self.electoral_action = ElectoralAction.objects.filter(
                action_id=self.get_secure_action(),
                electoral_process_id=self.get_to_secure_object().pk).first()
        return self.electoral_action

    def dispatch(self, request, *args, **kwargs):
        """ """
        to_secure_object = self.get_to_secure_object()
        if to_secure_object and not to_secure_object.actions_configured and \
                not self.is_manage_action():
            raise PermissionDenied()
        if request.method == 'POST' and not self.is_action_secure():
            messages.error(
                request, _("You are trying to made an invalidated operation."))
            return self.json_to_response(action=AjaxResponseAction.REFRESH)
        return super(SecureActionMixin, self)\
            .dispatch(request, *args, **kwargs)


class MessageMixin(object):
    """ """
    selected_menu = Menu.ELECPROCESS

    def get_context_data(self, **kwargs):
        slug = self.kwargs.get('slug', None)
        context = super(MessageMixin, self).get_context_data(**kwargs)
        if slug:
            process = get_object_or_404(ElectoralProcess, slug=slug)
            context.update({
                'obj': process,
                'member': get_object_or_404(
                    ElectoralMember, user=self.request.user,
                    electoral_process=process)
            })
        return context


class MessageViewManageMixin(object):

    def get_form_kwargs(self):
        """ """
        kwargs = super(MessageViewManageMixin, self).get_form_kwargs()
        kwargs.update({
            'obj': ElectoralProcess.objects.filter(
                slug=self.kwargs.get('slug', '')).first()
        })
        return kwargs

    def get_initial(self):
        initial = super(MessageViewManageMixin, self).get_initial()
        user = self.request.user
        is_member = getattr(user, 'is_electoral_member', False)
        if is_member and not self.response:
            process = get_object_or_404(
                ElectoralProcess, slug=self.kwargs['slug'])
            initial.update({
                'name': _('Electoral comission to ') + process.identifier +
                " - " + user.name,
                'email': getattr(settings, "EVOTE_MAILING_NOREPLY_DEFAULT", '')
            })
        return initial


class HistoryMixin(object):
    """ Mixin for the history bar """

    def get_electoral_process_pk(self):
        """
        Since we can have different models on each view this defines
        the current pk of electoral process
        """
        raise NotImplementedError

    def get_context_data(self, **kwargs):
        """ Set all context needed for the history bar """
        context = super(HistoryMixin, self).get_context_data(**kwargs)
        if self.request.user.is_electoral_member:

            context.update({
                'process_pk': self.get_electoral_process_pk(),
                'show_history': True,
                'next_page': 1,
                'more': True
            })

        return context
