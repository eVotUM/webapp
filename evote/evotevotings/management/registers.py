# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals


class SecureActionSessionRegister(object):

    SESSION_PREFIX = "secureaction_"

    def prefix_key(self, key):
        return "{}_{}".format(self.SESSION_PREFIX, key)

    def register(self, request, key, action):
        prefixed_key = self.prefix_key(key)
        request.session[prefixed_key] = str(action)

    def reset(self, request, key):
        prefixed_key = self.prefix_key(key)
        del request.session[prefixed_key]

    def validate(self, request, key, action):
        prefixed_key = self.prefix_key(key)
        session_action = request.session.get(prefixed_key, None)
        if session_action and str(action) == str(session_action):
            self.reset(request, key)
            return True
        return False


secure_action = SecureActionSessionRegister()
