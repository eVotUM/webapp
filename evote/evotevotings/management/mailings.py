# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from tempfile import NamedTemporaryFile

from evotemailings.mailings import BaseMailing


class MembersKeysNotificationMail(BaseMailing):
    """ """
    template_name = 'management/mails/members_keys_notification.html'

    subject = "eVotUM - Membro da Comissão Eleitoral " +\
              "| Member of the electoral commission"

    def get_attachments(self):
        key = self.get_context_data()['key']
        f = NamedTemporaryFile()
        f.write(key)
        f.seek(0)
        attachment = open(f.name, 'rb')
        return [('key.jwt', attachment.read(), 'text/plain')]
