# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.http import JsonResponse

from .registers import recipient_register


RECIPIENTS_LIMIT = 10


@method_decorator(login_required, name="dispatch")
class MessageRecipientsAjaxView(View):
    """ """

    def get(self, request, *args, **kwargs):
        """ """
        query = request.GET.get('q', '')
        group_pk = request.GET.get('group_pk', None)
        recipients = []
        # search by query and group on all recipients
        for recipient_class in recipient_register.all():
            recipient = recipient_class()
            if recipient.check_permission(self.request.user):
                qs = recipient.search(query, group_pk, user=self.request.user)
                recipients += recipient.data(qs[:RECIPIENTS_LIMIT])
        return JsonResponse(recipients, safe=False)
