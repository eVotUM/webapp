# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import patch

from django.utils import timezone
from django.test import SimpleTestCase

from evotemessages.models import Message


class MessageTest(SimpleTestCase):

    def test_str(self):
        message = Message(subject='dummy test')
        self.assertEqual(str(message), 'dummy test')

    def test_was_readed(self):
        message = Message()
        self.assertFalse(message.was_readed)
        message.read_at = timezone.now()
        self.assertTrue(message.was_readed)

    def test_is_archived(self):
        message = Message()
        self.assertFalse(message.is_archived)
        message.folder = Message.Folder.ARCHIVED
        self.assertTrue(message.is_archived)

    def test_is_sent(self):
        message = Message()
        self.assertTrue(message.is_sent)
        message.folder = Message.Folder.RECEIVED
        self.assertFalse(message.is_sent)

    def test_is_received(self):
        message = Message()
        self.assertFalse(message.is_received)
        message.folder = Message.Folder.RECEIVED
        self.assertTrue(message.is_received)

    @patch.object(Message, 'save')
    def test_mark_as_read(self, save):
        message = Message()
        message.mark_as_read()
        self.assertIsNotNone(message.read_at)
        save.assert_called_with(update_fields=['read_at'])

    @patch('evotemessages.models.send_to_recipients')
    def test_send_to_recipients(self, send_to_recipients):
        message = Message()
        message.send_to_recipients([1, 2])
        send_to_recipients.si.assert_called_with(None, [1, 2], True)

    @patch('evotemessages.models.send_to_recipients')
    def test_send_to_recipients_without_notify(self, send_to_recipients):
        message = Message()
        message.send_to_recipients([1, 2], False)
        send_to_recipients.si.assert_called_with(None, [1, 2], False)

    @patch('evotemessages.models.send_to_contacts')
    def test_send_to_contacts(self, send_to_contacts):
        message = Message()
        message.send_to_contacts([1, 2])
        send_to_contacts.si.assert_called_with(None, [1, 2], True)
