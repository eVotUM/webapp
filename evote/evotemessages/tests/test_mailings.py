# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import PropertyMock, patch

from django.test import SimpleTestCase

from evotemessages.mailings import MessageMail
from evotemessages.models import Message, MessageContact


class MessageMailTest(SimpleTestCase):
    """ """

    def test_get_subject(self):
        message = Message(subject="dummy title")
        mail = MessageMail(message)
        self.assertEqual(mail.get_subject(), "dummy title")

    def test_get_context_data(self):
        message = Message()
        mail = MessageMail(message)
        self.assertEqual({'message': message}, mail.get_context_data())

    @patch.object(MessageContact, 'is_anonymous', return_value=False,
                  new_callable=PropertyMock)
    def test_get_template_name(self, is_anonymous):
        message = Message(sender=MessageContact())
        mail = MessageMail(message)
        self.assertEqual(mail.get_template_name(),
                         "evotemessages/mails/message.html")

    @patch.object(MessageContact, 'is_anonymous', return_value=True,
                  new_callable=PropertyMock)
    def test_get_template_name_for_anonymous_sender(self, is_anonymous):
        message = Message(sender=MessageContact())
        mail = MessageMail(message)
        self.assertEqual(mail.get_template_name(),
                         "evotemessages/mails/message_anonymous.html")
