# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

# from django.test import TestCase

# from evotemessages.models import Message
# from evotemessages.managers import MessageManager
# from evoteusers.tests.utils import UserTestUtils


# class MessageManagerTest(UserTestUtils, TestCase):

#     def setUp(self):
#         self.user = self.create_user()
#         self.message = Message(
#             subject='Unit test message',
#             body='Unit message',
#         )

#     def test_send(self):
#         self.message.sender = self.user
#         sender_two = self.create_user()
#         self.message.save()
#         self.message.recipients = [self.message.sender, sender_two]
#         result = Message.objects.send(self.message)
#         self.assertEqual(result, 2)

#     def test_send_as_anonymous(self):
#         self.message.anonymous_email = 'dev@eurotux.com'
#         sender_two = self.create_user()
#         self.message.save()
#         self.message.recipients = [sender_two]
#         result = Message.objects.send(self.message)
#         self.assertEqual(result, 1)

#     def test_response_to_anonymous(self):
#         self.message.anonymous_email = 'dev@eurotux.com'
#         self.message.save()
#         result = Message.objects.send(self.message)
#         self.assertEqual(result, 1)

#     def test_inbox_no_obj(self):
#         self.test_send()
#         result = Message.objects.inbox(self.user)
#         expected = Message.objects.filter(
#             recipients=self.user, folder=Message.Folder.RECEIVED)
#         self.assertQuerysetEqual(result, map(repr, expected))

#     def test_sentbox_no_obj(self):
#         self.test_send()
#         result = Message.objects.sentbox(self.user)
#         expected = Message.objects.filter(
#             sender=self.user, folder=Message.Folder.SENT)
#         self.assertQuerysetEqual(result, map(repr, expected))

#     def test_archivedbox_no_obj(self):
#         self.message.folder = Message.Folder.ARCHIVED
#         result = Message.objects.archivedbox(self.user)
#         expected = Message.objects.filter(
#             recipients=self.user, folder=Message.Folder.ARCHIVED)
#         self.assertQuerysetEqual(result, map(repr, expected))

#     def test_notify_anonymous_sender(self):
#         self.message.anonymous_email = 'dev@eurotux.com'
#         result = MessageManager().notify_anonymous_sender(self.message)
#         self.assertEqual(result, 1)

#     def test_notify_anonymous_sender_nok(self):
#         self.message.anonymous_email = None
#         result = MessageManager().notify_anonymous_sender(self.message)
#         self.assertEqual(result, 0)
