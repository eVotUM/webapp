# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

# from django.test.client import RequestFactory
# from django.test import TestCase
# from django.template import Context, Template

# from evoteusers.models import User
# from evotemessages.models import Message


# class TemplateTagsTest(TestCase):

#     def render_template(self, string, context=None, request=None):
#         context = context or {}
#         context = Context(context)
#         return Template(string).render(context)


# class MessagesTagsTest(TemplateTagsTest):

#     def setUp(self):
#         self.rf = RequestFactory()
#         self.rf.user = User.objects.get(pk=1)
#         self.msg = Message.objects.create(
#             subject='Unit test',
#             body='Unit test',
#             anonymous_email='dev@eurotux.com',
#             folder=Message.Folder.RECEIVED
#         )
#         self.msg.recipients = [self.rf.user]
#         self.msg_sent = Message.objects.create(
#             subject='Unit test',
#             body='Unit test',
#             sender=self.rf.user,
#             anonymous_email='dev@eurotux.com',
#             folder=Message.Folder.SENT
#         )
#         self.msg_sent.recipients = [self.rf.user]

#     def test_inbox_tag(self):
#         template = "{% load messages_tags %}{% inbox_count %}"
#         rendered = self.render_template(template, {'request': self.rf})
#         self.assertEqual(rendered, "1")

#     def test_inbox_tag_with_obj(self):
#         template = "{% load messages_tags %}{% inbox_count %}"
#         rendered = self.render_template(
#             template, {'request': self.rf, 'obj': self.msg_sent})
#         self.assertEqual(rendered, "0")

#     def test_sentbox_tag(self):
#         template = "{% load messages_tags %}{% sentbox_count %}"
#         rendered = self.render_template(template, {'request': self.rf})
#         self.assertEqual(rendered, "1")

#     def test_sentbox_tag_with_obj(self):
#         template = "{% load messages_tags %}{% sentbox_count %}"
#         rendered = self.render_template(
#             template, {'request': self.rf, 'obj': self.msg_sent})
#         self.assertEqual(rendered, "0")

#     def test_archived_tag(self):
#         template = "{% load messages_tags %}{% archived_count %}"
#         rendered = self.render_template(template, {'request': self.rf})
#         self.assertEqual(rendered, "0")

#     def test_archived_tag_with_obj(self):
#         template = "{% load messages_tags %}{% archived_count %}"
#         rendered = self.render_template(
#             template, {'request': self.rf, 'obj': self.msg_sent})
#         self.assertEqual(rendered, "0")

#     def test_nested_url(self):
#         template = "{% load messages_tags %} \
#             {% nested_url 'messages-received' %}"
#         rendered = self.render_template(template)
#         self.assertEqual(rendered, "/en/voters/messages/received/")

#     def test_inbox_unread_count(self):
#         template = "{% load messages_tags %}{% inbox_unread_count %}"
#         rendered = self.render_template(
#             template, {'request': self.rf, 'object': self.msg_sent})
#         expected = "<span>(0)</span>"
#         self.assertEqual(rendered, expected)

#     def test_inbox_unread_count_all(self):
#         template = "{% load messages_tags %}{% inbox_unread_count" +\
#                    " filter_by_object=False %}"
#         rendered = self.render_template(
#             template, {'request': self.rf})
#         expected = "<span>(1)</span>"
#         self.assertEqual(rendered, expected)

#     def test_inbox_unread_count_no_obj(self):
#         template = "{% load messages_tags %}{% inbox_unread_count %}"
#         rendered = self.render_template(
#             template, {'request': self.rf})
#         expected = "<span>(1)</span>"
#         self.assertEqual(rendered, expected)
