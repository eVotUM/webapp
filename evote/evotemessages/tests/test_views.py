# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from mock import Mock, patch

from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.test import SimpleTestCase, RequestFactory

from evotemessages.models import Message
from evotemessages.views import (ContactNamespaceMixin, MessageGroupMixin,
                                 MessageListMixin, MessageReceivedListMixin,
                                 MessageSentListMixin,
                                 MessageArchivedListMixin, MessageDetailMixin)


class ContactNamespaceMixinTest(SimpleTestCase):

    class DummyView(ContactNamespaceMixin, TemplateView):
        """ """
        contact_namespace = "namespace"
        template_name = 'any_template.html'

    def setUp(self):
        self.view = self.DummyView()

    def test_get_context_data(self):
        data = self.view.get_context_data()
        self.assertIn('contact_namespace', data)
        self.assertEqual(data['contact_namespace'], "namespace")

    def test_get_contact_namespace(self):
        namespace = self.view.get_contact_namespace()
        self.assertEqual(namespace, "namespace")

    def test_get_contact_namespace_undefined(self):
        self.view.contact_namespace = None
        msg = "contact namespace not defined"
        with self.assertRaisesRegexp(ValueError, msg):
            self.view.get_contact_namespace()


class MessageGroupMixinTest(SimpleTestCase):

    class DummyView(MessageGroupMixin, TemplateView):
        """ """
        template_name = 'any_template.html'

    def setUp(self):
        self.request = RequestFactory().get("/dummy/")
        self.view = self.DummyView()

    def test_get_group(self):
        group = self.view.get_group()
        self.assertIsNone(group)

    def test_dispatch(self):
        self.view.request = self.request
        self.view.dispatch(self.request)
        self.assertTrue(hasattr(self.view, 'group_object'))

    def test_get_context_data(self):
        self.view.group_object = Mock()
        data = self.view.get_context_data()
        self.assertIn('group_object', data)
        self.assertEqual(self.view.group_object, data['group_object'])

    def test_get_context_data_without_group_object(self):
        self.view.group_object = None
        data = self.view.get_context_data()
        self.assertNotIn('group_object', data)


class MessageListMixinTest(SimpleTestCase):

    class DummyView(MessageListMixin, TemplateView):
        """ """
        title = "Dummy title"
        template_name = 'any_template.html'
        contact_namespace = "namespace"

    def setUp(self):
        self.view = self.DummyView()

    def test_view(self):
        self.assertEqual(self.view.model, Message)
        self.assertEqual(self.view.paginate_by, 10)

    def test_get_context_data(self):
        data = self.view.get_context_data()
        self.assertIn("title", data)
        self.assertIn("is_inbox", data)
        self.assertIn("is_sentbox", data)
        self.assertIn("is_archivedbox", data)
        self.assertEqual(data["title"], "Dummy title")
        self.assertTrue(data["is_inbox"])
        self.assertFalse(data["is_sentbox"])
        self.assertFalse(data["is_archivedbox"])


class MessageReceivedListMixinTest(SimpleTestCase):

    class DummyView(MessageReceivedListMixin, TemplateView):
        """ """
        template_name = 'any_template.html'
        contact_namespace = "namespace"

    def setUp(self):
        self.view = self.DummyView()
        self.view.request = Mock(user="user test")
        self.view.group_object = None

    def test_view(self):
        self.assertEqual(self.view.folder, Message.Folder.RECEIVED)
        self.assertEqual(self.view.title, "Received")

    @patch.object(Message.objects, 'inbox', return_value=[])
    def test_get_queryset(self, inbox):
        self.view.get_queryset()
        inbox.assert_called_with("user test", "namespace", None)


class MessageSentListMixinTest(SimpleTestCase):

    class DummyView(MessageSentListMixin, TemplateView):
        """ """
        template_name = 'any_template.html'
        contact_namespace = "namespace"

    def setUp(self):
        self.view = self.DummyView()
        self.view.request = Mock(user="user test")
        self.view.group_object = None

    def test_view(self):
        self.assertEqual(self.view.folder, Message.Folder.SENT)
        self.assertEqual(self.view.title, "Sent")

    @patch.object(Message.objects, 'sentbox', return_value=[])
    def test_get_queryset(self, sentbox):
        self.view.get_queryset()
        sentbox.assert_called_with("user test", "namespace", None)


class MessageArchivedListMixinTest(SimpleTestCase):

    class DummyView(MessageArchivedListMixin, TemplateView):
        """ """
        template_name = 'any_template.html'
        contact_namespace = "namespace"

    def setUp(self):
        self.view = self.DummyView()
        self.view.request = Mock(user="user test")
        self.view.group_object = None

    def test_view(self):
        self.assertEqual(self.view.folder, Message.Folder.ARCHIVED)
        self.assertEqual(self.view.title, "Archived")

    @patch.object(Message.objects, 'archivedbox', return_value=[])
    def test_get_queryset(self, archivedbox):
        self.view.get_queryset()
        archivedbox.assert_called_with("user test", "namespace", None)


class MessageDetailMixinTest(SimpleTestCase):

    class DummyView(MessageDetailMixin, DetailView):
        """ """
        template_name = 'any_template.html'
        contact_namespace = 'voters'

    def setUp(self):
        self.request = RequestFactory().get("/dummy/")
        self.view = self.DummyView()
        self.view.group_object = None

    def test_view(self):
        self.assertEqual(self.view.model, Message)

    @patch.object(DummyView, 'get_object')
    def test_get(self, get_object):
        self.view.request = self.request
        self.view.get(self.request)
        get_object().mark_as_read.assert_called()
