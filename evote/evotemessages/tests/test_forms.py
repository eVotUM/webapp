# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

# from django.test import TestCase
# from django.conf import settings

# from evotemessages.forms import (NewMessageBaseForm, MessageAnonymousForm,
#                                  MessageAuthenticatedForm)
# from evotemessages.registers import recipient_register
# from evoteusers.tests.utils import UserTestUtils
# from evotevotings.processes.tests.utils import ProcessesTestUtils


# class NewMessageBaseFormTest(UserTestUtils, TestCase):

#     def setUp(self):
#         self.sender = self.create_user()
#         self.recipient = self.create_user()
#         self.data = {
#             'name': self.sender.name,
#             'recipients': [self.recipient],
#             'subject': 'Unit test subject',
#             'body': 'Unit test message'
#         }

#     def test_form_valid(self):
#         form = NewMessageBaseForm(data=self.data)
#         self.assertEqual(form.is_valid(), True)

#     def test_form_invalid(self):
#         invalid_data = {
#             'name': 'Unit test',
#             'recipients': [self.recipient]
#         }
#         form = NewMessageBaseForm(data=invalid_data)
#         self.assertEqual(form.is_valid(), False)

#     def test_form_subject_over_length(self):
#         self.data['subject'] = 's' * 270
#         form = NewMessageBaseForm(data=self.data)
#         self.assertEqual(form.is_valid(), False)


# class MessageAnonymousFormTest(UserTestUtils, TestCase):

#     def setUp(self):
#         self.recipient = self.create_user()
#         self.data = {
#             'name': 'Tiago Brito',
#             'anonymous_email': 'tfb@eurotux.com',
#             'recipients': [self.recipient],
#             'subject': 'Unit test subject',
#             'body': 'Unit test message',
#             'g-recaptcha-response': 'PASSED',
#             'recaptcha_response_field': 'PASSED'
#         }

#     def test_form_valid(self):
#         form = MessageAnonymousForm(data=self.data)
#         self.assertEqual(form.is_valid(), True)

#     def test_form_invalid(self):
#         invalid_data = {
#             'name': 'Tiago Brito',
#             'anonymous_email': 'tfb@eurotux.com',
#             'recipients': [self.recipient],
#             'subject': 'Unit test subject',
#             'g-recaptcha-response': 'PASSED',
#             'recaptcha_response_field': 'PASSED'
#         }
#         form = MessageAnonymousForm(data=invalid_data)
#         self.assertEqual(form.is_valid(), False)


# class MessageAuthenticatedFormTest(
#         ProcessesTestUtils, UserTestUtils, TestCase):

#     def setUp(self):
#         self.sender = self.create_user()
#         setattr(self.sender, 'is_election_voter', True)
#         self.process = self.create_electoral_process()
#         self.member = self.create_electoral_member(
#             electoral_process=self.process)
#         self.recipient = recipient_register.get_all_recipients(
#             self.sender, q=self.member.user.name,
#             slug=[self.process.slug])[0].get('id')
#         self.data = {
#             'name': self.sender.name,
#             'email': self.sender.email,
#             'recipients': [self.recipient],
#             'subject': 'Unit test subject',
#             'body': 'Unit test message'
#         }

#     def test_form_valid(self):
#         form = MessageAuthenticatedForm(
#             data=self.data, user=self.sender, obj=None)
#         self.assertEqual(form.is_valid(), True)

#     def test_form_valid_with_initial(self):
#         form = MessageAuthenticatedForm(
#             data=self.data, user=self.sender, obj=None,
#             initial={'recipients': [self.recipient]})
#         self.assertEqual(form.is_valid(), True)

#     def test_form_valid_with_no_reply(self):
#         self.data = {
#             'name': 'Unit test',
#             'email': getattr(settings, "EVOTE_MAILING_NOREPLY_DEFAULT", ''),
#             'recipients': [self.recipient],
#             'subject': 'Unit test subject',
#             'body': 'Unit test message'
#         }
#         form = MessageAuthenticatedForm(
#             data=self.data, user=self.sender, obj=None,
#             initial={'recipients': [self.recipient]})
#         self.assertEqual(form.is_valid(), True)

#     def test_form_invalid(self):
#         invalid_data = {
#             'name': self.sender.name,
#             'email': self.sender.email,
#             'recipients': [self.recipient],
#             'subject': 'Unit test subject',
#         }
#         form = MessageAuthenticatedForm(
#             data=invalid_data, user=self.sender, obj=None)
#         self.assertEqual(form.is_valid(), False)

#     def test_invalid_recipient(self):
#         invalid_data = {
#             'name': self.sender.name,
#             'email': self.sender.email,
#             'recipients': ['dev@eurotux.com'],
#             'subject': 'Unit test subject',
#         }
#         form = MessageAuthenticatedForm(
#             data=invalid_data, user=self.sender, obj=None)
#         self.assertEqual(form.is_valid(), False)
