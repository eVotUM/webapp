# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django import template

from evotemessages.registers import recipient_register
from evotemessages.models import Message


register = template.Library()


@register.simple_tag(takes_context=True)
def inbox_unread_count(context, namespace=None, group_object=None):
    """ """
    return unread_count(context, Message.objects.inbox, namespace,
                        group_object)


@register.simple_tag(takes_context=True)
def sentbox_unread_count(context, namespace=None, group_object=None):
    """ """
    return unread_count(context, Message.objects.sentbox, namespace,
                        group_object)


@register.simple_tag(takes_context=True)
def archivedbox_unread_count(context, namespace=None, group_object=None):
    """ """
    return unread_count(context, Message.objects.archivedbox, namespace,
                        group_object)


def unread_count(context, box, namespace=None, group_object=None):
    user = context['request'].user
    namespace = namespace or context['contact_namespace']
    return box(user, namespace, group_object).unread().count()


@register.filter()
def recipient_groups(message, limit=2):
    labels = []
    if message.is_sent:
        recipient_uids = Message.objects\
            .filter(parent_id=message.pk, recipient_uid__isnull=False)\
            .values_list('recipient_uid', flat=True).distinct().order_by()
        for recipient_uid in recipient_uids:
            recipient_class, pk = recipient_register.get(recipient_uid)
            recipient = recipient_class()
            obj = recipient.get_object(pk)
            labels.append(recipient.format_label(obj))
        if not labels:
            messages = Message.objects.filter(parent_id=message.pk)\
                .only('recipient')
            for message in messages[:limit]:
                labels.append(message.recipient.__unicode__())
            total = messages.count() - limit
            if total > 0:
                labels.append("+{} recipients".format(total))
    else:
        labels.append(message.recipient)
    return labels
