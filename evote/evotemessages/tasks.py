# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.apps import apps

from evotecore.utils import assign_bulk_perms
from celery import shared_task

from .registers import recipient_register
from .mailings import MessageMail


logger = logging.getLogger('evote.messages')


@shared_task(ignore_result=True)
def send_to_recipients(to_send_messageid, recipient_uids, notify=True):
    """
    TODO: check if who is sending the message have permission for the selected
    recipients
    """
    logger.info("Message_id=%d: sending to recipients=%s", to_send_messageid,
                recipient_uids)
    Message = apps.get_model("evotemessages.Message")  # NOQA
    MessageContact = apps.get_model("evotemessages.MessageContact")  # NOQA
    total_sent = 0
    for recipient_uid in recipient_uids:
        recipient_class, pk = recipient_register.get(recipient_uid)
        recipient = recipient_class()
        obj = recipient.get_object(pk)
        user_ids = recipient.get_user_ids(obj)
        group_object = recipient.get_group_object(obj)
        contacts = MessageContact.objects.get_or_create_contacts(
            user_ids, recipient.namespace)
        total_sent += Message.objects.send_messages(
            to_send_messageid, contacts, group_object, recipient_uid)
    logger.info("Message_id=%d: total messages sended=%d", to_send_messageid,
                total_sent)
    update_messages_permissions.si(to_send_messageid, notify).delay()
    return total_sent


@shared_task(ignore_result=True)
def send_to_contacts(to_send_messageid, contact_ids, notify=True):
    """
    TODO: check if who is sending the message have permission for the selected
    recipients
    """
    logger.info("Message_id=%d: sending to contacts=%r", to_send_messageid,
                contact_ids)
    Message = apps.get_model("evotemessages.Message")  # NOQA
    MessageContact = apps.get_model("evotemessages.MessageContact")  # NOQA
    message = Message.objects.get(pk=to_send_messageid)
    contacts = MessageContact.objects.filter(pk__in=contact_ids)
    total_sent = Message.objects.send_messages(
        to_send_messageid, contacts, message.content_object)
    logger.info("Message_id=%d: total messages sended=%d", to_send_messageid,
                total_sent)
    update_messages_permissions.si(to_send_messageid, notify).delay()
    return total_sent


@shared_task(ignore_result=True)
def update_messages_permissions(sended_messageid, notify=True):
    Message = apps.get_model("evotemessages.Message")  # NOQA
    messages = Message.objects.select_related('recipient')\
        .filter(parent_id=sended_messageid)
    perms = []
    for message in messages:
        perms.append({
            'object_pk': message.pk,
            'user_id': message.recipient.user_id
        })
    total_perms = len(perms)
    logger.info("Message_id=%d: updating permissions on %d messages",
                sended_messageid, total_perms)
    assign_bulk_perms('evotemessages.view_message', perms)
    if notify:
        for message in messages:
            MessageMail(message).send(message.recipient)
        logger.info("Message_id=%d: %d recipients notified", sended_messageid,
                    total_perms)
    return total_perms
