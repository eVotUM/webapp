# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.db import models

from guardian.shortcuts import get_objects_for_user
from evotecore.utils import list_portions


class MessageQueryset(models.QuerySet):
    # TODO: Remove dependency of guardian

    def _get_messages(self, user, folder, obj, **kwargs):
        if obj:
            kwargs.update({'object_id': obj.pk})
        qs = self.filter(folder=folder, **kwargs)
        return get_objects_for_user(
            user, 'view_message', qs, use_groups=False, with_superuser=False)

    def inbox(self, user, namespace, obj=None):
        return self._get_messages(
            user, self.model.Folder.RECEIVED, obj,
            recipient__namespace=namespace)

    def sentbox(self, user, namespace, obj=None):
        return self._get_messages(
            user, self.model.Folder.SENT, obj, sender__namespace=namespace)

    def archivedbox(self, user, namespace, obj=None):
        return self._get_messages(
            user, self.model.Folder.ARCHIVED, obj,
            recipient__namespace=namespace)

    def unread(self):
        return self.filter(read_at=None)


class MessageBaseManager(models.Manager):

    def send_messages(self, to_send_messageid, contacts, group_object,
                      recipient_uid=None):
        to_send = self.get(pk=to_send_messageid)
        sent_to = []  # to register to who we already have sent messages
        messages_to_send = []
        data = contacts.values_list('pk', 'user_id', 'namespace')
        for contact_id, user_id, namespace in data:
            # avoid send repeated messaged
            uid = "{}{}".format(user_id, namespace)
            # avoid to send message with same sender and recipient contact
            if uid not in sent_to and contact_id != to_send.sender_id:
                messages_to_send.append(self.model(
                    sender_id=to_send.sender_id,
                    recipient_id=contact_id,
                    recipient_uid=recipient_uid,
                    subject=to_send.subject,
                    body=to_send.body,
                    parent=to_send,
                    folder=self.model.Folder.RECEIVED,
                    content_object=group_object
                ))
                sent_to.append(uid)
        for portion in list_portions(messages_to_send, 3000):
            self.bulk_create(portion)
        return len(messages_to_send)


MessageManager = MessageBaseManager.from_queryset(MessageQueryset)


class MessageContactManager(models.Manager):
    use_in_migrations = True

    def get_or_create_contact(self, user, namespace, name=None, email=None,
                              is_anonymous=False):
        """ """
        if not is_anonymous and user:
            contact, _ = self.update_or_create(
                user_id=user.pk, namespace=namespace)
        elif email:
            contact, _ = self.get_or_create(email=email, namespace=namespace,
                                            name=name)
        else:
            raise ValueError("No email provided for anonymous user")
        return contact

    def get_or_create_contacts(self, user_ids, namespace):
        """ """
        user_ids = list(set(user_ids))  # to remove duplicated user ids
        # calculate missing contacts for users
        created_user_ids = self.filter(
            user_id__in=user_ids, namespace=namespace)\
            .values_list('user_id', flat=True)
        missing_user_ids = list(set(user_ids) - set(created_user_ids))
        # add to bulk create new contacts
        to_create = []
        for missing_user_id in missing_user_ids:
            to_create.append(self.model(
                user_id=missing_user_id,
                namespace=namespace
            ))
        # create
        for portion in list_portions(to_create, 3000):
            self.bulk_create(portion)
        # return all contacts created
        return self.filter(user_id__in=user_ids, namespace=namespace)
