# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy
from django.utils.text import Truncator
from django.shortcuts import redirect
from django.utils import timezone

from guardian.shortcuts import assign_perm

from .mailings import MessageMail
from .models import Message, MessageContact
from .forms import MessageForm, MessageResponseForm, MessageArchiveForm


class ContactNamespaceMixin(object):
    """ """
    contact_namespace = None

    def get_context_data(self, **kwargs):
        context = super(ContactNamespaceMixin, self).get_context_data(**kwargs)
        context.update({
            'contact_namespace': self.get_contact_namespace()
        })
        return context

    def get_contact_namespace(self):
        if not self.contact_namespace:
            raise ValueError("contact namespace not defined")
        return self.contact_namespace


class MessageGroupMixin(object):

    def get_group(self):
        return None

    def dispatch(self, *args, **kwargs):
        self.group_object = self.get_group()
        return super(MessageGroupMixin, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(MessageGroupMixin, self).get_context_data(**kwargs)
        if hasattr(self, 'group_object') and self.group_object:
            context.update({
                'group_object': self.group_object
            })
        return context


class MessageListMixin(ContactNamespaceMixin, MessageGroupMixin):
    """ Defines base settings for lists view """
    model = Message
    folder = Message.Folder.RECEIVED
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(MessageListMixin, self).get_context_data(**kwargs)
        context.update({
            'title': self.title,
            'is_inbox': self.folder == Message.Folder.RECEIVED,
            'is_sentbox': self.folder == Message.Folder.SENT,
            'is_archivedbox': self.folder == Message.Folder.ARCHIVED,
        })
        return context


class MessageReceivedListMixin(MessageListMixin):
    """ """
    title = _("Received")

    def get_queryset(self):
        return Message.objects.inbox(
            self.request.user, self.get_contact_namespace(), self.group_object)


class MessageSentListMixin(MessageListMixin):
    """"""
    folder = Message.Folder.SENT
    title = _("Sent")

    def get_queryset(self):
        return Message.objects.sentbox(
            self.request.user, self.get_contact_namespace(), self.group_object)


class MessageArchivedListMixin(MessageListMixin):
    """ """
    folder = Message.Folder.ARCHIVED
    title = _("Archived")

    def get_queryset(self):
        return Message.objects.archivedbox(
            self.request.user, self.get_contact_namespace(), self.group_object)


class MessageDetailMixin(ContactNamespaceMixin, MessageGroupMixin):
    """ """
    model = Message

    def get(self, *args, **kwargs):
        response = super(MessageDetailMixin, self).get(*args, **kwargs)
        self.object.mark_as_read()
        return response


class MessageFormMixin(object):
    """ Defines form settings """
    model = Message
    form_class = MessageForm

    def get_initial(self):
        initial = super(MessageFormMixin, self).get_initial()
        if self.request.user.is_authenticated():
            initial.update({
                'name': self.request.user.name,
                'email': self.request.user.get_email()
            })
        return initial

    def get_form_kwargs(self):
        """ """
        form_kwargs = super(MessageFormMixin, self).get_form_kwargs()
        form_kwargs.update({
            'user': self.request.user
        })
        return form_kwargs


class MessageCreateMixin(MessageGroupMixin, ContactNamespaceMixin,
                         MessageFormMixin):
    """ """
    has_recipients = True
    anonymous_sender = False

    def get_recipients(self, form):
        return form.cleaned_data.get('recipients', [])

    def get_sender_contact(self, form):
        is_anonymous = not self.request.user.is_authenticated()
        if self.anonymous_sender:
            is_anonymous = True
        return MessageContact.objects.get_or_create_contact(
            user=self.request.user,
            namespace=self.get_contact_namespace(),
            name=form.cleaned_data.get('name', None),
            email=form.cleaned_data.get('email', None),
            is_anonymous=is_anonymous
        )

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.sender = self.get_sender_contact(form)
        self.object.read_at = timezone.now()
        if self.group_object:
            self.object.content_object = self.group_object
        self.object.save()
        self.object.send_to_recipients(self.get_recipients(form))
        if self.request.user.is_authenticated():
            assign_perm("evotemessages.view_message", self.request.user,
                        self.object)
        else:
            MessageMail(self.object).send(self.object.sender)
        return redirect(self.get_success_url())

    def get_form_kwargs(self):
        """ """
        form_kwargs = super(MessageCreateMixin, self).get_form_kwargs()
        if self.has_recipients:
            group_pk = self.group_object.pk if self.group_object else None
            url = reverse_lazy("evotemessages:messages-recipients")
            if group_pk:
                url = "{}?group_pk={}".format(url, group_pk)
            form_kwargs.update({
                'recipients_url': url
            })
        return form_kwargs


class MessageResponseMixin(MessageCreateMixin):
    """ """
    form_class = MessageResponseForm
    has_recipients = False

    def get_form_kwargs(self):
        form_kwargs = super(MessageResponseMixin, self).get_form_kwargs()
        del form_kwargs['instance']
        return form_kwargs

    def form_valid(self, form):
        response_msg = form.save(commit=False)
        response_msg.sender = self.get_sender_contact(form)
        response_msg.read_at = timezone.now()
        response_msg.content_object = self.object.content_object
        # save reference to replied message
        response_msg.parent_id = self.object.pk
        response_msg.save()
        response_msg.send_to_contacts([self.object.sender.pk])
        assign_perm("evotemessages.view_message", self.request.user,
                    response_msg)
        return redirect(self.get_success_url())

    def get_initial(self):
        initial = super(MessageFormMixin, self).get_initial()
        initial.update({
            'name': self.object.recipient.get_name(),
            'email': self.object.recipient.get_email(),
            'recipients': self.object.sender,
            'subject': Truncator('Re: ' + self.object.subject).chars(253)
        })
        return initial


class MessageArchiveMixin(object):
    """ """
    model = Message
    form_class = MessageArchiveForm

    def form_valid(self, form):
        if not self.object.is_archived:
            self.object.folder = Message.Folder.ARCHIVED
            self.object.save(update_fields=['folder'])
        return redirect(self.get_success_url())
