# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotemailings.mailings import BaseMailing


class MessageMail(BaseMailing):
    """ """
    template_name = 'evotemessages/mails/message.html'

    def __init__(self, message, *args, **kwargs):
        super(MessageMail, self).__init__(*args, **kwargs)
        self.message = message

    def get_template_name(self):
        if self.message.sender.is_anonymous:
            return 'evotemessages/mails/message_anonymous.html'
        return self.template_name

    def get_context_data(self):
        return {
            'message': self.message
        }

    def get_subject(self):
        return self.message.subject

    def get_mail_reply_to(self):
        return [self.message.sender.get_email()]
