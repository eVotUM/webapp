# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django.conf import settings
from django.db import models

from evotemailings.mixins import MailingListMixin
from evotelogs.decorators import auditable

from .managers import MessageManager, MessageContactManager
from .tasks import send_to_recipients, send_to_contacts


@python_2_unicode_compatible
class MessageContact(MailingListMixin, TimeStampedModel):
    """ """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    namespace = models.CharField(max_length=64, db_index=True)
    name = models.CharField(_('name'), max_length=128, null=True)
    email = models.EmailField(_('email'), max_length=128, null=True)
    objects = MessageContactManager()

    class Meta:
        verbose_name = _("message contact")
        verbose_name_plural = _("message contacts")

    def __str__(self):
        return "{} ({})".format(self.get_name(), self.get_email())

    def get_name(self):
        if not self.name and self.user_id:
            return self.user.name
        elif not self.name:
            return ""
        return self.name

    def get_email(self):
        if not self.email and self.user_id:
            return self.user.get_email()
        return self.email

    @property
    def is_anonymous(self):
        return bool(self.user_id is None)

    def get_mailing_list(self):
        return [(self.get_name(), self.get_email())]


@auditable
@python_2_unicode_compatible
class Message(TimeStampedModel):

    class Folder:
        RECEIVED = 0
        SENT = 1
        ARCHIVED = 2

        choices = (
            (RECEIVED, _("Received")),
            (SENT, _("Sent")),
            (ARCHIVED, _("Archived")),
        )

    sender = models.ForeignKey(
        MessageContact, verbose_name=_("sender"), related_name='sent')
    recipient = models.ForeignKey(
        MessageContact, verbose_name=_("recipient"), null=True)
    recipient_uid = models.CharField(_("recipient uid"), max_length=64,
                                     null=True)
    subject = models.CharField(_("subject"), max_length=260)
    body = models.TextField(_("body"))
    read_at = models.DateTimeField(_("read at"), null=True, blank=True)
    parent = models.ForeignKey(
        'self', verbose_name=_("parent message"), null=True,
        related_name='siblings')
    folder = models.PositiveSmallIntegerField(
        _("folder"), choices=Folder.choices, default=Folder.SENT)
    content_type = models.ForeignKey(ContentType, null=True)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    objects = MessageManager()

    class Meta:
        ordering = ['-created']
        verbose_name = _("message")
        verbose_name_plural = _("messages")
        permissions = (
            ('view_message', 'View message'),
        )

    def __str__(self):
        return "{}".format(self.subject)

    @property
    def was_readed(self):
        return bool(self.read_at)

    @property
    def is_archived(self):
        return self.folder == self.Folder.ARCHIVED

    @property
    def is_sent(self):
        return self.folder == self.Folder.SENT

    @property
    def is_received(self):
        return self.folder == self.Folder.RECEIVED

    def mark_as_read(self):
        if not self.read_at:
            self.read_at = timezone.now()
            self.save(update_fields=['read_at'])

    def send_to_recipients(self, recipient_uids, notify=True):
        """ """
        send_to_recipients.si(self.pk, recipient_uids, notify).delay()

    def send_to_contacts(self, contact_ids, notify=True):
        """ """
        send_to_contacts.si(self.pk, contact_ids, notify).delay()
