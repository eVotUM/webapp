# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from .utils import gen_uid


class BaseRecipientsGroup(object):
    """ """
    model = None
    prefix = ''
    namespace = None

    def __init__(self):
        self.name = self.__class__.__name__

    def get_queryset(self):
        return self.model._default_manager.all()

    def search(self, query, group_object_id):
        return self.get_queryset()

    def get_object(self, pk):
        return self.get_queryset().filter(pk=pk).first()

    def check_permission(self, user):
        return True

    def get_user_ids(self, obj):
        raise NotImplementedError

    def get_group_object(self, obj):
        raise NotImplementedError

    def data(self, qs):
        seen = set()  # to avoid repetitions
        items = []
        for item in qs:
            uid = self.get_unique_id(item)
            # check if object already processed
            if uid not in seen and not seen.add(uid):
                items.append({
                    'id': gen_uid(self.__class__, item.pk),
                    'text': self.format_label(item)
                })
        return items

    def get_unique_id(self, obj):
        return obj.pk

    def format_label(self, obj):
        return "{} {}".format(self.prefix, self.get_label(obj)).strip()

    def get_label(self, obj):
        return obj.__unicode__()
