# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django import forms

from evotecore import widgets

from .models import Message


class MessageBaseForm(forms.ModelForm):
    """ """
    name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = Message
        fields = ('name', 'email', 'subject', 'body')

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(MessageBaseForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = _('Name')
        self.fields['email'].widget.attrs['placeholder'] = _('Email')
        self.fields['subject'].widget.attrs['placeholder'] = _('Subject')
        self.fields['subject'].max_length = 260
        self.fields['body'].widget.attrs['placeholder'] = _('Message')
        if self.initial.get('name', None):
            self.fields['name'].disabled = True
        if self.initial.get('email', None):
            self.fields['email'].disabled = True


class MessageForm(MessageBaseForm):
    """ """
    recipients = forms.MultipleChoiceField(widget=widgets.MultipleAjaxSelect())

    class Meta(MessageBaseForm.Meta):
        fields = MessageBaseForm.Meta.fields + ('recipients',)

    def __init__(self, recipients_url, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        self.fields['recipients'].widget.attrs.update({
            'data-select-ajax-url': recipients_url,
            'data-placeholder': _('Recipients')
        })
        # when form has data
        if 'data' in kwargs:
            # populate with filled data to bypass validation
            self.fields['recipients'].choices = \
                [(recipient, recipient) for recipient in
                 kwargs.get('data').getlist('recipients')]


class MessageResponseForm(MessageBaseForm):
    """ """
    recipients = forms.CharField(disabled=True)

    class Meta(MessageBaseForm.Meta):
        fields = MessageBaseForm.Meta.fields + ('recipients',)

    def __init__(self, *args, **kwargs):
        super(MessageResponseForm, self).__init__(*args, **kwargs)
        self.fields['subject'].disabled = True
        self.fields['recipients'].disabled = True


class MessageArchiveForm(forms.ModelForm):
    """ """
    class Meta:
        model = Message
        fields = ['id']
