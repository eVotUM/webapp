# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.utils.module_loading import autodiscover_modules

from .utils import extract_uid


logger = logging.getLogger('evote.messages')


class RecipientRegister(object):
    """ """
    _registry = {}

    def add(self, recipient):
        self._registry[recipient.__name__] = recipient
        logger.debug("Recipient group registered: %s", recipient.__name__)

    def all(self):
        return self._registry.values()

    def get(self, recipient_uid):
        (class_name, pk) = extract_uid(recipient_uid)
        return self._registry[class_name], pk


recipient_register = RecipientRegister()


def autodiscover():
    """autodiscover and import conditions.py files to feed register
    """
    autodiscover_modules('recipients')
