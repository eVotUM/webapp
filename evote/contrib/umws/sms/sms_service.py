# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from contrib.umws.umws_service import UMWebService


logger = logging.getLogger('evote.contrib.umws')


class SMSService(UMWebService):
    """ """
    settings_name = "SMS_SERVICE_CONFIG"

    def mask_number(self, number):
        """ Mask phone number to be used in logs.
            examples: '919999999' returns '91****999'
                      '+351919999999' returns '+35191****999'
                      '1234' returns '1234'
        """
        if not isinstance(number, basestring):
            number = str(number)
        if len(number) > 8:
            prefix = number[:len(number) - 7]
            sufix = number[len(number) - 3:]
            return "%s****%s" % (prefix, sufix)
        return number

    def send_message(self, number, message, anonymous=False):
        """ Returns message id (int) """
        if self.in_debug():
            return "Success Message"
        client = self.get_client()
        try:
            msg_id = client.service.SendMessage(number, message, anonymous)
        except:
            logger.critical(
                "Send SMS Failed: message='%s' to='%s'",
                message, self.mask_number(number), exc_info=True)
            raise
        else:
            logger.info("Sent SMS: message='%s' to='%s' id=%s",
                        message, self.mask_number(number), msg_id)
        return msg_id

    def get_message_status(self, msgid):
        """ """
        if self.in_debug():
            return "sended"  # FIXME missing info
        client = self.get_client()
        status_msg = client.service.GetMessageStatus(msgid)
        return status_msg


sms_service = SMSService()
