# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import SimpleTestCase
from mock import patch
from suds.sax.text import Text
from django.core.management import call_command
from contrib.umws.test_utils import ClientMock
from .sms_service import sms_service


class ShortMessageServiceClientMock(ClientMock):
    """
    Mock object that implements Short Message Service
    """

    def SendMessage(self, number, message, anonymous):  # noqa
        """ Send Message method
        """
        return Text(999999)

    def GetMessageStatus(self, msgid):  # noqa
        """ Get Message Status
        """
        return Text("SMS does not exist in context.")


class SMSServiceTestCase(SimpleTestCase):
    """ """

    def setUp(self):
        sms_service.debug = False

    @patch('contrib.umws.sms.sms_service.get_client')
    def test_send_message(self, get_client):
        """ """
        get_client.return_value = ShortMessageServiceClientMock("url_wsdl")
        msgid = sms_service.send_message('910000000', 'Msg')
        self.assertEquals(msgid, '999999')

    @patch('contrib.umws.sms.sms_service.get_client')
    def test_get_message_status(self, get_client):
        """ """
        get_client.return_value = ShortMessageServiceClientMock("url_wsdl")
        status = sms_service.get_message_status("999999")
        self.assertEqual(status, "SMS does not exist in context.")

    def test_ping_command_fail(self):
        """ """
        with self.assertRaises(SystemExit) as cm:
            call_command('check_sms_service')
        self.assertEqual(cm.exception.code, 1)

    @patch('contrib.umws.sms.sms_service.ping')
    def test_ping_command_ok(self, mock_fn):
        mock_fn.return_value = True
        """ """
        with self.assertRaises(SystemExit) as cm:
            call_command('check_sms_service')
        self.assertEqual(cm.exception.code, 0)
