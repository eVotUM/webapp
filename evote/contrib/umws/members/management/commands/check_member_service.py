# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import sys

from django.core.management.base import BaseCommand
from contrib.umws.members.members_service import members_service


class Command(BaseCommand):
    help = 'Check if UM Member WebService is respondiing properly'

    def handle(self, child=None, *args, **options):
        exit_code = 1
        try:
            if members_service.ping():
                exit_code = 0
        except:
            pass
        sys.exit(exit_code)
