# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import base64
import logging
import os.path
from datetime import datetime
from datetime import timedelta
from suds import WebFault

from django.core.files.base import ContentFile

from contrib.umws.umws_service import UMWebService

# Dummy data to simulate the response of WS of DTSI
dummy_data = {
    'username': 'pg2711',
    'email': 'dev@eurotux.com',
    'phone': '+351912233441',
    'name': 'Dev Test'
}


logger = logging.getLogger('evote.contrib.umws')


class MembersService(UMWebService):
    """ """
    settings_name = "MEMBERS_SERVICE_CONFIG"

    def is_email_valid(self, email):
        """ """
        if self.in_debug():
            return True
        client = self.get_client()
        return client.service.IsEmailValid(email)

    def get_contact_info_by_email(self, email):
        """ """
        if self.in_debug():
            if email == "unknown@eurotux.com":
                return {}
            return dummy_data
        client = self.get_client()
        logger.info("Get contact info by email: %s", email)
        contact = client.service.GetContactInfoByEmail(email)
        if not contact:
            return None
        return self.get_data(contact)

    def get_contact_info_by_login(self, login):
        """ """
        if self.in_debug():
            if login == "unknown":
                return {}
            return dummy_data
        client = self.get_client()
        logger.info("Get contact info by login: %s", login)
        contact = client.service.GetContactInfoByLogin(login)
        if not contact:
            return None
        return self.get_data(contact)

    def get_photo_by_login(self, login):
        """ Returns only the base64 string """
        if self.in_debug():
            return None
        client = self.get_client()
        try:
            photo = client.service.GetPhotoBase64ByLogin(login)
            logger.debug("Photo base64 string returned: %s", photo)
            if photo:
                logger.info("Photo base64 string returned (size): %s",
                            len(photo))
            return photo
        except WebFault:
            logger.info("Photo base64 impossible to obtain", exc_info=True)
        return None

    def get_data(self, contact):
        """ Contact Element has this fields:
            'DataAlteracao', 'Email', 'Login', 'Name', 'Number', 'OutroEmail'
            For now we only need: 'Email', 'Name', 'Number'
        """
        data = {
            'email': contact.Email.lower(),
            'username': contact.Login.lower(),
            'phone': contact.Number,
            'name': contact.Name.title(),
        }
        logger.info("Contact info returned: %s", data)
        return data

    def is_time_to_update_photo(self, photo, days=1):
        """ """
        if not photo:
            return True
        ts = os.path.getmtime(photo.path)
        next_update = datetime.utcfromtimestamp(ts) + timedelta(days=days)
        if next_update > datetime.now():
            return False
        return True

    @staticmethod
    def set_user_photo(sender, user, request, **kwargs):
        """Callback staticmethod for user_logged_in signal"""

        if members_service.is_time_to_update_photo(user.photo):
            login = members_service.upn_to_login(user.upn)
            photo_base64 = members_service.get_photo_by_login(login)
            if photo_base64 and login:
                file_name = login + ".png"  # FIXME
                user.photo.save(file_name,
                                ContentFile(base64.b64decode(photo_base64)),
                                False)
                user.save(update_fields=['photo'])


members_service = MembersService()
