# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase
from mock import patch
from members_service import dummy_data
from django.core.management import call_command
from contrib.umws.test_utils import ClientMock
from .members_service import members_service


class Contact(object):
    """ """
    Login = ""
    Email = ""
    Name = ""
    Number = ""

    def __init__(self, Login, Email, Name, Number):  # noqa
        self.Login = Login
        self.Email = Email
        self.Name = Name
        self.Number = Number


class ValidationServiceClientMock(ClientMock):
    """
    Mock object that implements Validation Service
    """

    def IsEmailValid(self, email):  # noqa
        """ Check if email is valid
        """
        return True

    def GetContactInfoByEmail(self, email):  # noqa
        """ Get contact info by email
        """
        return Contact(
            Login="pg2711",
            Email="dev@eurotux.com",
            Name="Dev test",
            Number="+351912233441")

    def GetContactInfoByLogin(self, login):  # noqa
        """ Get contact info by login
        """
        return Contact(
            Login="pg2711",
            Email="dev@eurotux.com",
            Name="Dev test",
            Number="+351912233441")

    def GetPhotoBase64ByLogin(self, login):  # noqa
        """ Get photo by login
        """
        return "base64string"


class MembersServiceTestCase(SimpleTestCase):
    """ """

    def setUp(self):
        members_service.debug = False

    @patch('contrib.umws.members.members_service.get_client')
    def test_is_email_valid(self, get_client):
        """ """
        get_client.return_value = ValidationServiceClientMock("url_wsdl")
        valid = members_service.is_email_valid("")
        self.assertTrue(valid)

    @patch('contrib.umws.members.members_service.get_client')
    def test_get_contact_info_by_email(self, get_client):
        """ """
        get_client.return_value = ValidationServiceClientMock("url_wsdl")
        contact = members_service.get_contact_info_by_email("")
        self.assertEqual(contact, dummy_data)

    @patch('contrib.umws.members.members_service.get_client')
    def test_get_contact_info_by_login(self, get_client):
        """ """
        get_client.return_value = ValidationServiceClientMock("url_wsdl")
        contact = members_service.get_contact_info_by_login("")
        self.assertEqual(contact, dummy_data)

    @patch('contrib.umws.members.members_service.get_client')
    def test_get_photo_by_login(self, get_client):
        """ """
        get_client.return_value = ValidationServiceClientMock("url_wsdl")
        photo = members_service.get_photo_by_login("")
        self.assertEqual(photo, "base64string")

    def test_ping_command_fail(self):
        """ """
        with self.assertRaises(SystemExit) as cm:
            call_command('check_member_service')
        self.assertEqual(cm.exception.code, 1)

    @patch('contrib.umws.members.members_service.ping')
    def test_ping_command_ok(self, mock_fn):
        mock_fn.return_value = True
        """ """
        with self.assertRaises(SystemExit) as cm:
            call_command('check_member_service')
        self.assertEqual(cm.exception.code, 0)
