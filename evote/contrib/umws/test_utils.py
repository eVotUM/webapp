# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from suds.client import Client


class AlwaysCallable(object):
    """
    Represents a chainable-access object and proxies calls to ClientMock.
    """

    name = None

    def __init__(self, client_cls):
        self._client_cls = client_cls

    def __call__(self, *args, **kwargs):
        try:
            hook = object.__getattribute__(self._client_cls, self.name)
        except AttributeError:
            pass
        else:
            return hook(self._client_cls, *args, **kwargs)

    def __getattr__(self, item):
        new = object.__getattribute__(self, '__class__')(self._client_cls)
        new.name = item
        return new


class ClientMock(Client):
    """
    Abstract mock suds client.
    """

    def __init__(self, url, **kwargs):
        pass

    def __getattr__(self, item):
        return AlwaysCallable(self.__class__)

    def __unicode__(self):
        return 'Client mock'

    def __str__(self):
        return 'Client mock'
