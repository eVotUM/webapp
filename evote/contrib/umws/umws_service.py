# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import logging

from suds.client import Client
from suds.wsse import Security, UsernameToken
from django.conf import settings


logger = logging.getLogger('evote.contrib.umws')


class UMWebService(object):
    """ """

    settings_name = ""
    url = ""
    login = ""
    password = ""
    debug = ""
    client = None

    def __init__(self):
        """ """
        self.config = getattr(settings, self.settings_name, {})
        self.url = self.config.get("url", "")
        self.login = self.config.get("login", "")
        self.password = self.config.get("password", "")
        self.debug = self.config.get("debug", False)

    def in_debug(self):
        """ """
        return self.debug

    def upn_to_login(self, upn):
        """ """
        if upn:
            return upn.split('@')[0]
        return upn

    def get_client(self):
        """ """
        if not self.client and not self.in_debug():
            try:
                self.client = Client(url=self.url)
                security = Security()
                token = UsernameToken(self.login, self.password)
                security.tokens.append(token)
                self.client.set_options(wsse=security)
            except Exception as e:
                logger.critical(e, exc_info=True)
                raise
        return self.client

    def ping(self):
        """ """
        if self.in_debug():
            return True
        client = self.get_client()
        return client.service.Ping()


umws_service = UMWebService()
