# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase
from mock import patch
from .test_utils import ClientMock
from .umws_service import umws_service


class ShortMessageServiceClientMock(ClientMock):
    """
    Mock object that implements Generic methods
    """

    def Ping(self):  # noqa
        """ Get Message Status
        """
        return True


class UMWebServiceCase(SimpleTestCase):
    """ """

    def setUp(self):
        umws_service.debug = False

    def test_in_debug(self):
        """ """
        debug = umws_service.in_debug()
        self.assertEqual(debug, False)

    def test_upn_to_login(self):
        """ """
        upn = "q90001@no.mail.spam"
        login = umws_service.upn_to_login(upn)
        self.assertEqual(login, "q90001")

    def test_upn_to_login_no_upn(self):
        """ """
        upn = None
        login = umws_service.upn_to_login(upn)
        self.assertEqual(login, None)

    @patch('contrib.umws.umws_service.get_client')
    def test_ping(self, get_client):
        """ """
        get_client.return_value = ShortMessageServiceClientMock("url_wsdl")
        ping = umws_service.ping()
        self.assertEqual(ping, True)
