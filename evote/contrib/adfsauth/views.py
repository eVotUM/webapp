# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import logging

from django.conf import settings
from django.shortcuts import redirect, render
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.utils.decorators import method_decorator

from .ws_fed import WSFedAuthentication, adfs_wsfed


logger = logging.getLogger('evote.contrib.adfsauth')


@method_decorator(csrf_exempt, name='dispatch')
class WSFedSigninView(View):
    """ Sign in view """

    template_name = "adfsauth/signin_fail.html"

    def get(self, request, *args, **kwargs):
        """ """
        return redirect(adfs_wsfed.get_signin_url())

    def post(self, request, *args, **kwargs):
        """ """
        config = getattr(settings, "ADFS_WSFED_CONFIG", {})
        post_response = config.get('post_response', 'wresult')
        response = request.POST.get(post_response, '')
        # log response from adfs server
        logger.debug(response)
        wsfed_auth = WSFedAuthentication(request, response)
        is_authenticated = wsfed_auth.authenticate()
        if is_authenticated:
            return redirect(settings.LOGIN_REDIRECT_URL)
        # Authentication fail
        return render(request, config.get('template_fail', self.template_name))


class WSFedSignoutView(View):
    """ Sign out view """

    def get(self, request, *args, **kwargs):
        """ """
        logout(request)
        return redirect(adfs_wsfed.get_signout_url())
