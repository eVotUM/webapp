# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from unittest import skip
from mock import patch
import os

from django.test import (TestCase, SimpleTestCase, RequestFactory,
                         override_settings)
from django.conf import settings

from contrib.adfsauth.views import (WSFedSigninView, WSFedSignoutView,
                                    adfs_wsfed)


@override_settings(
    AUTHENTICATION_BACKENDS=('contrib.adfsauth.backends.WSFedBackend',),
    ADFS_WSFED=True,
    ADFS_WSFED_CONFIG={
        'url': 'https://auth.test.local/adfs/ls/',
        'wtrealm': 'urn:evotum',
        'cert': os.path.join(
            settings.PROJECT_DIR, 'contrib/adfsauth/tests/adfs_test_cert.pem'),
        'claim_mapping': {
            'nome': 'name',
            'email': 'email',
            'fotografia': 'photo',
            'telemovel': 'primary_phone',
            'nic': 'nic'
        },
    }
)
class WSFedSigninViewTest(TestCase):
    """ """

    @skip("TODO: update signature of xml response using the key and pem test")
    def test_post(self):
        response_path = os.path.join(
            settings.PROJECT_DIR,
            'contrib/adfsauth/tests/adfs_test_response.xml')
        data = {
            'wresult': open(response_path, 'rb').read()
        }
        request = RequestFactory().post("/wsignin/", data)
        with self.settings():
            response = WSFedSigninView.as_view()(request)
            print response


class WSFedSignoutViewTest(SimpleTestCase):
    """ """

    @patch.object(adfs_wsfed, 'get_signout_url', return_value="/test/")
    @patch('contrib.adfsauth.views.logout')
    def test_get(self, logout, mock_fn):
        request = RequestFactory().get("/wsignout/")
        response = WSFedSignoutView.as_view()(request)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/test/", fetch_redirect_response=False)
        logout.assert_called_with(request)
