# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from mock import Mock, patch

from django.test import SimpleTestCase

from contrib.adfsauth.backends import WSFedBackend


class WSFedBackendTest(SimpleTestCase):

    def setUp(self):
        self.backend = WSFedBackend()
        self.user_manager = Mock()
        self.user_manager.update_or_create.return_value = ("test", True)
        self.user_info = {
            'username': 'dev'
        }

    @patch('contrib.adfsauth.backends.get_user_model')
    @patch.object(WSFedBackend, 'user_can_authenticate', return_value=True)
    def test_authenticate_active_user(self, mock_fn, mget_user_model):
        usermodel = mget_user_model()
        usermodel._default_manager = self.user_manager
        usermodel.USERNAME_FIELD = "username"
        result = self.backend.authenticate(self.user_info)
        self.assertIsNotNone(result)
        self.assertEqual(result, "test")
        self.user_manager.update_or_create.assert_called_with(
            username=self.user_info['username'], defaults=self.user_info)

    @patch('contrib.adfsauth.backends.get_user_model')
    @patch.object(WSFedBackend, 'user_can_authenticate', return_value=False)
    def test_authenticate_inactive_user(self, mock_fn, mget_user_model):
        usermodel = mget_user_model()
        usermodel._default_manager = self.user_manager
        usermodel.USERNAME_FIELD = "username"
        result = self.backend.authenticate(self.user_info)
        self.assertIsNone(result)
        self.user_manager.update_or_create.assert_called_with(
            username=self.user_info['username'], defaults=self.user_info)

    def test_user_can_authenticate_with_user_active(self):
        user = Mock(is_active=True)
        result = self.backend.user_can_authenticate(user)
        self.assertTrue(result)

    def test_user_can_authenticate_with_user_inactive(self):
        user = Mock(is_active=False)
        result = self.backend.user_can_authenticate(user)
        self.assertFalse(result)

    @patch('contrib.adfsauth.backends.get_user_model')
    @patch.object(WSFedBackend, 'user_can_authenticate', return_value=True)
    def test_get_user(self, mock_fn, mget_user_model):
        get = Mock(return_value='test')
        mget_user_model()._default_manager.get = get
        result = self.backend.get_user(1)
        self.assertEqual(result, 'test')
        get.assert_called_with(pk=1)

    @patch('contrib.adfsauth.backends.get_user_model')
    @patch.object(WSFedBackend, 'user_can_authenticate', return_value=True)
    def test_get_user_with_exception(self, mock_fn, mget_user_model):
        get = Mock(side_effect=Exception)
        mget_user_model()._default_manager.get = get
        mget_user_model().DoesNotExist = Exception
        result = self.backend.get_user(1)
        self.assertIsNone(result)
