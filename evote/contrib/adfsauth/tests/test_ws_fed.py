# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase, RequestFactory

from contrib.adfsauth.ws_fed import WSFedAuthentication


class WSFedAuthenticationTest(SimpleTestCase):

    def setUp(self):
        request = RequestFactory().get("/wsignout/")
        self.obj = WSFedAuthentication(request, None)
        self.user_info = {
            'username': 'username',
            'name': 'nome',
            'email': 'email',
            'photo': 'fotografia',
            'primary_phone': 'telemovel',
            'nic': 'nic'
        }

    def test_clean_user_info_no_username(self):
        # Check no username provider
        self.user_info.pop('username')
        self.assertIsNone(self.obj.clean_user_info(self.user_info))

    def test_clean_user_info_valid_username(self):
        # Check username provided
        self.assertIsNotNone(self.obj.clean_user_info(self.user_info))

    def test_clean_user_info_invalid_phone_number(self):
        # Check for invalid phone number
        self.assertEqual(
            self.obj.clean_user_info(self.user_info)['primary_phone'], '')

    def test_clean_user_info_valid_phone_number(self):
        # Check for valid phone number
        self.user_info['primary_phone'] = '+351912243452'
        self.assertEqual(
            self.obj.clean_user_info(self.user_info)['primary_phone'],
            '+351912243452')
