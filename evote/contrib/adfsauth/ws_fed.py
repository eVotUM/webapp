# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals, absolute_import

import logging

from signxml import XMLVerifier, exceptions
from lxml import etree

from django.views.decorators.debug import sensitive_variables
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model, authenticate, login
from django.conf import settings

from phonenumber_field.validators import validate_international_phonenumber

from .utils import username_from_upn


logger = logging.getLogger('evote.contrib.adfsauth')

config = getattr(settings, 'ADFS_WSFED_CONFIG', {})


class WSFed(object):
    """ WS-Fed sign-in protocol """

    url = ""
    signin = ""
    signout = ""
    wtrealm = ""

    def __init__(self):
        """ """
        self.url = config.get("url", "")
        self.signin = config.get("signin", "wsignin1.0")
        self.signout = config.get("signout", "wsignout1.0")
        self.wtrealm = config.get("wtrealm", "")

    def get_signin_url(self):
        """ """
        return "%s?wa=%s&wtrealm=%s" % (self.url, self.signin, self.wtrealm)

    def get_signout_url(self):
        """ """
        return "%s?wa=%s" % (self.url, self.signout)


adfs_wsfed = WSFed()


class WSFedAuthentication(object):

    request = ""
    response = ""

    def __init__(self, request, response):
        """ """
        self.request = request
        self.response = response

    def authenticate(self):
        """ """
        verified_data = self.verify_response()

        # Authentication succeeded
        if verified_data is not None:
            user_info = self.extract_values(verified_data)
            cleaned_data = self.clean_user_info(user_info)
            if not cleaned_data:
                return False
            user = authenticate(user_info=cleaned_data)
            if user:
                logger.info("Auth: User with email='%s' authenticated",
                            cleaned_data['email'])
                login(self.request, user)
                return True
            else:
                logger.debug(
                    "Auth: User with email='%s' failed to authenticate",
                    cleaned_data['email'])
        return False

    @sensitive_variables('cert')
    def verify_response(self):
        """ """
        if not self.response:
            return None

        certificate = config.get('cert', '')

        cert = open(certificate).read()
        xml = etree.XML(self.response)
        try:
            verified_data = XMLVerifier().verify(xml, require_x509=False,
                                                 x509_cert=cert,
                                                 id_attribute="AssertionID")
        except exceptions.InvalidSignature, exceptions.InvalidInput:
            logger.exception('Invalid signature or digest')
            # verification fail
            return None
        # verification success return VerifyResult obj (XML)
        return verified_data.signed_xml

    def extract_values(self, xml):
        """ extract values from XML result """
        user_info = {}
        claims = config.get('claim_mapping', {})
        xpath = './/{urn:oasis:names:tc:SAML:1.0:assertion}Attribute'

        attributes = xml.findall(xpath)
        for attr in attributes:
            attr_name = attr.get('AttributeName', 'noattr')
            if attr_name in claims:
                key = claims.get(attr_name)
                user_info[key] = attr.getchildren()[0].text
        return user_info

    def clean_user_info(self, user_info):
        """Cleans user info """
        identifier = get_user_model().USERNAME_FIELD
        if identifier not in user_info or not user_info[identifier]:
            logger.warning("Empty '%s': %r", identifier, user_info)
            return None
        # extract username from return upn
        user_info[identifier] = username_from_upn(user_info[identifier])
        # Validate phone number
        try:
            validate_international_phonenumber(user_info['primary_phone'])
        except ValidationError:
            user_info['primary_phone'] = ""
            logger.warning("Invalid phone number: %r", user_info)
        return user_info
