# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.conf import settings


config = getattr(settings, 'ADFS_WSFED_CONFIG', {})


class WSFedBackend(object):
    """
    Create or update user based on ADFS WS-Fed response
    """

    def authenticate(self, user_info, **kwargs):
        """Check if user exists
        """
        UserModel = get_user_model()  # noqa
        user, created = UserModel._default_manager.update_or_create(**{
            UserModel.USERNAME_FIELD: user_info[UserModel.USERNAME_FIELD],
            'defaults': user_info
        })
        return user if self.user_can_authenticate(user) else None

    def user_can_authenticate(self, user):
        """
        Reject users with is_active=False. Custom user models that don't have
        that attribute are allowed.
        """
        is_active = getattr(user, 'is_active', None)
        return bool(is_active)

    def get_user(self, user_id):
        UserModel = get_user_model()  # noqa
        try:
            user = UserModel._default_manager.get(pk=user_id)
        except UserModel.DoesNotExist:
            return None
        return user if self.user_can_authenticate(user) else None
