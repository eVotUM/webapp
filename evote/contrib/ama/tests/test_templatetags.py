# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import os

from django.contrib.sessions.backends.db import SessionStore
from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.test import TestCase, RequestFactory
from django.conf import settings

from evoteusers.tests.utils import UserTestUtils


project_path = settings.PROJECT_DIR


class TemplateTagsTest(UserTestUtils, TestCase):

    def render_template(self, string, context=None, request=None):
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


class AmaTagsTest(TemplateTagsTest):
    """ """

    def test_render_form(self):
        testr_path = os.path.join(
            project_path, 'contrib/ama/tests/samlreqtest.txt')
        template = "{% load ama_tags %}{% show_autenticacaogov_login" + \
                   " '/test_url/' '/test_err_url/' " +\
                   "'' '2016-12-23T18:32:10Z'" +\
                   " 'evt967b594a768b0e3ca42eb51fbc2dc2fd07f673d8'" +\
                   " classes='unit' logo=False %}"
        expected = open(testr_path).read()
        request = RequestFactory().get(reverse('ama:samlresponse'))
        request.session = SessionStore()
        rendered = self.render_template(template, context={'request': request})
        self.assertInHTML(expected, rendered)
