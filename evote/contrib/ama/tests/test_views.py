# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import os

from django.conf import settings
from mock import patch
from django.core.management import call_command
from django.test import TestCase, override_settings
from django.core.urlresolvers import reverse
from lxml import etree

from evoteusers.tests.utils import UserTestUtils


project_path = settings.PROJECT_DIR


class ProccessSAMLResponseTest(UserTestUtils, TestCase):
    """ """

    def setUp(self):
        self.login()
        self.url = reverse("ama:samlresponse")
        self.sess_data = {
            'success_url': "/test_url/",
            'error_url': "/test_err_url/",
        }
        saml_confs = getattr(settings, 'SAML', '')
        self.sess_key = saml_confs.get('session_dict', None)

    @override_settings(ROOT_URLCONF="contrib.ama.tests.urls")
    def test_response_process(self):
        """ """
        self.respdata = {}
        testr_path = os.path.join(
            project_path, 'contrib/ama/tests/samlresptest.txt')
        testr = open(testr_path).read()
        self.respdata['SAMLResponse'] = testr
        self.respdata['RelayState'] = "SESSKEY"
        session = self.client.session
        session[self.sess_key] = self.sess_data
        session.save()
        response = self.client.post(self.url, follow=False, data=self.respdata)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.sess_data['success_url'])

    @override_settings(ROOT_URLCONF="contrib.ama.tests.urls")
    def test_invalid_signature_response_process(self):
        """ """
        self.respdata = {}
        testr_path = os.path.join(
            project_path,
            'contrib/ama/tests/samltestresp_error_invalidsignature.txt')
        testr = open(testr_path).read()
        self.respdata['SAMLResponse'] = testr
        self.respdata['RelayState'] = "SESSKEY"
        session = self.client.session
        session[self.sess_key] = self.sess_data
        session.save()
        response = self.client.post(self.url, follow=False, data=self.respdata)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, self.sess_data['error_url'])

    @patch('contrib.ama.management.commands.generate_test_response.XMLSigner')
    @patch('contrib.ama.management.commands.generate_test_response.open')
    def test_generate_test_response_command(self, mock_fn, mock_sign_xml):
        """ """
        mock_sign_xml.return_value.sign.return_value = etree.Element("root")
        mock_fn.return_value.read.return_value = "dummy"
        args = ["certpath", "keypath"]
        opts = {}
        call_command('generate_test_response', *args, **opts)
