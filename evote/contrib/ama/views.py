# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import base64
import cryptography
import logging

from lxml import etree
from signxml import XMLVerifier
from importlib import import_module

from django.conf import settings
from django.shortcuts import redirect
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.debug import sensitive_variables

from django.utils.encoding import smart_str


logger = logging.getLogger('evote.contrib.ama')

saml_confs = getattr(settings, 'SAML', '')


@method_decorator(csrf_exempt, name="dispatch")
class ProcessSAMLResponse(View):
    success = False

    @sensitive_variables('cert')
    def post(self, request, *args, **kwargs):
        sessid = request.POST.get("RelayState", "")
        session_dict_key = saml_confs.get('session_dict', None)

        if session_dict_key not in request.session:
            SessionStore = import_module(settings.SESSION_ENGINE).SessionStore
            session = SessionStore(session_key=sessid)
        else:
            session = request.session

        succtarget = session[session_dict_key].get('success_url', None)
        errtarget = session[session_dict_key].get('error_url', None)

        # Read saml response
        bsamlresp = request.POST.get("SAMLResponse", "")
        samlresp = base64.b64decode(bsamlresp)
        signed_root = etree.fromstring(samlresp)

        # Verify data Signature
        cert_path = saml_confs.get('idp_cert_path')
        cert = open(cert_path).read()

        if cert:
            try:
                # signed_xml
                assertion_data = XMLVerifier()\
                    .verify(signed_root, x509_cert=cert).signed_xml
            except cryptography.exceptions.InvalidSignature as e:
                logger.exception(e)
                pass
            else:
                session_data = {
                    'sessid': sessid,
                    'attributes': {}
                }
                # Read information from signed data
                samlns = "urn:oasis:names:tc:SAML:2.0:assertion"
                samlreqattributes = saml_confs.get('request_attributes')

                for attrid, attrname in samlreqattributes.items():
                    res = assertion_data.xpath(
                        '//fa:Attribute[@Name="{}"]'.format(attrname),
                        namespaces={'fa': samlns})
                    if res and len(res) > 0:
                        attr = res[0]
                        if len(attr) > 0:
                            txt = attr[0].text
                            session_data['attributes'][attrid] = smart_str(txt)
                            self.success = True
                # Reset auth gov session data
                session[session_dict_key] = session_data
                session.save()
        if self.success:
            return redirect(succtarget)
        else:
            logger.error("Invalid data returned, redirecting=%s", errtarget)
            logger.info(samlresp)
            return redirect(errtarget)
