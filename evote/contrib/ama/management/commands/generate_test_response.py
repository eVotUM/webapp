# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import os
import signxml
import base64  # noqa

from lxml import etree
from signxml import XMLSigner

from django.conf import settings
from django.core.files import File  # noqa
from django.core.management.base import BaseCommand

project_path = settings.PROJECT_DIR


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('certpath')
        parser.add_argument('keypath')

    def handle(self, *args, **options):
        certpath = options['certpath']
        pkeypath = options['keypath']

        cert_fpath = os.path.join(
            project_path, certpath)
        pkey_fpath = os.path.join(
            project_path, pkeypath)

        self.generate_response(cert_fpath, pkey_fpath)

    def generate_response(self, cert_path, pkey_path):

        # Auto generate the AuthnRequest ID
        # This should be a unique identifier for the SAML request
        reqid = '_53c47ee3-e790-49da-b7c4-9e681eabbb1a'
        in_response_to = 'evtd1e64fbe0dc08acffe82e964cda79cdfbd2cc476'
        issue_instant = "2016-12-21T18:16:50.2733078Z"
        destination = "http://127.0.0.1:8000/ama/response/"

        issuer_name = "https://autenticacao.cartaodecidadao.pt"

        # AuthnRequest namespaces
        samlns = "urn:oasis:names:tc:SAML:2.0:assertion"
        samlpns = "urn:oasis:names:tc:SAML:2.0:protocol"
        xsins = "http://www.w3.org/2001/XMLSchema-instance"
        xsdns = "http://www.w3.org/2001/XMLSchema"
        fans = "http://autenticacao.cartaodecidadao.pt/atributos"
        dsns = 'http://www.w3.org/2000/09/xmldsig#'

        nsmap = {"xsi": xsins,
                 "xsd": xsdns,
                 "samlp": samlpns}

        # Response
        root = etree.Element(
            'Response',
            nsmap=nsmap,
            InResponseTo=in_response_to,
            Version="2.0",
            Destination=destination,
            Consent="urn:oasis:names:tc:SAML:2.0:consent:unspecified",
            IssueInstant=issue_instant,
            ID=reqid
        )

        # Issuer namespace
        issuernsmap = {None: samlns}
        # Issuer
        issuer = etree.Element('Issuer', nsmap=issuernsmap)
        issuer.text = issuer_name
        root.append(issuer)

        # Signature namespace
        signnsmap = {None: dsns}
        # <Signature Id="placeholder"></Signature>
        signature = etree.Element('Signature',
                                  nsmap=signnsmap,
                                  Id="placeholder")
        root.append(signature)

        # Extensions namespace
        attrnsmap = {"fa": fans}
        # Extensions
        extensions = etree.Element('Extensions')
        # FAAALevel
        childname = etree.QName(fans, 'FAAALevel')
        faalevel = etree.Element(childname, nsmap=attrnsmap)
        faalevel.text = '3'
        extensions.append(faalevel)
        root.append(extensions)

        # Status
        status = etree.Element('Status')
        # Status Code
        statuscode = etree.Element(
            'StatusCode',
            Value="urn:oasis:names:tc:SAML:2.0:status:Success")
        status.append(statuscode)
        root.append(status)

        # Assertion
        assertid = '_ac8fc849-f7d9-40e1-b84a-a91019feaaea'
        assertnsmap = {None: samlns}
        assertion = etree.Element(
            'Assertion',
            nsmap=assertnsmap,
            Version="2.0",
            IssueInstant=issue_instant,
            ID=assertid
        )
        # Issuer
        issuer = etree.Element('Issuer')
        issuer.text = issuer_name
        assertion.append(issuer)

        # Subject
        subject = etree.Element('Subject')
        # NameID
        sformat = "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified"
        nameid = etree.Element('NameID', Format=sformat)
        nameid.text = sformat
        subject.append(nameid)
        # SubjectConfirmation
        sconfirmation = etree.Element(
            'SubjectConfirmation',
            Method="urn:oasis:names:tc:SAML:2.0:cm:bearer")
        # SubjectConfirmationData
        sconfirmationdata = etree.Element(
            'SubjectConfirmationData',
            NotOnOrAfter="2016-12-21T18:21:50Z",
            Recipient="evotum.uminho.pt",
            InResponseTo=in_response_to,
            Address="https://ignore.mordomo.gov.pt"
        )
        sconfirmation.append(sconfirmationdata)
        subject.append(sconfirmation)
        assertion.append(subject)

        # Conditions
        condition = etree.Element(
            'Conditions',
            NotBefore="2016-12-21T18:16:50Z",
            NotOnOrAfter="2016-12-21T18:21:50Z")
        # AudienceRestriction
        audiencerestriction = etree.Element('AudienceRestriction')
        # Audience
        audience = etree.Element('Audience')
        audience.text = 'evotum.uminho.pt'
        audiencerestriction.append(audience)
        condition.append(audiencerestriction)
        # OneTimeUse/
        onetimeuse = etree.Element('OneTimeUse')
        condition.append(onetimeuse)
        assertion.append(condition)

        # AuthnStatement
        authstmap = {"xsi": xsins}
        stype = 'xsd:string'
        authnstatement = etree.Element(
            'AuthnStatement',
            AuthnInstant="2016-12-21T18:16:50.2733078Z")
        authncontext = etree.Element('AuthnContext')
        authncontextdecl = etree.Element(
            'AuthnContextDecl',
            attrib={"{" + xsins + "}type": stype},
            nsmap=authstmap
        )

        authncontext.append(authncontextdecl)
        authnstatement.append(authncontext)
        assertion.append(authnstatement)

        # AttributeStatement
        d4p1ns = 'http://autenticacao.cartaodecidadao.pt/atributos'
        attrstns = {"d4p1": d4p1ns}
        attributestatement = etree.Element('AttributeStatement')
        # Attribute
        attribute = etree.Element(
            'Attribute',
            attrib={"{" + d4p1ns + "}AttributeStatus": 'Available'},
            Name="http://interop.gov.pt/MDC/Cidadao/NIC",
            NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri",
            nsmap=attrstns)
        # AttributeValue
        attributevalue = etree.Element(
            'AttributeValue',
            attrib={"{" + xsins + "}type": stype},
            nsmap=authstmap
        )
        attributevalue.text = '12345678'

        attribute.append(attributevalue)
        attributestatement.append(attribute)
        assertion.append(attributestatement)
        root.append(assertion)

        # Sign
        cert = open(cert_path).read()
        key = open(pkey_path).read()

        pkey_pass = settings.SECRET_KEY

        signed_root = XMLSigner(
            method=signxml.methods.enveloped,
            signature_algorithm=u'rsa-sha1',
            digest_algorithm=u'sha1',
            c14n_algorithm=u'http://www.w3.org/2001/10/xml-exc-c14n#').\
            sign(
                root,
                key=key,
                passphrase=pkey_pass,
                cert=cert
        )
        sr = etree.tostring(signed_root)  # noqa

        # # Print to file
        # sr_path = os.path.join(
        #     project_path, 'TestResponse.xml')
        # f = open(sr_path, 'w')
        # myfile = File(f)
        # myfile.write(sr)
        # myfile.close()

        # # Encode to base64
        # bsr = base64.b64encode(sr)

        # # Print to file
        # sr_path = os.path.join(
        #     project_path, 'TestB64EncResponse.txt')
        # f = open(sr_path, 'w')
        # myfile = File(f)
        # myfile.write(bsr)
        # myfile.close()
