# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import uuid
import signxml
import base64
from signxml import XMLSigner
from hashlib import sha1
from time import gmtime, strftime
from lxml import etree

from django.views.decorators.debug import sensitive_variables
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django import template

from contrib.ama.forms import SAMLForm


register = template.Library()

saml_confs = getattr(settings, 'SAML', '')


@sensitive_variables('cert', 'key', 'pkey_pass', 'signed_root')
@register.inclusion_tag('ama/index.html', takes_context=True)
def show_autenticacaogov_login(context, success_target, error_target,
                               submittext=None, issueinstant=None, reqid=None,
                               classes='', logo=True):
    # create XML
    # Auto generate the IssueInstant
    issueinstant = issueinstant or strftime("%Y-%m-%dT%H:%M:%SZ", gmtime())

    # Auto generate the AuthnRequest ID
    # This should be a unique identifier for the SAML request
    reqid = reqid or "evt%s" % sha1(uuid.uuid4().hex).hexdigest()

    # AuthnRequest namespaces
    samlns = "urn:oasis:names:tc:SAML:2.0:assertion"
    fans = "http://autenticacao.cartaodecidadao.pt/atributos"
    dsns = 'http://www.w3.org/2000/09/xmldsig#'

    nsmap = {
        "xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsd": "http://www.w3.org/2001/XMLSchema",
        "samlp": "urn:oasis:names:tc:SAML:2.0:protocol"
    }

    # Consumer Service URL
    csurl = context['request'].build_absolute_uri(reverse('ama:samlresponse'))

    # AuthnRequest
    destination = saml_confs.get('idp_addr')
    provider_name = saml_confs.get('provider_name')
    issuer_name = saml_confs.get('issuer')
    rootname = etree.QName(nsmap['samlp'], 'AuthnRequest')
    root = etree.Element(
        rootname,
        nsmap=nsmap,
        Version="2.0",
        Destination=destination,
        Consent="urn:oasis:names:tc:SAML:2.0:consent:unspecified",
        ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
        AssertionConsumerServiceURL=csurl,
        ProviderName=provider_name,
        IssueInstant=issueinstant,
        ID=reqid
    )

    # Issuer
    childname = etree.QName(samlns, 'Issuer')
    issuer = etree.Element(childname, nsmap={"saml": samlns})
    issuer.text = issuer_name
    root.append(issuer)

    # <Signature Id="placeholder"></Signature>
    childname = etree.QName(dsns, 'Signature')
    signature = etree.Element(childname, nsmap={"ds": dsns}, Id="placeholder")
    root.append(signature)

    # Extensions namespace
    attrnsmap = {"fa": fans}
    # Extensions
    childname = etree.QName(nsmap['samlp'], 'Extensions')
    extensions = etree.Element(childname)
    # RequestedAttributes
    childname = etree.QName(fans, 'RequestedAttributes')
    reqattrs = etree.Element(childname, nsmap=attrnsmap)
    samlreqattributes = saml_confs.get('request_attributes')
    for attrid, attrname in samlreqattributes.items():
        # RequestedAttribute
        childname = etree.QName(fans, 'RequestedAttribute')
        reqattr = etree.Element(
            childname,
            Name=attrname,
            NameFormat='urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
            isRequired='False'
        )
        reqattrs.append(reqattr)
    extensions.append(reqattrs)
    # FAAALevel
    childname = etree.QName(fans, 'FAAALevel')
    faalevel = etree.Element(childname, nsmap=attrnsmap)
    faalevel.text = '3'
    extensions.append(faalevel)

    root.append(extensions)

    # Sign the request
    with open(saml_confs.get('sp_cert_path')) as f:
        cert = f.read()
    with open(saml_confs.get('sp_pkey_path')) as p:
        key = p.read()

    pkey_pass = saml_confs.get('sp_pkey_passphrase').encode('utf-8')

    signed_root = XMLSigner(
        method=signxml.methods.enveloped,
        signature_algorithm=u'rsa-sha1',
        digest_algorithm=u'sha1',
        c14n_algorithm=u'http://www.w3.org/2001/10/xml-exc-c14n#')\
        .sign(root, key=key, passphrase=pkey_pass, cert=cert)

    signed_root_b64 = base64.b64encode(etree.tostring(signed_root))

    request = context['request']

    # Store the desired URL's in the session storage
    session_ama = saml_confs.get('session_dict')

    # Reset the standard storage
    request.session[session_ama] = {}
    request.session[session_ama]['success_url'] = success_target
    request.session[session_ama]['error_url'] = error_target

    form = SAMLForm({
        'SAMLRequest': signed_root_b64,
        'RelayState': str(request.session.session_key)
    })

    context = {
        'submittext': submittext or _('Authenticate'),
        'form': form,
        'idp_addr': destination,
        'classes': classes,
        'logo': logo
    }

    return context
