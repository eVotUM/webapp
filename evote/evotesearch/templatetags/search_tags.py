# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django import template
from django.utils.safestring import mark_safe
import re


register = template.Library()


@register.filter
def highlight(value, phrases):
    if isinstance(phrases, basestring):
        phrases = [phrases]

    phrases = map(re.escape, phrases)
    expr = re.compile(r"(%s)" % "|".join(phrases), re.I)
    template = '<span class="highlight">%s</span>'

    def replace(match):
        return template % match.group(0)

    return mark_safe(expr.sub(replace, value))
