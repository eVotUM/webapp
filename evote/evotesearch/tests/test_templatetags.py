# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.template import Context, Template
from django.test import SimpleTestCase


class TemplateTagsTest(object):

    def render_template(self, string, context=None, request=None):
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


class SearchTagsTest(TemplateTagsTest, SimpleTestCase):

    def test_highlight(self):
        """ The only highlighted word should be "test" """
        context = {'testtitle': 'test title', 'terms': ['test']}
        template = "{% load search_tags %}{{testtitle|highlight:terms}}"
        expected = "<span class='highlight'>test</span> title"
        rendered = self.render_template(template, context)
        self.assertInHTML(expected, rendered)
