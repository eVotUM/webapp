# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.db.models import Q


class SearchableRegister(object):

    def __init__(self, model, title, content, extra=None):
        self.model = model
        self.title = title
        self.content = content
        self.extra = extra or {}

    def search(self, search_terms):
        results = []

        title_lookup = self.title + "__icontains"
        content_lookup = self.content + "__icontains"

        q = Q()
        for term in search_terms:
            title_arg = {title_lookup: term}
            content_arg = {content_lookup: term}

            q &= Q(**title_arg) | Q(**content_arg)

        qs = self.model._default_manager.filter(q, **self.extra)

        for result in qs:
            results.append({
                'title': getattr(result, self.title).encode('utf-8'),
                'content': getattr(result, self.content),
                'url': result.get_absolute_url()
            })

        return results


search_register = []
