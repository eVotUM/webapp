# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .registers import search_register


class SearchMixin(object):

    def get_context_data(self, **kwargs):
        """  """
        context = super(SearchMixin, self).get_context_data(**kwargs)
        results = []
        # Search the models based on the GET 'q' parameter value
        # as query string
        query_string = self.request.GET.get("q", "")
        if query_string != "":

            # Split each word of the querystring into a list
            terms = query_string.split()

            # Iterate over the search register in order to detect all the
            # classes that have the "searchable" annotation.
            for r in search_register:
                results += r.search(terms)

            # Prepare pagination
            page = self.request.GET.get('page')
            # Define pagination (and number of results p/ page)
            paginator = Paginator(results, 10)
            try:
                results = paginator.page(page)
            except PageNotAnInteger:
                # If not a valid page param, return the first page.
                results = paginator.page(1)
            except EmptyPage:
                # If page out of range, return last
                results = paginator.page(paginator.num_pages)

            context['terms'] = terms
        context.update({
            'query': query_string,
            'results': results,
            'results_count': len(results)
        })
        return context
