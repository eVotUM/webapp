# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import SimpleTestCase

import evote.formats.pt.formats as formats_pt


class CeleryTest(SimpleTestCase):

    def test_load_app(self):
        from evote.celery import app
        from celery import Celery
        self.assertIsInstance(app, Celery)


class WsgiTest(SimpleTestCase):

    def test_load_wsgi(self):
        from evote.wsgi import application
        from raven.contrib.django.raven_compat.middleware.wsgi import Sentry
        self.assertIsInstance(application, Sentry)
        self.assertIsNotNone(application.application._request_middleware)


class FormatsTest(SimpleTestCase):

    def test_formats_pt(self):
        self.assertEqual(formats_pt.DATETIME_FORMAT, 'd-m-Y H:i')
        self.assertEqual(formats_pt.DATETIME_INPUT_FORMATS, ['%d-%m-%Y %H:%M'])
