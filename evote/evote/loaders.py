# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import absolute_import

import logging

from celery.loaders.base import BaseLoader
from celery import signals

from django import db


logger = logging.getLogger('celery.loader')


class EvoteLoader(BaseLoader):
    """The Evote loader."""

    def _install_signal_handlers(self):
        # Need to close any open database connection after
        # any embedded celerybeat process forks.
        signals.beat_embedded_init.connect(self.close_database)

    def close_database(self):
        """Close all db connection"""
        db.close_old_connections()
        logger.debug('Closed old database connections')

    def on_process_cleanup(self):
        """Called after a task is executed."""
        self.close_database()

    def on_task_init(self, task_id, task):
        """Called before a task is executed."""
        try:
            is_eager = task.request.is_eager
        except AttributeError:
            is_eager = False
        if not is_eager:
            self.close_database()

    def on_worker_init(self):
        """Called when the worker (celery worker) starts."""
        self.close_database()

    def on_worker_process_init(self):
        """Called when a child process starts."""
        self.close_database()
