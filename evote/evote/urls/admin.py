# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.views.decorators.http import last_modified
from django.conf.urls.i18n import i18n_patterns
from django.views.i18n import JavaScriptCatalog
from django.conf.urls import include, url
from django.utils import timezone
from django.conf import settings

from evotevotings.admin.views import AdminErrorView
from evotecore.ajax import PingView


last_modified_date = timezone.now()


urlpatterns = [
    url(
        regex=r'^i18n/',
        view=include('django.conf.urls.i18n')
    ),
    url(
        regex=r'^adfs/',
        view=include("contrib.adfsauth.urls")
    ),

    # ajax view
    url(
        regex=r'^ping/$',
        view=PingView.as_view(),
        name="ping"
    ),
]

urlpatterns += i18n_patterns(
    url(
        regex=r'^jsi18n/$',
        view=last_modified(lambda req, **kw: last_modified_date)(
            JavaScriptCatalog.as_view()),
        name="javascript-catalog"
    ),
    url(
        regex=r'^',
        view=include('evotevotings.admin.urls')
    )
)


handler400 = AdminErrorView.get_view_rendered(400)
handler403 = AdminErrorView.get_view_rendered(403)
handler404 = AdminErrorView.get_view_rendered(404)
handler500 = AdminErrorView.get_view_rendered(500)


if settings.DEBUG:
    import debug_toolbar

    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += [
        url(
            regex=r'^__debug__/',
            view=include(debug_toolbar.urls)
        )
    ]
    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
