# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os

from django.core.urlresolvers import reverse_lazy

from evote.settings.dev import *  # noqa


# Django settings

SITE_ID = 6
ALLOWED_HOSTS = [os.environ.get('SN_ADMIN')]
ROOT_URLCONF = 'evote.urls.admin'


# Auth settings

LOGIN_REDIRECT_URL = reverse_lazy("evoteadmin:page-list")
LOGIN_URL = reverse_lazy("evoteadmin:login")
LOGOUT_URL = reverse_lazy("evoteadmin:logout")


# javascript_settings

JAVASCRIPT_SETTINGS_SCAN_MODULES = {
    'evoteadmin': 'evotevotings.admin.jssettings'
}
