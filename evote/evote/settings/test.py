# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os

from django.core.urlresolvers import reverse_lazy

from evote.settings import *  # noqa


# Django settings

SITE_ID = 1
DEBUG = False
TEMPLATE_DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "TEST": {
            "SERIALIZE": False
        }
    },
}


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'evotevotings.voters.middleware.VotersMiddleware'
]

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
)

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)


LOGIN_URL = reverse_lazy("voters:login")
LOGOUT_URL = reverse_lazy("voters:logout")
LOGIN_REDIRECT_URL = reverse_lazy("voters:electoralprocess-list")

AUTHENTICATION_BACKENDS += [  # NOQA
    'django.contrib.auth.backends.ModelBackend',  # this is default
]


# celery

CELERY_TASK_ALWAYS_EAGER = True
CELERY_TASK_EAGER_PROPAGATES = True
CELERY_BROKER_URL = "memory:///"
CELERY_EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'


# Celery email backend

DEFAULT_FROM_EMAIL = 'default@eurotux.com'
EVOTE_MAILING_REPLYTO_DEFAULT = 'default@eurotux.com'
EVOTE_MAILING_SUBJECT_DEFAULT = 'Unit test default'
EVOTE_MAILING_NOREPLY_DEFAULT = 'noreply@eurotux.com'


# sorl-thumbnails settings

THUMBNAIL_FORMAT = "PNG"


# Recaptcha testing

os.environ['RECAPTCHA_TESTING'] = 'True'


# SAML Configurations

SAML = {
    'session_dict': 'authgov',
    'sp_cert_path': os.path.join(
        PROJECT_DIR, 'contrib/ama/tests/test.crt'), # NOQA
    'sp_pkey_path': os.path.join(
        PROJECT_DIR, 'contrib/ama/tests/test.private.key'), # NOQA
    'sp_pkey_passphrase': SECRET_KEY, # NOQA
    'idp_cert_path': os.path.join(
        PROJECT_DIR, 'contrib/ama/tests/test.crt'), # NOQA
    'idp_addr': 'https://preprod.autenticacao.gov.pt/fa/default.aspx',
    'provider_name': 'Universidade do Minho',
    'issuer': 'evotum.uminho.pt',
    'request_attributes': {
        'NIC': 'http://interop.gov.pt/MDC/Cidadao/NIC'
    }
}


# javascript_settings

JAVASCRIPT_SETTINGS_SCAN_MODULES = {}


# Evote services settings

EVOTE_SERVICES_ENDPOINTS = {
    'anonymizer': None,
    'counter': None
}


# LOGGING

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        }
    },
    'loggers': {
        'evote': {
            'level': 'INFO',
            'handlers': ['null']
        }
    },
}
