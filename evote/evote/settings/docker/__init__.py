# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os

from django.core.urlresolvers import reverse_lazy

from evote.settings import *  # NOQA


# Django settings

SITE_ID = 1

DEBUG = True
TEMPLATE_DEBUG = True
THUMBNAIL_DEBUG = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('MYSQL_DATABASE'),
        'USER': os.environ.get('MYSQL_USER'),
        'PASSWORD': os.environ.get('MYSQL_PASSWORD'),
        'HOST': 'db',
        'PORT': '3306'
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.environ.get('REDIS_URL'),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}


# Auth settings

LOGIN_REDIRECT_URL = reverse_lazy("voters:electoralprocess-list")
LOGIN_URL = reverse_lazy("voters:login")
LOGOUT_URL = reverse_lazy("voters:logout")
LOGIN_REDIRECT_URL = reverse_lazy("voters:electoralprocess-list")

AUTHENTICATION_BACKENDS += [  # NOQA
    'django.contrib.auth.backends.ModelBackend',  # this is default
]


# Celery settings

CELERY_BROKER_URL = 'amqp://{}:{}@rabbitmq//'.format(
    os.environ.get('RABBITMQ_DEFAULT_USER'),
    os.environ.get('RABBITMQ_DEFAULT_PASS'))
CELERY_EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Sorl thumbnail settings

THUMBNAIL_KVSTORE = "sorl.thumbnail.kvstores.redis_kvstore.KVStore"
THUMBNAIL_DUMMY = True
THUMBNAIL_DEBUG = True


# debug toolbar settings

DEBUG_TOOLBAR_PATCH_SETTINGS = False
INSTALLED_APPS += (  # NOQA
    'debug_toolbar',
)
MIDDLEWARE += [  # NOQA
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda x: True,
}

EVOTE_SERVICES_ENDPOINTS = {
    'anonymizer': 'http://{}/api/v1/'.format(os.environ.get("SN_EVOTE_AS")),
    'counter': 'http://{}/api/v1/'.format(os.environ.get("SN_EVOTE_CS"))
}
