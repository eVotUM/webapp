# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.urlresolvers import reverse_lazy

from evote.settings.docker import *  # NOQA


# Django settings

SITE_ID = 2
ROOT_URLCONF = 'evote.urls.management'

MIDDLEWARE += (  # NOQA
    "evotevotings.management.middleware.ManagementMiddleware",
)


# Auth settings

LOGIN_URL = reverse_lazy("management:login")
LOGOUT_URL = reverse_lazy("management:logout")
LOGIN_REDIRECT_URL = reverse_lazy("management:electoralprocess-list")


# javascript_settings

JAVASCRIPT_SETTINGS_SCAN_MODULES = {}


# Evote messages settings

EVOTEMESSAGES_TEMPLATE_BASE_DIR = 'management/'
EVOTEMESSAGES_BASE_APP_NAME = 'management'


# Evote logs settings

def show_in_history(user):
    return bool(user and hasattr(user, 'is_electoral_member') and
                user.is_electoral_member)


EVOTELOG_SHOWINHISTORY_CALLBACK = show_in_history
