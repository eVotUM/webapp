# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os

from django.utils.translation import ugettext_lazy as _


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_DIR = os.path.join(BASE_DIR, '..')


# WARNING: only used on development environment!
SECRET_KEY = '-kll-u!+nvn&4&jf1osp79*6-1-ikoz3)0ogq=u++ox%_+lu2w'


# default project name
PROJ_NAME = os.environ.setdefault('PROJ_NAME', 'evote')


ALLOWED_HOSTS = ['*']

ADMINS = [('Dev', 'dev-errors@eurotux.com')]

# Application definition

INSTALLED_APPS = [
    # must be on top
    'modeltranslation',

    # django apps
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'django.contrib.sites',

    # external apps
    'compressor',
    'sorl.thumbnail',
    'django_extensions',
    'django_tables2',
    'djangoformsetjs',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'captcha',
    'javascript_settings',
    'guardian',
    'raven.contrib.django.raven_compat',

    # custom apps
    'contrib.ama',
    'contrib.adfsauth',
    'contrib.umws.members',
    'contrib.umws.sms',
    'evotelogs',
    'evoteajaxforms',
    'evoteusers',
    'evotecore',
    'evotedocs',
    'evotemailings',
    'evotemessages',
    'evotesearch',
    'evotevotings.processes',
    'evotevotings.elections',
    'evotevotings.voters',
    'evotevotings.management',
    'evotevotings.admin',
    'evotevotings.services',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'evotevotings.voters.middleware.VotersMiddleware',
    'evotelogs.middleware.EvoteLogsMiddleware'
]

ROOT_URLCONF = 'evote.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.request',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'evotecore.context_processors.global_settings',
            ],
        },
    },
]


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]


# Auth settings

AUTH_USER_MODEL = 'evoteusers.User'
ADFS_WSFED = False

AUTHENTICATION_BACKENDS = [
    'guardian.backends.ObjectPermissionBackend',
]

GUARDIAN_RAISE_403 = True


# Sessions

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 15 * 60  # 15 minutes
SESSION_SAVE_EVERY_REQUEST = True  # to renew the cookie on every request
SESSION_COOKIE_SECURE = True


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGES = (
    ('en', _('English')),
    ('pt', _('Portuguese')),
)
LANGUAGE_CODE = 'en'
TIME_ZONE = 'Europe/Lisbon'
USE_I18N = True
USE_L10N = True
USE_TZ = True

LOCALE_PATHS = [
    os.path.join(PROJECT_DIR, 'locale'),
    os.path.join(PROJECT_DIR, 'evoteusers/locale')
]

# Formats paths

FORMAT_MODULE_PATH = ['evote.formats']


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_ROOT = os.path.join(PROJECT_DIR, '../data/static')
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(PROJECT_DIR, "static"),
]
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
    'javascript_settings.finders.JavascriptSettingsFinder',
)


# Media files

MEDIA_ROOT = os.path.join(PROJECT_DIR, '../data/media')
MEDIA_URL = '/media/'


# CELERY

CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ENABLE_UTC = False
CELERY_TIMEZONE = TIME_ZONE
CELERY_EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'


# Compressor settings

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)


# sorl thumbnail settings

THUMBNAIL_REDIS_URL = os.environ.get('REDIS_URL')
THUMBNAIL_FORMAT = "PNG"
THUMBNAIL_DUMMY_SOURCE = "https://placehold.it/%(width)sx%(height)s"


# https://github.com/stefanfoulis/django-phonenumber-field

# PHONENUMBER_DB_FORMAT = 'E164'  # Application default
PHONENUMBER_DB_FORMAT = 'NATIONAL'
PHONENUMBER_DEFAULT_REGION = 'PT'


# Django Filter Settings

FILTERS_HELP_TEXT_FILTER = False


# Email settings

EMAIL_BACKEND = 'evotemailings.backends.EvoteEmailBackend'
EMAIL_FROM = 'dev@eurotux.com'
DEFAULT_FROM_EMAIL = 'dev@eurotux.com'

EVOTE_MAILING_SUBJECT_DEFAULT = 'Evotum'
EVOTE_MAILING_NOREPLY_DEFAULT = 'noreply@eurotux.com'


# Evote election notifications settings

EVOTE_ELECTIONS = {
    'notification_days_before': [3, 1],
    # time when to send notifications
    'notification_time': (7, 0),
    # identifies the error state defined by the counter service
    'counting_error_state': 0,
    # identifies the finish state defined by the counter service
    'counting_finish_state': 5,
    # number of days before elections to unlock operations management
    'manage_operations_after': 3,
    # number of days after approval period end to archive the election
    'archive_after': 90
}


# javascript_settings

JAVASCRIPT_SETTINGS_SCAN_MODULES = {
    "genericservice": "evotevotings.services.jssettings",
    "evoteadmin": "evotevotings.admin.jssettings",
}


# SAML Configurations

SAML = {
    'session_dict': 'authgov',
    'sp_cert_path': os.path.join(
        PROJECT_DIR, '..', 'data', 'certs', 'evotum.uminho.pt.crt'),
    'sp_pkey_path': os.path.join(
        PROJECT_DIR, '..', 'data', 'certs', 'evotum.uminho.pt.private.key'),
    'sp_pkey_passphrase': os.environ.get('SAML_PK_PASSPHRASE'),
    'idp_cert_path': os.path.join(
        PROJECT_DIR, '..', 'data', 'certs', 'ama_autenticacao_gov_pt.crt'),
    'idp_addr': 'https://preprod.autenticacao.gov.pt/fa/default.aspx',
    'provider_name': 'Universidade do Minho',
    'issuer': 'evotum.uminho.pt',
    'request_attributes': {
        'NIC': 'http://interop.gov.pt/MDC/Cidadao/NIC',
        'NIF': 'http://interop.gov.pt/MDC/Cidadao/NIF',
        'FullName': 'http://interop.gov.pt/MDC/Cidadao/NomeCompleto',
        'NISS': 'http://interop.gov.pt/MDC/Cidadao/NISS'
    }
}


# Recaptcha keys

RECAPTCHA_PUBLIC_KEY = '6Lf2jwwUAAAAAKFNFkjUgvufiQzyfjHvnlW6tw5U'
RECAPTCHA_PRIVATE_KEY = '6Lf2jwwUAAAAAI9B4-5XEymD4-fRPBUm2xbRkUXg'
NOCAPTCHA = True
# Override captcha widget template since the default one
# loaded the JS of recpatcha in the middle of the page (it's loaded by us only
# on required places now)
CAPTCHA_WIDGET_TEMPLATE = 'recaptcha/recaptcha.html'


# Contact Info

EVOTE_CONTACT_NAME = 'Evotum Team'
EVOTE_CONTACT_MAIL = 'dev@eurotux.com'
EVOTE_CONTACT_PHONE = '+351 000 000 000'
EVOTE_CONTACT_ADDRESS = 'Universidade do Minho'
EVOTE_CONTACT_GPS = '41.5598965,-8.3988118'


# Evote messages settings

EVOTEMESSAGES_TEMPLATE_BASE_DIR = 'voters/'


# Evote pin settings

PIN_SIZE = 8
PIN_TIMETOLIVE = 480  # time in seconds
PIN_SMS_CONTENT = ("eVotUM - Para confirmar o seu voto, por favor introduza "
                   "o seguinte código de votação: {pin}")   # NOQA


# Certificate Request constants

EVOTE_CSR_COUNTRYNAME = 'PT'
EVOTE_CSR_STATENAME = 'Portugal'
EVOTE_CSR_LOCALITYNAME = 'Braga'
EVOTE_CSR_ORGNAME = 'Universidade do Minho'
EVOTE_CSR_ORGUNITNAME = 'eVotUM Poll'


# Evote services endpoints

EVOTE_SERVICES_ENDPOINTS = {
    'anonymizer': 'https://{}/api/v1/'.format(os.environ.get("SN_EVOTE_AS")),
    'counter': 'https://{}/api/v1/'.format(os.environ.get("SN_EVOTE_CS"))
}


# WS UM Members

MEMBERS_SERVICE_CONFIG = {
    'debug': True
}


# WS UM SMS Notifications

SMS_SERVICE_CONFIG = {
    'debug': True
}

# LOGGING

EVOTELOG_DIR = os.path.join(PROJECT_DIR, '../logs/') # noqa

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'formatters': {
        'verbose': {
            'format': "[%(asctime)s %(levelname)s %(name)s] %(pathname)s in %(funcName)s at line %(lineno)d - %(message)s",  # NOQA
            'datefmt': '%d-%m-%Y %H:%M:%S'
        },
        'simple': {
            'format': '[%(asctime)s %(levelname)s] %(message)s',
            'datefmt': '%d-%m-%Y %H:%M:%S'
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'critical_file': {
            'level': 'CRITICAL',
            'filters': ['require_debug_false'],
            'class': 'logging.FileHandler',
            'filename': os.path.join(EVOTELOG_DIR, 'critical.log'),
            'formatter': 'verbose'
        },
        'main_file': {
            'level': 'INFO',
            'filters': ['require_debug_false'],
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(EVOTELOG_DIR, 'evote.log'),
            'maxBytes': 10 * 1024 * 1024,
            'backupCount': 5,
            'formatter': 'verbose'
        },
        'sentry': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler'
        }
    },
    'loggers': {
        'evote': {
            'level': 'INFO',
            'handlers': ['console', 'mail_admins', 'critical_file',
                         'main_file', 'sentry']
        },
        'evote.auditlogs': {
            'level': 'INFO',
            'handlers': ['null'],
            'propagate': False
        },
        'django': {
            'level': 'INFO',
            'handlers': ['console', 'mail_admins', 'sentry']
        },
        'celery': {
            'level': 'INFO',
            'handlers': ['console', 'mail_admins', 'sentry']
        }
    },
}
