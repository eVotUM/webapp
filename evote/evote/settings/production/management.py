# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os

from django.core.urlresolvers import reverse_lazy

from evote.settings.production import *  # noqa


# Django settings

SITE_ID = 11
ALLOWED_HOSTS = [os.environ.get('SN_MANAGE')]
ROOT_URLCONF = 'evote.urls.management'

MIDDLEWARE += (  # noqa
    "evotevotings.management.middleware.ManagementMiddleware",
)


# Auth settings

LOGIN_URL = reverse_lazy("management:login")  # noqa
LOGIN_REDIRECT_URL = reverse_lazy("management:electoralprocess-list")  # noqa

# redefined 'contrib.adfsauth.backends.WSFedBackend'
del AUTHENTICATION_BACKENDS[-1]  # NOQA
AUTHENTICATION_BACKENDS += [  # NOQA
    'evotevotings.management.backends.WSFedManagementBackend'
]


# javascript_settings

JAVASCRIPT_SETTINGS_SCAN_MODULES = {}


# ADFS AUTH (WS-Fed Sign-In Protocol)

ADFS_WSFED_CONFIG['wtrealm'] = 'urn:uminho:evotum-manage'  # noqa
ADFS_WSFED_CONFIG['template_fail'] = 'management/signin_fail.html'  # noqa


# evote messages settings

EVOTEMESSAGES_TEMPLATE_BASE_DIR = 'management/'
EVOTEMESSAGES_BASE_APP_NAME = 'management'


# evote log settings

def show_in_history(user):
    return bool(user and hasattr(user, 'is_electoral_member') and
                user.is_electoral_member)


EVOTELOG_SHOWINHISTORY_CALLBACK = show_in_history

# Google Analytics ID
GOOGLE_ANALYTICS_ID = None
