# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import os

from django.core.urlresolvers import reverse_lazy

from evote.settings import *  # NOQA
from evote import __version__


# Django settings

SITE_ID = 10

DEBUG = False
TEMPLATE_DEBUG = True
COMPRESS_OFFLINE = True

ALLOWED_HOSTS = [os.environ.get('SN_EVOTE')]
SECRET_KEY = os.environ.get('SECRET_KEY')
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get('MYSQL_DATABASE'),
        'USER': os.environ.get('MYSQL_USER'),
        'PASSWORD': os.environ.get('MYSQL_PASSWORD'),
        'HOST': os.environ.get('MYSQL_HOST'),
        'PORT': '',
        'OPTIONS': {
            'init_command': 'SET storage_engine=INNODB'
        },
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": os.environ.get('REDIS_URL'),
        "KEY_PREFIX": "evote",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

TEMPLATES[0]['APP_DIRS'] = False
TEMPLATES[0]['OPTIONS'].update({
    'loaders': [
        ('django.template.loaders.cached.Loader', [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        ]),
    ]
})


# Auth settings

LOGIN_URL = reverse_lazy("voters:login")
LOGOUT_URL = reverse_lazy("adfsauth:ws-fed-signout")
LOGIN_REDIRECT_URL = reverse_lazy("voters:electoralprocess-list")

AUTHENTICATION_BACKENDS += [  # NOQA
    'contrib.adfsauth.backends.WSFedBackend',
]


# ADFS AUTH (WS-Fed Sign-In Protocol)

ADFS_WSFED = True
ADFS_WSFED_CONFIG = {
    'url': 'https://login.uminho.pt/adfs/ls/',
    'wtrealm': 'urn:uminho:evotum',
    'cert': os.path.join(PROJECT_DIR, '..', 'data', 'certs', 'um_adfs_autenticacao.pem'),  # noqa
    'claim_mapping': {
        'upn': 'username',
        'nome': 'name',
        'email': 'email',
        'telemovel': 'primary_phone',
        'nic': 'nic'
    },
    "template_fail": 'voters/signin_fail.html',
    "post_response": "wresult",
    "identifier_field": "username"
}


# Define the Celery Settings
# http://docs.celeryproject.org/en/latest/userguide/configuration.html

CELERY_BROKER_URL = os.environ.get('BROKER_URL')


# sorl thumbnail settings

THUMBNAIL_KVSTORE = "sorl.thumbnail.kvstores.redis_kvstore.KVStore"


# WS UM Members

MEMBERS_SERVICE_CONFIG = {
    'url': 'https://ws.uminho.pt/ValidationServiceBasic/ValidationService.svc?wsdl',  # NOQA
    'login': os.environ.get('WS_MEMBERS_LOGIN'),
    'password': os.environ.get('WS_MEMBERS_PASSWORD')
}


# WS UM SMS Notifications

SMS_SERVICE_CONFIG = {
    'url': 'https://ws.uminho.pt/ShortMessageServiceBasic/SimpleMessageService.svc?wsdl',  # NOQA
    'login': os.environ.get('WS_SMS_LOGIN'),
    'password': os.environ.get('WS_SMS_PASSWORD')
}


# SAML Configurations

SAML.update({
    'idp_addr': 'https://autenticacao.gov.pt/fa/default.aspx'
})


# CONTACT configurations

EVOTE_CONTACT_NAME = 'Universidade do Minho'
EVOTE_CONTACT_MAIL = 'gcii@reitoria.uminho.pt'
EVOTE_CONTACT_PHONE = '+351 253 601 100'
EVOTE_CONTACT_ADDRESS = 'Largo do Paço, 4704-553 Braga'


# EMAIL configurations

EMAIL_FROM = 'no-reply@uminho.pt'
DEFAULT_FROM_EMAIL = 'no-reply@uminho.pt'

EVOTE_MAILING_NOREPLY_DEFAULT = 'no-reply@uminho.pt'


# Sentry configurations

RAVEN_CONFIG = {
    'dsn': 'https://6c5653397e964e1699a7b40745076d78:'
           'fdcd9c89cfd24f8cb5eae30a6669ffca@sentry.io/142993',
    'release': __version__
}

RAVEN_SHOW_ID_ON_ERROR = True


# Google Analytics ID

GOOGLE_ANALYTICS_ID = os.environ.get('GOOGLE_ANALYTICS_ID', None)


# LOGGING
# can't put syslog settings on main settings/__init__.py to avoid docker
# syslo install
LOGGING['handlers'].update({
    'syslog': {
        'level': 'INFO',
        'filters': ['require_debug_false'],
        'class': 'logging.handlers.SysLogHandler',
        'address': '/dev/log',
        'formatter': 'verbose',
    }
})
LOGGING['loggers']['evote']['handlers'] += ['syslog']
LOGGING['loggers']['evote.auditlogs']['handlers'] = ['syslog']
