# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase
from django.utils import timezone, formats
from django.conf import settings

from evotedocs.documents import PdfDocument


class PdfDocumentTest(SimpleTestCase):

    class UnitPdf(PdfDocument):

        def set_story(self):
            self.story.append(self.space())
            self.story.append(self.hr())
            self.story.append(self.title('Unit test'))
            self.story.append(self.body('Unit test'))
            self.story.append(self.pure_sec_title('Unit test'))
            self.story.append(self.label('Unit test'))
            self.story.append(self.label('Unit test'))
            self.story.append(self.table([['unit', 'test']]))
            self.story.append(self.votes_table([['unit', 'test']]))
            self.story.append(self.long_table([['unit', 'test']]))
            self.story.append(self.long_table([['unit', 'test']], header=True))
            self.story.append(self.custom_header_table([['unit', 'test']]))

    def setUp(self):
        self.object = PdfDocument()

    def test_add_col_space(self):
        data = [['Unit test', 'Unit Test snd'], ['Value 1', 'Value2']]
        res, widths = self.object.add_col_space(
            data, col_widths=[150, 150])
        expected_data = [['Unit test', '', 'Unit Test snd'],
                         ['Value 1', '', 'Value2']]
        expected_width = [150, 10, 150]
        self.assertEqual(res, expected_data)
        self.assertEqual(widths, expected_width)

    def test_get_formated_date(self):
        date = timezone.now()
        formated_date = formats.date_format(date, 'DATETIME_FORMAT')
        result = self.object.get_formated_date(date, icon=True)
        src = settings.PROJECT_DIR + '/static/images/calendar.png'

        expected = "<img src='" + src + "' width='21' height='15'" +\
            " valign='-4'></img>" + formated_date
        self.assertEqual(result, expected)

    def test_get_formated_date_no_icon(self):
        date = timezone.now()
        formated_date = formats.date_format(date, 'DATETIME_FORMAT')
        result = self.object.get_formated_date(date)
        self.assertEqual(result, formated_date)

    def test_render_all(self):
        document = self.object.render()
        self.assertIn('ReportLab', document)
