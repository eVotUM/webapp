# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.forms import modelformset_factory
from django import forms

from .models import Document


class DocumentForm(forms.ModelForm):
    """ """
    class Meta:
        model = Document
        fields = ('designation', 'document')


class OrderedDocumentForm(forms.ModelForm):
    """ """
    class Meta:
        model = Document
        fields = ('designation', 'document', 'order')


OrderedDocumentFormset = modelformset_factory(
    Document, extra=0, min_num=1, form=OrderedDocumentForm, can_delete=True,
    validate_min=True)

DocumentFormset = modelformset_factory(
    Document, extra=0, min_num=1, form=DocumentForm, can_delete=True)
