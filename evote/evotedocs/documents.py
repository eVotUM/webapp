# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from io import BytesIO

from django.utils.translation import ugettext as _
from django.utils import formats
from django.conf import settings

from reportlab.platypus.flowables import HRFlowable
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import cm
from reportlab.lib.utils import ImageReader
from reportlab.platypus import (
    SimpleDocTemplate, Paragraph, Table, TableStyle, Image, Spacer, LongTable,
    Frame)
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors

from evotecore.utils import exec_time


class PdfDocument(object):
    """ """
    story = []
    footer = []
    custom_styles = {}

    def __init__(self):
        """ """
        self.buffer = BytesIO()
        self.styles = getSampleStyleSheet()
        self.define_fonts()
        self.define_styles()
        self.doc = SimpleDocTemplate(
            self.buffer, rightMargin=30, leftMargin=40, topMargin=30,
            bottomMargin=30, pagesize=A4)

    def set_story(self):
        pass

    def set_footer(self):
        pass

    def get_title(self):
        return _('PDF Document')

    def render(self):
        """ """
        with exec_time(self.__class__.__name__):
            self.set_story()
            self.set_footer()
            self.doc.title = self.get_title()
            self.doc.build(self.story, onFirstPage=self.draw_page)
            document = self.buffer.getvalue()
            self.buffer.close()
        return document

    def draw_page(self, canvas, doc):
        footer = self.footer
        Frame(doc.leftMargin, 0, doc.width, 1.5 * cm).addFromList(
            footer, canvas)

    def define_fonts(self):
        """ """
        pdfmetrics.registerFont(
            TTFont('RobotoRegular', settings.PROJECT_DIR +
                   '/static/fonts/Roboto-Regular-webfont.ttf'))
        pdfmetrics.registerFont(
            TTFont('RobotoLight', settings.PROJECT_DIR +
                   '/static/fonts/Roboto-Light-webfont.ttf'))

    def define_styles(self):
        """
        Define custom evote styles for PDF
        """
        self.custom_styles['title-row-h1'] = ParagraphStyle(
            'title-row-h1',
            parent=self.styles['Heading1'],
            fontSize=13.5,
            textColor=colors.HexColor(0x7f96b8),
            textTransform='uppercase',
            fontName='RobotoRegular'
        )
        self.custom_styles['pure-sec-title'] = ParagraphStyle(
            'pure-sec-title',
            parent=self.styles['Heading3'],
            fontSize=12,
            textColor=colors.HexColor(0x2c323f),
            backColor=colors.HexColor(0xf1f4f8),
            fontName='RobotoRegular',
            borderPadding=(8, 0, 8, 15)
        )
        self.custom_styles['label'] = ParagraphStyle(
            'label',
            fontSize=12,
            textColor=colors.HexColor(0x7f96b8),
            leading=15
        )
        self.custom_styles['body'] = ParagraphStyle(
            'body',
            parent=self.styles['BodyText'],
            fontSize=10,
            textColor=colors.black,
            leading=15
        )
        self.custom_styles['thcell'] = ParagraphStyle(
            'thcell',
            fontSize=11,
            textColor=colors.HexColor(0x7f96b8),
            fontName='RobotoRegular'
        )
        self.custom_styles['tdcell'] = ParagraphStyle(
            'tdcell',
            fontSize=10,
            textColor=colors.black,
            fontName='RobotoLight'
        )

    def space(self, width=10, height=10):
        """ Create vertical space """
        return Spacer(width, height)

    def hr(self, width="100%", thickness=1, line_cap='round',
           color=colors.HexColor(0xe4eaf1), space_before=0, space_after=0):
        """ Create horizontal rule """
        return HRFlowable(
            width=width, thickness=thickness, lineCap=line_cap, color=color,
            spaceBefore=space_before, spaceAfter=space_after)

    def title(self, text):
        """ Create title """
        return Paragraph(text, self.custom_styles["title-row-h1"])

    def body(self, text):
        """ Create text with body style """
        return Paragraph(text, self.custom_styles['body'])

    def pure_sec_title(self, text):
        """ Create pure-sec-title text """
        return Paragraph(text, self.custom_styles["pure-sec-title"])

    def label(self, text):
        """ Create label """
        return Paragraph(text, self.custom_styles["label"])

    def static_image(self, path, width):
        """ Create image """
        src = settings.PROJECT_DIR + path
        return self.image(src, width)

    def image(self, path, width):
        """ Create image """
        img = ImageReader(path)
        iw, ih = img.getSize()
        aspect = ih / float(iw)
        return Image(path, width=width, height=(width * aspect))

    def thcell(self, text):
        """ Create text with body style """
        return Paragraph(text, self.custom_styles['thcell'])

    def tdcell(self, text):
        """ Create text with body style """
        return Paragraph(text, self.custom_styles['tdcell'])

    def add_col_space(self, data, col_widths=None, col_space=10):
        """ Adds empty space between collums """
        for i, line in enumerate(data):
            for j, col in enumerate(line):
                if j != 0 and j % 2 != 0:
                    line.insert(j, '')
                    if i == 0:
                        col_widths.insert(j, col_space)

        return data, col_widths

    def get_formated_date(self, date, icon=False):
        """ Returns the date in defined format """
        src = settings.PROJECT_DIR + '/static/images/calendar.png'
        calendar_icon = ''
        if icon:
            calendar_icon = "<img src='" + src +\
                "' width='21' height='15' valign='-4'></img>"
        result = calendar_icon + formats.date_format(date, 'DATETIME_FORMAT')
        return result

    def calc_col_widths(self, *percentages):
        total = sum(percentages)
        if total > 1:
            raise ValueError("total is higher than 1")
        return [self.doc.width * perc for perc in percentages]

    def table(self, data, space_before=17, space_after=17, col_widths=None,
              col_percs=None, header=True):
        """ Returns simple table """
        if col_percs:
            col_widths = self.calc_col_widths(*col_percs)

        table_style = TableStyle()
        table_style.add('FONTNAME', (0, 0), (-1, -1), 'RobotoLight')
        table_style.add('FONTSIZE', (0, 0), (-1, -1), 10)
        table_style.add('VALIGN', (0, 0), (-1, -1), 'TOP')
        if header:
            table_style.add('FONTNAME', (0, 0), (-1, 0), 'RobotoRegular')
            table_style.add('FONTSIZE', (0, 0), (-1, 0), 11)
            table_style.add(
                'TEXTCOLOR', (0, 0), (-1, 0), colors.HexColor(0x7f96b8))
            table_style.add('VALIGN', (0, 0), (-1, 0), 'BOTTOM')

        table = Table(data, spaceBefore=space_before, spaceAfter=space_after,
                      colWidths=col_widths)
        table.setStyle(table_style)
        return table

    def long_table(self, data, col_widths=None, col_percs=None):
        """ Returns long table with width 100% and optimized for huge data """
        if col_percs:
            col_widths = self.calc_col_widths(*col_percs)

        table_style = TableStyle()
        table_style.add('FONTNAME', (0, 0), (-1, -1), 'RobotoLight')
        table_style.add('FONTSIZE', (0, 0), (-1, -1), 10)
        table_style.add('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
        table_style.add('ROWBACKGROUNDS', (0, 0), (-1, -1),
                        (colors.HexColor(0xf1f4f8), None))
        # style header
        table_style.add('FONTNAME', (0, 0), (-1, 0), 'RobotoRegular')
        table_style.add('FONTSIZE', (0, 0), (-1, -1), 11)
        table_style.add(
            'TEXTCOLOR', (0, 0), (-1, 0), colors.HexColor(0x7f96b8))
        table_style.add(
            'LINEBELOW', (0, 0), (-1, 0), 1, colors.HexColor(0xa7b7cf))
        table_style.add('VALIGN', (0, 0), (-1, 0), 'BOTTOM')
        table_style.add('BOTTOMPADDING', (0, 0), (-1, 0), 11)

        table = LongTable(data, colWidths=col_widths, rowHeights=29,
                          repeatRows=1)
        table.setStyle(table_style)
        return table
