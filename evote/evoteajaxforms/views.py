# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.views.generic.detail import SingleObjectMixin
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.http import JsonResponse

from evotecore.mixins import FeedbackMessageMixin

from .utils import add_errors_prefix_form, add_errors_prefix_formset


class AjaxResponseAction():

    NOTHING = "nothing"
    REDIRECT = "redirect"
    REFRESH = "refresh"

    choices = (
        NOTHING,
        REDIRECT,
        REFRESH
    )


class FormJsonSimpleView(FeedbackMessageMixin, FormView):
    """ """
    action = AjaxResponseAction.REDIRECT
    success_message = _("The operations were successfully performed")

    def form_invalid(self, form, prefix=None):
        """return a json response with a list with fields errors"""
        data = {
            "errors_list": self.add_prefix(form.errors, prefix),
        }
        self.set_error_message()
        return JsonResponse(data)

    def form_valid(self, form):
        """save the form and return json response"""
        if hasattr(form, 'save'):
            self.object = form.save()
        self.set_success_message()
        return self.json_to_response()

    def add_prefix(self, errors, prefix):
        """Add form prefix to errors"""
        if not prefix:
            prefix = self.get_prefix()
        if prefix:
            return add_errors_prefix_form(errors, prefix)
        return errors

    def json_to_response(self, action=None, success_url=None):
        """valid response with next action to be followed by the javascript"""
        self.action = action or self.get_action()
        if self.action not in AjaxResponseAction.choices:
            raise ValueError(
                "Invalid action selected: '{}'".format(self.action))
        data = {
            "action": self.action,
        }
        if self.action == AjaxResponseAction.REDIRECT:
            data["action_url"] = success_url or self.get_success_url()
        return JsonResponse(data)

    def formset_invalid(self, formset, prefix=None):
        """like form_invalid, but return field errors with prefixed indexes
        too
        """
        if not prefix:
            prefix = self.get_prefix()
        data = {
            "errors_list": add_errors_prefix_formset(
                formset.errors, formset.non_form_errors(), prefix)
        }
        self.set_error_message()
        return JsonResponse(data)

    def get_form_kwargs(self):
        """ """
        kwargs = super(FormJsonSimpleView, self).get_form_kwargs()
        if hasattr(self, 'object') and \
                (not hasattr(self.form_class, '_meta') or
                    self.model == self.form_class._meta.model):
            kwargs.update({'instance': self.object})
        return kwargs

    def get_action(self):
        return self.action


class FormJsonView(SingleObjectMixin, FormJsonSimpleView):
    """View for forms that support validation by ajax and submition by http
    default behavior.
    """

    def get(self, request, *args, **kwargs):
        """ """
        self.object = self.get_object()
        return super(FormJsonView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """ """
        self.object = self.get_object()
        return super(FormJsonView, self).post(request, *args, **kwargs)


class FormsetJsonSimpleView(FormJsonSimpleView):
    """ """
    def form_invalid(self, form, prefix=None):
        return self.formset_invalid(form, prefix)


class FormsetJsonView(FormJsonView):
    """ """
    def form_invalid(self, form, prefix=None):
        return self.formset_invalid(form, prefix)


class PartialJsonMixin(TemplateView):
    """ """

    def get(self, request, *args, **kwargs):
        """ """
        context = self.get_context_data()
        data = {
            "content": render_to_string(self.template_name, context,
                                        request=request)
        }
        return JsonResponse(data)


class FormPartialJsonView(PartialJsonMixin, FormJsonSimpleView):
    """View for forms that support validation and submition by ajax.
    Added an action argument wich is passed to javascript to indicate what
    to do after a valid submition.
    """
    action = AjaxResponseAction.NOTHING


class PartialJsonView(PartialJsonMixin, TemplateView):
    """ """


class DetailPartialJsonView(SingleObjectMixin, PartialJsonView):
    """ """

    def get(self, request, *args, **kwargs):
        """ """
        if not getattr(self, "object", None):
            self.object = self.get_object()
        return super(DetailPartialJsonView, self).get(request, *args, **kwargs)
