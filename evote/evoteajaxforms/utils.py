# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.




def add_errors_prefix_form(errors, prefix):
    return {"%s-%s" % (prefix, k): v for k, v in errors.items()}


def add_errors_prefix_formset(errors, non_form_errors, prefix):
    prefixed_errors = {}
    for index, form_errors in enumerate(errors):
        prefixed_errors.update(
            add_errors_prefix_form(form_errors, "{}-{}".format(prefix, index)))

    if non_form_errors and '__all__' not in prefixed_errors:
        prefixed_errors['__all__'] = non_form_errors
    elif non_form_errors:
        prefixed_errors['__all__'].append(non_form_errors)
    return prefixed_errors
