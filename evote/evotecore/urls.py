# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


"""Evotecore URL configuration."""
from __future__ import unicode_literals

from django.conf.urls import url
from django.conf import settings

from evotevotings.admin.views import PageDetailView

from .views import DemoView, HomepageView


app_name = "evotecore"

urlpatterns = [
    url(
        regex=r'^$',
        view=HomepageView.as_view(),
        name="index"
    ),
    url(
        regex=r'page/(?P<slug>[-\w]+)/$',
        view=PageDetailView.as_view(),
        name="page-detail"
    ),
]


if settings.DEBUG:
    urlpatterns += [
        url(
            regex=r'^demo/$',
            view=DemoView.as_view(),
            name="demo"
        )
    ]
