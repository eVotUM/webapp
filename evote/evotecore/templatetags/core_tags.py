# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation.trans_real import language_code_prefix_re
from django.core.urlresolvers import reverse
from django.utils.text import capfirst
from django.conf import settings
from django import template

import re

register = template.Library()


@register.filter
def num_range(self):
    return range(1, self + 1)


@register.inclusion_tag('evotecore/includes/breadcrumb.html')
def breadcrumb(title, url=None, sep=None, no_sep=None, hashtag=None,
               **kwargs):
    """ """
    if url:
        url = reverse(url, kwargs=kwargs)
        if hashtag:
            url += "#{}".format(hashtag)
    elif no_sep is None:
        no_sep = True
    return {
        'title': title,
        'url': url,
        'sep': sep,
        'no_sep': no_sep
    }


@register.inclusion_tag('evotecore/includes/pagination.html',
                        takes_context=True)
def pagination(context, num_pages, has_prev, has_next, current=1, **kwargs):
    """ Template tag to paginate.
        Create pagination with separators, example:
        From: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] and current page 6
        Create: [1, 'sep', 5, 6, 7, 'sep', 10]
    """
    final_pages = pages_list = range(1, num_pages + 1)

    if num_pages > 5:
        begin = []
        end = []
        if current == 1:
            middle = pages_list[(current - 1):(current + 2)]
        elif current == num_pages:
            middle = pages_list[(current - 3):current]
        else:
            middle = pages_list[(current - 2):(current + 1)]

        if current > 2:
            begin = pages_list[:1] + ['sep']
            if current < 5:
                begin = pages_list[:(current - 2)]

        if (num_pages - current) > 1:
            end = ['sep'] + pages_list[-1:]
            if (num_pages - current) < 4:
                end = pages_list[(current + 1):]

        final_pages = begin + middle + end

    prev_page = 0
    if has_prev:
        prev_page = current - 1

    next_page = 0
    if has_next:
        next_page = current + 1

    context.update({
        'num_pages': num_pages,
        'final_pages': final_pages,
        'prev_page': prev_page,
        'next_page': next_page,
        'current': current,
    })
    return context


@register.simple_tag
def debug(value):
    """ debug method """
    print "dict:", value.__dict__
    print "dir :", dir(value)


@register.simple_tag(takes_context=True)
def accessibility_classes(context):
    """
    Reads from the accessibility cookie in order to decide if
    extra classes attached to body are needed.
    If yes, those classes are returned in string form.
    """

    request = context['request']
    theme = request.COOKIES.get('ev_access-color', '')
    font = request.COOKIES.get('ev_access-text', '')

    classes = ''

    if theme and theme != 'false':
        classes = 'access-color'

    if font and font != 'false':
        classes = classes + ' access-text'

    return classes


@register.filter
def model_verbose_name(obj, name):
    return capfirst(obj._meta.get_field(name).verbose_name)


@register.inclusion_tag('evotecore/includes/google_analytics.html')
def google_analytics(**kwargs):
    """ Template tag to add Google Analytics """
    return {
        'analytics_id': getattr(settings, 'GOOGLE_ANALYTICS_ID', None),
    }


@register.filter
def strip_lang_prefix(url):
    """
    This filter removes the language prefix from URL
    """
    return re.sub(language_code_prefix_re, '/', url)
