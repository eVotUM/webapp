# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django import template

from django.forms import BoundField

from evotecore.widgets import DateInput, DateTimeInput


register = template.Library()


@register.inclusion_tag('evotecore/includes/field_tags.html')
def prepare_field(field, icon="", light_field=False, use_label=False):
    """ """
    if not isinstance(field, BoundField):
        raise TypeError("provided field has invalid type")
    if isinstance(field.field.widget, DateInput) or \
            isinstance(field.field.widget, DateTimeInput):
        icon = icon or "fi-calendar"
    return {
        'field': field,
        'widget': field.field.widget.__class__.__name__,
        'icon': icon,
        'use_label': use_label,
        'light_field': light_field,
    }
