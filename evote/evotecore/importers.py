# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from csvvalidator import CSVValidator
import unicodecsv as csv
import logging

from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


logger = logging.getLogger('evote.importers')


class BaseImporter(object):
    """ """
    DELIMITER = b';'

    error_messages = {
        'invalid_header': _("Invalid header"),
        'invalid_record_length': _("Invalid record length"),
    }

    def __init__(self, delimiter=None):
        """ """
        if delimiter:
            self.DELIMITER = delimiter

    def run(self):
        raise NotImplementedError()

    def validate(self, document, limit=0, verbose_errors=False):
        """Raises UnicodeDecodeError if charset not supported"""
        try:
            data = self.get_csv_data(document)
            result = self.get_validator().validate(data, limit=limit)
            if verbose_errors:
                return self.get_verbose_errors(result, limit)
            return result
        except UnicodeDecodeError:
            logger.warning("File charset not supported", exc_info=True)
            raise ValidationError(_("File charset not supported (valid "
                                    "charsets: utf-8)"))

    def get_validator(self):
        """ """
        validator = CSVValidator(self.field_names)
        validator.add_header_check(
            message=self.error_messages['invalid_header'])
        validator.add_record_length_check(
            message=self.error_messages['invalid_record_length'])
        return validator

    def get_verbose_errors(self, errors, limit):
        messages = [
            _("(row {}) {}".format(e['row'], e['message'].encode('utf-8')))
            for e in errors
        ]
        if limit and limit <= len(messages):
            messages.append("...")
        return messages

    def get_csv_data(self, document, has_dict=False):
        kwargs = {
            'delimiter': self.DELIMITER,
            'encoding': 'utf-8-sig'  # to avoid problems with BOM flag
        }
        reader = csv.reader if not has_dict else csv.DictReader
        data = reader(document, **kwargs)
        return data
