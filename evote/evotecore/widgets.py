# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.forms import widgets


class Select(widgets.Select):

    def __init__(self, *args, **kwargs):
        super(Select, self).__init__(*args, **kwargs)
        self.attrs.update({'data-style-select': ''})


class SearchableSelect(widgets.Select):

    def __init__(self, *args, **kwargs):
        super(SearchableSelect, self).__init__(*args, **kwargs)
        self.attrs.update({'data-style-select-search': ''})


class MultipleSelect(widgets.SelectMultiple):

    def __init__(self, *args, **kwargs):
        super(MultipleSelect, self).__init__(*args, **kwargs)
        self.attrs.update({'data-style-select': ''})


class MultipleAjaxSelect(widgets.SelectMultiple):

    def __init__(self, *args, **kwargs):
        super(MultipleAjaxSelect, self).__init__(*args, **kwargs)
        self.attrs.update({'data-style-select-ajax': ''})


class DateInput(widgets.DateInput):

    def __init__(self, *args, **kwargs):
        super(DateInput, self).__init__(*args, **kwargs)
        self.attrs.update({'data-datepicker': '', 'readonly': 'readonly'})


class DateTimeInput(widgets.DateTimeInput):

    def __init__(self, *args, **kwargs):
        super(DateTimeInput, self).__init__(*args, **kwargs)
        self.attrs.update({'data-datetimepicker': '', 'readonly': 'readonly'})


class TinymceTextarea(widgets.Textarea):

    def __init__(self, *args, **kwargs):
        super(TinymceTextarea, self).__init__(*args, **kwargs)
        self.attrs.update({'data-tinymce': ''})
