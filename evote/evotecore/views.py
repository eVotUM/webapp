# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.views.generic import TemplateView
from django.shortcuts import redirect
from django.conf import settings


class DemoView(TemplateView):
    """Demo view."""

    template_name = "evotecore/demo.html"


class HomepageView(TemplateView):
    """Homepage view."""
    template_name = "evotecore/index.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(settings.LOGIN_REDIRECT_URL)
        return super(HomepageView, self).get(request, *args, **kwargs)


class ErrorBaseView(TemplateView):
    """ Error base view """
    error = None

    @classmethod
    def get_view_rendered(cls, error_code):
        view = cls.as_view(error=error_code)

        def view_function(request):
            view_instance = view(request)
            if hasattr(view_instance, 'render'):
                return view_instance.render()
            return view_instance

        return view_function

    def get(self, request, *args, **kwargs):
        response = super(ErrorBaseView, self).get(request, *args, **kwargs)
        response.status_code = self.error
        return response

    def get_context_data(self, **kwargs):
        context = super(ErrorBaseView, self).get_context_data(**kwargs)
        context.update({
            'template': 'evotecore/includes/errors/{}.html'.format(self.error),
            'show_raven_id': getattr(settings, 'RAVEN_SHOW_ID_ON_ERROR', False)
        })
        return context
