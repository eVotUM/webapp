# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.test import TestCase
from django.conf import settings

from evoteusers.tests.utils import UserTestUtils


class HomepageViewTest(UserTestUtils, TestCase):
    """ """

    def test_get(self):
        """ """
        response = self.client.get(reverse("evotecore:index"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "evotecore/index.html")

    def test_redirect_to_authenticated_users(self):
        """ """
        self.login()
        response = self.client.get(reverse("evotecore:index"))
        self.assertRedirects(response, settings.LOGIN_REDIRECT_URL)
