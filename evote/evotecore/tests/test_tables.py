# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.formats import localize
from django.utils import timezone
from django.test import SimpleTestCase

from evotecore.tables import DateTimeColumn, ImageColumn


class TableColumnsTest(SimpleTestCase):
    """
    Test custom columns
    """

    def test_date_column(self):
        col_date = DateTimeColumn()
        date = timezone.now()
        date_with_timezone = localize(timezone.localtime(date))

        content = col_date.render(date)
        expected = "<i class='ev-icon evi-calendar alg-l'></i>" +\
            date_with_timezone

        self.assertEqual(content, expected)

    def test_image_column(self):
        col_image = ImageColumn()
        content = col_image.render(None)
        expected = ""

        self.assertEqual(content, expected)
