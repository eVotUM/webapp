# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import SimpleTestCase

from evotecore.utils import documents_upload_to, images_upload_to, compare_data


class UtilsCase(SimpleTestCase):
    """ """

    def test_documents_upload_to_path(self):
        path = documents_upload_to(None, "test.txt")
        self.assertIn("documents", path)
        self.assertIn("test.txt", path)

    def test_images_upload_to_path(self):
        path = images_upload_to(None, "test.txt")
        self.assertIn("images", path)
        self.assertIn("test.txt", path)

    def test_compare_data_with_dicts(self):
        dict1 = {"key1": "test", "key2": "test"}
        dict2 = {"key1": "test", "key2": "test"}
        self.assertTrue(compare_data(dict1, dict2, ["key1", "key2"]))
        dict1["key1"] = "test2"
        self.assertFalse(compare_data(dict1, dict2, ["key1", "key2"]))

    def test_compare_data_with_objects(self):
        object1 = type(str("test"), (object,), dict(key1="test", key2="test"))
        object2 = type(str("test"), (object,), dict(key1="test", key2="test"))
        self.assertTrue(compare_data(object1, object2, ["key1", "key2"]))
        object1.key1 = "test2"
        self.assertFalse(compare_data(object1, object2, ["key1", "key2"]))

    def test_compare_data_with_objects_as_attrs(self):
        obj = object()
        dict1 = {"key1": "test", "key2": obj}
        dict2 = {"key1": "test", "key2": obj}
        self.assertTrue(compare_data(dict1, dict2, ["key1", "key2"]))

    def test_compare_data_with_encoded_and_unicoded_str_as_attrs(self):
        dict1 = {"key1": "test", "key2": "test".encode("utf-8")}
        dict2 = {"key1": "test", "key2": "test"}
        self.assertTrue(compare_data(dict1, dict2, ["key1", "key2"]))

    def test_compare_data_with_special_characters(self):
        dict1 = {"key1": "test", "key2": "test é"}
        dict2 = {"key1": "test", "key2": "test é"}
        self.assertTrue(compare_data(dict1, dict2, ["key1", "key2"]))
