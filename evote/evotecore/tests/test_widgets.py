# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import SimpleTestCase

from evotecore.widgets import (Select, SearchableSelect, DateInput,
                               DateTimeInput)


class WidgetsCase(SimpleTestCase):
    """ """

    def test_select_widget(self):
        widget = Select()
        self.assertIn('data-style-select', widget.attrs)

    def test_searchableselect_widget(self):
        widget = SearchableSelect()
        self.assertIn('data-style-select-search', widget.attrs)

    def test_dateinput_widget(self):
        widget = DateInput()
        self.assertIn('data-datepicker', widget.attrs)

    def test_datetimeinput_widget(self):
        widget = DateTimeInput()
        self.assertIn('data-datetimepicker', widget.attrs)
