# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.template import Context, Template
from django.test import SimpleTestCase, override_settings, RequestFactory
from django import forms

from evotecore.templatetags.core_tags import pagination, google_analytics


class TemplateTagsTest(SimpleTestCase):

    def render_template(self, string, context=None, request=None):
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


class CoreTagsTest(TemplateTagsTest):
    """ TODO: test breadcrumbs with urls"""

    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()

    def test_num_range(self):
        template = "{% load core_tags %}{{ 3|num_range }}"
        rendered = self.render_template(template)
        self.assertEqual(rendered, "[1, 2, 3]")

    def test_num_range_with_non_integer(self):
        template = "{% load core_tags %}{{ 'a'|num_range }}"
        with self.assertRaises(TypeError):
            self.render_template(template)

    def test_breadcrumb(self):
        template = "{% load core_tags %}{% breadcrumb 'Breadcrumb' %}"
        expected = "<li title='Breadcrumb'>Breadcrumb</li>"
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    def test_breadcrumb_with_separator(self):
        template = (
            "{% load core_tags %}{% breadcrumb 'Breadcrumb' no_sep=False %}"
        )
        expected = (
            "<li title='Breadcrumb'>Breadcrumb</li>"
            "<li class='breadcrumb-sep'>»</li>"
        )
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    def test_breadcrumb_with_different_separator(self):
        template = (
            "{% load core_tags %}{% breadcrumb 'Breadcrumb' no_sep=False "
            "sep='->' %}"
        )
        expected = (
            "<li title='Breadcrumb'>Breadcrumb</li>"
            "<li class='breadcrumb-sep'>-></li>"
        )
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    @override_settings(ROOT_URLCONF="evotecore.tests.urls")
    def test_breadcrumb_with_url(self):
        template = (
            "{% load core_tags %}{% breadcrumb 'Breadcrumb' 'test' %}"
        )
        expected = (
            "<li title='Breadcrumb'><a href='/test_url/'>Breadcrumb</a></li>"
            "<li class='breadcrumb-sep'>»</li>"
        )
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    @override_settings(ROOT_URLCONF="evotecore.tests.urls")
    def test_breadcrumb_with_url_and_url_kwargs(self):
        template = (
            "{% load core_tags %}{% breadcrumb 'Breadcrumb' 'test-kwargs' "
            "pk=10 %}"
        )
        expected = (
            "<li title='Breadcrumb'><a href='/test_url/10/'>Breadcrumb</a>"
            "</li><li class='breadcrumb-sep'>»</li>"
        )
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    @override_settings(ROOT_URLCONF="evotecore.tests.urls")
    def test_breadcrumb_with_url_and_no_separator(self):
        template = (
            "{% load core_tags %}{% breadcrumb 'Breadcrumb' 'test' "
            "no_sep=True %}"
        )
        expected = (
            "<li title='Breadcrumb'><a href='/test_url/'>Breadcrumb</a></li>"
        )
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    def test_pagination(self):
        template = "{% load core_tags %}{% pagination 1 False False 1 %}"
        expected = ""
        rendered = self.render_template(template)
        self.assertInHTML(expected, rendered)

    def test_pagination_with_pages(self):
        request = self.factory.get('/test/')
        template = "{% load core_tags %}{% pagination 5 False False 1 %}"
        expected = (
            '<div class="pure-pag">'
            '  <ul>'
            '    <li class="previous"><a href="#" class="disabled"></a></li>'
            '    <li><a href="?page=1" class="selected">1</a></li>'
            '    <li><a href="?page=2" >2</a></li>'
            '    <li><a href="?page=3" >3</a></li>'
            '    <li><a href="?page=4" >4</a></li>'
            '    <li><a href="?page=5" >5</a></li>'
            '    <li class="next"><a href="#" class="disabled"></a></li>'
            '  </ul>'
            '</div>'
        )
        rendered = self.render_template(template, {'request': request})
        self.assertInHTML(expected, rendered)

    def test_pagination_with_function_case_first_page(self):
        result = pagination({}, 10, False, True, 1)
        expected = {
            'num_pages': 10,
            'final_pages': [1, 2, 3, 'sep', 10],
            'next_page': 2,
            'prev_page': 0,
            'current': 1
        }
        self.assertEqual(result, expected)

    def test_pagination_with_function_case_middle_page(self):
        result = pagination({}, 10, True, True, 6)
        expected = {
            'num_pages': 10,
            'final_pages': [1, 'sep', 5, 6, 7, 'sep', 10],
            'next_page': 7,
            'prev_page': 5,
            'current': 6
        }
        self.assertEqual(result, expected)

    def test_pagination_with_function_last_page(self):
        result = pagination({}, 10, True, False, 10)
        expected = {
            'num_pages': 10,
            'final_pages': [1, 'sep', 8, 9, 10],
            'next_page': 0,
            'prev_page': 9,
            'current': 10
        }
        self.assertEqual(result, expected)

    def test_pagination_with_function_inicial_pages(self):
        result = pagination({}, 10, True, True, 4)
        expected = {
            'num_pages': 10,
            'final_pages': [1, 2, 3, 4, 5, 'sep', 10],
            'next_page': 5,
            'prev_page': 3,
            'current': 4
        }
        self.assertEqual(result, expected)

    def test_pagination_with_function_final_pages(self):
        result = pagination({}, 10, True, True, 8)
        expected = {
            'num_pages': 10,
            'final_pages': [1, 'sep', 7, 8, 9, 10],
            'next_page': 9,
            'prev_page': 7,
            'current': 8
        }
        self.assertEqual(result, expected)

    @override_settings(GOOGLE_ANALYTICS_ID="unitID")
    def test_google_analytics(self):
        request = self.factory.get('/test/')
        template = "{% load core_tags %}{% google_analytics %}"
        rendered = self.render_template(template, {'request': request})
        tag_result = google_analytics()
        expected = {'analytics_id': 'unitID'}
        self.assertEqual(tag_result, expected)
        self.assertIn('unitID', rendered)

    @override_settings(GOOGLE_ANALYTICS_ID=None)
    def test_google_analytics_no_id(self):
        request = self.factory.get('/test/')
        template = "{% load core_tags %}{% google_analytics %}"
        expected_html = '\n'
        rendered = self.render_template(template, {'request': request})
        tag_result = google_analytics()
        expected = {'analytics_id': None}
        self.assertEqual(tag_result, expected)
        self.assertEqual(rendered, expected_html)

    def test_strip_lang_prefix_en(self):
        template = "{% load core_tags %}{{'/en/unit/test'|strip_lang_prefix}}"
        rendered = self.render_template(template)
        self.assertEqual(rendered, "/unit/test")

    def test_strip_lang_prefix_pt(self):
        template = "{% load core_tags %}{{'/pt/unit/test'|strip_lang_prefix}}"
        rendered = self.render_template(template)
        self.assertEqual(rendered, "/unit/test")


class FormTagsTest(TemplateTagsTest):

    class TestForm(forms.Form):
        field = forms.CharField(label='Test')

    def test_prepare_field(self):
        template = "{% load form_tags %}{% prepare_field form.field %}"
        expected = (
            "<div id='field_id_field' class='pure-field textinput'>"
            "<div class='field-wrapper'>"
            "<input id='id_field' name='field' type='text' required /></div>"
            "</div>"
        )
        rendered = self.render_template(template, {"form": self.TestForm()})
        self.assertInHTML(expected, rendered)

    def test_prepare_field_with_label(self):
        template = (
            "{% load form_tags %}{% prepare_field form.field use_label=True %}"
        )
        expected = (
            "<div id='field_id_field' class='pure-field textinput'>"
            "<label for='id_field'>Test:</label>"
            "<div class='field-wrapper'>"
            "<input id='id_field' name='field' type='text' required /></div>"
            "</div>"
        )
        rendered = self.render_template(template, {"form": self.TestForm()})
        self.assertInHTML(expected, rendered)

    def test_prepare_field_with_icon(self):
        template = (
            "{% load form_tags %}{% prepare_field form.field icon='icon' %}"
        )
        expected = (
            "<div id='field_id_field' class='pure-field fi-icon icon "
            "textinput'>"
            "<div class='field-wrapper'>"
            "<input id='id_field' name='field' type='text' required /></div>"
            "</div>"
        )
        rendered = self.render_template(template, {"form": self.TestForm()})
        self.assertInHTML(expected, rendered)

    def test_prepare_field_with_light_field(self):
        template = (
            "{% load form_tags %}{% prepare_field form.field "
            "light_field=True %}"
        )
        expected = (
            "<div id='field_id_field' class='pure-field light-field "
            "textinput'>"
            "<div class='field-wrapper'>"
            "<input id='id_field' name='field' type='text' required /></div>"
            "</div>"
        )
        rendered = self.render_template(template, {"form": self.TestForm()})
        self.assertInHTML(expected, rendered)

    def test_prepare_field_with_invalid_field_type(self):
        template = "{% load form_tags %}{% prepare_field form %}"
        with self.assertRaises(TypeError):
            self.render_template(template, {"form": self.TestForm()})
