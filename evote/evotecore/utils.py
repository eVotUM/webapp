# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging
import hashlib
import time
import six
from contextlib import contextmanager
from datetime import date

from guardian.models import Permission, UserObjectPermission


logger = logging.getLogger("evote.core")


def documents_upload_to(instance, filename):
    today = date.today().strftime("%Y/%m/%d")
    return "uploads/%s/documents/%s" % (today, filename)


def images_upload_to(instance, filename):
    today = date.today().strftime("%Y/%m/%d")
    return "uploads/%s/images/%s" % (today, filename)


def compare_data(model_or_dict1, model_or_dict2, keys):
    """ """
    uhash1 = calculate_hash(model_or_dict1, keys)
    uhash2 = calculate_hash(model_or_dict2, keys)
    return uhash1 == uhash2


def calculate_hash(model_or_dict, keys):
    uhash = ""
    for key in keys:
        if isinstance(model_or_dict, dict):
            value = model_or_dict.get(key)
        else:
            value = getattr(model_or_dict, key)
        if value:
            if isinstance(value, six.string_types):
                uhash = "{}{}".format(uhash, value)
            else:
                uhash += repr(value)
    return hashlib.md5(uhash.encode('utf-8')).hexdigest()


def list_portions(alist, batch):
    return [alist[i:i + batch] for i in range(0, len(alist), batch)]


@contextmanager
def exec_time(name):
    start = time.time()
    yield
    finish = time.time()
    logger.debug("%s in: %.5fs", name, finish - start)


def get_perm(perm):
    try:
        app_label, codename = perm.split('.', 1)
    except ValueError:
        raise ValueError("First argument must be in format: "
                         "'app_label.codename' (is %r)" % perm)
    return Permission.objects.get(
        content_type__app_label=app_label, codename=codename)


def assign_bulk_perms(perm, user_obj_perms):
    """
    Assigns permission with given ``perm`` to a bulk ``user_obj_perms`` data.
    """
    permission = get_perm(perm)
    # force query
    assigns = []
    for user_obj_perm in user_obj_perms:
        assigns.append(UserObjectPermission(
            permission_id=permission.pk,
            content_type_id=permission.content_type_id,
            **user_obj_perm
        ))
    for portion in list_portions(assigns, 3000):
        UserObjectPermission.objects.bulk_create(portion)
    return len(assigns)


def remove_bulk_perms(perm, user_ids, obj):
    """
    Removes permission ``perm`` for a ``obj`` and given ``user_ids`` queryset.
    """
    permission = get_perm(perm)
    return UserObjectPermission.filter(
        user_id__in=user_ids, object_pk=obj.pk, permission=permission).delete()
