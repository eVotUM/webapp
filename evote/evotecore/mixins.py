# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django_tables2 import SingleTableMixin
from django.contrib import messages

from evoteajaxforms.utils import add_errors_prefix_formset
from guardian import mixins


class DjangoTablesFilterMixin(SingleTableMixin):
    """
    Allows to add filters to the table
    """
    filter_class = None

    def get_filter(self, qs):
        return self.filter_class(self.request.GET, qs)

    def get_context_data(self, **kwargs):
        context = super(DjangoTablesFilterMixin, self)\
            .get_context_data(**kwargs)
        table_filter = self.get_filter(self.get_table_data())
        context.update({
            'filter': table_filter
        })
        return context

    def get_table_data(self):
        """ """
        table_filter = self.get_filter(self.get_queryset())
        return table_filter.qs


class ModelFieldChangedMixin(object):
    """ """
    _changed_prefix = "_prev_"

    def track_changes(self, fields):
        """ """
        for field in fields:
            if self.is_trackable(field):
                setattr(self, self.get_track_field_name(field),
                        getattr(self, field))

    def has_changed(self, field):
        if self.is_trackable(field):
            value = getattr(self, field)
            prev_value = getattr(self, self.get_track_field_name(field))
            return value != prev_value
        return False

    def is_tracking(self, field):
        """ """
        return hasattr(self, self.get_track_field_name(field))

    def is_trackable(self, field):
        return field not in self.get_deferred_fields() and hasattr(self, field)

    def get_prev_field(self, field):
        if hasattr(self, self.get_track_field_name(field)):
            return getattr(self, self.get_track_field_name(field))
        return None

    def get_track_field_name(self, field):
        return "{}{}".format(self._changed_prefix, field)


class NestedFormsetFormMixin(object):

    formset_class = None
    formset_prefix = None

    def __init__(self, *args, **kwargs):
        # data and files need to be declared here because if equals to None,
        # the init will change it to dict and will mark form as bounded
        data = kwargs.get('data')
        files = kwargs.get('files')
        prefix = kwargs.get('prefix', self.prefix)
        super(NestedFormsetFormMixin, self).__init__(*args, **kwargs)
        if not self.ignore():
            # bind documents formset to the form instance
            self.formset = self.formset_class(
                data=data, files=files, queryset=self.get_queryset(),
                prefix=prefix + "-" + self.formset_prefix)

    def full_clean(self):
        """Inject errors of nested formset"""
        super(NestedFormsetFormMixin, self).full_clean()
        if not self.ignore() and not self.formset.is_valid():
            self._errors[self.formset_prefix] = add_errors_prefix_formset(
                self.formset.errors, self.formset.non_form_errors(),
                self.formset.prefix)

    def has_changed(self):
        """Has changed reflects the formset too"""
        if not self.ignore():
            return super(NestedFormsetFormMixin, self).has_changed() or \
                self.formset.has_changed()
        return super(NestedFormsetFormMixin, self).has_changed()

    def ignore(self):
        """method to ignore formset initialization and validation"""
        return False


class PermissionListMixin(mixins.PermissionListMixin):
    """ """
    # HACK: login_url avoid errors on tests. never used, we always raise 403
    login_url = "/"
    get_objects_for_user_extra_kwargs = {
        "with_superuser": False,  # even superuser cant view all
        "use_groups": False  # avoid search on groups
    }


class PermissionRequiredMixin(mixins.PermissionRequiredMixin):
    """ """
    # HACK: this avoid errors on tests. never used, we always raise 403
    login_url = "/"
    return_403 = True


class SelectedMenuMixin(object):
    """ """
    selected_menu = 0

    def get_selected_menu(self):
        return self.selected_menu

    def get_context_data(self, **kwargs):
        context = super(SelectedMenuMixin, self).get_context_data(**kwargs)
        context.update({
            'selected_menu': self.get_selected_menu()
        })
        return context


class FeedbackMessageMixin(object):
    """
    Adds a success or error message on successful or error form submission.
    """
    success_message = ''
    error_message = ''

    def form_valid(self, form):
        response = super(FeedbackMessageMixin, self).form_valid(form)
        self.set_success_message()
        return response

    def form_invalid(self, form):
        response = super(FeedbackMessageMixin, self).form_invalid(form)
        self.set_error_message()
        return response

    def get_success_message(self):
        return self.success_message

    def get_error_message(self):
        return self.error_message

    def set_success_message(self, success_message=None):
        success_message = success_message or self.get_success_message()
        if success_message:
            messages.success(self.request, success_message)

    def set_error_message(self, error_message=None):
        error_message = error_message or self.get_error_message()
        if error_message:
            messages.error(self.request, error_message)
