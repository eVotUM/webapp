# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import itertools

from django.utils.html import format_html
from django.utils.formats import localize
from django.utils import timezone

import django_tables2 as tables
from sorl.thumbnail import get_thumbnail


class ImageColumn(tables.Column):
    """
    Custom Column of image
    """

    def render(self, value):
        if value:
            thumb = get_thumbnail(value, '78', crop='center', quality=99)
            if thumb:
                return format_html(u"<img src='{}' width='{}' height='{}'>",
                                   thumb.url, thumb.width, thumb.height)
        return ""


class DateTimeColumn(tables.Column):
    """
    Custom Column of Date Time field
    """

    def __init__(self, icon=True, icon_classes="alg-l", options=None, *args,
                 **kwargs):
        super(DateTimeColumn, self).__init__(*args, **kwargs)
        self.icon = icon
        self.icon_classes = icon_classes

    def render(self, value):
        icon = ""
        if self.icon:
            icon = "<i class='ev-icon evi-calendar " + self.icon_classes + \
                "'></i>"
        return format_html(icon + localize(timezone.localtime(value)))


class CounterColumn(tables.Column):
    """
    Custom Column for line numbers
    """

    def render(self):
        self.row_counter = getattr(self, 'row_counter',
                                   itertools.count(start=1))
        return next(self.row_counter)
