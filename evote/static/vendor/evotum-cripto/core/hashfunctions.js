// eVotUM - Electronic Voting System
//
// hashfunctions.js
//
// Cripto-6.0.0 - Hash Functions
//
// Copyright (c) 2016 Universidade do Minho
// Developed by André Baptista - Devise Futures, Lda. (andre.baptista@devisefutures.com)
// Reviewed by Ricardo Barroso - Devise Futures, Lda. (ricardo.barroso@devisefutures.com)
//
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//
//


//Cripto-6.1.2
evotum.hashfunctions.generateSHA256Hash = function(data) {
//  Generate SHA256 hash.
//
//  Args:
//      data (str): stream of bytes
//  Returns:
//      hash (SHA256) of the input data (hex).
  sha256Hash = forge.md.sha256.create();
  sha256Hash.update(evotum.hashfunctions.utf8Encode(data));
  return sha256Hash.digest().toHex();
}

//Cripto-6.2.2
evotum.hashfunctions.verifySHA256Hash = function(data, sha256Hash) {
//  Verifies the SHA256 hash.
//
//  Args:
//      data (str): stream of bytes
//      sha256Hash (hex): hash (SHA256) of data, to be verified
//  Returns:
//      True if SHA256(data) == sha256Hash; False otherwise.
  return (evotum.hashfunctions.generateSHA256Hash(data) == sha256Hash);
}

//utils
evotum.hashfunctions.utf8Encode = function(data) {
  return unescape(encodeURIComponent(data));
}
