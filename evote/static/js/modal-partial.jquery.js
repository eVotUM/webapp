// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery */

/******************************************************************
 * Site Name: eVote
 * Author: SFR
 *
 * Modal with content loaded by ajax support
 * https://github.com/VodkaBears/Remodal#remodal
 *****************************************************************/

"use strict";

(function($) {

    $.modalPartial = function(url, options)
    {
        var opts = $.extend({
            selector: "modalPartials",
            remodal: {}
        }, $.modalPartial.defaults, options);

        var deferred = $.Deferred();

        function getModal(id) {
            return $("[data-remodal-id=" + id + "]");
        }

        if (url) {
            var modal = getModal(opts.selector);
            modal.remodal(opts.remodal).open();

            $.ajax({
                method: "GET",
                url: url
            })
            .done(function(data) {
                modal.find(".remodal-content").html(data.content)
                    .promise().done(function(){
                        deferred.resolve(modal);
                    });
            })
            .fail(function(e) {
                deferred.reject(e);
            });
        }

        return deferred.promise();
    };

    $.modalPartial.defaults = {};


    $(document).ready(function () {

        $(document).on('click', "[data-partial-url]", function (e) {
            e.preventDefault();
            var noajaxsubmit = $(this).attr("data-noajax-submit");
            $.modalPartial( $(this).data("partial-url") ).done(function(modal){
                if(noajaxsubmit !== ''){
                    modal.find('form').djangoAjaxForms();
                }
                // Validate exit of user from forms
                modal.find('form').areYouSure();
            });
        });

    });

})(jQuery);
