// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/******************************************************************
 * Site Name: eVote
 * Author: Luis Silva
 *
 * JavaScript: noty customizations (theme and message size)
 *
 *****************************************************************/


$.noty.themes.evoteTheme = {
    name: 'evoteTheme',
    modal: {
        css: {
            position       : 'fixed',
            width          : '100%',
            height         : '100%',
            backgroundColor: '#000',
            zIndex         : 10000,
            opacity        : 0.6,
            display        : 'none',
            left           : 0,
            top            : 0
        }
    },
    style: function() {

        switch (this.options.type) {
            case 'alert': case 'notification':
                this.$bar.addClass( "pure-help" );
                this.$bar.append('<i class="ev-icon evi-msg"></i>');
                break;
            case 'warning':
                this.$bar.addClass( "pure-warning" );
                this.$bar.append('<i class="ev-icon evi-alert"></i>');
                break;
            case 'error':
                this.$bar.addClass( "pure-alert" );
                this.$bar.append('<i class="ev-icon evi-alert"></i>');
                break;
            case 'information':
                this.$bar.addClass( "pure-help" );
                this.$bar.append('<i class="ev-icon evi-help"></i>');
                break;
            case 'success':
                this.$bar.addClass( "pure-success" );
                this.$bar.append('<i class="ev-icon evi-alert-success"></i>');
                break;
        }
        this.$message.css({
            fontSize: '16px',
            lineHeight: 'auto',
        });
    },
    callback: {
        onShow: function() {  },
        onClose: function() {  }
    }
};

/*
 * This is replaced because the message width and box shadow
 * TODO: calculate responsive width based on window size
 */
$.noty.layouts.topCenter = {
    name     : 'topCenter',
    options  : { // overrides options

    },
    container: {
        object  : '<ul id="noty_topCenter_layout_container" />',
        selector: 'ul#noty_topCenter_layout_container',
        style   : function() {
            $(this).css({
                top          : 20,
                left         : 0,
                position     : 'fixed',
                width        : '600px',
                height       : 'auto',
                margin       : 0,
                padding      : 0,
                listStyleType: 'none',
                zIndex       : 10000000
            });

            $(this).css({
                left: ($(window).width() - $(this).outerWidth(false)) / 2 + 'px'
            });
        }
    },
    parent   : {
        object  : '<li />',
        selector: 'li',
        css     : {}
    },
    css      : {
        display   : 'none',
        boxShadow : '0 2px 4px rgba(0, 0, 0, 0.35)',
        width     : '600px'
    },
    addClass : ''
};
