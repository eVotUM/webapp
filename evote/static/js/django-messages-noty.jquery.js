// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, noty */

/******************************************************************
 * Site Name: eVote
 * Author: SFR
 *
 * Integrate Noty with django messages.
 * Noty check for a .noty-messages ul element and create notification
 * to every li present and associate is class to notification type.
 * TODO: support alert and information type
 *
 * http://ned.im/noty/
 *****************************************************************/

"use strict";

(function($) {

    $.fn.djangoMessagesNoty = function()
    {
        return this.each(function()
        {
            $(this).find('li').each(function() {
                var notification = $(this);
                var type = notification.attr('class');

                noty({
                    text: notification.html(),
                    type: type
                });
            });
        });
    };

})(jQuery);
