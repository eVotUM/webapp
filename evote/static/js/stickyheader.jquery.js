// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery */

"use strict";

(function($) {

	$.fn.stickyHeader = function(stickyClass, scrollOffset)
	{
		return this.each(function() {
			var $this = $(this);
			var itemHeight = $this.outerHeight(true);

			scrollOffset = typeof(scrollOffset) == "undefined" ? 0 : scrollOffset;

			function onScroll() {
				var topValue = parseInt($('body').css('padding-top'));
				if ($(window).scrollTop() > scrollOffset) {
					// Optimization
					if( $this.hasClass(stickyClass) )
						return;
					// Append the item height to the DOM
					$('body').css('padding-top', topValue + itemHeight);
					$this.css('top', topValue).addClass(stickyClass);
		        }
		        else {
		        	if( ! $this.hasClass(stickyClass) )
						return;
		        	$('body').css('padding-top', topValue - itemHeight );
		        	$this.css('top', 0).removeClass(stickyClass);
		        }
			}

			$(window).scroll(onScroll);

			// initial call
			onScroll();
		});
	};

})(jQuery);
