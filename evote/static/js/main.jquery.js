// eVotUM - Electronic Voting System
// Copyright (c) 2020 Universidade do Minho
// Developed by Eurotux (dev@eurotux.com)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* globals jQuery, django, noty, Ladda, grecaptcha */
/******************************************************************
 * Site Name: eVote
 * Author: Luis Silva
 *
 * JavaScript: Main JavaScript (Initializations, etc...)
 *
 *****************************************************************/

"use strict";

(function($) {

    $.fn.submitOnChange = function()
    {
        return this.each(function()
        {
            var form = $(this);

            form.on('change', ":input", function() {
                form.submit();
            });
        });
    };

    $.fileUpload = function(selector)
    {
        $(document).on('click', selector, function(e)
        {
            e.preventDefault();

            var $this = $(this);
            var $inputfile = $this.find("input[type='file']");
            var $inputname = $this.find("input[data-upload-name]");

            $inputfile.on('click', function (e) {
                e.stopImmediatePropagation();
            });
            $inputfile.on('change', function(){
                var filename = $inputfile.val().split('\\').pop();
                $inputname.val(filename);
            });
            $inputfile.trigger('click');
        });
    };

    $.fn.sortableInit = function (selector, options)
    {
        var defaults = {
            axis: "y",
            opacity: 0.6,
            revert: true,
            start: function (e, ui) {
                var num = ui.placeholder.find('> *').length,
                    height = ui.helper.outerHeight(true);
                ui.placeholder.empty().html("<td colspan='" + num + "'></td>").css("height", height + "px");
            }
        };

        var opts = $.extend({}, defaults, options);

        return this.each(function() {
            var $this = $(this);

            if ($this.find("[data-handle]").length) {
                opts.handle = "[data-handle]";
            }

            $this.sortable(opts).disableSelection();
        });
    };

    $.evoteSettings = function evoteSettings() {

        /*
         * Define specific defaults for noty plugin for eVote project
         * http://ned.im/noty/
         */
        (function notyInit() {
            $.noty.defaults.timeout = 5000;
            $.noty.defaults.theme = 'evoteTheme';
            $.noty.defaults.layout = 'topCenter';
            $.noty.defaults.animation = {
                open: 'animated fadeIn',
                close: 'animated fadeOut',
            };
        })();

        /*
         * Define specific defaults and instantiate a djangoAjaxForms plugin for eVote project
         * /static/evoteajaxforms/forms.jquery.js
         */
        (function djangoAjaxFormsInit() {
            $.fn.djangoAjaxForms.defaults.canSubmitFn = function canSubmitFn($form) {
                // check if form need to be validated before submit
                if ($form.data('needSecuredAction')) {
                    var wasValidated = $form.data('wasValidated');
                    $form.data('wasValidated', false);
                    return wasValidated;
                }

                return true;
            };

            $.fn.djangoAjaxForms.defaults.onFailFn = function onFailFn() {
                noty({
                    text: django.gettext("It was not possible submit form."),
                    type: "error"
                });
            };
            $.fn.djangoAjaxForms.defaults.onFieldErrorFn = function onFieldErrorFn($form) {
                if (typeof grecaptcha !== 'undefined'){
                    grecaptcha.reset();
                }
                noty({
                    text: django.gettext("Fix the errors on the form and resubmit the form"),
                    type: "warning"
                });
                $form.trigger('checkform.areYouSure');
                Ladda.stopAll();
            };
            $.fn.djangoAjaxForms.defaults.onNonFieldErrorFn = function onNonFieldErrorFn(errors) {
                noty({
                    text: errors,
                    type: "warning"
                });
            };
            $.fn.djangoAjaxForms.defaults.onSubmit = function onSubmit($form) {
                Ladda.create( $form.find("[type=submit]").get(0) ).start();
            };

            $.fn.djangoAjaxForms.defaults.onBeforeSubmit = function onBeforeSubmit($form) {
                // if there's sortable rows on the form, update the order value
                $form.find("[data-sortable] > *").each(function (index) {
                    $(this).find("[name$='order']").val(index + 1);
                });
            };
        })();
    };


    var DROPDOWNS = (function() {

        var self = {
            menus: '.pure-menu-children',
            item : '.pure-menu-list a[data-type="menu"]',
            close: '.pure-menu-children .close-menu',
            elem : '<div class="selected-menu"></div>',
            search: '.menu-search input[type="submit"]',

            dropdownInit: function dropdownInit() {
                /*$(self.menus).appendTo('body');*/
            },

            bindEvents: function bindEvents() {
                $(document).on('click', self.close, function (e) {
                    e.preventDefault();
                    $(self.item).find('.selected-menu').remove();
                    $(self.menus).hide();
                });
                $(document).click(function() {
                    $(self.item).find('.selected-menu').remove();
                    $(self.menus).hide();
                });
                $(document).on('click', self.menus, function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                });
                $(document).on('click', self.search, function (e) {
                    $(this).parent("form").submit();
                });
                $(document).on('click', self.item, function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var menu = $(this).data('menu');
                    $(self.item).find('.selected-menu').remove();
                    $(this).append(self.elem);
                    $(self.menus).hide();
                    $("ul[data-menu='" + menu +"']").show();
                });
            },

            initialize: function initialize() {
                self.dropdownInit();
                self.bindEvents();
            }
        };
        return self;
    })();


    var ACCESSIBILITY = (function() {

        var self = {
            // Color vars
            color_storage: 'ACCESS_COLOR',
            color_class  : 'access-color',
            access_color : 'box-color2',
            change_color : 'div[data-box="color"]',
            // Text vars
            text_storage : 'ACCESS_TEXT',
            text_class   : 'access-text',
            access_text  : 'box-large',
            change_text  : 'div[data-box="text"]',

            init: function init() {
                self.initAccessibility(self.color_storage, self.color_class,
                                       self.change_color, self.access_color);
                self.initAccessibility(self.text_storage, self.text_class,
                                       self.change_text, self.access_text);
            },

            initAccessibility: function initAccessibility(storage, item_class, change, access) {
                var selected = localStorage.getItem(storage);
                if (typeof selected !== 'undefined' && selected !== null && selected == 'yes') {
                    $(change).removeClass('selected');
                    $(change+'.'+access).addClass('selected');
                }
            },

            selectOption: function selectOption(selector, change, access, storage, item_class) {
                if ( !$(selector).hasClass('selected') ) {
                    $(change).removeClass('selected');
                    $(selector).addClass('selected');
                    var date = new Date();
                    date.setTime(date.getTime()+(2*24*60*60*1000));
                    var expires = "; expires="+date.toGMTString();
                    if ( $(selector).hasClass(access) ) {
                        localStorage[storage] = "yes";
                        $('body').addClass(item_class);
                        // Add cookie (with explicit expiration date and path) for this accessibility preference.
                        document.cookie = 'ev_' + item_class + '=true; ' + expires + ';path=/';
                    } else {
                        localStorage[storage] = "";
                        $('body').removeClass(item_class);
                        // Also delete the cookie for this accessibility preference.
                        document.cookie = 'ev_' + item_class + '=false; ' + expires + ';path=/';
                    }
                }
            },

            bindEvents: function bindEvents() {
                $(document).on('click', self.change_color, function (e) {
                    e.preventDefault();
                    self.selectOption(this, self.change_color, self.access_color,
                                      self.color_storage, self.color_class);

                });

                $(document).on('click', self.change_text, function (e) {
                    e.preventDefault();
                    self.selectOption(this, self.change_text, self.access_text,
                                      self.text_storage, self.text_class);
                });
            },

            initialize: function initialize() {
                self.init();
                self.bindEvents();
            }
        };
        return self;
    })();

    var ACCORDION = (function() {

        var self = {
            acc_title: '.pure-sec-accordion .pure-sec-title',
            acc_body : '.pure-sec-accordion .pure-sec-body',

            bindEvents: function bindEvents() {
                $(document).on('click', self.acc_title, function (e) {
                    e.preventDefault();
                    $(this).find('i.ev-icon').toggleClass('evi-sel-drk-dwn').toggleClass('evi-sel-drk-up');
                    $(this).next(self.acc_body).slideToggle();
                });

                // Controls open on hash found
                var hash = window.location.hash;
                if (hash){
                    var element = $(hash);
                    element.find('.pure-sec-title i.ev-icon').toggleClass('evi-sel-drk-dwn').toggleClass('evi-sel-drk-up');
                    element.find(".pure-sec-body").slideToggle();
                }
            },

            initialize: function initialize() {
                self.bindEvents();
            }
        };
        return self;
    })();

    var DATETIMEPICKER = (function() {

        var self = {

            initialize: function initialize() {
                $('[data-datepicker]').datepicker({
                    dateFormat: 'dd-mm-yy',
                });
                $('[data-datetimepicker]').datetimepicker({
                    dateFormat: 'dd-mm-yy',
                    timeFormat: 'HH:mm',
                    showTime: false
                });
            }
        };
        return self;
    })();

    $(document).ready(function() {

        $.evoteSettings();

        //Instantiate select2 plugin for eVote project https://select2.github.io/
        $('select[data-style-select]').select2({
            minimumResultsForSearch: Infinity
        });
        $('select[data-style-select-search]').select2();

        //Instantiate select2 plugin with ajax loading
        $('select[data-style-select-ajax]').select2({
            cache: true,
            minimumInputLength: 3,
            ajax: {
                delay: 250,
                dataType: 'json',
                url: function () {
                    return $(this).attr('data-select-ajax-url');
                },
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data
                    };
                }
            }
        });

        // django forms instantion
        $("form[data-ajax-submit]").djangoAjaxForms();

        // automatically submit form on input change
        $("form[data-submit-onchange]").submitOnChange();

        // transform django messages into noty notifications
        $('.noty-messages').djangoMessagesNoty();

        // Remodal instantiation
        $('[data-remodal-id]').remodal();

        // Formset initialize
        $("[data-formset]").formset({
            animateForms: true
        });

        $("[data-formset]").on('formAdded', function () {
            $(this).find("[data-formset]").formset({
                animateForms: true
            });
            DATETIMEPICKER.initialize();
            $("[data-sortable]").sortableInit();
        });

        $("[data-sortable]").sortableInit();

        // infinite scroll
        if ($.fn.jscroll) {
            $("[data-jscroll]").jscroll({
                nextSelector: '[data-jscroll-next]'
            });
        }

        //
        $("[href='#']").on("click", function(e) {
            e.preventDefault();
        });

        $.fileUpload(".pure-field.clearablefileinput, .pure-field.fileinput, [data-upload]");


        ACCESSIBILITY.initialize();
        DROPDOWNS.initialize();
        ACCORDION.initialize();
        DATETIMEPICKER.initialize();

        // Sticky Header
        $('[data-sticky]').stickyHeader('sticky');

        // Removal modals
        function bindRemodalClick(modal, id) {
            $(document).on('click', "[data-remodal-target='" + id + "']", function(e){
                e.preventDefault();
                var elem = $(this);

                $(document).on('confirmation', modal, function(){
                    window.location.href = elem.attr('href');
                });
            });
        }

        var confirmModal = $("[data-remodal-id='modalConfirmOP']").remodal({'closeOnCancel': true});
        bindRemodalClick(confirmModal, 'modalConfirmOP');

        var confirmexitvote = $("[data-remodal-id='modalConfirmOP']").remodal({'closeOnCancel': true});
        bindRemodalClick(confirmexitvote, 'modalConfirmVoteExit');

        // Print function
        $('.print-btn').on('click', function(){
            window.print();
        });

        // Validate exit of user from forms
        $('form').not('[data-areyousure-off]').areYouSure();

        // Open datetimepicker on click of icon
        $('.pure-field').on('click', function(e){
            var $elem = $(e.target);
            if ($elem.hasClass('fi-calendar')){
                $elem.find('input').focus();
            }
        });

        // Ajax logout for adfs authentication system
        $('a[data-ajax-logout]').on('click', function(e) {
            e.preventDefault();
            var link = $(this).attr('href');
            var redirect = $(this).data('after-logout');
            var iframe = document.createElement('iframe');
            iframe.setAttribute("src", link);
            $(iframe).css({
                'width': '0',
                'height': '0',
                'border': 'none'
            });
            $("body").append(iframe);
            iframe.onload=function() {
                window.location.href = redirect;
            };
        });

        /* Cookie consent */
        window.cookieconsent.initialise({
            container: document.getElementById("content"),
            content: {
                header: django.gettext('Cookies used on the website!'),
                message: django.gettext('This website uses cookies to ensure you get the best experience on our website.'),
                dismiss: django.gettext('Accept'),
                allow: django.gettext('Allow cookies'),
                deny: django.gettext('Decline'),
                link: django.gettext('Learn more'),
            },
            elements: {
                message: '<span id="cookieconsent:desc" class="cc-message pure-cookie-msg">{{message}}</span>',
                dismiss: '<a aria-label="dismiss cookie message" tabindex="0" class="cc-btn cc-dismiss pure-button pure-button-primary">{{dismiss}}</a>',
            },
            palette:{
              popup: {background: "#ffffff"}
            },
            showLink: false
          });

    });

})(jQuery);
