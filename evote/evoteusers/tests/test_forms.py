# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.test import TestCase

from evoteusers.tests.utils import UserTestUtils
from evoteusers.forms import EvoteAuthenticationForm, PersonalDataForm


class EvoteAuthenticationFormTest(TestCase, UserTestUtils):
    """ """

    def setUp(self):
        self.credentials = {'email': 'dev@eurotux.com', 'password': 'password'}
        self.user = self.create_user(**self.credentials)

    def test_init(self):
        form = EvoteAuthenticationForm()
        username_attrs = form.fields['username'].widget.attrs
        password_attrs = form.fields['password'].widget.attrs
        self.assertIn('autocomplete', username_attrs)
        self.assertIn('placeholder', username_attrs)
        self.assertIn('autocomplete', password_attrs)
        self.assertIn('placeholder', password_attrs)


class PersonalDataFormTest(TestCase, UserTestUtils):
    """ """

    def setUp(self):
        self.login()

    def test_clean_notice_email_same(self):
        """ Test new notice email change (same) """
        self.user.notice_email = 'dev@eurotux.com'
        self.user.save(update_fields=['notice_email'])
        data = {
            'photo': self.user.photo,
            'name': self.user.email,
            'email': self.user.email,
            'primary_phone': self.user.primary_phone,
            'notice_method': self.user.notice_method,
            'notice_phone': self.user.notice_phone,
            'notice_email': self.user.notice_email,
        }
        form = PersonalDataForm(data=data, instance=self.user,
                                base_url='http://base.com/')
        form.is_valid()
        self.assertEqual(form.clean_notice_email(), self.user.notice_email)

    def test_clean_notice_email_empty(self):
        """ Test new notice email change (empty) """
        self.user.notice_email = 'dev@eurotux.com'
        self.user.save(update_fields=['notice_email'])
        data = {
            'photo': self.user.photo,
            'name': self.user.email,
            'email': self.user.email,
            'primary_phone': self.user.primary_phone,
            'notice_method': self.user.notice_method,
            'notice_phone': self.user.notice_phone,
            'notice_email': '',
        }
        form = PersonalDataForm(data=data, instance=self.user,
                                base_url='http://base.com/')
        form.is_valid()
        self.assertEqual(form.clean_notice_email(), '')

    def test_clean_notice_email_to_pending(self):
        """ Test new notice email change (to pending) """
        self.user.notice_email = 'dev@eurotux.com'
        self.user.save(update_fields=['notice_email'])
        data = {
            'photo': self.user.photo,
            'name': self.user.email,
            'email': self.user.email,
            'primary_phone': self.user.primary_phone,
            'notice_method': self.user.notice_method,
            'notice_phone': self.user.notice_phone,
            'notice_email': 'unit@eurotux.com',
        }
        form = PersonalDataForm(data=data, instance=self.user,
                                base_url='http://base.com/')
        form.is_valid()
        self.assertEqual(form.clean_notice_email(), self.user.notice_email)
        self.assertEqual(self.user.pending_email, 'unit@eurotux.com')
