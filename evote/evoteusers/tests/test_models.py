# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.cache import cache
from django.utils import timezone
from django.test import TestCase

from evoteusers.models import User


class UserTestCase(TestCase):
    """ """
    def test_full_name(self):
        user = User(name="Sandro Rodrigues")
        self.assertEqual(user.name, user.get_full_name())

    def test_short_name(self):
        user = User(name="Sandro Rodrigues")
        self.assertEqual(user.name, user.get_short_name())

    def test_notice_email_as_default(self):
        user = User(notice_email="dev@eurotux.com")
        self.assertEqual(user.get_email(), user.notice_email)

    def test_notice_email_fallback_to_email(self):
        user = User(email="dev@eurotux.com", notice_email="")
        self.assertEqual(user.get_email(), user.email)

    def test_notice_phone_as_default(self):
        user = User(notice_phone="00351911111111",
                    primary_phone="00351933333333")
        self.assertEqual(user.get_phone(), user.notice_phone)

    def test_notice_phone_fallback_to_primary_phone(self):
        user = User(notice_phone="", primary_phone="00351933333333")
        self.assertEqual(user.get_phone(), user.primary_phone)

    def test_set_pending_email(self):
        user = User.objects.create(notice_email='dev@eurotux.com')
        user.set_pending_email('unit@eurotux.com', 'http://base.com/')
        self.assertEqual(user.pending_email, 'unit@eurotux.com')
        self.assertNotEqual(user.pending_email_token, None)

    def test_send_pending_email_token_mail(self):
        user = User.objects.create(notice_email='dev@eurotux.com',
                                   pending_email='unit@eurotux.com',
                                   pending_email_token='9g76ck8')
        mails_sent = user.send_pending_email_token_mail('http://base.com/')
        self.assertEqual(mails_sent, 1)

    def test_confirm_pending_email(self):
        user = User.objects.create(notice_email='dev@eurotux.com',
                                   pending_email='unit@eurotux.com')
        token = user.generate_token()
        user.pending_email_token = token
        user.save()
        user.confirm_pending_email(token)
        self.assertEqual(user.notice_email, 'unit@eurotux.com')
        self.assertEqual(user.pending_email_token, None)
        self.assertEqual(user.pending_email, None)

    def test_confirm_pending_email_wrong_token(self):
        user = User.objects.create(notice_email='dev@eurotux.com',
                                   pending_email='unit@eurotux.com')
        token = user.generate_token()
        user.pending_email_token = token
        user.save()
        validated = user.confirm_pending_email('wrongtoken322')
        self.assertEqual(validated, False)
        self.assertEqual(user.notice_email, 'dev@eurotux.com')
        self.assertNotEqual(user.pending_email_token, None)
        self.assertEqual(user.pending_email, 'unit@eurotux.com')

    def test_is_not_allowed_to_resend(self):
        user = User.objects.create(notice_email='dev@eurotux.com',
                                   pending_email='unit@eurotux.com')
        token = user.generate_token()
        user.pending_email_token = token
        user.save()
        cache.set(user.get_pending_key(), timezone.now())
        self.assertEqual(user.is_allowed_to_resend, False)

    def test_is_allowed_to_resend(self):
        user = User.objects.create(notice_email='dev@eurotux.com',
                                   pending_email='unit@eurotux.com')
        token = user.generate_token()
        user.pending_email_token = token
        user.save()
        cache.set(user.get_pending_key(),
                  timezone.now() - timezone.timedelta(minutes=10))
        self.assertEqual(user.is_allowed_to_resend, True)

    def test_is_allowed_to_resend_no_date(self):
        user = User.objects.create(notice_email='dev@eurotux.com',
                                   pending_email='unit@eurotux.com')
        token = user.generate_token()
        user.pending_email_token = token
        user.save()
        cache.delete(user.get_pending_key())
        self.assertEqual(user.is_allowed_to_resend, True)
