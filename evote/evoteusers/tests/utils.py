# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from autofixture import create_one

from evoteusers.models import User


class UserTestUtils(object):

    def create_user(self, **field_values):
        password = field_values.pop('password', None)
        user = create_one(User, field_values=field_values)
        if password:
            user.set_password(password)
            user.save()
        return user

    def login(self):
        credentials = {
            "username": "dev",
            "password": "password"
        }
        self.user = self.create_user(**credentials)
        self.client.login(**credentials)
        return credentials
