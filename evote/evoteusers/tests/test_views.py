# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from mock import patch

from django.contrib.auth.models import AnonymousUser
from django.test import TestCase, RequestFactory
from django.http import HttpResponse
from django.http import Http404

from evoteusers.views import (EvoteLoginView, PersonalDataBaseView,
                              ValidateEmailBaseView,
                              ResendPendingTokenMailBaseView)

from .utils import UserTestUtils


def empty_response(*args, **kwargs):
    return HttpResponse("")


class EvoteLoginViewTest(UserTestUtils, TestCase):
    """ """

    def setUp(self):
        self.factory = RequestFactory()
        self.user = self.create_user()

    @patch("evoteusers.views.EvoteLoginView.render_to_response",
           new=empty_response)
    def test_login_user(self):
        request = self.factory.get('/login/')
        request.user = AnonymousUser()
        request.session = self.client.session
        response = EvoteLoginView.as_view()(request)
        self.assertEqual(response.status_code, 200)


class PersonalDataBaseViewTest(UserTestUtils, TestCase):
    """ """

    class DummyView(PersonalDataBaseView):
        template_name = 'base.html'

        def get_form_kwargs(self):
            """ """
            return {'base_url': 'http://base.com/'}

    def setUp(self):
        self.factory = RequestFactory()
        self.user = self.create_user()

    def test_get_object(self):
        request = self.factory.get('/validate/')
        request.user = self.user
        request.session = self.client.session
        response = self.DummyView.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context_data['object'], request.user)


class ValidateEmailBaseViewTest(UserTestUtils, TestCase):
    """ """

    def setUp(self):
        self.factory = RequestFactory()
        self.user = self.create_user(pending_email="dev_pending@eurotux.com",
                                     pending_email_token='1bv46gbc54x54')

    def test_email_validate(self):
        request = self.factory.get('/validate/?token=1bv46gbc54x54')
        request.user = self.user
        request.session = self.client.session
        response = ValidateEmailBaseView.as_view()(request,
                                                   token='1bv46gbc54x54')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user.notice_email, "dev_pending@eurotux.com")
        self.assertEqual(self.user.pending_email, None)

    def test_no_pending_email(self):
        self.user.notice_email = ''
        self.user.pending_email = None
        self.user.save()
        request = self.factory.get('/validate/')
        request.user = self.user
        request.session = self.client.session
        self.assertRaises(Http404, ValidateEmailBaseView.as_view(), request)
        self.assertEqual(self.user.notice_email, '')
        self.assertEqual(self.user.pending_email, None)


class ResendPendingTokenMailBaseViewTest(UserTestUtils, TestCase):
    """ """

    class DummyView(ResendPendingTokenMailBaseView):
        """ """

    def test_view_nok(self):
        view = self.DummyView()
        self.assertRaises(NotImplementedError, view.get_base_validate_url)
