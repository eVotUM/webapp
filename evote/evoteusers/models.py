# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_init, post_save
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible
from django.db import models

from phonenumber_field.modelfields import PhoneNumberField
from django_extensions.db.models import TimeStampedModel
from evotelogs.decorators import auditable
from guardian.shortcuts import assign_perm, remove_perm
from evotecore.mixins import ModelFieldChangedMixin
from evotecore.utils import images_upload_to
from sorl.thumbnail import ImageField

from .mailings import EmailChangeOldMail, MethodChangeMail
from .mixins import PendingEmailMixin


NO_UPN_LABEL = "noupn{}"


@auditable
@python_2_unicode_compatible
class User(AbstractUser, TimeStampedModel, ModelFieldChangedMixin,
           PendingEmailMixin):
    """Basic django user using email as username and without default
    django permissions
    """

    class NoticeMethod:
        SMS = 0
        EMAIL = 1

        choices = (
            (SMS, _("SMS")),
            (EMAIL, _("Email")),
        )

    email = models.EmailField(
        _("predefined email address"), db_index=True, blank=True)
    name = models.CharField(_("name"), max_length=255, db_index=True)
    # specific evote profile fields
    is_institutional_responsible = models.BooleanField(
        _("institutional responsible status"), default=False)
    photo = ImageField(_("photo"), upload_to=images_upload_to, blank=True)
    primary_phone = PhoneNumberField(_('predefined mobile number'), blank=True)
    notice_method = models.PositiveSmallIntegerField(
        _("security key sending method"), choices=NoticeMethod.choices,
        default=NoticeMethod.EMAIL)
    # number used to notified user, defaults to primary if blank
    notice_phone = PhoneNumberField(
        _("notification mobile number"), blank=True,
        help_text=_("Phone number user to send notifications. If not "
                    "defined, primary phone used instead"))
    # email used to notified user, defaults to user email if blank
    notice_email = models.EmailField(
        _("notification email address"), blank=True,
        help_text=_("Email used to send notifications. If not defined, "
                    "primary email used instead"))
    nic = models.CharField(_("citizen card number"), max_length=64)
    upn = models.CharField(_("user personnel number"), max_length=128,
                           blank=True, null=True)

    def __str__(self):
        return "{} ({})".format(self.name, self.username)

    def get_phone(self):
        """Use this property to obtain phone notice number instead of direct
        attribute notice_phone."""
        return self.notice_phone or self.primary_phone

    def get_email(self):
        """Use this property to obtain email notice instead of direct
        attribute email_phone."""
        return self.notice_email or self.email

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def set_as_admin(self):
        """ """
        self.is_staff = True
        self.save(update_fields=['is_staff'])

    def set_as_institutional_responsible(self):
        """TODO: remove assign_perm of external model. maybe add a signal
        triggered on every set_insitutional_responsible call"""
        self.is_institutional_responsible = True
        assign_perm("evoteprocesses.add_electoralprocess", self)
        assign_perm("evoteprocesses.change_electoralprocess", self)
        self.save(update_fields=['is_institutional_responsible'])

    def unset_as_institutional_responsible(self):
        """TODO: remove remove_perm of external model. maybe add a signal
        triggered on every unset_as_institutional_responsible call"""
        self.is_institutional_responsible = False
        remove_perm("evoteprocesses.add_electoralprocess", self)
        remove_perm("evoteprocesses.change_electoralprocess", self)
        self.save(update_fields=['is_institutional_responsible'])

    @staticmethod
    def post_init(sender, instance, **kwargs):
        """Callback staticmethod for post_init signal"""
        instance.track_changes(['notice_email', 'notice_method'])

    @staticmethod
    def post_save(sender, instance, **kwargs):
        """Callback staticmethod for post_save signal"""
        if instance.id:
            if instance.has_changed('notice_email') and\
                    instance.get_prev_field('notice_email'):
                # Send email to old address
                context = {}
                old = instance.get_prev_field('notice_email')
                context = {
                    'old': old,
                    'new': instance.notice_email
                }
                EmailChangeOldMail(context).send([(instance.name, old)])

            if instance.has_changed('notice_method'):
                old_value = instance.get_prev_field('notice_method')
                old = User.NoticeMethod.choices[old_value][1]
                context = {
                    'old': old,
                    'new': instance.get_notice_method_display()
                }
                MethodChangeMail(context).send(
                    [(instance.name, instance.get_email())])


post_init.connect(User.post_init, sender=User)
post_save.connect(User.post_save, sender=User)
