# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin

from evoteusers.models import User


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    """ """
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('name', 'email', 'primary_phone',
                                         'notice_phone', 'notice_method',
                                         'notice_email', 'nic')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'is_institutional_responsible',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    list_display = ('name', 'username', 'email', 'nic', 'is_active',
                    'is_institutional_responsible')
    list_filter = ('is_institutional_responsible',)
    search_fields = ('name', 'username', 'email', 'nic')
    ordering = ('username',)
