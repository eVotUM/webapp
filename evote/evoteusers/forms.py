# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals, absolute_import

from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
from django import forms

from evotecore import widgets

from .models import User


class EvoteAuthenticationForm(AuthenticationForm):
    """
    """

    def __init__(self, *args, **kwargs):
        """ """
        super(EvoteAuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({
            'autocomplete': 'off',
            'placeholder': _('Username')
        })
        self.fields['password'].widget.attrs.update({
            'autocomplete': 'off',
            'placeholder': _('Password')
        })


class PersonalDataForm(forms.ModelForm):
    """ """
    error_messages = {
        'empty_notice_email': _("Please provide the notice email"),
        'empty_notice_phone': _("Please provide the notice phone"),
        'invalid_notice_email': _("Enter a valid email address"),
        'invalid_notice_phone': _("Enter a valid phone number"),
    }

    class Meta:
        model = User
        fields = ('notice_method', 'notice_phone', 'notice_email')
        widgets = {
            'notice_method': widgets.Select
        }

    def __init__(self, *args, **kwargs):
        self.base_url = kwargs.pop("base_url")
        super(PersonalDataForm, self).__init__(*args, **kwargs)
        self.fields['notice_phone'].widget.attrs.update({
            'placeholder': "+351900000000"})
        self.fields['notice_phone'].error_messages.update({
            'invalid': self.error_messages['invalid_notice_phone']
        })
        self.fields['notice_email'].error_messages.update({
            'invalid': self.error_messages['invalid_notice_email']
        })

    def clean_notice_email(self):
        notice_email = self.cleaned_data.get('notice_email', None)
        if notice_email and notice_email != self.instance.notice_email:
            self.instance.set_pending_email(notice_email, self.base_url)
        elif notice_email == '':
            return notice_email
        return self.instance.notice_email

    def clean(self):
        super(PersonalDataForm, self).clean()
        notice_method = self.cleaned_data.get('notice_method', None)
        notice_email = self.cleaned_data.get('notice_email', None)
        notice_phone = self.cleaned_data.get('notice_phone', None)
        if notice_method == User.NoticeMethod.EMAIL and \
                not self.instance.email and not notice_email:
            self.add_error('notice_email',
                           self.error_messages['empty_notice_email'])
        if notice_method == User.NoticeMethod.SMS and \
                not self.instance.primary_phone and not notice_phone:
            self.add_error('notice_phone',
                           self.error_messages['empty_notice_phone'])
        return self.cleaned_data
