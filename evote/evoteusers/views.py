# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext_lazy as _
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView, TemplateView
from django.contrib.auth import REDIRECT_FIELD_NAME, login, logout
from django.utils.http import is_safe_url
from django.conf import settings
from django.http import Http404

from evoteajaxforms.views import FormJsonView, AjaxResponseAction
from evoteusers.forms import EvoteAuthenticationForm
from evotecore.mixins import FeedbackMessageMixin

from .models import User
from .forms import PersonalDataForm


class EvoteLoginView(FeedbackMessageMixin, FormView):
    """Provides user the ability to login
    """
    form_class = EvoteAuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    success_message = _("Successfully authenticated")

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        # Sets a test cookie to make sure the user has cookies enabled
        request.session.set_test_cookie()
        return super(EvoteLoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())
        # If the test cookie worked, go ahead and
        # delete it since its no longer needed
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
        return super(EvoteLoginView, self).form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.GET.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = settings.LOGIN_REDIRECT_URL
        return redirect_to


class EvoteLogoutView(RedirectView):
    """Provides users the ability to logout"""

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(EvoteLogoutView, self).get(request, *args, **kwargs)

    def get_redirect_url(self):
        return settings.LOGIN_URL


@method_decorator(login_required, name="dispatch")
class PersonalDataBaseView(FormJsonView):
    """
    Shows the personal data view
    """
    model = User
    form_class = PersonalDataForm
    action = AjaxResponseAction.REFRESH
    prefix = 'personal'

    def get_object(self):
        return self.request.user


@method_decorator(login_required, name="dispatch")
class ValidateEmailBaseView(TemplateView):
    """ Validates email change of user """
    template_name = 'base.html'

    def get(self, request, *args, **kwargs):
        validated = self.request.user.confirm_pending_email(
            request.GET.get('token', None))
        if not validated:
            raise Http404
        return super(ValidateEmailBaseView, self).get(request, *args, **kwargs)


@method_decorator(login_required, name="dispatch")
class ResendPendingTokenMailBaseView(FeedbackMessageMixin, RedirectView):
    """ Resends email with token """
    success_message = _("Validation email was sent")
    error_message = _("You are not allowed to resend the email")

    def get(self, request, *args, **kwargs):
        if request.user.is_allowed_to_resend:
            self.request.user.send_pending_email_token_mail(
                self.request.build_absolute_uri(self.get_base_validate_url()))
            self.set_success_message()
        else:
            self.set_error_message()

        return super(ResendPendingTokenMailBaseView, self).get(
            request, *args, **kwargs)

    def get_base_validate_url(self):
        raise NotImplementedError
