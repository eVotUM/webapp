# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model


class Command(BaseCommand):
    help = 'Used to define a user as admin.'

    def add_arguments(self, parser):
        parser.add_argument(
            'username', nargs='*',
            help='Specify the username of registered user.',
        )

    def handle(self, *args, **options):
        User = get_user_model()  # NOQA
        for username in options.get('username'):
            try:
                User.username_validator(username)
                user = User.objects.get(username=username)
            except ValidationError:
                self.print_error("Invalid username '{}'".format(username))
            except User.DoesNotExist:
                self.print_error("User with username '{}' does not exists"
                                 .format(username))
            else:
                user.set_as_admin()
                self.print_success("User with username '{}' setted as admin"
                                   .format(username))

    def print_success(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def print_error(self, msg):
        self.stderr.write(self.style.ERROR(msg))
