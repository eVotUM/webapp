# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.db import IntegrityError

from contrib.umws.members import members_service
from evoteusers.models import NO_UPN_LABEL


class Command(BaseCommand):
    help = 'Syncronize users without username using DTSI members service.'

    def handle(self, *args, **options):
        qs = get_user_model().objects.filter(
            username__istartswith=NO_UPN_LABEL.format(''))
        for user in qs:
            contact = members_service.get_contact_info_by_email(user.email)
            if not contact:
                self.print_error("No contact info for user: {}"
                                 .format(user.email))
            else:
                try:
                    user.username = contact.get('username')
                    user.save(update_fields=['username'])
                except IntegrityError:
                    self.print_error("Username duplicated: {} ({})"
                                     .format(user.username, user.email))
        self.print_success("Finished sync of {} users".format(qs.count()))

    def print_success(self, msg):
        self.stdout.write(self.style.SUCCESS(msg))

    def print_error(self, msg):
        self.stderr.write(self.style.ERROR(msg))
