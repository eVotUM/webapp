# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import hashlib
import random

from django.utils.translation import ugettext_lazy as _
from django.core.cache import cache
from django.utils import timezone
from django.db import models

from .mailings import ValidateNoticeMail


class PendingEmailMixin(models.Model):
    """ """
    pending_email = models.EmailField(
        _("Email pending of validation"), blank=True, null=True,
        help_text=_("Email that will be used to send notifications."))
    pending_email_token = models.CharField(max_length=64, unique=True,
                                           null=True)

    class Meta:
        abstract = True

    def generate_token(self):
        """ Generates random token using pending email """
        data = [self.pending_email] +\
            [str(random.SystemRandom().getrandbits(512))]
        token = hashlib.sha256("".join(data).encode("utf-8")).hexdigest()
        return token

    def send_pending_email_token_mail(self, base_url):
        """ Send email with activation link """
        context = {
            'old': self.get_email(),
            'new': self.pending_email,
            'url': base_url + "?token=" + self.pending_email_token
        }
        cache.set(self.get_pending_key(), timezone.now())
        return ValidateNoticeMail(context).send(
            [(self.get_short_name(), self.get_email())])

    def set_pending_email(self, email, base_url):
        """ Sets email as pending, generates token and send email """
        self.pending_email = email
        self.pending_email_token = self.generate_token()
        self.save(update_fields=['pending_email', 'pending_email_token'])
        self.send_pending_email_token_mail(base_url)

    def confirm_pending_email(self, url_token):
        """ Confirms the change of pending email """
        if not self.pending_email or not self.pending_email_token or\
                url_token != self.pending_email_token:
            return False
        self.notice_email = self.pending_email
        self.pending_email = None
        self.pending_email_token = None
        self.save(update_fields=['notice_email', 'pending_email',
                                 'pending_email_token'])
        return True

    @property
    def is_allowed_to_resend(self):
        """ Checks if user is allowed to request new email with link """
        date = cache.get(self.get_pending_key(), None)
        if date and date + timezone.timedelta(minutes=5) <= timezone.now():
            return True
        elif not date:
            return True
        return False

    def get_pending_key(self):
        return "pending-{}-{}".format(self.pending_email, self.pk)
