# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from evotemailings.mailings import BaseMailing


class EmailChangeOldMail(BaseMailing):
    """ """
    template_name = 'evoteusers/mails/email_change_old.html'
    subject = "eVotUM - Alteração do endereço de correio eletrónico de" +\
              "comunicação | Change of email address"


class MethodChangeMail(BaseMailing):
    """ """
    template_name = 'evoteusers/mails/method_change.html'
    subject = "eVotUM - Alteração do mecanismo de envio de chave de" +\
              " segurança | Change of security key sending method"


class ValidateNoticeMail(BaseMailing):
    """ """
    template_name = 'evoteusers/mails/email_validate.html'
    subject = "eVotUM - Alteração do endereço de correio eletrónico de" +\
              " comunicação | Change of email address"
