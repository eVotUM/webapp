# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django_extensions.db.models import TimeStampedModel
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import models

from .managers import AuditLogsManager


class AuditableEvent(TimeStampedModel):
    """ """
    class EventType:
        CREATE = 0
        UPDATE = 1
        DELETE = 2

        choices = (
            (CREATE, _("Created")),
            (UPDATE, _("Updated")),
            (DELETE, _("Deleted")),
        )

    entry_hash = models.CharField(
        max_length=128, null=False, blank=True,
        db_index=True, default="", verbose_name=_("entry hash"))
    event_type = models.PositiveSmallIntegerField(
        _("event type"), choices=EventType.choices,
        default=EventType.CREATE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("user"),
        null=True, db_index=True, default=None,
        related_name="user")
    content_type = models.ForeignKey(
        ContentType, verbose_name=_("model"), related_name="audit_entry")
    object_id = models.IntegerField(db_index=True, verbose_name=_("object id"))
    object_repr = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_("identifier"))
    object_json_changes = models.TextField(
        null=True, blank=True, verbose_name=_("object diff"))
    group_content_type = models.ForeignKey(
        ContentType, null=True, verbose_name=_("parent model"),
        related_name="audit_group")
    group_object_id = models.IntegerField(
        null=True, blank=False, verbose_name=_("parent object id"))
    show_in_history = models.BooleanField(
        _("show in history"), default=False)

    objects = AuditLogsManager()

    class Meta:
        verbose_name = _("auditlog")
        verbose_name_plural = _("auditlogs")
        index_together = ['group_content_type', 'group_object_id']

    def __str__(self):
        return self.entry_hash

    def get_name(self):
        if self.user:
            return self.user.get_full_name()
        return _('System')

    def get_description(self):
        return _("The user {} {} the {}").format(
            self.get_name(), self.get_event_type_display().lower(),
            self.content_type.name)

    def is_new(self, last_login):
        return self.created >= last_login
