# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from django.contrib.auth.models import AnonymousUser
from django.contrib.contenttypes.models import ContentType
from django.db.models import signals as models_signals
from django.core import serializers
from django.conf import settings

from .registers import audit_register
from .middleware import EvoteLogsMiddleware
from .models import AuditableEvent


def prepare_signal(event_type, sender, instance, using, **kwargs):
    if not audit_register.check_if_auditable(sender):
        return False

    # Get the Object JSON representation
    object_json = serializers.serialize("json", [instance])

    # Try to get the user using the custom middleware
    try:
        user = EvoteLogsMiddleware.request.user
    except:
        user = None

    # Edge case for anonymous users
    if isinstance(user, AnonymousUser):
        user = None

    show_in_history = getattr(
        settings, 'EVOTELOG_SHOWINHISTORY_CALLBACK', False)

    if callable(show_in_history):
        show_in_history = show_in_history(user)

    # Get the instance content type
    instance_type = ContentType.objects.get_for_model(instance)

    # Get group parameters if they are defined
    group_type_id, group_object_id = getattr(
        instance, 'audit_group', (None, None))

    # Call the custom create using the object manager
    return AuditableEvent.objects.audit_create(
        event_type=event_type,
        user=user,
        object_repr=str(instance),
        object_id=instance.pk,
        object_json_changes=object_json,
        group_content_type_id=group_type_id,
        group_object_id=group_object_id,
        content_type_id=instance_type.id,
        show_in_history=show_in_history
    )


def post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    """ """
    if created:
        event_type = AuditableEvent.EventType.CREATE
    else:
        event_type = AuditableEvent.EventType.UPDATE

    return prepare_signal(
        event_type,
        sender, instance, using, **kwargs)


def post_delete(sender, instance, using, **kwargs):
    """ """
    return prepare_signal(
        AuditableEvent.EventType.DELETE,
        sender, instance, using, **kwargs)


models_signals.post_save.connect(post_save)
models_signals.post_delete.connect(post_delete)
