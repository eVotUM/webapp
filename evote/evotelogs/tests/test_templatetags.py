# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test.client import RequestFactory
from django.template import Context, Template
from django.test import TestCase

from evotelogs.models import AuditableEvent
from evoteusers.models import User


class TemplateTagsTest(TestCase):

    def render_template(self, string, context=None, request=None):
        context = context or {}
        context = Context(context)
        return Template(string).render(context)


class LogsTagsTest(TemplateTagsTest):

    def setUp(self):
        self.rf = RequestFactory()
        self.rf.user = User.objects.get(pk=1)
        self.entry = AuditableEvent()

    def test_is_new_entry_tag(self):
        template = "{% load logs_tags %}{% is_new_entry entry %}"
        rendered = self.render_template(
            template, {'request': self.rf, 'entry': self.entry})
        self.assertEqual(rendered, 'True')
