# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

import logging

from django.contrib.contenttypes.models import ContentType
from django.core import serializers
from django.conf import settings
from django.db import models, OperationalError, IntegrityError, transaction

from eVotUM.Cripto import auditlogs


logger = logging.getLogger('evote.auditlogs')


class AuditLogsManager(models.Manager):
    """ """
    def audit_create(self, event_type, user, object_repr, object_id,
                     object_json_changes, content_type_id,
                     group_content_type_id, group_object_id, show_in_history):
        """
        Creates an entry with auditable capabilities as a
        sequence of log entries represented by 'log entry N - hash1 N'
        """
        # Build the object to save
        audit_entry = self.model(
            event_type=event_type,
            user=user,
            object_id=object_id,
            object_repr=object_repr,
            object_json_changes=object_json_changes,
            group_content_type_id=group_content_type_id,
            group_object_id=group_object_id,
            content_type_id=content_type_id,
            show_in_history=show_in_history
        )
        # Maintain database consistency
        return self.atomic_save(audit_entry)

    def atomic_save(self, log_object):
        """ Save the entry maintaining the hash consistency """
        try:
            # Clear the existing entry if it exists
            if hasattr(log_object, "entry_hash"):
                del(log_object.entry_hash)

            with transaction.atomic():
                # Get the last Hash from the DB
                last_hash = ""
                prev_log = self.model.objects.select_for_update().last()
                # Generate the next Hash
                if prev_log is not None:
                    last_hash = prev_log.entry_hash

                entry_hash = auditlogs.generateLogEntryHash(
                    # The entry contents will be the object diff
                    str(log_object.object_json_changes),
                    str(last_hash),
                    str(settings.SECRET_KEY)
                )
                # Set the new hash
                log_object.entry_hash = entry_hash
                # Save the object
                log_object.save()
                logger.info(serializers.serialize("json", [log_object]))
                return True

        except (IntegrityError, OperationalError):
            # If a change was commited after aquiring the lock
            # Re-fetch the most recent entry
            self.atomic_save(log_object)

    def get_history_of_model_obj(self, model, object_pk):
        """ Returns the history of object from model """
        content_type = ContentType.objects.get_for_model(model)
        qs = self.filter(group_content_type=content_type,
                         group_object_id=object_pk,
                         show_in_history=True).order_by('-created')
        return qs

    def get_history_new_count(self, model, object_pk, last_login):
        """ Returns the count of new items """
        content_type = ContentType.objects.get_for_model(model)
        qs = self.filter(group_content_type=content_type,
                         group_object_id=object_pk,
                         show_in_history=True,
                         created__gte=last_login).count()
        return qs
