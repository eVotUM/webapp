# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from __future__ import unicode_literals

from json import dumps, loads
import base64
from email.mime.base import MIMEBase

from django.conf import settings
from django.core.mail.message import EmailMultiAlternatives
from django.core.mail import get_connection


def serialize_email(email):
    """
    Serializes EmailMultiAlternatives to JSON to pass to Celery
    """
    json_obj = {
        'headers': email.extra_headers,
        'connection': settings.EMAIL_BACKEND,
        'from_email': email.from_email,
        'to': email.to,
        'bcc': email.bcc,
        'cc': email.cc,
        'reply_to': email.reply_to,
        'subject': email.subject,
        'body': email.body,
        'alternatives': email.alternatives
    }

    json_obj['attachments'] = []
    for attachment in email.attachments:
        if isinstance(attachment, MIMEBase):
            filename = attachment.get_filename('')
            binary_contents = attachment.get_payload(decode=True)
            mimetype = attachment.get_content_type()
        else:
            filename, binary_contents, mimetype = attachment
        contents = base64.b64encode(binary_contents).decode('ascii')
        json_obj['attachments'].append((filename, contents, mimetype))

    return dumps(json_obj)


def deserialize_email(email):
    """
    Deserializes JSON to EmailMessage pass to Celery
    """

    def get_att(attachments):
        att = []
        for attachment in attachments:
            filename, contents, mimetype = attachment
            binary_contents = base64.b64decode(contents.encode('ascii'))
            att.append((filename, binary_contents, mimetype))
        return att

    email_json = loads(email)

    email_obj = EmailMultiAlternatives(
        headers=email_json.get('headers'),
        connection=get_connection(backend=email_json.get('connection')),
        from_email=email_json.get('from_email'),
        to=email_json.get('to'),
        bcc=email_json.get('bcc'),
        cc=email_json.get('cc'),
        reply_to=email_json.get('reply_to'),
        subject=email_json.get('subject', ''),
        body=email_json.get('body', ''),
        attachments=get_att(email_json.get('attachments')))

    for alternative in email_json.get('alternatives'):
        email_obj.attach_alternative(alternative[0], alternative[1])

    return email_obj
