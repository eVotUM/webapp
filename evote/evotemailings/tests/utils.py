# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from tempfile import NamedTemporaryFile


def compare_emails(email_one, email_two):
    """
    Check if both Emails Messages are equal
    """
    dict_one = email_one.__dict__
    dict_two = email_two.__dict__
    for elem in dict_one:
        if elem == "connection":
            pass
        elif dict_one[elem] != dict_two[elem]:
            return False
    return True


def create_attachment():
    """
    Create temporary file to be used on test
    """
    f = NamedTemporaryFile()
    f.write('unit test')
    f.seek(0)
    attachment = open(f.name, 'rb')
    return [('test.txt', attachment.read(), 'text/plain')]
