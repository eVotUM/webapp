# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from mock import patch
from smtplib import SMTPException
from django.test import SimpleTestCase

from evotemailings.tasks import send_email


def raise_ex(*args, **kwargs):
    raise SMTPException


class TasksTest(SimpleTestCase):

    def setUp(self):
        self.email = '{' +\
            '"headers": {},' +\
            '"connection": "django.core.mail.backends.locmem.EmailBackend",' +\
            '"from_email": "dev@eurotux.com",' +\
            '"to": ["tfb@eurotux.com"],' +\
            '"bcc": [],' +\
            '"cc": [],' +\
            '"reply_to": [],' +\
            '"subject": "Unit Test",' +\
            '"body": "Hello",' +\
            '"attachments": [],' +\
            '"alternatives": []' +\
            '}'

    def test_send_task_ok(self):
        result = send_email.delay(self.email)
        self.assertEqual(result.successful(), True)

    @patch('django.core.mail.backends.dummy.EmailBackend.send_messages',
           new=raise_ex)
    def test_send_task_fail(self):
        result = send_email(self.email)
        self.assertIsNone(result)

    def test_raise_exception(self):
        found = False
        try:
            raise_ex()
        except SMTPException:
            found = True
        self.assertEqual(found, True)
