# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.mail.message import EmailMultiAlternatives
from django.core.mail import get_connection
from django.test import SimpleTestCase

from evotemailings.backends import EvoteEmailBackend


class BackendTest(SimpleTestCase):

    def setUp(self):
        self.backend = EvoteEmailBackend()
        self.email = EmailMultiAlternatives(
            headers={},
            connection=get_connection(
                backend='django.core.mail.backends.locmem.EmailBackend'),
            from_email="dev@eurotux.com",
            to=["tfb@eurotux.com"],
            bcc=[],
            cc=[],
            reply_to=[],
            subject="Unit Test",
            body="Hello",
            attachments=[]
        )

    def test_send_task_ok(self):
        result = self.backend.send_messages([self.email])
        self.assertEqual(result, 1)
