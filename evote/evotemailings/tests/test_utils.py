# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.mail.message import EmailMultiAlternatives
from django.core.mail import get_connection
from django.test import SimpleTestCase

from evotemailings.utils import serialize_email, deserialize_email

from .utils import compare_emails


class UtilsTest(SimpleTestCase):

    def setUp(self):
        self.email = EmailMultiAlternatives(
            headers={},
            connection=get_connection(
                backend='django.core.mail.backends.locmem.EmailBackend'),
            from_email="dev@eurotux.com",
            to=["tfb@eurotux.com"],
            bcc=[],
            cc=[],
            reply_to=[],
            subject="Unit Test",
            body="Hello",
            attachments=[]
        )
        self.email_diff = EmailMultiAlternatives(
            headers={},
            connection=get_connection(
                backend='django.core.mail.backends.locmem.EmailBackend'),
            from_email="dev@eurotux.com",
            to=[],
            bcc=[],
            cc=[],
            reply_to=[],
            subject="Unit Test",
            body="",
            attachments=[]
        )
        self.email.attach_alternative('<b>test</b>', 'text/html')
        self.email_as_json = '{' +\
            '"headers": {},' +\
            '"connection": "django.core.mail.backends.locmem.EmailBackend",' +\
            '"from_email": "dev@eurotux.com",' +\
            '"to": ["tfb@eurotux.com"],' +\
            '"bcc": [],' +\
            '"cc": [],' +\
            '"reply_to": [],' +\
            '"subject": "Unit Test",' +\
            '"body": "Hello",' +\
            '"attachments": [],' +\
            '"alternatives": [["<b>test</b>", "text/html"]]' +\
            '}'

        self.invalid = '{' +\
            '"headers": {},' +\
            '"connection": "django.core.mail.backends.locmem.EmailBackend",' +\
            '"from_email": "other@eurotux.com",' +\
            '"to": ["tfb@eurotux.com"],' +\
            '"bcc": [],' +\
            '"cc": [],' +\
            '"reply_to": [],' +\
            '"subject": "Unit Test",' +\
            '"body": "Hello",' +\
            '"attachments": [],' +\
            '"alternatives": [["<b>test</b>", "text/html"]]' +\
            '}'

    def test_serialize_email(self):
        self.assertJSONEqual(serialize_email(self.email), self.email_as_json)

    def test_serialize_email_invalid(self):
        self.assertJSONNotEqual(serialize_email(self.email), self.invalid)

    def test_deserialize_email(self):
        result = compare_emails(deserialize_email(
            self.email_as_json), self.email)
        self.assertEqual(result, True)

    def test_compare_emails_fail(self):
        result = compare_emails(self.email_diff, self.email)
        self.assertEqual(result, False)
