# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase, TestCase

from evotevotings.processes.tests.utils import ProcessesTestUtils
from evotemailings.mailings import BaseMailing

from .utils import create_attachment


class TestBaseObject(BaseMailing):
    template_name = 'evotemailings/base.html'
    mail_to = [('dev', 'dev@eurotux.com')]
    mail_cc = ['dev@eurotux.com']
    mail_bcc = ['dev@eurotux.com']
    mail_reply_to = ['dev@eurotux.com']
    mail_from = 'dev@eurotux.com'
    subject = 'Unit test'

    def get_attachments(self):
        return create_attachment()


class TestBaseNoInfoObject(BaseMailing):
    template_name = 'evotemailings/base.html'
    mail_to = [('dev', 'dev@eurotux.com')]
    mail_cc = ['dev@eurotux.com']
    mail_bcc = ['dev@eurotux.com']


class MailingsSimpleTest(SimpleTestCase):

    def setUp(self):
        self.object = TestBaseObject(context={})
        self.no_info_object = TestBaseNoInfoObject()

    def test_get_attachments(self):
        result = self.object.get_attachments()
        expected = [('test.txt', 'unit test', 'text/plain')]
        self.assertEqual(result, expected)

    def test_get_context_data(self):
        result = self.object.get_context_data()
        self.assertEqual(result, {})

    def test_get_mail_to(self):
        result = self.object.get_mail_to()
        self.assertEqual(result, [('dev', 'dev@eurotux.com')])

    def test_get_mail_cc(self):
        result = self.object.get_mail_cc()
        self.assertEqual(result, ['dev@eurotux.com'])

    def test_get_mail_bcc(self):
        result = self.object.get_mail_bcc()
        self.assertEqual(result, ['dev@eurotux.com'])

    def test_get_mail_reply_to(self):
        result = self.object.get_mail_reply_to()
        self.assertEqual(result, ['dev@eurotux.com'])

    def test_get_mail_no_info_reply_to(self):
        result = self.no_info_object.get_mail_reply_to()
        self.assertEqual(result, ['default@eurotux.com'])

    def test_get_mail_from(self):
        result = self.object.get_mail_from()
        self.assertEqual(result, 'dev@eurotux.com')

    def test_get_mail_no_info_from(self):
        result = self.no_info_object.get_mail_from()
        self.assertEqual(result, 'default@eurotux.com')

    def test_get_subject(self):
        result = self.object.get_subject()
        self.assertEqual(result, 'Unit test')

    def test_get_no_info_subject(self):
        result = self.no_info_object.get_subject()
        self.assertEqual(result, 'Unit test default')

    def test_get_template_name(self):
        result = self.object.get_template_name()
        self.assertEqual(result, 'evotemailings/base.html')


class MailingsTest(TestCase, ProcessesTestUtils):

    def setUp(self):
        self.object = TestBaseObject()

    def test_get_send_no_mails(self):
        result = self.object.send(None)
        self.assertEqual(result, 1)

    def test_get_send_with_object(self):
        process = self.create_electoral_process()
        self.create_electoral_member(electoral_process=process)
        result = self.object.send(process)
        self.assertEqual(result, 1)

    def test_send(self):
        emails = list()
        emails.append(('tfb', 'tfb@eurotux.com'))
        result = self.object.send(emails)
        self.assertEqual(result, 1)
