# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.conf import settings
from django.template import loader
from django.core.mail.message import EmailMultiAlternatives
from django.core.mail import get_connection
from django.utils.html import strip_tags
from django.contrib.sites.models import Site


class BaseMailing(object):
    """ """

    template_name = None
    mail_to = []
    mail_cc = []
    mail_bcc = []
    mail_reply_to = []
    mail_from = None
    subject = None
    context_data = {}

    def __init__(self, context={}, *args, **kwargs):
        self.context_data = context
        super(BaseMailing, self).__init__(*args, **kwargs)

    def get_attachments(self):
        return []

    def get_context_data(self):
        return self.context_data

    def get_mail_to(self):
        return self.mail_to

    def get_mail_cc(self):
        return self.mail_cc

    def get_mail_bcc(self):
        return self.mail_bcc

    def get_mail_reply_to(self):
        if self.mail_reply_to:
            return self.mail_reply_to
        default = getattr(settings, "EVOTE_MAILING_REPLYTO_DEFAULT", '')
        if default:
            return [default]

    def get_mail_from(self):
        if self.mail_from:
            return self.mail_from
        return getattr(settings, "DEFAULT_FROM_EMAIL", None)

    def get_subject(self):
        if self.subject:
            return self.subject
        return getattr(settings, "EVOTE_MAILING_SUBJECT_DEFAULT", '')

    def get_template_name(self):
        return self.template_name

    def get_base_url(self):
        return {
            'base_url': "https://" + Site.objects.get_current().domain
        }

    def get_contact_phone(self):
        return {
            'contact_phone': getattr(settings, "EVOTE_CONTACT_PHONE", None)
        }

    def send(self, object_or_list=None):
        if isinstance(object_or_list, object) and\
                hasattr(object_or_list, 'get_mailing_list'):
            emails = object_or_list.get_mailing_list()
        elif isinstance(object_or_list, list):
            emails = [(name, email) for name, email in object_or_list]
        else:
            emails = [(name, email) for name, email in self.get_mail_to()]

        messages = list()
        context = self.get_context_data()
        context.update(self.get_base_url())
        context.update(self.get_contact_phone())
        for user, mail_to in emails:
            context.update({'user': user})
            html_as_string = loader.render_to_string(self.get_template_name(),
                                                     context)
            text_part = strip_tags(html_as_string)
            if not isinstance(mail_to, list):
                mail_to = [mail_to]
            msg = EmailMultiAlternatives(
                self.get_subject(),
                text_part,
                self.get_mail_from(),
                to=mail_to,
                cc=self.get_mail_cc(),
                bcc=self.get_mail_bcc(),
                reply_to=self.get_mail_reply_to())
            msg.attach_alternative(html_as_string, "text/html")
            for attachment in self.get_attachments():
                filename, contents, mimetype = attachment
                msg.attach(filename, contents, mimetype)

            messages.append(msg)

        return get_connection().send_messages(messages)
