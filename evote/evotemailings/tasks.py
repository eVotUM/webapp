# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from smtplib import SMTPException
import logging

from django.conf import settings
from django.core.mail import get_connection

from celery import shared_task

from .utils import deserialize_email


logger = logging.getLogger('evote.maillings')


@shared_task(ignore_result=True)
def send_email(email_serialized):
    email = deserialize_email(email_serialized)
    connection = get_connection(backend=settings.CELERY_EMAIL_BACKEND)
    connection.open()
    try:
        email_sent = connection.send_messages([email])
        if not email_sent:
            logger.error("Failed sending email to: %r", email.to)
            return None
    except SMTPException:
        logger.exception("Failed sending email to: %r", email.to)
        return None
    connection.close()
    return email.to
