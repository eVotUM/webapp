# -*- coding: utf-8 -*-
#
# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.core.mail.backends.base import BaseEmailBackend

from .utils import serialize_email
from .tasks import send_email


class EvoteEmailBackend(BaseEmailBackend):

    def __init__(self, fail_silently=False, **kwargs):
        super(EvoteEmailBackend, self).__init__(fail_silently)

    def send_messages(self, email_messages):
        for email in email_messages:
            send_email.si(serialize_email(email)).delay()
        return len(email_messages)
